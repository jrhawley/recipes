:PROPERTIES:
:ID:       631337ac-e10c-4331-917a-5598d82fa471
:ROAM_ALIASES: "Farm to Chef - Lynn Crawford (2017)" "Farm to Chef - Lynn Crawford" "Farm to Chef"
:END:
#+TITLE: Farm to Chef: Cooking Through the Seasons - Lynn Crawford (2017)
#+DATE: [2024-02-17 Sat 23:09]
#+LAST_MODIFIED: [2024-02-18 Sun 22:03]
#+FILETAGS: :cookbooks:books:
#+HUGO_BASE_DIR: ../../
#+HUGO_AUTO_SET_LASTMOD: t
#+HUGO_SECTION: references
