:PROPERTIES:
:ID:       f2c39994-b2bc-4290-932e-60bd6755377e
:END:
#+TITLE: Gingersnaps
#+DATE: [2018-12-18]
#+LAST_MODIFIED: [2023-12-21 Thu 21:53]
#+FILETAGS: :baking:recipes:desserts:
#+HUGO_BASE_DIR: ../../
#+HUGO_AUTO_SET_LASTMOD: t
#+HUGO_SECTION: recipes

| Info      | Amount     |
|-----------+------------|
| Prep Time | 15 min     |
| Cook Time | 10 min     |
| Yields    | 36 cookies |

#+CAPTION: Gingersnaps
[[../assets/gingersnaps.jpg]]

* Ingredients

| Quantity | Item                    |
|----------+-------------------------|
| 1 cup    | [[id:9ab2d99f-49fe-49a5-9432-cbc493ac826d][white sugar]]             |
| 3/4 cup  | [[id:2da7b183-9139-4ba6-9578-3d55c818b151][shortening]]              |
| 1/4 cup  | Crosby's fancy [[id:48c198d6-bee2-40ed-8f99-749446ada8a3][molasses]] |
| 1        | [[id:1bf90d00-d03c-4492-9f4f-16fff79fc251][egg]]                     |
| 2 cup    | [[id:52b06361-3a75-4b35-84ff-6b1f3ac96b23][flour]]                   |
| 2 tsp    | [[id:3977eee0-56ec-42eb-af6c-6b31a703c1f6][baking soda]]             |
| 1/2 tsp  | [[id:505e3767-00ab-4806-8966-555302b06297][table salt]]              |
| 1/2 tsp  | [[id:daefa35d-bb1d-46d4-93f7-e09c42df4d14][cinnamon]]                |
| 1/4 tsp  | ground [[id:f120187f-f080-4f7c-b2cc-72dc56228a07][cloves]]           |
| 1/4 tsp  | ground [[id:5650869d-ec01-477c-ba1b-7b2a830a5c9e][ginger]]           |

* Directions

1. Grease cookies sheets
2. In large bowl, combine sugar, shortening, molasses, and egg, blending well
3. Mix remaining ingredients together in a smaller bowl
   1. Blend with creamed mixture
   2. If desired, chill dough for easier handling
4. Shape into 1" balls
   1. Roll each in white sugar, if desired
   2. Place 2" apart on cookie sheet
   3. Press down lightly with a wet fork
5. Bake for 10 min at 350 F or until edges are set
6. Let sit on cookie sheet for 5 min before removing to cool on rack

