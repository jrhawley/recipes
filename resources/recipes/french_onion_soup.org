:PROPERTIES:
:ID:       18c86a2e-0346-41a5-bb88-fd900d8c0d12
:END:
#+TITLE: French onion soup
#+DATE: [2020-09-22]
#+LAST_MODIFIED: [2024-05-23 Thu 21:29]
#+FILETAGS: :slow_cooker:soups:recipes:vegetarian:entrees:
#+HUGO_BASE_DIR: ../../
#+HUGO_AUTO_SET_LASTMOD: t
#+HUGO_SECTION: recipes

| Info      | Amount     |
|-----------+------------|
| Prep Time | 30 min     |
| Cook Time | 5 h        |
| Yields    | 6 servings |

* Ingredients

| Quantity | Item                                   |
|----------+----------------------------------------|
| 3        | yellow [[id:8a695016-03b5-4059-9a54-668f3b794e33][onion]], peeled and sliced thinly |
| 3 Tbsp   | [[id:c2560014-7e89-4ef5-a628-378773b307e5][butter]]                                 |
| 2 Tbsp   | [[id:02ccfac6-e705-4b80-949e-1dff24216a5b][brown sugar]]                            |
| 2 Tbsp   | [[id:86d558aa-6ec7-4401-8a9b-9a70c790dc7e][worcestershire sauce]]                   |
| 2 Tbsp   | [[id:f120187f-f080-4f7c-b2cc-72dc56228a07][garlic]], minced                         |
| 8 cups   | low sodium [[id:6e6ebcb8-32a9-45bf-932a-f432577fc322][beef broth]]                  |
| 1        | [[id:e2e9621c-0f45-41ca-b381-22b8fa8694b9][bay leaf]]                               |
| 1 sprig  | fresh [[id:e9291faa-bd9d-4b1d-a751-3f99f7757fc6][thyme]]                            |
| 6 slices | [[id:857e2e38-6c9d-46e0-a46b-a3ce9a25c0e7][French bread]]                           |
| 1 cup    | [[id:5994fb2f-57a2-46df-9802-047e0013e079][Swiss cheese]], shredded                 |
| 1 cup    | [[id:a2ed6c9e-2e2c-4918-b61b-78c3c9d36c8c][parmesan]], shredded                     |

* Directions

1. Peel and slice the onions into thin slices
   1. In a medium skillet on medium-high heat, add the butter, sliced onions, and brown sugar to saute until translucent and caramelized
2. Transfer cooked onions into a slow cooker and add broth, garlic, bay leaf, and thyme
   1. Cover and cook on low for 4-5 h
3. Serve soup in oven-proof bowls and top with a slice of French bread
   1. Add Swiss cheese and parmesan to the top of the bread
   2. Place bowls on a cookie sheet and place under the broiler for 1 min at most, until cheese is melted and bubbles

* References & Notes

1. Original recipe: [[https://crockpotladies.com/wp-content/plugins/wp-ultimate-recipe-premium/core/templates/print.php][Crockpot Ladies]]

