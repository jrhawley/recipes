:PROPERTIES:
:ID:       8d0c3c2e-aa89-4312-a6a8-69ca18535375
:END:
#+TITLE: Pumpkin muffins
#+DATE: [2013-11-09]
#+LAST_MODIFIED: [2023-12-28 Thu 01:21]
#+FILETAGS: :breakfasts:recipes:muffins:pumpkin:
#+HUGO_BASE_DIR: ../../
#+HUGO_AUTO_SET_LASTMOD: t
#+HUGO_SECTION: recipes

| Info      | Amount     |
|-----------+------------|
| Prep Time | 15 min     |
| Cook Time | 25 min     |
| Yields    | 12 muffins |

* Ingredients

| Quantity  | Item                         |
|-----------+------------------------------|
| 7/4 cup   | [[id:fc946687-bd9b-40d2-8b93-745d0724aeab][all-purpose flour]]            |
| 1 tsp     | [[id:3977eee0-56ec-42eb-af6c-6b31a703c1f6][baking soda]]                  |
| 3/2 tsp   | [[id:daefa35d-bb1d-46d4-93f7-e09c42df4d14][ground cinnamon]]              |
| 3/2 tsp   | [[id:6f7fcb45-3840-441b-98a0-cf58af4170c4][pumpkin pie spice]]            |
| 1/4 tsp   | [[id:5650869d-ec01-477c-ba1b-7b2a830a5c9e][ground ginger]]                |
| 1/2 tsp   | [[id:505e3767-00ab-4806-8966-555302b06297][table salt]]                   |
| 1/2 cup   | [[id:6594dbcb-ac42-4c68-a9f9-c1ba749b408a][vegetable oil]] or [[id:f0fdd31d-6bdf-4c11-8622-9d7115165301][coconut oil]] |
| 1/2 cup   | [[id:9ab2d99f-49fe-49a5-9432-cbc493ac826d][white sugar]]                  |
| 1/3 cup   | [[id:02ccfac6-e705-4b80-949e-1dff24216a5b][brown sugar]]                  |
| 14 oz can | [[id:6f0f0e58-76c7-44e1-92f1-33cf8468e9b3][pumpkin puree]]                |
| 2         | [[id:1bf90d00-d03c-4492-9f4f-16fff79fc251][eggs]]                         |
| 1/4 cup   | [[id:5f1d6346-a46a-4d90-b1cd-ab72ada2716a][milk]]                         |

* Directions

1. Preheat the oven to 220 C (425 F) and butter the muffin pan.
2. In a large bowl, whisk the oil, sugar, pumpkin puree, eggs, and milk together until combined.
3. In a medium bowl, whisk the flour, baking soda, cinnamon, pumpkin pie spice, ginger, and salt together.
4. Fold the dry ingredients into the wet ingredients until everything is combined and no pockets remain.
5. Spoon the batter into each cup in the muffin tray to the top, sprinkling with coarse sugar, if desired.
6. Bake for 5 min at 425, then reduce the temperature to 180 C (350 F) and bake for an additional 16 - 18 min.

 
* References & Notes

1. Original recipe: [[https://sallysbakingaddiction.com/pumpkin-muffins-recipe/print/76102/][Sally's Baking Addiction]]
2. Muffins will last in the fridge for ~ 1 week, or in the freezer for ~ 3 months.
3. The higher initial temperature can help the muffins rise quickly, ensure a tall muffin top.

