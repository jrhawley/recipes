:PROPERTIES:
:ID:       66eef9c0-ebca-432d-9497-3ae382c463ad
:END:
#+TITLE: Peanut butter fruit smoothie
#+DATE: [2017-05-28]
#+LAST_MODIFIED: [2023-12-21 Thu 22:26]
#+FILETAGS: :smoothies:recipes:beverages:
#+HUGO_BASE_DIR: ../../
#+HUGO_AUTO_SET_LASTMOD: t
#+HUGO_SECTION: recipes

| Info      | Amount |
|-----------+--------|
| Prep Time | 2 min  |

#+CAPTION: Peanut butter fruit smoothie
[[file:../assets/peanut-butter-fruit-smoothie.jpg]]

* Ingredients

| Quantity | Item                |
|----------+---------------------|
| 1/4 cup  | frozen [[id:8f289db4-29e6-4df7-87b1-b7de8753e246][blueberries]]  |
| 1/4 cup  | frozen [[id:1e9655ad-169a-4d0e-ba18-12f3035676b1][blackberries]] |
| 1/4 cup  | frozen [[id:f4be28ec-f5d2-4a1a-bf6d-29b941c8f922][raspberry]]    |
| 1/3 cup  | [[id:3e80ceb1-aa3e-425a-a18b-d3acdc4353cf][Greek yogurt]]        |
| 1 Tbsp   | [[id:6e61a7f2-b5a0-4740-9cc0-c915469ee602][peanut butter]]       |
| 3/4 cup  | almond [[id:5f1d6346-a46a-4d90-b1cd-ab72ada2716a][milk]]         |
| 1        | [[id:c286f712-e223-4775-80f8-dc4973d0dfb5][dates]]               |

* Directions

1. Place the date at the bottom of your blender and then all of the other ingredients
2. Blend until you get a smooth consistency
