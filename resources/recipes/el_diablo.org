:PROPERTIES:
:ID:       0c786a2b-6f7b-40ef-aa88-15fe515e23b2
:END:
#+TITLE: El Diablo
#+DATE: [2022-11-12 Sat 23:30]
#+LAST_MODIFIED: [2023-12-21 Thu 21:43]
#+FILETAGS: :alcohols:recipes:beverages:
#+HUGO_BASE_DIR: ../../
#+HUGO_AUTO_SET_LASTMOD: t
#+HUGO_SECTION: recipes

| Info      | Amount     |
|-----------+------------|
| Prep Time | 2 min      |
| Cook Time | 0 min      |
| Yields    | 1 cocktail |

* Ingredients

| Quantity | Item            |
|----------+-----------------|
| 2 oz     | [[id:1cc52ced-0115-42f9-9fe7-6ad85fc3d3ca][tequila]]         |
| 1 oz     | [[id:75a5e2c3-fc9d-406f-acb3-0a044fb43863][creme de cassis]] |
| 1 oz     | [[id:4728f717-972e-46f4-9eb3-d847be411c3a][lime juice]]      |
|          | [[id:f8b77928-823a-4305-aa77-1dbb6248a0be][ginger ale]]      |
| 1        | [[id:3eeb40b4-7420-4c99-8a55-6c3e0193dfaa][lime wedge]]      |
|          | ice             |

* Directions

1. Add the first 3 ingredients to a highball glass filled with ice.
2. Top with ginger ale and garnish with a lime wedge.

* References & Notes

1. Original recipe: [[id:29874965-6ff0-4dc7-a567-30b3e3a67d4e][The Periodic Table of Cocktails]]
