:PROPERTIES:
:ID:       bcb8bba4-f71f-44b8-84c1-88e4c042c253
:END:
#+TITLE: Green curry with tofu and cashews
#+DATE: [2022-07-25 Mon]
#+LAST_MODIFIED: [2023-12-21 Thu 21:58]
#+FILETAGS: :recipes:entrees:vegan:vegetarian:
#+HUGO_BASE_DIR: ../../
#+HUGO_AUTO_SET_LASTMOD: t
#+HUGO_SECTION: recipes

| Info      | Amount     |
|-----------+------------|
| Prep Time | 10 min     |
| Cook Time | 15 min     |
| Yields    | 4 servings |

* Ingredients

| Quantity | Item                           |
|----------+--------------------------------|
| 1 Tbsp   | [[id:f0fdd31d-6bdf-4c11-8622-9d7115165301][coconut oil]]                    |
| 2        | [[id:7b7e13cf-0fb9-4dc6-a707-94bfd73417b2][shallots]]                       |
| 3 cloves | [[id:f120187f-f080-4f7c-b2cc-72dc56228a07][garlic]], minced                 |
| 2 Tbsp   | [[id:9ac96c74-939f-4b1f-b812-46d545e3d046][green curry paste]]              |
| 3/2 cups | [[id:6aaa4d74-e28e-4e22-afc6-dc6cf0dee4ac][vegetable broth]]                |
| 400 mL   | [[id:161b2192-11b9-4ba8-a11b-c16b851a6e0d][coconut milk]]                   |
| 1 Tbsp   | [[id:72cd69cc-a1da-4d58-93e3-7c654fa6a28f][soy sauce]]                      |
| 1 tsp    | [[id:02ccfac6-e705-4b80-949e-1dff24216a5b][brown sugar]]                    |
| 3 cups   | [[id:dfedd038-479e-42e5-9995-bd0beeea0dd4][broccoli florets]]               |
| 1        | [[id:76e9ca6f-2a60-4a79-a531-1d0c9af7e6d1][zucchini]]                       |
| 6 oz     | [[id:7e70dd06-871f-47f8-a1bc-af4c7f194ef5][green beans]]                    |
| 1 lb     | [[id:e530156c-ec74-467a-a30c-0b7283baa2bf][extra-firm tofu]]                |
| 1 cup    | [[id:229255c9-73ba-48f6-9216-7e4fa5938c06][parsley]] leaves, lightly packed |
| 2 tsp    | [[id:4728f717-972e-46f4-9eb3-d847be411c3a][lime juice]]                     |
| 1        | [[id:3eeb40b4-7420-4c99-8a55-6c3e0193dfaa][lime]]                           |
| 1 cup    | [[id:9b67d62e-13d2-46e0-9366-196393f90028][rice]]                           |
| 1/2 cup  | [[id:9c21df94-eb4c-47b4-8490-12ef0b3e8a57][cashews]]                        |
|          | [[id:026747d6-33c9-43c8-9d71-e201ed476116][kosher salt]]                    |
|          | [[id:f62c8021-74a6-4070-a240-25e5c072cdba][basil leaves]]                   |

* Directions

1. Prepare the vegetables.
   1. Thinly slice the shallots.
   2. Mince the garlic.
   3. Halve the zucchini lengthwise and slice into finger-width chunks with a bias.
   4. Halve the green beans
   5. Drain the tofu and cut into 1" cubes.
2. In a large pot, heat oil over medium heat.
   1. Add shallots and cook until just starting to brown, ~4 min.
   2. Add garlic and cook for 1 min.
   3. Add curry paste, cook for 30 s.
3. Add vegetable broth, coconut milk, soy sauce, brown sugar and salt.
   1. Raise heat to high.
   2. When it comes to a boil, add broccoli, zucchini, beans and tofu.
   3. When it returns to a boil, partially cover and reduce heat to maintain a brisk simmer.
   4. Cook, gently stirring twice, until vegetables are al dente, ~4 ­min.
   5. Remove from heat.
4. Place parsley in a food processor.
   1. Pulse until finely chopped.
   2. Add 1 cup (250 mL) of hot cooking liquid.
   3. Run processor until very finely chopped and liquid is light green.
   4. Return mixture to curry and gently stir in along with lime juice.
   5. Taste for salt.
5. Using a rasp, lightly grate a little lime zest overtop.
   1. Sprinkle with cashews and basil.
   2. Serve with steamed rice.

* References & Notes

1. Original recipe: [[https://www.lcbo.com/webapp/wcs/stores/servlet/en/lcbo/recipe/vegan-green-curry-with-tofu-cashews/F202105023][LCBO]]

