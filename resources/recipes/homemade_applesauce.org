:PROPERTIES:
:ID:       f902fb89-6c7e-4e5e-b605-2b0f0bef6204
:END:
#+TITLE: Homemade applesauce
#+DATE: [2024-02-10 Sat 15:50]
#+LAST_MODIFIED: [2024-02-10 Sat 15:58]
#+FILETAGS: :sauces:desserts:recipes:
#+HUGO_BASE_DIR: ../../
#+HUGO_AUTO_SET_LASTMOD: t
#+HUGO_SECTION: recipes

| Info      | Amount |
|-----------+--------|
| Prep Time | 5 min  |
| Cook Time | 25 min |
| Yields    | 5 cups |

* Equipment

- large soup pot with tight-fitting lid
- immersion blender
- soup spoon
- large cutting board
- prep knife
- measuring cups
- measuring spoons

* Ingredients

| Quantity | Item            |
|----------+-----------------|
| 2 kg     | assorted [[id:f9ceed1b-9c02-40b0-910b-cb65cb53125d][apples]] |
| 1/4 cup  | [[id:e9ee3129-a5de-42d0-8326-44c17067826f][sugar]]           |
| 1 Tbsp   | [[id:18730889-23b6-49e0-8c23-89b600b3566b][lemon juice]]     |
| 1/4 tsp  | [[id:daefa35d-bb1d-46d4-93f7-e09c42df4d14][ground cinnamon]] |
|          | [[id:026747d6-33c9-43c8-9d71-e201ed476116][Kosher salt]]     |

* Directions

1. Peel, core, and chop the apples.
2. Combine apples, sugar, and 1/2 tsp of salt to a boil in the pot over medium-high heat.
   - Reduce heat to medium-low, cover with the lid, and simmer for 18 - 20 min, until apples are soft.
3. Stir in the lemon juice and cinnamon, then let cool for 5 min.
   - With the immersion blender, puree to desired viscosity.
4. Let cool, then transfer to a bowl and refrigerate.

* References

1. Original recipe: [[https://www.foodnetwork.com/recipes/food-network-kitchen/the-best-homemade-applesauce-8613215][The Food Network]]
2. Store leftovers in an airtight container in the fridge for up to 5 days.

