:PROPERTIES:
:ID:       eb735956-70bd-48bb-87c4-0de8a799a039
:END:
#+TITLE: Braised lamb shanks with pommes puree
#+DATE: [2024-02-17 Sat 23:13]
#+LAST_MODIFIED: [2024-02-18 Sun 22:25]
#+FILETAGS: :lamb:entrees:recipes:
#+HUGO_BASE_DIR: ../../
#+HUGO_AUTO_SET_LASTMOD: t
#+HUGO_SECTION: recipes

| Info      | Amount     |
|-----------+------------|
| Prep Time | 10 min     |
| Cook Time | 2 h 30 min |
| Yields    | 4 servings |

* Equipment

- large cutting board
- chef's knife
- garlic press
- Dutch oven
- wooden spoon
- large plate
- large pot
- colander
- potato masher or food mill
- tongs
- ladle
- small pot

* Ingredients

** Braised lamb shanks

| Quantity | Item                      |
|----------+---------------------------|
| 30 mL    | [[id:a3cbe672-676d-4ce9-b3d5-2ab7cdef6810][olive oil]]                 |
| 4        | [[id:c28681f0-07fd-4df3-abf9-05edabb6d37e][lamb shanks]]               |
| 3        | [[id:3bff7e0c-815d-44cd-ae7b-296924ca59b0][carrots]], peeled and diced |
| 4 stalks | [[id:83c6ab82-bb15-4724-a51e-77237cb01b95][celery]], diced             |
| 4        | [[id:7b7e13cf-0fb9-4dc6-a707-94bfd73417b2][shallots]], chopped         |
| 4 cloves | [[id:f120187f-f080-4f7c-b2cc-72dc56228a07][garlic]], minced            |
| 250 mL   | dry [[id:23825845-5bf6-4a38-9010-3cfe7ee37c0f][red wine]]              |
| 800 mL   | [[id:419803fa-a1ca-47e6-8eda-93d83cd90bd5][whole peeled tomatoes]]     |
| 1 L      | [[id:1cc5c77b-9d7f-4644-b678-f69daaa25963][chicken stock]]             |
| 2 sprigs | [[id:473555c6-ad53-42f0-9301-71ed769e25e8][rosemary]]                  |
| 2 sprigs | [[id:e9291faa-bd9d-4b1d-a751-3f99f7757fc6][thyme]]                     |
| 2        | [[id:e2e9621c-0f45-41ca-b381-22b8fa8694b9][bay leaves]]                |
| 30 mL    | unsalted [[id:c2560014-7e89-4ef5-a628-378773b307e5][butter]]           |
| 30 mL    | [[id:229255c9-73ba-48f6-9216-7e4fa5938c06][parsley]]                   |
|          | [[id:026747d6-33c9-43c8-9d71-e201ed476116][Kosher salt]]               |
|          | [[id:68516e6c-ad08-45fd-852b-ba45ce50a68b][black pepper]]              |

** Pommes puree

| Quantity | Item                        |
|----------+-----------------------------|
| 900 g    | [[id:90910e36-410c-4b76-8b19-97d1c735b083][Yukon gold potatoes]], peeled |
| 450 g    | unsalted [[id:c2560014-7e89-4ef5-a628-378773b307e5][butter]], chunked    |
| 75 mL    | [[id:5f1d6346-a46a-4d90-b1cd-ab72ada2716a][milk]]                        |
|          | [[id:026747d6-33c9-43c8-9d71-e201ed476116][Kosher salt]]                 |

* Directions

** Lamb shanks

1. Preheat the oven to 160 C (325 F).
2. In a Dutch oven, heat olive oil over medium heat.
   1. Add shanks, 2 at a time, and brown btoh sides, ~ 10 min per batch.
   2. Transfer shanks to a large plate.
3. Add carrots, celery, shallots, onion, and garlic to the pot and cook until tender, ~ 10 min.
   1. Deglze with wine and bring to a boil, then reduce for 3 - 4 min.
   2. Using a wooden spoon, scrape any brown bits.
4. Add tomatoes and their juices, stock, rosemary, thyme, and bay leaves.
   1. Return lamb shanks to the Dutch oven and submerge them in the braising liquid.
   2. Bring to a boil, cover, then place in the oven.
   3. Braise for 1.5 - 2 h, until the meat is nearly falling off the bone.

** Pommes puree

1. In a large pot, cover the potatoes with cold, salted water.
   1. Bring to a boil, then reduce heat to low and simmer until tender, ~ 40 min.
   2. Drain, then puree potatoes until smooth [fn:1].
   3. Add butter, a few cubes at a time, allowing each to melt before adding the next, stirring constantly.
2. Add milk slowly, whisking constantly, preventing milk from cooling the potatoes.
   1. Whisk vigorously, and season to taste.
   2. Cover and keep warm.

** Serving

1. Using tongs, remove the shanks from the Dutch oven and place on a clean plate.
2. Spoon off as much fat as possible from the surface of the braising liquid.
   1. Strain the liquid into a small pot, and reduce over medium heat until thickened [fn:2].
   2. Remove from heat and whisk in butter and parsley.
   3. Season with salt and pepper.
3. Divide pommes puree among the plates, place lamb shanks on top, then spoon thickened sauce over top.

* References

1. Original recipe: [[id:631337ac-e10c-4331-917a-5598d82fa471][Farm to Chef: Cooking Through the Seasons - Lynn Crawford (2017)]]

* Footnotes

[fn:1] Pureeing can be done in a few ways, depending on how smooth you want the puree to be.
Pushing potatoes through a food mill on the finest setting will work best for a smooth puree.
Mashing them with a potato masher or hand mixer can leave them slightly lumpy.

[fn:2] An alternative to get restaurant-style sauce is to add gelatin packs, or flour. 
