:PROPERTIES:
:ID:       0ccc9ddd-0aa1-4295-8246-cded8a3e6874
:END:
#+TITLE: Bourbon Smash
#+DATE: [2022-03-13]
#+LAST_MODIFIED: [2023-12-19 Tue 22:07]
#+FILETAGS: :recipes:alcohols:beverages:
#+HUGO_BASE_DIR: ../../
#+HUGO_AUTO_SET_LASTMOD: t
#+HUGO_SECTION: recipes

* Ingredients

| Quantity | Ingredient   |
|----------+--------------|
| 6        | [[id:f4be28ec-f5d2-4a1a-bf6d-29b941c8f922][raspberries]]  |
| 6        | [[id:03c1357c-225f-455d-af1f-86c11d3eafb9][mint leaves]]  |
| 5 / 3 oz | [[id:437447cf-eabf-4f1a-89d0-e4d402eddf18][bourbon]]      |
| 5 / 6 oz | [[id:4728f717-972e-46f4-9eb3-d847be411c3a][lime juice]]   |
| 2 /3 oz  | [[file:simple-syrup.md][simple syrup]] |

* Directions

1. Gently muddle the raspberries and mint in the bottom of a cocktail shaker [fn:1].
2. Add the bourbon, lime juice, and simple syrup with ice to the shaker.
3. Shake and strain into an ice-filled highball glass.
4. Garnish with a lime wedge or mint sprig (or both).

* References & Notes

1. Original recipe: [[id:29874965-6ff0-4dc7-a567-30b3e3a67d4e][The Periodic Table of Cocktails]]

* Footnotes

[fn:1] Be careful not to overly-muddle the mint, as it will bring out the bitter flavours in the leaves.
