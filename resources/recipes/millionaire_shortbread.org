:PROPERTIES:
:ID:       00775440-d6ea-4f3f-bf8f-7dc4f722bd27
:END:
#+TITLE: Millionaire Shortbread
#+DATE: [2018-03-30]
#+LAST_MODIFIED: [2023-12-22 Fri 00:50]
#+FILETAGS: :recipes:desserts:
#+HUGO_BASE_DIR: ../../
#+HUGO_AUTO_SET_LASTMOD: t
#+HUGO_SECTION: recipes

| Info      | Amount |
|-----------+--------|
| Prep Time | 10 min |
| Cook Time | 20 min |
| Yields    |        |

* Ingredients

** Shortbread

| Quantity | Item              |
|----------+-------------------|
| 4 oz     | [[id:c2560014-7e89-4ef5-a628-378773b307e5][butter]]            |
| 2 oz     | [[id:9ab2d99f-49fe-49a5-9432-cbc493ac826d][white sugar]]       |
| 6 oz     | [[id:fc946687-bd9b-40d2-8b93-745d0724aeab][all-purpose flour]] |

** Caramel

| Quantity | Item                           |
|----------+--------------------------------|
| 8 oz     | [[id:c2560014-7e89-4ef5-a628-378773b307e5][butter]]                         |
| 4 Tbsp   | [[id:716dd7d0-46db-4224-9391-75b5eaad5cfd][maple syrup]]                    |
| 4 oz     | [[id:9ab2d99f-49fe-49a5-9432-cbc493ac826d][white sugar]]                    |
| 400 oz   | [[id:bcc6418a-d36b-45db-b7aa-7b41f19d6115][condensed milk]]                 |
|          | drops of [[id:924ad22d-2f73-4def-b12f-f0133df00ff1][vanilla extract]]       |
| 100 g    | [[id:ec628584-7b20-4285-95b0-509043ff03a6][milk chocolate]] (bars or chips) |

* Directions

1. Cream together margarine and sugar, then mix in flour
2. Line a square baking tray with parchment paper or grease
3. Cook at 180 C/350 F until golden (about 20 min? possibly?)
4. While shortbread is baking, melt together caramel ingredients and boil for 5 min
5. Pour over shortbread and let set
6. Melt chocolate and pour on top of caramel layer
7. Refrigerate for 1 h before serving

* References & Notes

1. Original recipe: Julie Milner

