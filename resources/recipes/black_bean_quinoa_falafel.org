:PROPERTIES:
:ID:       22a16a98-1ce6-4df0-9bad-633fbe9bfa73
:END:
#+TITLE: Black bean quinoa falafel
#+DATE: [2020-09-24]
#+LAST_MODIFIED: [2023-12-19 Tue 21:57]
#+FILETAGS: :recipes:vegetarian:entrees:
#+HUGO_BASE_DIR: ../../
#+HUGO_AUTO_SET_LASTMOD: t
#+HUGO_SECTION: recipes

| Info      | Amount     |
|-----------+------------|
| Prep Time | 15 min     |
| Cook Time | 45 min     |
| Yields    | 12 falafel |

* Ingredients

| Quantity | Item                                |
|----------+-------------------------------------|
| 1 cup    | cooked [[id:cc0d409b-ba32-4755-b5ee-41837ba5d47d][quinoa]], cooled               |
| 15 oz    | [[id:285345d6-78f4-42cd-af32-0738783c781d][black beans]], rinsed, drained, dried |
| 1/4 cup  | [[id:26ca4439-8ab4-4422-a478-8c2354ea8724][pumpkin seeds]]                       |
| 5 cloves | [[id:f120187f-f080-4f7c-b2cc-72dc56228a07][garlic]], minced                      |
| 1/2 tsp  | [[id:505e3767-00ab-4806-8966-555302b06297][table salt]]                          |
| 1 tsp    | ground [[id:591e51ef-30b7-48f4-9232-a0834f4c31af][cumin]]                        |
| 2 Tbsp   | [[id:e6fe5a89-23f4-4236-8d7f-5f5575b9719f][tomato paste]]                        |

* Directions

1. Prepare quinoa according to instructions, preheat oven to 350 F
2. Add black beans to parchment-lined baking sheet
   1. Bake for 15 min until beans crack
   2. Remove from oven, increase temperature to 375 F
3. Add black beans to food processor with pumpkin seeds and garlic
   1. Pulse into a loose meal
   2. Add quinoa and all remaining ingredients
   3. Blend to combine until a textured dough forms
4. Scoop 3/2 Tbsp at a time, form into small discs
   1. Place on parchment-lined baking sheet
   2. Repeat for entire dough mixture
5. Bake for 15 min
   1. Flip and back for another 10-15 min
6. Garnish with chili garlic (optional) and serve with hummus or baba ghanoush

* References & Notes

1. Original recipe: [[https://minimalistbaker.com/baked-quinoa-black-bean-falafel/][Minimalist Baker]]
