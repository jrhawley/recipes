:PROPERTIES:
:ID:       10fbc2d0-e29b-4f3a-a6b4-d21ad5788de4
:END:
#+TITLE: Honey mustard chicken and potatoes
#+DATE: [2020-08-17]
#+LAST_MODIFIED: [2023-12-21 Thu 22:04]
#+FILETAGS: :recipes:chicken:potatoes:entrees:
#+HUGO_BASE_DIR: ../../
#+HUGO_AUTO_SET_LASTMOD: t
#+HUGO_SECTION: recipes

| Info      | Amount     |
|-----------+------------|
| Prep Time | 10 min     |
| Cook Time | 1 h        |
| Yields    | 4 servings |

* Ingredients

| Quantity | Item                         |
|----------+------------------------------|
| 4        | [[id:844b425a-0bc1-486c-a3ce-755652960211][chicken breasts]]              |
| 1 Tbsp   | [[id:42bb6cab-f3f5-4018-814f-dba5fcf0e95a][garlic powder]]                |
| 1 Tbsp   | [[id:a3cbe672-676d-4ce9-b3d5-2ab7cdef6810][olive oil]]                    |
| 2 cloves | [[id:f120187f-f080-4f7c-b2cc-72dc56228a07][garlic]], minced               |
| 1/4 cup  | [[id:257897fc-30ec-4477-aa93-abff6398d8c1][honey]]                        |
| 3 Tbsp   | [[id:8229d1fa-b72c-4ed1-8d8e-cc10f0ff35ef][whole grain mustard]]          |
| 2 Tbsp   | [[id:00a48416-bb29-468a-9498-dacf8e0491ba][dijon mustard]]                |
| 2 Tbsp   | [[id:970d7f49-6f00-4caf-b73f-90d3e7f5039c][water]]                        |
| 1 lbs    | baby [[id:c4a7d6a1-55f7-4c1a-a28c-de8b2020b89d][red potatoes]], quartered |
| 8 oz     | [[id:7e70dd06-871f-47f8-a1bc-af4c7f194ef5][green beans]], halved          |
| 2 sprigs | [[id:473555c6-ad53-42f0-9301-71ed769e25e8][rosemary]]                     |
|          | [[id:026747d6-33c9-43c8-9d71-e201ed476116][kosher salt]]                  |
|          | [[id:68516e6c-ad08-45fd-852b-ba45ce50a68b][pepper]]                       |

* Directions

1. Preheat oven to 400 F
2. Generously season chicken breasts with salt, pepper and garlic powder
3. Heat olive oil in a large, oven-proof non-stick pan over medium-high heat
   1. Sear chicken thighs for 3 minutes each side, until the skin becomes golden and crisp
   2. Leave 2 tablespoons of chicken juices in the pan for added flavour, and drain any excess
4. Fry the garlic in the same pan around the chicken for 1 min until fragrant
   1. Add the honey, both mustards, and water to the pan, mixing well, and combine all around the chicken
5. Add in the potatoes; mix them through the sauce
   1. Season with salt and pepper, to your tastes
   2. Allow the honey mustard sauce to simmer for 2 min
   3. Transfer to the hot oven and bake for 40-45 min
6. Remove from the oven after 30 minutes
   1. Add in the green beans
   2. Return to the oven to bake for a further 15 min

* References & Notes

1. Original recipe: [[https://cafedelites.com/honey-mustard-chicken-potatoes/][Cafe Delites]]

