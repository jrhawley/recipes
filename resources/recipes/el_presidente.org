:PROPERTIES:
:ID:       e604ca69-fc4d-4876-a922-9c4b2bbb2b40
:END:
#+TITLE: El Presidente
#+DATE: [2021-02-17]
#+LAST_MODIFIED: [2023-11-20 Mon 09:25]
#+FILETAGS: :recipes:alcohols:beverages:
#+HUGO_BASE_DIR: ../../
#+HUGO_AUTO_SET_LASTMOD: t
#+HUGO_SECTION: recipes

* Ingredients

| Quantity | Item       |
|----------+------------|
| 2 oz     | [[id:d78b447f-e427-4c72-b184-91cc4f570a24][rum]]        |
| 1 oz     | [[id:e5be28df-0ad7-4af2-8128-15a50f98c04f][vermouth]]   |
| 1/3 oz   | [[id:0abff3a4-882b-4b60-a2f1-598710047b2f][Triple Sec]] |
| 1 dash   | [[id:3a37487a-599e-4c93-b961-8d13cfbaa780][grenadine]]  |

* Directions

1. Fill a mixing glass with ice and add liquids
2. Stir until chilled
3. Strain into a chilled serving glass and garnish with orange peel

* References & Notes

1. Original recipe: [[id:29874965-6ff0-4dc7-a567-30b3e3a67d4e][The Periodic Table of Cocktails]]

