:PROPERTIES:
:ID:       a811a78b-34f1-4522-8af7-61c8a95fec97
:END:
#+TITLE: French toast
#+DATE: [2013-09-14]
#+LAST_MODIFIED: [2023-11-06 Mon 10:40]
#+FILETAGS: :food:recipes:breakfasts:
#+HUGO_BASE_DIR: ../../
#+HUGO_AUTO_SET_LASTMOD: t
#+HUGO_SECTION: recipes

| Info      | Amount     |
|-----------+------------|
| Prep Time | 10 min     |
| Cook Time | 5-10 min   |
| Yields    | 4 servings |

* Ingredients

| Quantity | Item            |
|----------+-----------------|
| 2        | [[id:32d73adc-34f4-4ff8-ace7-e19dbd9905aa][eggs]]            |
| 2/3 cup  | [[id:5f1d6346-a46a-4d90-b1cd-ab72ada2716a][milk]]            |
| 2 tsp    | ground [[id:daefa35d-bb1d-46d4-93f7-e09c42df4d14][cinnamon]] |
| 8 slices | [[id:7cb5311c-afae-4dca-8bc2-e3a6159709dc][bread]]           |
| 1 tsp    | [[id:c2560014-7e89-4ef5-a628-378773b307e5][butter]], melted  |

* Directions

1. Either melt butter in frying pan/griddle or melt and mix with other ingredients
2. Beat to make the mixture light
3. Dip bread and fry
4. Top with berries, jam, icing sugar, etc
