:PROPERTIES:
:ID:       498e6f65-985f-469c-995d-c3ca2a77ee69
:END:
#+TITLE: Martini
#+DATE: [2021-02-17]
#+LAST_MODIFIED: [2023-12-21 Thu 22:13]
#+FILETAGS: :recipes:alcohols:beverages:
#+HUGO_BASE_DIR: ../../
#+HUGO_AUTO_SET_LASTMOD: t
#+HUGO_SECTION: recipes

* Ingredients

| Quantity | Item     |
|----------+----------|
| 1/3 oz   | [[id:e5be28df-0ad7-4af2-8128-15a50f98c04f][vermouth]] |
| 2 oz     | [[id:fccdeb8e-92f8-4058-b058-b9e9ae72b7fd][gin]]      |

* Directions

1. Fill a mixing glass with ice
2. Add vermouth, stir to coat ice
3. Add gin and stir until chilled and diluted
4. Strain into a chilled cocktail glass
5. Garnish with a strip of lemon peel

* References & Notes

1. Original recipe: [[id:29874965-6ff0-4dc7-a567-30b3e3a67d4e][The Periodic Table of Cocktails]]

