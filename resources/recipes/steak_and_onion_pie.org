:PROPERTIES:
:ID:       fffed145-775e-4594-951f-0a02295b28e3
:END:
#+TITLE: Steak and onion pie
#+DATE: [2024-02-17 Sat 22:22]
#+LAST_MODIFIED: [2024-02-17 Sat 23:19]
#+FILETAGS: :pies:entrees:beef:recipes:
#+HUGO_BASE_DIR: ../../
#+HUGO_AUTO_SET_LASTMOD: t
#+HUGO_SECTION: recipes

| Info      | Amount     |
|-----------+------------|
| Prep Time | 5 min      |
| Cook Time | 3h         |
| Yields    | 6 servings |

* Equipment

- large cutting board
- chef knife
- Dutch oven
- large skillet
- wooden spoon
- measuring cups
- measuring spoons
- pastry brush
- small bowl

* Ingredients

| Quantity  | Item                              |
|-----------+-----------------------------------|
| 900 g     | [[id:c342fb42-79ed-440a-a545-39220d1f6fba][stewing beef]]                      |
| 125 ml    | [[id:fc946687-bd9b-40d2-8b93-745d0724aeab][all-purpose flour]]                 |
| 75 mL     | [[id:a3cbe672-676d-4ce9-b3d5-2ab7cdef6810][olive oil]], divided                |
| 45 mL     | unsalted [[id:c2560014-7e89-4ef5-a628-378773b307e5][butter]]                   |
| 375 mL    | [[id:23825845-5bf6-4a38-9010-3cfe7ee37c0f][red wine]], divided                 |
| 1         | [[id:6987cb0c-1483-4808-af39-c4fd6efb6d5c][white onion]], chopped              |
| 1         | [[id:c635f344-f574-4eff-8bfd-a2e6df8323e1][yellow onion]], chopped             |
| 2         | [[id:7b7e13cf-0fb9-4dc6-a707-94bfd73417b2][shallots]], halved lengthwise       |
| 2         | [[id:e2e9621c-0f45-41ca-b381-22b8fa8694b9][bay leaves]]                        |
| 2 springs | [[id:e9291faa-bd9d-4b1d-a751-3f99f7757fc6][thyme]]                             |
| 1 sprig   | [[id:473555c6-ad53-42f0-9301-71ed769e25e8][rosemary]]                          |
| 500 mL    | [[id:76b02319-d075-4034-a0f2-bb85e5f8e337][beef stock]]                        |
|           | [[id:026747d6-33c9-43c8-9d71-e201ed476116][kosher salt]]                       |
|           | [[id:68516e6c-ad08-45fd-852b-ba45ce50a68b][black pepper]]                      |
| 750 mL    | [[id:5a8e9a34-aaac-4b0f-81d8-da36b254be6b][pearl onions]], peeled and blanched |
| 1         | [[id:0d0e82a6-d955-4efa-8e1a-ffb418d0ff8c][pastry sheet]], thawed overnight    |
| 1         | [[id:1bf90d00-d03c-4492-9f4f-16fff79fc251][egg]], lightly beaten               |

* Directions

1. Cut the stewing beef into 5 cm cubes and dust with flour.
   1. In the Dutch oven over medium heat, heat 30 mL of olive oil and 30 mL of butter.
   2. Brown beef cubes on all sides, working in batches if needed.
   3. Transfer browned beef to a bowl and set aside.
2. Add 175 mL of red wine and scrape the browned bits with a wooden spoon, then pour the deglazing liquid into the bowl with the beef.
3. Heat the remaining olive oil, add onions and shallots, and saute until golden.
4. Add bay leaves, thyme, rosemary, remaining red wine, beef stock, and reserved beef and its juices.
   1. Stir to combine, and season to taste with salt and pepper.
   2. Simmer, uncovered, over medium-low heat until beef is fork tender, ~ 2 h.
5. While the beef is simmering, melt 15 ml of olive oil 15 mL of butter in the skillet over medium heat.
   1. Add pearl onions and cook until golden, stirring often.
   2. Remove from heat.
6. Preheat oven to 190 C (375 F).
7. When beef has finished, discard the bay leaves, thyme, and rosemary sprigs, then stir in pearl onions.
   1. Spoon mixter into a greased baking dish and let cool for 15 min before laying chilled pastry sheet over top.
   2. Trim edges of pastry sheet, and cut slits to allow venting.
   3. Brush pastry surface with beaten egg.
8. Bake pie until heated through and pastry is golden, ~ 25 - 30 min.
9. Let rest for 5 min before serving.

* References

1. Original recipe: [[id:631337ac-e10c-4331-917a-5598d82fa471][Farm to Chef: Cooking Through the Seasons - Lynn Crawford (2017)]]

