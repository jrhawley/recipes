:PROPERTIES:
:ID:       cbced30e-6bd5-401c-b817-3691b3bce89f
:END:
#+TITLE: Chicken lo mein
#+DATE: [2020-01-15]
#+LAST_MODIFIED: [2023-12-19 Tue 22:42]
#+FILETAGS: :chicken:recipes:slow_cooker:entrees:
#+HUGO_BASE_DIR: ../../
#+HUGO_AUTO_SET_LASTMOD: t
#+HUGO_SECTION: recipes

| Info      | Amount |
|-----------+--------|
| Prep Time | 30 min |
| Cook Time | 3 h    |
| Yields    | 6      |

* Ingredients

| Quantity | Item                               |
|----------+------------------------------------|
| 2 lbs    | boneless, skinless [[id:844b425a-0bc1-486c-a3ce-755652960211][chicken breasts]] |
| 5 oz     | [[id:439b4d67-0be5-41c8-a1c6-be508254bf6d][snap peas]]                          |
| 1        | large [[id:4390c023-512f-49c7-8320-0b6fba85a579][red bell pepper]], sliced      |
| 5 oz     | [[id:ca962d8d-8d43-4353-b35d-4b39592e6e52][water chestnuts]], sliced            |
| 4 stalks | [[id:1a3ef043-075e-45ac-af8a-02dfee2bc251][green onion]], chopped               |
| 1 cup    | [[id:6aaa4d74-e28e-4e22-afc6-dc6cf0dee4ac][vegetable broth]]                    |
| 1/4 cup  | [[id:86d558aa-6ec7-4401-8a9b-9a70c790dc7e][worcestershire sauce]]               |
| 1/4 cup  | low sodium [[id:72cd69cc-a1da-4d58-93e3-7c654fa6a28f][soy sauce]]               |
| 2 Tbsp   | [[id:257897fc-30ec-4477-aa93-abff6398d8c1][honey]]                              |
| 2 tsp    | [[id:f120187f-f080-4f7c-b2cc-72dc56228a07][garlic]], minced                     |
| 1 tsp    | ground [[id:5650869d-ec01-477c-ba1b-7b2a830a5c9e][ginger]]                      |
| 14 oz    | [[id:55e8f7d0-5d66-41cf-b669-dce30eab787b][lo mein noodles]], uncooked          |

* Directions

1. Slice chicken and place chicken breasts in slow cooker
   1. Add snap peas, bell pepper, water chestnuts, green onion, broth
2. In a small bowl, whisk together Worcestershire sauce, soy sauce, honey, garlic, and ginger
   1. Pour into slow cooker to coat chicken
3. Cook on low for 6 h or high for 3 h
4. After slow cooker has finished, cook noodles according to instructions
   1. Stir noodles into slow cooker, serve
   2. Garnish with extra green onion

* References & Notes

1. Original recipe: [[https://www.eatingonadime.com/wprm_print/23335][Eating on a Dime]]

