:PROPERTIES:
:ID:       3c791923-a3c3-4102-a593-73caf1ab9b59
:END:
#+TITLE: Gingerbread waffles
#+DATE: [2024-01-06 Sat 22:55]
#+LAST_MODIFIED: [2024-01-06 Sat 23:02]
#+FILETAGS: :breakfasts:recipes:
#+HUGO_BASE_DIR: ../../
#+HUGO_AUTO_SET_LASTMOD: t
#+HUGO_SECTION: recipes

| Info      | Amount |
|-----------+--------|
| Prep Time | 15 min |
| Cook Time | 15 min |
| Yields    | 2 waffles |

* Equipment

- 2 medium mixing bowls
- 1 small microwaveable bowl
- 1 whisk
- 1 waffle iron
- measuring spoons
- measuring cup

* Ingredients

| Quantity | Item            |
|----------+-----------------|
| 1 cup    | [[id:52b06361-3a75-4b35-84ff-6b1f3ac96b23][flour]]           |
| 1 tsp    | [[id:218f6314-47d3-4d12-bc91-3adc8baf97a8][baking powder]]   |
| 1/2 tsp  | [[id:3977eee0-56ec-42eb-af6c-6b31a703c1f6][baking soda]]     |
| 1/4 tsp  | [[id:505e3767-00ab-4806-8966-555302b06297][table salt]]      |
| 2 tsp    | [[id:0a1a45f9-b9b7-4004-a104-801fb3cdea65][ground ginger]]   |
| 2 tsp    | [[id:daefa35d-bb1d-46d4-93f7-e09c42df4d14][ground cinnamon]] |
| 1/2 tsp  | [[id:a13bb3ad-60ac-4296-a52a-dda7a88177cc][allspice]]        |
| 1/4 tsp  | [[id:1e78c132-4830-4a9e-82f8-6466628ec493][ground cloves]]   |
| 1/4 tsp  | [[id:cfdf3e4e-72c5-4bd2-aa76-dc03d92305ca][nutmeg]]          |
| 1 cup    | [[id:5f1d6346-a46a-4d90-b1cd-ab72ada2716a][milk]]            |
| 1        | [[id:1bf90d00-d03c-4492-9f4f-16fff79fc251][egg]]             |
| 3/2 Tbsp | [[id:c2560014-7e89-4ef5-a628-378773b307e5][butter]], melted  |

* Directions

1. Heat lightly oiled pan or griddle to medium-high
2. In separate bowls, combine dry ingredients and wet ingredients, respectively
   1. Add the dry bowl into the wet bowl, stirring slowly
   2. Whisk together until batter is fully mixed and slightly lumpy
3. Cook for ~ 3 min, until golden brown
4. Add fruit, cinnamon, icing sugar, etc as toppings

* References & Notes

1. Same recipe as this [[id:cb656b8d-7f89-49bb-9f11-f47d3ab01cfe][pancakes and waffles]] recipe, with extra spices to create the gingerbread flavour
