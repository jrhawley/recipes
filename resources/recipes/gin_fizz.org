:PROPERTIES:
:ID:       40935080-1806-4c82-9a68-792e63c43749
:END:
#+TITLE: Gin fizz
#+DATE: [2020-12-19]
#+LAST_MODIFIED: [2023-12-21 Thu 21:52]
#+FILETAGS: :recipes:alcohols:beverages:
#+HUGO_BASE_DIR: ../../
#+HUGO_AUTO_SET_LASTMOD: t
#+HUGO_SECTION: recipes

| Info      | Amount     |
|-----------+------------|
| Prep Time | 2 min      |
| Yields    | 1 cocktail |

* Ingredients

| Quantity | Item         |
|----------+--------------|
| 1.5 oz   | [[id:fccdeb8e-92f8-4058-b058-b9e9ae72b7fd][gin]]          |
| 1 oz     | [[./simple-syrup.md][simple syrup]] |
| 1 oz     | [[id:4728f717-972e-46f4-9eb3-d847be411c3a][lime juice]]   |
|          | ice cubes    |
| 4 oz     | [[id:dba4890c-1a4a-44b8-8ab2-c6414d166052][club soda]]    |

* Directions

1. Place all ingredients but the club soda into a cocktail shaker.
2. Cover and shake vigorously
3. Strain into highball glasses, then add in club soda
