:PROPERTIES:
:ID:       5449ba9b-3df0-4752-a97d-d2331570d01a
:ROAM_ALIASES: chicken
:END:
#+TITLE: Chicken
#+DATE: [2023-08-27 Sun 20:47]
#+LAST_MODIFIED: [2023-11-06 Mon 08:08]
#+FILETAGS: :chicken:food:
#+HUGO_BASE_DIR: ../../
#+HUGO_AUTO_SET_LASTMOD: t
#+HUGO_SECTION: items

* Derivatives

- [[id:844b425a-0bc1-486c-a3ce-755652960211][chicken breast]]
- [[id:5a18cc90-a71f-4dca-acd5-9453254d8778][chicken tender]]
