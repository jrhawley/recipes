:PROPERTIES:
:ID:       8beb763d-3bd8-4f03-9f7b-f438dd208a9a
:ROAM_ALIASES: oreo oreos Oreos
:END:
#+TITLE: Oreo
#+DATE: [2023-10-24 Tue]
#+LAST_MODIFIED: [2023-11-19 Sun 21:52]
#+FILETAGS: :snacks:cookies:food:
#+HUGO_BASE_DIR: ../../
#+HUGO_AUTO_SET_LASTMOD: t
#+HUGO_SECTION: items
