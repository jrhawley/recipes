:PROPERTIES:
:ID:       844b425a-0bc1-486c-a3ce-755652960211
:ROAM_ALIASES: "Chicken breast" "chicken breasts" "chicken breast"
:END:
#+TITLE: Chicken breast
#+DATE: [2023-08-27 Sun 20:47]
#+LAST_MODIFIED: [2024-05-23 Thu 21:51]
#+FILETAGS: :chicken:food:
#+HUGO_BASE_DIR: ../../
#+HUGO_AUTO_SET_LASTMOD: t
#+HUGO_SECTION: items
