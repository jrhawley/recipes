:PROPERTIES:
:ID:       285345d6-78f4-42cd-af32-0738783c781d
:ROAM_ALIASES: "turtle bean" "turtle beans" "Turtle bean" "Turtle beans" "black bean" "black beans" "Black beans"
:END:
#+TITLE: Black bean
#+DATE: [2023-11-19 Sun 18:23]
#+LAST_MODIFIED: [2023-11-19 Sun 18:25]
#+FILETAGS: :beans:food:
#+HUGO_BASE_DIR: ../../
#+HUGO_AUTO_SET_LASTMOD: t
#+HUGO_SECTION: items
