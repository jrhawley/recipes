:PROPERTIES:
:ID:       06c536cf-1cdc-40d0-9f4e-a4da2c4ad89b
:ROAM_ALIASES: "chocolate syrup"
:END:
#+TITLE: Chocolate syrup
#+DATE: [2023-11-06 Mon]
#+LAST_MODIFIED: [2023-11-19 Sun 21:24]
#+FILETAGS: :sweeteners:syrups:food:
#+HUGO_BASE_DIR: ../../
#+HUGO_AUTO_SET_LASTMOD: t
#+HUGO_SECTION: items
