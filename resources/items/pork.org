:PROPERTIES:
:ID:       726f1a8d-3dad-4a6e-9260-6f0913b1ffaa
:ROAM_ALIASES: pork
:END:
#+TITLE: Pork
#+DATE: [2023-11-06 Mon 07:46]
#+LAST_MODIFIED: [2024-01-06 Sat 15:45]
#+FILETAGS: :pork:food:
#+HUGO_BASE_DIR: ../../
#+HUGO_AUTO_SET_LASTMOD: t
#+HUGO_SECTION: items

* Derivatives

- [[id:0f9ccc03-2787-43d0-bf48-78a1bbb2730b][ground pork]]
- [[id:34c17f10-7cb1-4333-a9b7-c8543df37d60][pork shoulder]]
- [[id:4172ff60-dd6f-4154-ae3c-e301116ff4fa][pork tenderloin]]
- [[id:bd79e1cd-b0ac-4263-958f-59d804b99da9][pork chop]]
- [[id:48f96fae-bf50-4574-8608-ea5d7804578f][ham]]
- [[id:5cc2568e-a3bd-491c-a91e-3e401956a451][prosciutto]]
- [[id:04434c0e-9dd0-47ec-b155-6dcfde554690][bacon]]
- [[id:54cc750e-d455-414f-ac0e-e7f14d34d0fa][pepperette]]
