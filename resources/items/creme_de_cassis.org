:PROPERTIES:
:ID:       75a5e2c3-fc9d-406f-acb3-0a044fb43863
:ROAM_ALIASES: "Blackcurrant liqueur" "blackcurrant liqueur" "creme de cassis"
:END:
#+TITLE: Creme de cassis
#+DATE: [2023-10-24 Tue]
#+LAST_MODIFIED: [2023-10-25 Wed 00:29]
#+FILETAGS: :alcohols:beverages:food:
#+HUGO_BASE_DIR: ../../
#+HUGO_AUTO_SET_LASTMOD: t
#+HUGO_SECTION: items
