:PROPERTIES:
:ID:       492b7f86-5fef-49ba-bc09-ef09dde8360f
:ROAM_ALIASES: "Pomegranate arils" "pomegranate arils" "pomegranate aril" "Pomegranate seeds" "pomegranate seeds"
:END:
#+TITLE: Pomegranate aril
#+DATE: [2023-09-10 Sun 21:12]
#+LAST_MODIFIED: [2023-12-21 Thu 22:28]
#+FILETAGS: :fruits:seeds:food:
#+HUGO_BASE_DIR: ../../
#+HUGO_AUTO_SET_LASTMOD: t
#+HUGO_SECTION: items
