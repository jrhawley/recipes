:PROPERTIES:
:ID:       598fa707-15fe-43b2-a2ac-ead0dea4d6e5
:ROAM_ALIASES: "black olive" "black olives" "Black olives"
:END:
#+TITLE: Black olive
#+DATE: [2023-11-20 Mon 07:46]
#+LAST_MODIFIED: [2023-11-20 Mon 07:47]
#+FILETAGS: :olives:fruits:drupes:food:
#+HUGO_BASE_DIR: ../../
#+HUGO_AUTO_SET_LASTMOD: t
#+HUGO_SECTION: items
