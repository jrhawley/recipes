:PROPERTIES:
:ID:       c6f773d8-fa05-4277-b4ed-1df0a0d11975
:ROAM_ALIASES: samosa samosas Samosas
:END:
#+TITLE: Samosa
#+DATE: [2023-11-11 Sat 21:30]
#+LAST_MODIFIED: [2023-11-11 Sat 21:31]
#+FILETAGS: :food:
#+HUGO_BASE_DIR: ../../
#+HUGO_AUTO_SET_LASTMOD: t
#+HUGO_SECTION: items
