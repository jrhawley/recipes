:PROPERTIES:
:ID:       0aa7b5a8-d27f-40ec-b39c-f5aae4209aba
:ROAM_ALIASES: pecans Pecans pecan
:END:
#+TITLE: Pecan
#+DATE: [2023-09-10 Sun 21:07]
#+LAST_MODIFIED: [2023-11-05 Sun 22:15]
#+FILETAGS: :nuts:food:
#+HUGO_BASE_DIR: ../../
#+HUGO_AUTO_SET_LASTMOD: t
#+HUGO_SECTION: items
