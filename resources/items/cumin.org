:PROPERTIES:
:ID:       591e51ef-30b7-48f4-9232-a0834f4c31af
:ROAM_ALIASES: cumin
:END:
#+TITLE: Cumin
#+DATE: [2023-10-24 Tue]
#+LAST_MODIFIED: [2023-10-25 Wed 00:33]
#+FILETAGS: :spices:food:
#+HUGO_BASE_DIR: ../../
#+HUGO_AUTO_SET_LASTMOD: t
#+HUGO_SECTION: items
