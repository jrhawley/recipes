:PROPERTIES:
:ID:       d61eb78d-018f-4b93-be85-aca2bec92552
:ROAM_ALIASES: mangoes Mangoes mango
:END:
#+TITLE: Mango
#+DATE: [2023-11-12 Sun 15:35]
#+LAST_MODIFIED: [2023-11-12 Sun 15:35]
#+FILETAGS: :produce:drupes:fruits:food:
#+HUGO_BASE_DIR: ../../
#+HUGO_AUTO_SET_LASTMOD: t
#+HUGO_SECTION: items
