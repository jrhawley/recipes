:PROPERTIES:
:ID:       6ada6ef5-151b-4713-8a08-bdaafa41a0fe
:ROAM_ALIASES: "black rum" "black rums" "Black rums"
:END:
#+TITLE: Black rum
#+DATE: [2023-11-20 Mon 09:21]
#+LAST_MODIFIED: [2023-11-20 Mon 09:21]
#+FILETAGS: :alcohols:beverages:rums:food:
#+HUGO_BASE_DIR: ../../
#+HUGO_AUTO_SET_LASTMOD: t
#+HUGO_SECTION: items
