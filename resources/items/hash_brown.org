:PROPERTIES:
:ID:       841b4d73-72a0-4fab-93c8-751bcddf5c63
:ROAM_ALIASES: "hash brown" "hash browns" "Hash browns"
:END:
#+TITLE: Hash brown
#+DATE: [2023-11-13 Mon 18:51]
#+LAST_MODIFIED: [2023-11-13 Mon 18:51]
#+FILETAGS: :vegetables:food:
#+HUGO_BASE_DIR: ../../
#+HUGO_AUTO_SET_LASTMOD: t
#+HUGO_SECTION: items
