:PROPERTIES:
:ID:       daefa35d-bb1d-46d4-93f7-e09c42df4d14
:ROAM_ALIASES: "Ground cinnamon" "ground cinnamon" cinnamon
:END:
#+TITLE: Cinnamon
#+DATE: [2023-09-10 Sun 09:22]
#+LAST_MODIFIED: [2023-11-05 Sun 15:25]
#+FILETAGS: :spices:food:
#+HUGO_BASE_DIR: ../../
#+HUGO_AUTO_SET_LASTMOD: t
#+HUGO_SECTION: items

* Derivatives

  - ground cinnamon
  - [[id:0e283442-f6f9-4404-9e01-5a9306f036dd][cinnamon sticks]]

