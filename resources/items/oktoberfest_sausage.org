:PROPERTIES:
:ID:       4870f74a-6a77-490f-a691-07fbc9a5876b
:ROAM_ALIASES: "Oktoberfest sausages"
:END:
#+TITLE: Oktoberfest sausage
#+DATE: [2023-11-11 Sat 21:35]
#+LAST_MODIFIED: [2023-11-11 Sat 21:35]
#+FILETAGS: :food:
#+HUGO_BASE_DIR: ../../
#+HUGO_AUTO_SET_LASTMOD: t
#+HUGO_SECTION: items
