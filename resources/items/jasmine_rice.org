:PROPERTIES:
:ID:       212403bf-61d3-4590-80fc-127cf80cbea8
:ROAM_ALIASES: "jasmine rice"
:END:
#+TITLE: Jasmine rice
#+DATE: [2023-11-19 Sun 18:29]
#+LAST_MODIFIED: [2023-11-19 Sun 22:33]
#+FILETAGS: :grains:rices:food:
#+HUGO_BASE_DIR: ../../
#+HUGO_AUTO_SET_LASTMOD: t
#+HUGO_SECTION: items
