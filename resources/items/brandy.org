:PROPERTIES:
:ID:       b4520ef7-5e65-4c7f-8485-9c4c05037cad
:ROAM_ALIASES: Brandy brandy
:END:
#+TITLE: Brandy
#+DATE: [2023-10-24 Tue]
#+LAST_MODIFIED: [2023-10-25 Wed 00:02]
#+FILETAGS: :alcohols:beverages:food:
#+HUGO_BASE_DIR: ../../
#+HUGO_AUTO_SET_LASTMOD: t
#+HUGO_SECTION: items
