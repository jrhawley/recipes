:PROPERTIES:
:ID:       5342ded8-95a1-48c7-add9-5fe5101172f1
:ROAM_ALIASES: "sour cream"
:END:
#+TITLE: Sour cream
#+DATE: [2023-09-10 Sun 20:29]
#+LAST_MODIFIED: [2023-11-13 Mon 17:35]
#+FILETAGS: :dairy:spreads:food:
#+HUGO_BASE_DIR: ../../
#+HUGO_AUTO_SET_LASTMOD: t
#+HUGO_SECTION: items
