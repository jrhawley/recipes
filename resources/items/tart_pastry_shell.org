:PROPERTIES:
:ID:       5774d517-ec0f-40ed-b715-1248c23e02d7
:ROAM_ALIASES: "tart pastry shell" "Tart pastry shells" "tart pastry shells"
:END:
#+TITLE: Tart pastry shell
#+DATE: [2024-07-24 Wed 18:15]
#+LAST_MODIFIED: [2024-07-24 Wed 18:16]
#+FILETAGS: :pastry:food:
#+HUGO_BASE_DIR: ../../
#+HUGO_AUTO_SET_LASTMOD: t
#+HUGO_SECTION: items
