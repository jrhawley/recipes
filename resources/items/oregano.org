:PROPERTIES:
:ID:       88239f38-3c15-4b0d-8052-54718aaea7a3
:ROAM_ALIASES: oregano
:END:
#+TITLE: Oregano
#+DATE: [2023-11-11 Sat 21:38]
#+LAST_MODIFIED: [2023-11-12 Sun 14:26]
#+FILETAGS: :spices:food:
#+HUGO_BASE_DIR: ../../
#+HUGO_AUTO_SET_LASTMOD: t
#+HUGO_SECTION: items
