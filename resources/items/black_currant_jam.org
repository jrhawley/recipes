:PROPERTIES:
:ID:       4e0854a7-f4b7-4535-be8e-6dd7503bc3b3
:ROAM_ALIASES: "black currant jam"
:END:
#+TITLE: Black currant jam
#+DATE: [2023-10-24 Tue]
#+LAST_MODIFIED: [2023-10-24 Tue 23:56]
#+FILETAGS: :spreads:food:
#+HUGO_BASE_DIR: ../../
#+HUGO_AUTO_SET_LASTMOD: t
#+HUGO_SECTION: items
