:PROPERTIES:
:ID:       4728f717-972e-46f4-9eb3-d847be411c3a
:ROAM_ALIASES: "lime juice"
:END:
#+TITLE: Lime juice
#+DATE: [2023-08-12 Sat 16:29]
#+LAST_MODIFIED: [2023-11-05 Sun 15:59]
#+FILETAGS: :citrus:juices:food:
#+HUGO_BASE_DIR: ../../
#+HUGO_AUTO_SET_LASTMOD: t
#+HUGO_SECTION: items
