:PROPERTIES:
:ID:       5fbb3539-a5ca-4196-bee7-e9c96ecfbfdc
:ROAM_ALIASES: havarti
:END:
#+TITLE: Havarti
#+DATE: [2024-07-24 Wed 18:50]
#+LAST_MODIFIED: [2024-07-24 Wed 18:50]
#+FILETAGS: :dairy:cheeses:food:
#+HUGO_BASE_DIR: ../../
#+HUGO_AUTO_SET_LASTMOD: t
#+HUGO_SECTION: items
