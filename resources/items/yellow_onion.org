:PROPERTIES:
:ID:       c635f344-f574-4eff-8bfd-a2e6df8323e1
:ROAM_ALIASES: "yellow onion" "yellow onions" "Yellow onions"
:END:
#+TITLE: Yellow onion
#+DATE: [2024-02-17 Sat 22:47]
#+LAST_MODIFIED: [2024-02-17 Sat 22:47]
#+FILETAGS: :produce:vegetables:onions:food:
#+HUGO_BASE_DIR: ../../
#+HUGO_AUTO_SET_LASTMOD: t
#+HUGO_SECTION: items
