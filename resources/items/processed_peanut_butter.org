:PROPERTIES:
:ID:       c72e44eb-e3c5-400d-8927-043018b94060
:ROAM_ALIASES: "processed peanut butter"
:END:
#+TITLE: Processed peanut butter
#+DATE: [2023-08-13 Sun 09:36]
#+LAST_MODIFIED: [2023-08-13 Sun 09:37]
#+FILETAGS: :spreads:food:
#+HUGO_BASE_DIR: ../../
#+HUGO_AUTO_SET_LASTMOD: t
#+HUGO_SECTION: items
#+HUGO_PUBLISHDATE: 2023-08-13
