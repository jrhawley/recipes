:PROPERTIES:
:ID:       c58031b2-32ef-4ccb-8d12-5a1b25d8cfc4
:ROAM_ALIASES: steak
:END:
#+TITLE: Steak
#+DATE: [2023-11-13 Mon 17:49]
#+LAST_MODIFIED: [2023-11-13 Mon 17:50]
#+FILETAGS: :beef:food:
#+HUGO_BASE_DIR: ../../
#+HUGO_AUTO_SET_LASTMOD: t
#+HUGO_SECTION: items

* Varieties

- [[id:f3145c4e-a9cd-43e5-9aa7-d3d4d4a8800d][sirloin steak]]

* Recipes

- [[id:148297fa-47b3-44c1-bdf3-458a3af53340][Steak]]
