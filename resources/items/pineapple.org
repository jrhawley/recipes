:PROPERTIES:
:ID:       6850cbbb-964c-488c-8aaf-11faae0c6912
:ROAM_ALIASES: Pineapples pineapples pineapple
:END:
#+TITLE: Pineapple
#+DATE: [2023-11-05 Sun 11:09]
#+LAST_MODIFIED: [2023-11-05 Sun 11:10]
#+FILETAGS: :fruits:produce:food:
#+HUGO_BASE_DIR: ../../
#+HUGO_AUTO_SET_LASTMOD: t
#+HUGO_SECTION: items

* Derivatives

- [[id:ff705bb2-5bd0-49aa-8ed4-4680996fafe0][pineapple juice]]
