:PROPERTIES:
:ID:       cfdf3e4e-72c5-4bd2-aa76-dc03d92305ca
:ROAM_ALIASES: "Ground nutmeg" "ground nutmeg" nutmeg
:END:
#+TITLE: Nutmeg
#+DATE: [2023-08-16 Wed 23:11]
#+LAST_MODIFIED: [2023-11-12 Sun 20:58]
#+FILETAGS: :spices:food:
#+HUGO_BASE_DIR: ../../
#+HUGO_AUTO_SET_LASTMOD: t
#+HUGO_SECTION: items
