:PROPERTIES:
:ID:       2342fa49-98bd-4ab8-8d24-ed836ac157de
:ROAM_ALIASES: "summer savory"
:END:
#+TITLE: Summer savory
#+DATE: [2023-11-05 Sun 14:24]
#+LAST_MODIFIED: [2023-11-05 Sun 14:26]
#+FILETAGS: :produce:herbs:food:
#+HUGO_BASE_DIR: ../../
#+HUGO_AUTO_SET_LASTMOD: t
#+HUGO_SECTION: items
