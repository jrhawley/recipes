:PROPERTIES:
:ID:       50842a78-0ab3-4c11-a495-04059bf7c7ee
:ROAM_ALIASES: yeast
:END:
#+TITLE: Yeast
#+DATE: [2023-08-21 Mon 20:38]
#+LAST_MODIFIED: [2023-12-05 Tue 20:51]
#+FILETAGS: :yeast:food:
#+HUGO_BASE_DIR: ../../
#+HUGO_AUTO_SET_LASTMOD: t
#+HUGO_SECTION: items

* Varieties

- [[id:f70a5670-bf7f-4058-a1c9-cf83ab7d08bb][nutritional yeast]]
- [[id:87dc0381-68e7-4bc0-a570-38a0ee678619][quick-rise yeast]]
- [[id:e85b2983-504f-4cf5-a710-b032b3f21410][traditional yeast]]
