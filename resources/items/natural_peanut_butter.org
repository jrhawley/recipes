:PROPERTIES:
:ID:       3a0c6c55-d885-4bc3-a5e3-f4cdf27656d2
:ROAM_ALIASES: "natural peanut butter"
:END:
#+TITLE: Natural peanut butter
#+DATE: [2023-08-13 Sun 09:57]
#+LAST_MODIFIED: [2023-08-13 Sun 09:58]
#+FILETAGS: :spreads:food:
#+HUGO_BASE_DIR: ../../
#+HUGO_AUTO_SET_LASTMOD: t
#+HUGO_SECTION: items
#+HUGO_PUBLISHDATE: 2023-08-13
