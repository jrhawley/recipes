:PROPERTIES:
:ID:       8aaf3913-dd5e-486b-b53a-da11b83d4ad6
:ROAM_ALIASES: "English muffins"
:END:
#+TITLE: English muffin
#+DATE: [2023-11-05 Sun 22:47]
#+LAST_MODIFIED: [2023-11-05 Sun 22:47]
#+FILETAGS: :breads:food:
#+HUGO_BASE_DIR: ../../
#+HUGO_AUTO_SET_LASTMOD: t
#+HUGO_SECTION: items
