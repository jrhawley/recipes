:PROPERTIES:
:ID:       5ef6d9fc-4f8c-412d-be11-efaf44b151cb
:ROAM_ALIASES: prosecco
:END:
#+TITLE: Prosecco
#+DATE: [2023-08-20 Sun 22:25]
#+LAST_MODIFIED: [2023-08-20 Sun 22:25]
#+FILETAGS: :beverages:alcohols:food:
#+HUGO_BASE_DIR: ../../
#+HUGO_AUTO_SET_LASTMOD: t
#+HUGO_SECTION: items
#+HUGO_PUBLISHDATE: 2023-08-20
