:PROPERTIES:
:ID:       b57a8dae-55d5-44ba-9145-d618c3fe48e2
:ROAM_ALIASES: passata "crushed tomato" "Crushed tomato" "crushed tomatoes" "Crushed tomatoes"
:END:
#+TITLE: Passata
#+DATE: [2023-11-12 Sun 20:39]
#+LAST_MODIFIED: [2023-11-12 Sun 20:40]
#+FILETAGS: :sauces:food:
#+HUGO_BASE_DIR: ../../
#+HUGO_AUTO_SET_LASTMOD: t
#+HUGO_SECTION: items
