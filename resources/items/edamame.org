:PROPERTIES:
:ID:       426f19a5-a43a-45d4-af0c-7c0537117834
:ROAM_ALIASES: "Edamame beans" "Edamame bean" "edamame beans" "edamame bean" edamame
:END:
#+TITLE: Edamame
#+DATE: [2023-10-21 Sat 15:12]
#+LAST_MODIFIED: [2023-10-21 Sat 15:13]
#+FILETAGS: :produce:legumes:food:
#+HUGO_BASE_DIR: ../../
#+HUGO_AUTO_SET_LASTMOD: t
#+HUGO_SECTION: items
