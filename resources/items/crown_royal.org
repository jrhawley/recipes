:PROPERTIES:
:ID:       cbe8b4c4-77ee-4bb5-a3b4-e2e769b1264c
:END:
#+TITLE: Crown Royal
#+DATE: [2023-10-24 Tue]
#+LAST_MODIFIED: [2023-10-25 Wed 00:33]
#+FILETAGS: :alcohols:beverages:whiskeys:food:
#+HUGO_BASE_DIR: ../../
#+HUGO_AUTO_SET_LASTMOD: t
#+HUGO_SECTION: items

* Flavours

- [[id:7ce2fdd1-075b-4065-b612-0db8aa48198e][Crown Royal Apple]]
