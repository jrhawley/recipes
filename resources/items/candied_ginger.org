:PROPERTIES:
:ID:       e076a8c8-4634-44ee-a1a7-29d67c0cb896
:ROAM_ALIASES: "candied ginger"
:END:
#+TITLE: Candied ginger
#+DATE: [2023-11-06 Mon]
#+LAST_MODIFIED: [2023-11-19 Sun 21:10]
#+FILETAGS: :snacks:food:
#+HUGO_BASE_DIR: ../../
#+HUGO_AUTO_SET_LASTMOD: t
#+HUGO_SECTION: items
