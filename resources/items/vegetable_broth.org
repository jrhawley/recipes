:PROPERTIES:
:ID:       6aaa4d74-e28e-4e22-afc6-dc6cf0dee4ac
:ROAM_ALIASES: "vegetable broth"
:END:
#+TITLE: Vegetable broth
#+DATE: [2023-08-12 Sat 23:53]
#+LAST_MODIFIED: [2023-11-05 Sun 16:19]
#+FILETAGS: :broths:vegetarian:food:
#+HUGO_BASE_DIR: ../../
#+HUGO_AUTO_SET_LASTMOD: t
#+HUGO_SECTION: items
