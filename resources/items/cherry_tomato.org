:PROPERTIES:
:ID:       32d73adc-34f4-4ff8-ace7-e19dbd9905aa
:ROAM_ALIASES: "Cherry tomatoes" "cherry tomatoes" "cherry tomato"
:END:
#+TITLE: Cherry tomato
#+DATE: [2023-10-21 Sat 15:27]
#+LAST_MODIFIED: [2023-10-21 Sat 15:27]
#+FILETAGS: :fruits:produce:food:
#+HUGO_BASE_DIR: ../../
#+HUGO_AUTO_SET_LASTMOD: t
#+HUGO_SECTION: items
