:PROPERTIES:
:ID:       801ebcbd-7174-4ee0-ae80-acebdefde124
:ROAM_ALIASES: "crab stick" "crab sticks" "Crab sticks" "imitation crab stick" "Imitation crab stick" "imitation crab sticks" "Imitation crab sticks" "kanikama crab stick" "Kanikama crab stick" "kanikama crab sticks" "Kanikama crab sticks"
:END:
#+TITLE: Crab stick
#+DATE: [2023-11-25 Sat 09:06]
#+LAST_MODIFIED: [2023-11-25 Sat 09:08]
#+FILETAGS: :seafood:food:
#+HUGO_BASE_DIR: ../../
#+HUGO_AUTO_SET_LASTMOD: t
#+HUGO_SECTION: items
