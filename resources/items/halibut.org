:PROPERTIES:
:ID:       3ec83339-9f9a-4bbd-b361-675af8ab2467
:ROAM_ALIASES: halibut
:END:
#+TITLE: Halibut
#+DATE: [2023-10-24 Tue]
#+LAST_MODIFIED: [2023-11-05 Sun 15:16]
#+FILETAGS: :fish:seafood:food:
#+HUGO_BASE_DIR: ../../
#+HUGO_AUTO_SET_LASTMOD: t
#+HUGO_SECTION: items
