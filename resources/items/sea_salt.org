:PROPERTIES:
:ID:       0072c0fd-c843-44b6-92de-27f3e7845c52
:ROAM_ALIASES: "Fine sea salt" "Flaky sea salt" "sea salt" "fine sea salt" "flaky sea salt"
:END:
#+TITLE: Sea salt
#+DATE: [2023-08-12 Sat 23:57]
#+LAST_MODIFIED: [2023-11-06 Mon 08:28]
#+FILETAGS: :salts:food:
#+HUGO_BASE_DIR: ../../
#+HUGO_AUTO_SET_LASTMOD: t
#+HUGO_SECTION: items
