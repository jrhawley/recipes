:PROPERTIES:
:ID:       b3692180-16ba-453c-ac93-9d3c5787cecc
:ROAM_ALIASES: "chocolate chip" "chocolate chips" "Chocolate chips"
:END:
#+TITLE: Chocolate chip
#+DATE: [2023-11-06 Mon]
#+LAST_MODIFIED: [2023-11-19 Sun 21:24]
#+FILETAGS: :sweeteners:food:
#+HUGO_BASE_DIR: ../../
#+HUGO_AUTO_SET_LASTMOD: t
#+HUGO_SECTION: items
