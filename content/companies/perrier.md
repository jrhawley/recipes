+++
title = "Perrier"
author = ["James Hawley"]
date = 2024-03-17T00:00:00-04:00
lastmod = 2024-04-02T00:25:57-04:00
tags = ["food", "company"]
draft = false
+++

A subsidiary of [Nestle]({{< relref "nestle.md" >}}).
