+++
title = "Carrot, lentil, and spinach soup"
author = ["James Hawley"]
date = 2024-04-09T10:25:00-04:00
lastmod = 2024-04-15T10:06:45-04:00
tags = ["soups", "vegetarian", "vegan", "recipes"]
draft = false
+++

\#+HUGO_SECTION : recipes

| Info      | Amount     |
|-----------|------------|
| Prep Time | 10 min     |
| Cook Time | 25 min     |
| Yields    | 4 servings |


## Equipment {#equipment}

-   1 large cutting board
-   prep knife
-   1 large soup pot with lid
-   1 garlic press
-   measuring cups and spoons


## Ingredients {#ingredients}

| Quantity   | Item                                                                 |
|------------|----------------------------------------------------------------------|
| 1 Tbsp     | [olive oil]({{< relref "../items/olive_oil.md" >}})                  |
| 1/2        | [red onion]({{< relref "../items/red_onion.md" >}}), diced           |
| 6          | [carrots]({{< relref "../items/carrot.md" >}}), cut into 1 cm rounds |
| 2 cloves   | [garlic]({{< relref "../items/garlic.md" >}}), minced                |
| 1/2 tsp    | [cumin]({{< relref "../items/cumin.md" >}})                          |
| 1/2 tsp    | [turmeric]({{< relref "../items/turmeric.md" >}})                    |
| 1/2 tsp    | [black pepper]({{< relref "../items/black_pepper.md" >}})            |
| 1/2 tsp    | [paprika]({{< relref "../items/paprika.md" >}})                      |
| 1 tsp      | [sea salt]({{< relref "../items/sea_salt.md" >}}), to taste          |
| 15 oz      | [diced tomatoes]({{< relref "../items/diced_tomatoes.md" >}})        |
| 1 L        | [vegetable broth]({{< relref "../items/vegetable_broth.md" >}})      |
| 15 oz      | [lentils]({{< relref "../items/lentil.md" >}})                       |
| 4 handfuls | [spinach]({{< relref "../items/spinach.md" >}})                      |
| 1/2        | [lemon]({{< relref "../items/lemon.md" >}}), juiced                  |


## Directions {#directions}

1.  In a large pot over medium heat, add olive oil and red onion, sauteing until soft and fragrant.
    1.  Stir in carrots and cook for a few more minutes, followed by the garlic, spices, and salt.
    2.  Stir to coat the vegetables, then add the tomatoes and broth.
    3.  If using dry lentils, add them now.
        If lentils are pre-cooked/soaked, reserve them until later.
2.  Increase the heat, bring the soup to a boil, then cover and reduce heat to low.
    Let simmer for 20 min, or until the carrots are soft.
3.  Once the carrots are soft, add the lemon juice and spinach, and stir together.
    1.  If using pre-cooked/soaked lentils, add them now.
    2.  Stir together, then season to tase.
4.  Serve and let cool for a few minutes before eating.


## References {#references}

1.  Original recipe: [Occassionally Eggs](https://www.occasionallyeggs.com/carrot-red-lentil-spinach-soup/#wprm-recipe-container-6155)
