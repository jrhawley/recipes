+++
title = "Cheerios"
author = ["James Hawley"]
date = 2023-11-20T07:22:00-05:00
lastmod = 2023-11-20T08:51:23-05:00
tags = ["cereal", "grains", "food"]
draft = false
+++

## Flavours {#flavours}

-   multigrain
-   apple cinnamon
