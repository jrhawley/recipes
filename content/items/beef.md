+++
title = "Beef"
author = ["James Hawley"]
date = 2023-11-13T17:47:00-05:00
lastmod = 2024-04-17T17:25:43-04:00
tags = ["beef", "food"]
draft = false
+++

## Varieties {#varieties}

-   [beef back ribs]({{< relref "beef_back_ribs.md" >}})
-   [beef burger]({{< relref "beef_burger.md" >}})
-   [beef chuck]({{< relref "beef_chuck.md" >}})
-   [steak]({{< relref "steak.md" >}})


## Derivatives {#derivatives}

-   [beef broth]({{< relref "beef_broth.md" >}})
-   [ground beef]({{< relref "ground_beef.md" >}})


## Cuts {#cuts}

{{< figure src="/ox-hugo/beef-cuts.jpg" caption="<span class=\"figure-number\">Figure 1: </span>Beef cuts" >}}


## References {#references}

1.  [Food Fire Friends](https://www.foodfirefriends.com/beef-cuts/)
