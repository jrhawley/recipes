+++
title = "Olive"
author = ["James Hawley"]
date = 2023-11-20T07:45:00-05:00
lastmod = 2023-11-20T09:06:58-05:00
tags = ["olives", "drupes", "fruits", "drupe", "food"]
draft = false
+++

## Varieties {#varieties}

-   [green olive]({{< relref "green_olive.md" >}})
-   [black olive]({{< relref "black_olive.md" >}})
-   [kalamata olive]({{< relref "kalamata_olive.md" >}})


## Derivatives {#derivatives}

-   [olive oil]({{< relref "olive_oil.md" >}})
