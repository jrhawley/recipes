+++
title = "Yeast"
author = ["James Hawley"]
date = 2023-08-21T20:38:00-04:00
lastmod = 2023-12-05T20:51:31-05:00
tags = ["yeast", "food"]
draft = false
+++

## Varieties {#varieties}

-   [nutritional yeast]({{< relref "nutritional_yeast.md" >}})
-   [quick-rise yeast]({{< relref "quick-rise_yeast.md" >}})
-   [traditional yeast]({{< relref "traditional_yeast.md" >}})
