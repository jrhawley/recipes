+++
title = "Grapefruit"
author = ["James Hawley"]
date = 2023-11-05T23:03:00-05:00
lastmod = 2023-11-05T23:05:00-05:00
tags = ["fruits", "citrus", "food"]
draft = false
+++

## Derivatives {#derivatives}

-   [grapefruit juice]({{< relref "grapefruit_juice.md" >}})
