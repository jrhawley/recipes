+++
title = "Ginger"
author = ["James Hawley"]
date = 2023-09-10T21:24:00-04:00
publishDate = 2023-09-10
lastmod = 2023-09-10T22:13:57-04:00
tags = ["vegetables", "food"]
draft = false
+++

## Derivatives {#derivatives}

-   [ginger powder]({{< relref "ginger_powder.md" >}})
-   [ginger ale]({{< relref "ginger_ale.md" >}})
-   [ginger beer]({{< relref "ginger_beer.md" >}})
