+++
title = "Digby scallop"
author = ["James Hawley"]
date = 2023-08-17T01:39:00-04:00
publishDate = 2023-08-17
lastmod = 2023-08-20T09:36:06-04:00
tags = ["scallops", "seafood", "food"]
draft = false
+++

A type of [bay scallop]({{< relref "bay_scallop.md" >}}) native to Digby, Nova Scotia.
Digby is located on the shore of the Bay of Fundy and has been harvesting these scallops for &gt; 100 years.
