+++
title = "Bailey's"
author = ["James Hawley"]
date = 2023-10-21T20:18:00-04:00
lastmod = 2023-12-23T01:59:56-05:00
tags = ["alcohols", "beverages", "whiskeys", "food"]
draft = false
+++

An Irish cream whisky.
