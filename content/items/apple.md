+++
title = "Apple"
author = ["James Hawley"]
date = 2020-03-01T00:00:00-05:00
lastmod = 2024-04-15T10:05:29-04:00
tags = ["food", "produce", "fruits"]
draft = false
+++

## Varieties {#varieties}

-   McIntosh
-   Red Delicious
-   Gala
-   Crispin
-   Braeburn
-   Fuji
-   Granny Smith
-   Empire
-   Pink Lady
-   Cameo
-   Jazz
-   Golden Delicious
-   Jonagold
-   20 Ounce
-   Macoun
-   Pink Pearl
-   Cortland
-   Ginger Gold
-   Paula Red
-   Arkansas black
-   Rome
-   Northern Spy
-   Newtonw Pippin
-   Idared
-   Ambrosia
-   Winesap
-   Aurora Golden Gala
-   Grimes Golden
-   Crimson Gold
-   Jonathan
-   Honeycrisp
-   Ashmead's Kernel
-   Sonya
-   Fortune
-   Roxbury Russet
-   Stayman
-   York Imperial
-   Envy
-   Mutzu


## Derivatives {#derivatives}

-   [applesauce]({{< relref "applesauce.md" >}})
