+++
title = "Grape"
author = ["James Hawley"]
date = 2023-10-21T20:34:00-04:00
lastmod = 2023-11-05T22:17:05-05:00
tags = ["fruits", "food"]
draft = false
+++

## Varieties {#varieties}


## Derivatives {#derivatives}

-   [raisins]({{< relref "raisin.md" >}})
