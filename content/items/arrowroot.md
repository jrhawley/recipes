+++
title = "Arrowroot"
author = ["James Hawley"]
date = 2023-11-20T08:35:00-05:00
lastmod = 2023-11-20T08:48:57-05:00
tags = ["starches", "food"]
draft = false
+++

## Derivatives {#derivatives}

-   [arrowroot cookies]({{< relref "arrowroot_cookie.md" >}})
