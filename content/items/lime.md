+++
title = "Lime"
author = ["James Hawley"]
date = 2023-08-12T22:20:00-04:00
publishDate = 2023-08-12
lastmod = 2023-11-05T11:14:41-05:00
tags = ["citrus", "fruits", "food"]
draft = false
+++

## Derivatives {#derivatives}

-   [lime juice]({{< relref "lime_juice.md" >}})
-   [lime zest]({{< relref "lime_zest.md" >}})
