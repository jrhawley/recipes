+++
title = "White wine"
author = ["James Hawley"]
date = 2023-11-12T20:47:00-05:00
lastmod = 2023-11-19T22:30:28-05:00
tags = ["wines", "alcohols", "beverages", "food"]
draft = false
+++

## Varieties {#varieties}

-   [sauvignon blanc]({{< relref "sauvignon_blanc.md" >}})


## Derivatives {#derivatives}

-   [white wine vinegar]({{< relref "white_wine_vinegar.md" >}})


## Brands {#brands}
