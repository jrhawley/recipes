+++
title = "Broth"
author = ["James Hawley"]
date = 2023-08-12T23:53:00-04:00
publishDate = 2023-08-12
lastmod = 2023-08-13T01:06:31-04:00
tags = ["broths", "food"]
draft = false
+++

## Varieties {#varieties}

-   [chicken broth]({{< relref "chicken_broth.md" >}})
-   [vegetable broth]({{< relref "vegetable_broth.md" >}})
