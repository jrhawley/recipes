+++
title = "Kale"
author = ["James Hawley"]
date = 2023-10-21T20:42:00-04:00
lastmod = 2023-11-19T22:29:59-05:00
tags = ["vegetables", "leafy-greens", "food"]
draft = false
+++

## Derivatives {#derivatives}

-   [kale gnocchi]({{< relref "kale_gnocchi.md" >}})
