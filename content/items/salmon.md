+++
title = "Salmon"
author = ["James Hawley"]
date = 2023-11-05T15:00:00-05:00
lastmod = 2023-11-05T15:03:00-05:00
tags = ["fish", "seafood", "food"]
draft = false
+++

## Derivatives {#derivatives}

-   [smoked salmon]({{< relref "smoked_salmon.md" >}})
