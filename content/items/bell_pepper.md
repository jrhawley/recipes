+++
title = "Bell pepper"
author = ["James Hawley"]
date = 2023-08-13T09:21:00-04:00
lastmod = 2023-10-26T00:02:12-04:00
tags = ["peppers", "vegetables", "produce", "food"]
draft = false
+++

## Varieties {#varieties}

-   red
-   orange
-   yellow
-   green
