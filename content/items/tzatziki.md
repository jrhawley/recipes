+++
title = "Tzatziki"
author = ["James Hawley"]
date = 2023-11-12T21:08:00-05:00
lastmod = 2024-10-19T01:05:58-04:00
tags = ["sauces", "food"]
draft = false
+++

-   1/2 large cucumber, grated and drained
-   500 g greek yogurt
-   4 Tbsp lemon juice
-   1 Tbsp fresh dill
-   2 tbsp mint
-   1 Tbsp olive oil
-   1/2 tsp salt
-   [Middle Eats](https://youtube.com/watch?v=elDgcvpAvf8)
