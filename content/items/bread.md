+++
title = "Bread"
author = ["James Hawley"]
date = 2023-08-13T09:09:00-04:00
lastmod = 2023-11-05T22:57:07-05:00
tags = ["breads", "food"]
draft = false
+++

## Varieties {#varieties}

-   [baguette]({{< relref "baguette.md" >}})
-   [French bread]({{< relref "french_bread.md" >}})
-   [Italian bread]({{< relref "italian_bread.md" >}})
-   [malt bread]({{< relref "malt_bread.md" >}})
-   [naan]({{< relref "naan.md" >}})
-   [sourdough]({{< relref "sourdough.md" >}})
-   [tortilla]({{< relref "tortilla.md" >}})


## Derivatives {#derivatives}
