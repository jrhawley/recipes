+++
title = "Squash"
author = ["James Hawley"]
date = 2023-10-24T00:00:00-04:00
lastmod = 2023-11-05T14:21:43-05:00
tags = ["vegetables", "gourds", "food"]
draft = false
+++

## Varieties {#varieties}

-   [acorn squash]({{< relref "acorn_squash.md" >}})
-   [butternut squash]({{< relref "butternut_squash.md" >}})
-   [spaghetti squash]({{< relref "spaghetti_squash.md" >}})
