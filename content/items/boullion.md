+++
title = "Boullion"
author = ["James Hawley"]
date = 2024-01-21T14:54:00-05:00
lastmod = 2024-01-31T22:25:19-05:00
tags = ["food"]
draft = false
+++

## Varieties {#varieties}

-   [chicken boullion]({{< relref "chicken_boullion.md" >}})
-   [lobster boullion]({{< relref "lobster_boullion.md" >}})
-   [beef boullion]({{< relref "beef_boullion.md" >}})
