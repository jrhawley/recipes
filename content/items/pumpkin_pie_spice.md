+++
title = "Pumpkin pie spice"
author = ["James Hawley"]
date = 2023-09-10T21:09:00-04:00
publishDate = 2023-09-10
lastmod = 2023-09-10T21:27:15-04:00
tags = ["spices", "food"]
draft = false
+++

A blend of spices that doesn't include pumpkin, but what is typically used in pumpkin pies.
