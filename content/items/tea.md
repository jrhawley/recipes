+++
title = "Tea"
author = ["James Hawley"]
date = 2023-08-13T11:17:00-04:00
lastmod = 2023-11-19T22:30:13-05:00
tags = ["teas", "beverages", "food"]
draft = false
+++

## Varieties {#varieties}

-   [herbal tea]({{< relref "herbal_tea.md" >}})
-   [black tea]({{< relref "black_tea.md" >}})
