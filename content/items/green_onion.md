+++
title = "Green onion"
author = ["James Hawley"]
date = 2023-08-27T20:31:00-04:00
lastmod = 2024-10-19T00:54:17-04:00
tags = ["vegetables", "food"]
draft = false
+++

## Storage {#storage}

Green onions can be stored on the countertop at room temperature standing upright in a container with water.
Alternatively, they can be wrapped in a wet paper towel and stored in a plastic bag in the vegetable crisper in the refrigerator.
