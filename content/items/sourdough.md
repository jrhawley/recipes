+++
title = "Sourdough"
author = ["James Hawley"]
date = 2023-08-13T09:10:00-04:00
lastmod = 2024-10-19T01:06:23-04:00
tags = ["breads", "food"]
draft = false
+++

## Brands {#brands}

-   Stonemill


## Making sourdough {#making-sourdough}

-   [Easy Sourdough at home - Brian Lagerstrom](https://www.youtube.com/watch?v=dtWFkODWxBg)
-   [Easy Sourdough Starter Guide - Brian Lagerstrom](https://www.youtube.com/watch?v=ZHm1aKxAsIs)
