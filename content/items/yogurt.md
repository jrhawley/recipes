+++
title = "Yogurt"
author = ["James Hawley"]
date = 2023-11-12T20:57:00-05:00
lastmod = 2023-12-21T21:38:31-05:00
tags = ["dairy", "food"]
draft = false
+++

## Varieties {#varieties}

-   [Greek yogurt]({{< relref "greek_yogurt.md" >}})
