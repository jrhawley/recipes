+++
title = "Sausage"
author = ["James Hawley"]
date = 2023-11-11T21:32:00-05:00
lastmod = 2023-11-12T14:26:59-05:00
tags = ["food"]
draft = false
+++

## Varieties {#varieties}

-   [Italian sausage]({{< relref "italian_sausage.md" >}})
-   [breakfast sausage]({{< relref "breakfast_sausage.md" >}})
-   [Oktoberfest sausage]({{< relref "oktoberfest_sausage.md" >}})
