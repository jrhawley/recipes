+++
title = "Rum"
author = ["James Hawley"]
date = 2023-11-20T09:20:00-05:00
lastmod = 2023-11-20T09:23:05-05:00
tags = ["alcohols", "beverages", "rums", "food"]
draft = false
+++

## Varieties {#varieties}

-   [aged rum]({{< relref "aged_rum.md" >}})
-   [black rum]({{< relref "black_rum.md" >}})
-   [gold rum]({{< relref "gold_rum.md" >}})
-   [white rum]({{< relref "white_rum.md" >}})
