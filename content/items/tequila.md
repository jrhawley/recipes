+++
title = "Tequila"
author = ["James Hawley"]
date = 2023-09-08T11:26:00-04:00
lastmod = 2024-10-19T01:05:38-04:00
tags = ["alcohols", "beverages", "food"]
draft = false
+++

## Brands {#brands}

-   [Jose Cuervo Silver tequila]({{< relref "jose_cuervo_silver_tequila.md" >}})
