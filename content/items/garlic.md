+++
title = "Garlic"
author = ["James Hawley"]
date = 2023-08-12T22:04:00-04:00
lastmod = 2023-12-05T20:53:17-05:00
tags = ["produce", "vegetables", "food"]
draft = false
+++

## Derivatives {#derivatives}

-   [garlic powder]({{< relref "garlic_powder.md" >}})
-   [garlic salt]({{< relref "garlic_salt.md" >}})
