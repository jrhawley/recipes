+++
title = "Pineapple"
author = ["James Hawley"]
date = 2023-11-05T11:09:00-05:00
lastmod = 2023-11-05T11:14:22-05:00
tags = ["fruits", "produce", "food"]
draft = false
+++

## Derivatives {#derivatives}

-   [pineapple juice]({{< relref "pineapple_juice.md" >}})
