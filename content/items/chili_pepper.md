+++
title = "Chili pepper"
author = ["James Hawley"]
date = 2023-08-13T09:41:00-04:00
lastmod = 2023-12-19T21:51:04-05:00
tags = ["peppers", "vegetables", "produce", "food"]
draft = false
+++

## Varieties {#varieties}

-   [jalapeno pepper]({{< relref "jalapeno_pepper.md" >}})


## Derivatives {#derivatives}

-   [chili powder]({{< relref "chili_powder.md" >}})
-   [red pepper flakes]({{< relref "red_pepper_flake.md" >}})
