+++
title = "Sparkling water"
author = ["James Hawley"]
date = 2023-10-24T00:00:00-04:00
lastmod = 2024-04-02T00:22:06-04:00
tags = ["beverages", "food"]
draft = false
+++

## Brands {#brands}

-   [San Pellegrino]({{< relref "../companies/san_pellegrino.md" >}})
-   [Perrier]({{< relref "../companies/perrier.md" >}})
