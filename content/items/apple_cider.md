+++
title = "Apple cider"
author = ["James Hawley"]
date = 2020-10-16T00:00:00-04:00
lastmod = 2023-11-20T09:41:03-05:00
tags = ["juices", "beverages", "food"]
draft = false
+++

## Recipes {#recipes}

-   [Apple cider]({{< relref "../recipes/apple_cider.md" >}})
