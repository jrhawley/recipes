+++
title = "Chicken"
author = ["James Hawley"]
date = 2023-08-27T20:47:00-04:00
lastmod = 2023-11-06T08:09:22-05:00
tags = ["chicken", "food"]
draft = false
+++

## Derivatives {#derivatives}

-   [chicken breast]({{< relref "chicken_breast.md" >}})
-   [chicken tender]({{< relref "chicken_tender.md" >}})
