+++
title = "Herbal tea"
author = ["James Hawley"]
date = 2023-08-13T11:19:00-04:00
lastmod = 2023-11-19T22:29:44-05:00
tags = ["herbal-teas", "teas", "beverages", "food"]
draft = false
+++

## Flavours {#flavours}

-   [hibiscus tea]({{< relref "hibiscus_tea.md" >}})
-   [honey lemon tea]({{< relref "honey_lemon_tea.md" >}})
-   [peppermint tea]({{< relref "peppermint_tea.md" >}})
