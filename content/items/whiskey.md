+++
title = "Whiskey"
author = ["James Hawley"]
date = 2023-10-24T00:00:00-04:00
lastmod = 2023-11-20T09:16:01-05:00
tags = ["alcohols", "beverages", "whiskeys", "food"]
draft = false
+++

## Varieties {#varieties}

-   [bourbon]({{< relref "bourbon.md" >}})
-   [scotch]({{< relref "scotch.md" >}})
-   [rye]({{< relref "rye_whiskey.md" >}})


## Brands {#brands}

-   [Fireball]({{< relref "fireball.md" >}})
