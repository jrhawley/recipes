+++
title = "Milk"
author = ["James Hawley"]
date = 2023-08-16T23:06:00-04:00
lastmod = 2023-12-22T00:32:38-05:00
tags = ["milks", "beverages", "food"]
draft = false
+++

## Varieties {#varieties}

-   [almond milk]({{< relref "almond_milk.md" >}})
-   [dairy milk]({{< relref "dairy_milk.md" >}})
-   [oat milk]({{< relref "oat_milk.md" >}})
-   [coconut milk]({{< relref "coconut_milk.md" >}})
-   [buttermilk]({{< relref "buttermilk.md" >}})


## Flavours {#flavours}

-   [chocolate milk]({{< relref "chocolate_milk.md" >}})


## Derivatives {#derivatives}

-   [evaporated milk]({{< relref "evaporated_milk.md" >}})
-   [condensed milk]({{< relref "condensed_milk.md" >}})
