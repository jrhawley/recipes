+++
title = "Jam"
author = ["James Hawley"]
date = 2023-10-24T23:58:00-04:00
lastmod = 2023-11-05T23:17:09-05:00
tags = ["spreads", "food"]
draft = false
+++

## Varieties {#varieties}

-   [black currant jam]({{< relref "black_currant_jam.md" >}})
-   [blueberry jam]({{< relref "blueberry_jam.md" >}})
-   [raspberry jam]({{< relref "raspberry_jam.md" >}})
-   [strawberry jam]({{< relref "strawberry_jam.md" >}})
