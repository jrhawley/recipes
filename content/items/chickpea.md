+++
title = "Chickpea"
author = ["James Hawley"]
date = 2023-08-13T09:47:00-04:00
lastmod = 2023-11-05T16:24:43-05:00
tags = ["legumes", "food"]
draft = false
+++

## Varieties {#varieties}

-   [garbanzo]({{< relref "garbanzo.md" >}})
