+++
title = "Red wine"
author = ["James Hawley"]
date = 2023-11-12T20:49:00-05:00
lastmod = 2023-11-12T20:52:35-05:00
tags = ["alcohols", "beverages", "food"]
draft = false
+++

## Varieties {#varieties}


## Derivatives {#derivatives}

-   [red wine vinegar]({{< relref "red_wine_vinegar.md" >}})


## Brands {#brands}
