+++
title = "Apricot"
author = ["James Hawley"]
date = 2023-08-02T00:00:00-04:00
lastmod = 2023-11-05T22:26:09-05:00
tags = ["drupes", "food", "fruits", "produce"]
draft = false
+++

## Derivatives {#derivatives}

-   [apricot preserve]({{< relref "apricot_preserve.md" >}})
-   [dried apricot]({{< relref "dried_apricot.md" >}})
