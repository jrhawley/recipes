+++
title = "Mustard"
author = ["James Hawley"]
date = 2023-08-12T22:06:00-04:00
lastmod = 2023-11-20T09:06:10-05:00
tags = ["sauces", "mustards", "food"]
draft = false
+++

## Varieties {#varieties}

-   [Dijon mustard]({{< relref "dijon_mustard.md" >}})
-   [brown mustard]({{< relref "brown_mustard.md" >}})
-   [yellow mustard]({{< relref "yellow_mustard.md" >}})
