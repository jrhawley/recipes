+++
title = "Cheese"
author = ["James Hawley"]
date = 2023-08-12T23:58:00-04:00
lastmod = 2023-11-13T17:25:22-05:00
tags = ["cheeses", "food"]
draft = false
+++

## Varieties {#varieties}

-   [blue cheese]({{< relref "blue_cheese.md" >}})
-   [bocconcini]({{< relref "bocconcini.md" >}})
-   [brie]({{< relref "brie.md" >}})
-   [burrata]({{< relref "burrata.md" >}})
-   [cheddar]({{< relref "cheddar.md" >}})
-   [cheese curd]({{< relref "cheese_curd.md" >}})
-   [cream cheese]({{< relref "cream_cheese.md" >}})
-   [feta]({{< relref "feta.md" >}})
-   [goat cheese]({{< relref "goat_cheese.md" >}})
-   [gouda]({{< relref "gouda.md" >}})
-   [mozzarella]({{< relref "mozzarella.md" >}})
-   [parmesan]({{< relref "parmesan.md" >}})
-   [ricotta]({{< relref "ricotta.md" >}})
-   [stilton]({{< relref "stilton_cheese.md" >}})
