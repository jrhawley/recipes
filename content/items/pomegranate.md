+++
title = "Pomegranate"
author = ["James Hawley"]
date = 2023-09-10T21:10:00-04:00
publishDate = 2023-09-10
lastmod = 2023-09-10T22:11:17-04:00
tags = ["fruits", "food"]
draft = false
+++

## Derivatives {#derivatives}

-   [pomegranate juice]({{< relref "pomegranate_juice.md" >}})
-   [pomegranate arils]({{< relref "pomegranate_aril.md" >}})
