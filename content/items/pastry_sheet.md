+++
title = "Pastry sheet"
author = ["James Hawley"]
date = 2023-11-20T07:19:00-05:00
lastmod = 2024-10-19T01:04:13-04:00
tags = ["pastry", "food"]
draft = false
+++

## Derivatives {#derivatives}

-   [tart pastry shell]({{< relref "tart_pastry_shell.md" >}})
