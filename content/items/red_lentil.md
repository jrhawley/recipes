+++
title = "Red lentil"
author = ["James Hawley"]
date = 2023-11-12T15:21:00-05:00
lastmod = 2023-11-20T09:12:38-05:00
tags = ["legumes", "food"]
draft = false
+++

## Derivatives {#derivatives}

-   [red lentil pasta]({{< relref "red_lentil_pasta.md" >}})
