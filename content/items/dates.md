+++
title = "Dates"
author = ["James Hawley"]
date = 2023-08-12T17:23:00-04:00
lastmod = 2023-11-05T20:42:25-05:00
tags = ["fruits", "food"]
draft = false
+++

## Varieties {#varieties}

-   [Medjool dates]({{< relref "medjool_date.md" >}})
