+++
title = "Scallop"
author = ["James Hawley"]
date = 2023-08-20T09:14:00-04:00
publishDate = 2023-08-20
lastmod = 2023-08-20T09:28:33-04:00
tags = ["scallops", "seafood", "food"]
draft = false
+++

A slightly sweet, buttery, and creamy seafood dish.
If store-bought, these tend to be very simple to prepare: pat dry then sear for a few minutes on each side.
The result is a slightly crispy, soft, melt-in-your-mouth seafood that is great on its own, pairs well with other seafood, or is the star of the show in a risotto or salad.


## Varieties {#varieties}

-   Atlantic sea scallop
-   Calico scallop
-   [bay scallop]({{< relref "bay_scallop.md" >}})
    -   [Digby scallop]({{< relref "digby_scallop.md" >}})
-   King scallop
-   Queen scallop
-   Tasmanian scallop
-   Dayboat scallop
-   Diver scallop
