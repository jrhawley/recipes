+++
title = "Corn"
author = ["James Hawley"]
date = 2023-08-12T23:20:00-04:00
lastmod = 2023-11-13T22:08:23-05:00
tags = ["vegetables", "food"]
draft = false
+++

## Varieties {#varieties}

-   [corn cob]({{< relref "corn_cob.md" >}})
-   [corn niblets]({{< relref "corn_niblet.md" >}})


## Derivatives {#derivatives}

-   [corn starch]({{< relref "corn_starch.md" >}})
