+++
title = "Carrot"
author = ["James Hawley"]
date = 2023-08-12T23:38:00-04:00
lastmod = 2023-11-05T16:25:23-05:00
tags = ["produce", "vegetables", "food"]
draft = false
+++

## Varieties {#varieties}

-   [baby carrots]({{< relref "baby_carrot.md" >}})
