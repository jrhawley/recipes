+++
title = "Potato"
author = ["James Hawley"]
date = 2023-08-13T09:26:00-04:00
lastmod = 2023-11-13T18:54:49-05:00
tags = ["vegetables", "potatoes", "food"]
draft = false
+++

## Varieties {#varieties}

-   [yam]({{< relref "yam.md" >}})
-   [russet potato]({{< relref "russet_potato.md" >}})
-   [Yukon gold potato]({{< relref "yukon_gold_potato.md" >}})
-   [red potato]({{< relref "red_potato.md" >}})


## Derivaties {#derivaties}

-   [hash browns]({{< relref "hash_brown.md" >}})
