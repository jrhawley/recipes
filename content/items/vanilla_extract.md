+++
title = "Vanilla extract"
author = ["James Hawley"]
date = 2023-08-12T17:32:00-04:00
lastmod = 2023-11-05T15:25:17-05:00
tags = ["sweeteners", "food"]
draft = false
+++

## Varieties {#varieties}

-   [artificial vanilla extract]({{< relref "artificial_vanilla_extract.md" >}})
-   [pure vanilla extract]({{< relref "pure_vanilla_extract.md" >}})
