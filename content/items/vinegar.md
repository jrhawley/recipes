+++
title = "Vinegar"
author = ["James Hawley"]
date = 2023-09-10T21:18:00-04:00
publishDate = 2023-09-10
lastmod = 2023-09-10T22:14:57-04:00
tags = ["food"]
draft = false
+++

## Varieties {#varieties}

-   [apple cider vinegar]({{< relref "apple_cider_vinegar.md" >}})
-   [balsamic vinegar]({{< relref "balsamic_vinegar.md" >}})
-   [malt vinegar]({{< relref "malt_vinegar.md" >}})
-   [red wine vinegar]({{< relref "red_wine_vinegar.md" >}})
-   [white vinegar]({{< relref "white_vinegar.md" >}})
-   [white wine vinegar]({{< relref "white_wine_vinegar.md" >}})
