+++
title = "Coffee syrup"
author = ["James Hawley"]
date = 2023-11-20T07:53:00-05:00
lastmod = 2024-10-19T00:53:44-04:00
tags = ["sweeteners", "food"]
draft = false
+++

## Flavours {#flavours}

-   hazelnut
-   caramel
-   gingerbread
-   peppermint
-   smores
