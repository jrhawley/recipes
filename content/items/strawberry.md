+++
title = "Strawberry"
author = ["James Hawley"]
date = 2023-11-05T14:22:00-05:00
lastmod = 2023-11-05T16:43:39-05:00
tags = ["berries", "fruits", "produce", "food"]
draft = false
+++

## Derivatives {#derivatives}

-   [strawberry jam]({{< relref "strawberry_jam.md" >}})
