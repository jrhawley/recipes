+++
title = "Flour"
author = ["James Hawley"]
date = 2023-08-12T23:41:00-04:00
lastmod = 2023-12-05T20:08:52-05:00
tags = ["baking", "flours", "food"]
draft = false
+++

## Varieties {#varieties}

-   [all-purpose flour]({{< relref "all-purpose_flour.md" >}})
-   [almond flour]({{< relref "almond_flour.md" >}})
-   [bread flour]({{< relref "bread_flour.md" >}})
-   [cake flour]({{< relref "cake_flour.md" >}})
-   [whole wheat flour]({{< relref "whole_wheat_flour.md" >}})
