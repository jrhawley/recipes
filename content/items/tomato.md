+++
title = "Tomato"
author = ["James Hawley"]
date = 2023-08-13T09:29:00-04:00
lastmod = 2023-11-12T20:41:19-05:00
tags = ["fruits", "produce", "tomatoes", "food"]
draft = false
+++

## Varieties {#varieties}

-   [cherry tomato]({{< relref "cherry_tomato.md" >}})
-   [beefsteak tomato]({{< relref "beefsteak_tomato.md" >}})
-   [field tomato]({{< relref "field_tomato.md" >}})
-   [plum tomato]({{< relref "plum_tomato.md" >}})
-   [roma tomato]({{< relref "roma_tomato.md" >}})


## Derivatives {#derivatives}

-   [diced tomatoes]({{< relref "diced_tomatoes.md" >}})
-   [passata]({{< relref "passata.md" >}})
-   [sun-dried tomato]({{< relref "sun-dried_tomato.md" >}})
-   [tomato paste]({{< relref "tomato_paste.md" >}})
-   [tomato soup]({{< relref "tomato_soup.md" >}})
-   [whole peeled tomato]({{< relref "whole_peeled_tomato.md" >}})
