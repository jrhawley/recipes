+++
title = "Bitter"
author = ["James Hawley"]
date = 2023-12-24T19:59:00-05:00
lastmod = 2023-12-24T20:00:49-05:00
tags = ["beverages", "bitters", "food"]
draft = false
+++

## Flavours {#flavours}

-   apple
-   cranberry
-   ginger
-   peach
-   pear


## Brands {#brands}

-   [Angostura bitters]({{< relref "angostura_bitters.md" >}})
