+++
title = "Lamb"
author = ["James Hawley"]
date = 2023-11-13T17:43:00-05:00
lastmod = 2024-01-31T22:26:49-05:00
tags = ["mutton", "food"]
draft = false
+++

## Varieties {#varieties}

-   [lamb chop]({{< relref "lamb_chop.md" >}})
