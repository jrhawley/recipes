+++
title = "Cheddar"
author = ["James Hawley"]
date = 2023-08-13T00:00:00-04:00
lastmod = 2023-11-19T22:55:26-05:00
tags = ["dairy", "cheeses", "food"]
draft = false
+++

## Varieties {#varieties}

-   yellow cheddar
-   [white cheddar]({{< relref "white_cheddar.md" >}})
