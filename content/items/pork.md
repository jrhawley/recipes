+++
title = "Pork"
author = ["James Hawley"]
date = 2023-11-06T07:46:00-05:00
lastmod = 2024-01-31T22:27:23-05:00
tags = ["pork", "food"]
draft = false
+++

## Derivatives {#derivatives}

-   [ground pork]({{< relref "ground_pork.md" >}})
-   [pork shoulder]({{< relref "pork_shoulder.md" >}})
-   [pork tenderloin]({{< relref "pork_tenderloin.md" >}})
-   [pork chop]({{< relref "pork_chop.md" >}})
-   [ham]({{< relref "ham.md" >}})
-   [prosciutto]({{< relref "prosciutto.md" >}})
-   [bacon]({{< relref "bacon.md" >}})
-   [pepperette]({{< relref "pepperette.md" >}})
