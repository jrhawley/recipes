+++
title = "Curry"
author = ["James Hawley"]
date = 2023-11-05T23:06:00-05:00
lastmod = 2023-11-05T23:09:46-05:00
tags = ["curries", "spices", "food"]
draft = false
+++

## Varieties {#varieties}

-   [green curry]({{< relref "green_curry.md" >}})


## Derivatives {#derivatives}

-   [curry powder]({{< relref "curry_powder.md" >}})
