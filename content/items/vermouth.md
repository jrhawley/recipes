+++
title = "Vermouth"
author = ["James Hawley"]
date = 2023-08-13T11:22:00-04:00
lastmod = 2023-11-20T09:13:57-05:00
tags = ["alcohols", "beverages", "vermouths", "food"]
draft = false
+++

## Varieties {#varieties}

-   [dry vermouth]({{< relref "dry_vermouth.md" >}})
-   [sweet vermouth]({{< relref "sweet_vermouth.md" >}})
