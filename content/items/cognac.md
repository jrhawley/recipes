+++
title = "Cognac"
author = ["James Hawley"]
date = 2023-11-23T20:36:00-05:00
lastmod = 2023-12-05T20:52:12-05:00
tags = ["alcohols", "beverages", "cognacs", "food"]
draft = false
+++

## Brands {#brands}

-   [Hennessy]({{< relref "hennessy.md" >}})
