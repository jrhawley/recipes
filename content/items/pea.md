+++
title = "Pea"
author = ["James Hawley"]
date = 2023-11-13T18:43:00-05:00
lastmod = 2023-11-13T18:48:18-05:00
tags = ["seeds", "food"]
draft = false
+++

## Recipes {#recipes}

-   [Sweet potato fries]({{< relref "../recipes/sweet_potato_fries.md" >}})
