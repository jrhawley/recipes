+++
title = "Bun"
author = ["James Hawley"]
date = 2023-11-05T22:38:00-05:00
lastmod = 2023-11-05T22:41:04-05:00
tags = ["breads", "food"]
draft = false
+++

## Varieties {#varieties}

-   [hot cross bun]({{< relref "hot_cross_bun.md" >}})
