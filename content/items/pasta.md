+++
title = "Pasta"
author = ["James Hawley"]
date = 2023-11-20T07:38:00-05:00
lastmod = 2023-11-20T09:09:04-05:00
tags = ["pastas", "food"]
draft = false
+++

## Varieties {#varieties}

-   [brown rice pasta]({{< relref "brown_rice_pasta.md" >}})
-   [canneloni]({{< relref "canneloni.md" >}})
-   [farfalle]({{< relref "farfalle.md" >}})
-   [gnocchi]({{< relref "gnocchi.md" >}})
-   [lasagna]({{< relref "lasagna.md" >}})
-   [linguine]({{< relref "linguine.md" >}})
-   [macaroni]({{< relref "macaroni.md" >}})
-   [red lentil pasta]({{< relref "red_lentil_pasta.md" >}})
-   [rigatoni]({{< relref "rigatoni.md" >}})
-   [spaghetti]({{< relref "spaghetti.md" >}})
-   [tortellini]({{< relref "tortellini.md" >}})
