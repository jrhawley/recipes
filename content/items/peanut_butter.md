+++
title = "Peanut butter"
author = ["James Hawley"]
date = 2023-08-13T09:35:00-04:00
publishDate = 2023-08-13
lastmod = 2023-08-13T09:58:18-04:00
tags = ["spreads", "food"]
draft = false
+++

## Varieties {#varieties}

-   [processed peanut butter]({{< relref "processed_peanut_butter.md" >}})
-   [natural peanut butter]({{< relref "natural_peanut_butter.md" >}})
