+++
title = "Lemon"
author = ["James Hawley"]
date = 2023-09-10T09:19:00-04:00
lastmod = 2023-11-05T14:02:53-05:00
tags = ["fruits", "citrus", "food"]
draft = false
+++

## Derivatives {#derivatives}

-   [lemonade]({{< relref "lemonade.md" >}})
-   [lemon juice]({{< relref "lemon_juice.md" >}})
-   [lemon zest]({{< relref "lemon_zest.md" >}})
