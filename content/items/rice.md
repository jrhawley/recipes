+++
title = "Rice"
author = ["James Hawley"]
date = 2023-11-19T18:26:00-05:00
lastmod = 2023-11-19T22:33:18-05:00
tags = ["grains", "rices", "food"]
draft = false
+++

## Varieties {#varieties}

-   [arborio rice]({{< relref "arborio_rice.md" >}})
-   [brown rice]({{< relref "brown_rice.md" >}})
-   [jasmine rice]({{< relref "jasmine_rice.md" >}})


## Derivatives {#derivatives}

-   [rice noodles]({{< relref "rice_noodle.md" >}})
