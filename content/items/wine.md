+++
title = "Wine"
author = ["James Hawley"]
date = 2023-11-12T20:46:00-05:00
lastmod = 2023-11-12T20:53:31-05:00
tags = ["alcohols", "beverages", "food"]
draft = false
+++

## Varieties {#varieties}

-   [red wine]({{< relref "red_wine.md" >}})
-   [rose wine]({{< relref "rose_wine.md" >}})
-   [white wine]({{< relref "white_wine.md" >}})
