+++
title = "Pumpkin"
author = ["James Hawley"]
date = 2023-11-20T08:12:00-05:00
lastmod = 2023-11-20T09:11:56-05:00
tags = ["fruits", "gourds", "berries", "food"]
draft = false
+++

## Derivatives {#derivatives}

-   [pumpkin puree]({{< relref "pumpkin_puree.md" >}})
-   [pumpkin seeds]({{< relref "pumpkin_seed.md" >}})
