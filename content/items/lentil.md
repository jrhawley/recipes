+++
title = "Lentil"
author = ["James Hawley"]
date = 2023-11-12T15:20:00-05:00
lastmod = 2023-11-12T15:23:20-05:00
tags = ["legumes", "food"]
draft = false
+++

## Varieties {#varieties}

-   [green lentil]({{< relref "green_lentil.md" >}})
-   [puy lentil]({{< relref "puy_lentil.md" >}})
-   [red lentil]({{< relref "red_lentil.md" >}})
