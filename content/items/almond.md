+++
title = "Almond"
author = ["James Hawley"]
date = 2023-11-05T17:59:00-05:00
lastmod = 2023-11-05T18:01:09-05:00
tags = ["nuts", "seeds", "food"]
draft = false
+++

## Derivatives {#derivatives}

-   [almond butter]({{< relref "almond_butter.md" >}})
-   [almond flour]({{< relref "almond_flour.md" >}})
-   [almond milk]({{< relref "almond_milk.md" >}})
