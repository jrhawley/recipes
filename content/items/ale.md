+++
title = "Ale"
author = ["James Hawley"]
date = 2023-08-12T23:45:00-04:00
publishDate = 2023-08-12
lastmod = 2023-08-13T00:56:41-04:00
tags = ["beverages", "food"]
draft = false
+++

## Varieties {#varieties}


### Alcoholic ales {#alcoholic-ales}


### Non-alcoholic ales {#non-alcoholic-ales}
