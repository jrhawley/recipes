+++
title = "Potato salad"
author = ["James Hawley"]
date = 2023-10-24T00:00:00-04:00
lastmod = 2023-11-06T08:29:57-05:00
tags = ["vegetarian", "salads", "food"]
draft = false
+++

## Recipes {#recipes}

-   [Creamy avocado potato salad]({{< relref "../recipes/creamy_avocado_potato_salad.md" >}})
