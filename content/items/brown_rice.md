+++
title = "Brown rice"
author = ["James Hawley"]
date = 2023-10-24T00:00:00-04:00
lastmod = 2023-11-19T22:54:57-05:00
tags = ["grains", "rices", "food"]
draft = false
+++

## Derivatives {#derivatives}

-   [brown rice pasta]({{< relref "brown_rice_pasta.md" >}})
