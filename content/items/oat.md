+++
title = "Oat"
author = ["James Hawley"]
date = 2023-11-05T15:23:00-05:00
lastmod = 2023-11-05T15:24:41-05:00
tags = ["grains", "food"]
draft = false
+++

## Derivatives {#derivatives}

-   [oat milk]({{< relref "oat_milk.md" >}})
