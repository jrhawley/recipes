+++
title = "Lasagna"
author = ["James Hawley"]
date = 2023-11-20T08:07:00-05:00
lastmod = 2023-11-20T09:03:20-05:00
tags = ["pastas", "noodles", "food"]
draft = false
+++

## Recipes {#recipes}

-   [Lasagna]({{< relref "../recipes/lasagna.md" >}})
