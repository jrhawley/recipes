+++
title = "Salt"
author = ["James Hawley"]
date = 2023-08-12T17:36:00-04:00
lastmod = 2024-01-31T22:58:43-05:00
tags = ["salts", "food"]
draft = false
+++

## Varieties {#varieties}

-   [kitchen salt]({{< relref "kitchen_salt.md" >}})
-   [sea salt]({{< relref "sea_salt.md" >}})
-   [table salt]({{< relref "table_salt.md" >}})
