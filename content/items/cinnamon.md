+++
title = "Cinnamon"
author = ["James Hawley"]
date = 2023-09-10T09:22:00-04:00
lastmod = 2023-11-05T15:25:37-05:00
tags = ["spices", "food"]
draft = false
+++

## Derivatives {#derivatives}

-   ground cinnamon
-   [cinnamon sticks]({{< relref "cinnamon_stick.md" >}})
