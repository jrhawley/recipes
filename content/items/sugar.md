+++
title = "Sugar"
author = ["James Hawley"]
date = 2023-08-21T20:36:00-04:00
publishDate = 2023-08-21
lastmod = 2023-08-22T22:31:39-04:00
tags = ["sugars", "sweeteners", "food"]
draft = false
+++

## Varieties {#varieties}

-   [brown sugar]({{< relref "brown_sugar.md" >}})
-   [white sugar]({{< relref "white_sugar.md" >}})
-   [muscovado sugar]({{< relref "muscovado_sugar.md" >}})
