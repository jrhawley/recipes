+++
title = "Burger"
author = ["James Hawley"]
date = 2023-11-13T18:06:00-05:00
lastmod = 2023-11-13T18:11:27-05:00
tags = ["food"]
draft = false
+++

## Varieties {#varieties}

-   [beef burger]({{< relref "beef_burger.md" >}})
-   [turkey burger]({{< relref "turkey_burger.md" >}})
