+++
title = "Steak"
author = ["James Hawley"]
date = 2023-11-13T17:49:00-05:00
lastmod = 2023-11-13T17:53:11-05:00
tags = ["beef", "food"]
draft = false
+++

## Varieties {#varieties}

-   [sirloin steak]({{< relref "sirloin_steak.md" >}})


## Recipes {#recipes}

-   [Steak]({{< relref "../recipes/steak.md" >}})
