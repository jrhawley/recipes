+++
title = "Onion"
author = ["James Hawley"]
date = 2023-08-12T23:35:00-04:00
lastmod = 2024-02-18T22:14:11-05:00
tags = ["produce", "vegetables", "onions", "food"]
draft = false
+++

## Varieties {#varieties}

-   [pearl onion]({{< relref "pearl_onion.md" >}})
-   [red onion]({{< relref "red_onion.md" >}})
-   [sweet onion]({{< relref "sweet_onion.md" >}})
-   [yellow onion]({{< relref "yellow_onion.md" >}})
-   [white onion]({{< relref "white_onion.md" >}})


## Derivatives {#derivatives}

-   [pickled onion]({{< relref "pickled_onion.md" >}})
-   [onion powder]({{< relref "onion_powder.md" >}})
