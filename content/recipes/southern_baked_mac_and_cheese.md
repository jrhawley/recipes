+++
title = "Southern baked macaroni and cheese"
author = ["James Hawley"]
date = 2022-08-09T00:00:00-04:00
lastmod = 2024-05-23T21:14:06-04:00
tags = ["vegetarian", "pastas", "dairy", "entrees"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 10 min     |
| Cook Time | 35 min     |
| Yields    | 6 servings |


## Ingredients {#ingredients}

| Amount  | Ingredient                                                                          |
|---------|-------------------------------------------------------------------------------------|
| 3 cup   | [macaroni]({{< relref "../items/macaroni.md" >}}), uncooked                         |
| 3/2 cup | [milk]({{< relref "../items/milk.md" >}})                                           |
| 1/2 cup | [whipping cream]({{< relref "../items/whipping_cream.md" >}})                       |
| 1 cup   | [monterey jack cheese]({{< relref "../items/monterey_jack_cheese.md" >}}), shredded |
| 1 cup   | [cheddar cheese]({{< relref "../items/cheddar.md" >}}), shredded                    |
| 2       | [eggs]({{< relref "../items/eggs.md" >}})                                           |
|         | [table salt]({{< relref "../items/table_salt.md" >}})                               |
|         | [pepper]({{< relref "../items/black_pepper.md" >}})                                 |
|         | [paprika]({{< relref "../items/paprika.md" >}})                                     |


## Directions {#directions}

1.  Preheat oven to 350 F and boik a large pot of salted water
2.  Cook macaroni until _al dente_&nbsp;[^fn:1]
    -   Drain and set aside
3.  While the pasta is cooking, combine milk, heavy cream, and cheeses except for the smoked cheddar in a large bowl

    -   Season with salt, pepper, and paprika to taste
    -   Mix in the eggs and lightly beat

    <!--listend-->

    3.  Butter a 9" x 9" baking dish and add the cooked macaroni
        -   Pour the cheese mixture over the macaroni and carefully stir to combine
        -   Top with smoked cheddar, and any leftover shredded cheese
        -   Add additional salt, pepper, and paprika, to taste

5 . Bake for 35 min&nbsp;[^fn:1]

1.  Let sit for 10 min before serving


## References {#references}

1.  Original recipe: [Divas Can Cook](https://divascancook.com/wprm_print/recipe/18403)

[^fn:1]: Be careful not to overcook
