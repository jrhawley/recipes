+++
title = "Steak and onion pie"
author = ["James Hawley"]
date = 2024-02-17T22:22:00-05:00
lastmod = 2024-02-18T22:26:50-05:00
tags = ["pies", "entrees", "beef", "recipes"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 5 min      |
| Cook Time | 3h         |
| Yields    | 6 servings |


## Equipment {#equipment}

-   large cutting board
-   chef knife
-   Dutch oven
-   large skillet
-   wooden spoon
-   measuring cups
-   measuring spoons
-   pastry brush
-   small bowl


## Ingredients {#ingredients}

| Quantity  | Item                                                                          |
|-----------|-------------------------------------------------------------------------------|
| 900 g     | [stewing beef]({{< relref "../items/stewing_beef.md" >}})                     |
| 125 ml    | [all-purpose flour]({{< relref "../items/all-purpose_flour.md" >}})           |
| 75 mL     | [olive oil]({{< relref "../items/olive_oil.md" >}}), divided                  |
| 45 mL     | unsalted [butter]({{< relref "../items/butter.md" >}})                        |
| 375 mL    | [red wine]({{< relref "../items/red_wine.md" >}}), divided                    |
| 1         | [white onion]({{< relref "../items/white_onion.md" >}}), chopped              |
| 1         | [yellow onion]({{< relref "../items/yellow_onion.md" >}}), chopped            |
| 2         | [shallots]({{< relref "../items/shallot.md" >}}), halved lengthwise           |
| 2         | [bay leaves]({{< relref "../items/bay_leaf.md" >}})                           |
| 2 springs | [thyme]({{< relref "../items/thyme.md" >}})                                   |
| 1 sprig   | [rosemary]({{< relref "../items/rosemary.md" >}})                             |
| 500 mL    | [beef stock]({{< relref "../items/beef_stock.md" >}})                         |
|           | [kosher salt]({{< relref "../items/kitchen_salt.md" >}})                      |
|           | [black pepper]({{< relref "../items/black_pepper.md" >}})                     |
| 750 mL    | [pearl onions]({{< relref "../items/pearl_onion.md" >}}), peeled and blanched |
| 1         | [pastry sheet]({{< relref "../items/pastry_sheet.md" >}}), thawed overnight   |
| 1         | [egg]({{< relref "../items/eggs.md" >}}), lightly beaten                      |


## Directions {#directions}

1.  Cut the stewing beef into 5 cm cubes and dust with flour.
    1.  In the Dutch oven over medium heat, heat 30 mL of olive oil and 30 mL of butter.
    2.  Brown beef cubes on all sides, working in batches if needed.
    3.  Transfer browned beef to a bowl and set aside.
2.  Add 175 mL of red wine and scrape the browned bits with a wooden spoon, then pour the deglazing liquid into the bowl with the beef.
3.  Heat the remaining olive oil, add onions and shallots, and saute until golden.
4.  Add bay leaves, thyme, rosemary, remaining red wine, beef stock, and reserved beef and its juices.
    1.  Stir to combine, and season to taste with salt and pepper.
    2.  Simmer, uncovered, over medium-low heat until beef is fork tender, ~ 2 h.
5.  While the beef is simmering, melt 15 ml of olive oil 15 mL of butter in the skillet over medium heat.
    1.  Add pearl onions and cook until golden, stirring often.
    2.  Remove from heat.
6.  Preheat oven to 190 C (375 F).
7.  When beef has finished, discard the bay leaves, thyme, and rosemary sprigs, then stir in pearl onions.
    1.  Spoon mixter into a greased baking dish and let cool for 15 min before laying chilled pastry sheet over top.
    2.  Trim edges of pastry sheet, and cut slits to allow venting.
    3.  Brush pastry surface with beaten egg.
8.  Bake pie until heated through and pastry is golden, ~ 25 - 30 min.
9.  Let rest for 5 min before serving.


## References {#references}

1.  Original recipe: [Farm to Chef: Cooking Through the Seasons - Lynn Crawford (2017)]({{< relref "../references/farm_to_chef_-_lynn_crawford_(2017).md" >}})
