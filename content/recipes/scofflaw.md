+++
title = "Scofflaw"
author = ["James Hawley"]
date = 2021-02-17T00:00:00-05:00
lastmod = 2023-12-22T01:01:27-05:00
tags = ["recipes", "alcohols", "beverages"]
draft = false
+++

## Ingredients {#ingredients}

| Quantity | Item                                                      |
|----------|-----------------------------------------------------------|
| 3/2 oz   | [rye whiskey]({{< relref "../items/whiskey.md" >}})       |
| 1 oz     | [dry vermouth]({{< relref "../items/dry_vermouth.md" >}}) |
| 2/3 oz   | [lemon juice]({{< relref "../items/lemon_juice.md" >}})   |
| 2/3 oz   | [grenadine]({{< relref "../items/grenadine.md" >}})       |


## Directions {#directions}

1.  Add ingredients to cocktail shaker and shake well
2.  Add ice and shake well
3.  Double strain into a chilled cocktail glass
4.  Garnish with a twist of lemon peel


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [The Periodic Table of Cocktails]({{< relref "../references/Periodic_Table_of_Cocktails.md" >}})
