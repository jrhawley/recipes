+++
title = "Cinnamon maple whiskey sour"
author = ["James Hawley"]
date = 2020-10-09T00:00:00-04:00
lastmod = 2023-12-19T22:55:23-05:00
tags = ["recipes", "alcohols", "beverages"]
draft = false
+++

{{< figure src="../assets/cinnamon-maple-whiskey-sour.jpg" caption="<span class=\"figure-number\">Figure 1: </span>Cinnamon maple whiskey sour" >}}


## Ingredients {#ingredients}

| Quantity | Item                                                            |
|----------|-----------------------------------------------------------------|
| 1.5 oz   | [Bulleit bourbon]({{< relref "../items/bulleit_bourbon.md" >}}) |
| 1 oz     | fresh [lemon juice]({{< relref "../items/lemon_juice.md" >}})   |
| 1 Tbsp   | [maple syrup]({{< relref "../items/maple_syrup.md" >}})         |
|          | ice cubes                                                       |


## Directions {#directions}

1.  Fill a cocktail shaker with ice cubes
2.  Add all ingredients and shake until frothy
3.  Fill cocktail glass with ice, pour drink over top and serve


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [Cookie and Kate](https://cookieandkate.com/cinnamon-maple-whiskey-sour-recipe/print/24019/)
