+++
title = "London fog"
author = ["James Hawley"]
date = 2021-08-13T00:00:00-04:00
lastmod = 2023-12-22T00:47:24-05:00
tags = ["recipes", "beverages"]
draft = false
+++

## Ingredients {#ingredients}

| Quantity | Item                                                            |
|----------|-----------------------------------------------------------------|
| 1/2 cup  | [water]({{< relref "../items/water.md" >}})                     |
| 1/2 cup  | [milk]({{< relref "../items/milk.md" >}})                       |
| 1/4 tsp  | [vanilla extract]({{< relref "../items/vanilla_extract.md" >}}) |
| 1 tsp    | [white sugar]({{< relref "../items/white_sugar.md" >}})         |
| 1 sachet | [Earl Grey tea]({{< relref "../items/earl_grey_tea.md" >}})     |


## Directions {#directions}

1.  Boil the water and steep the Earl Grey tea for 5 min
2.  While steeping, foam the milk, then add sugar and vanilla extract to the tea
