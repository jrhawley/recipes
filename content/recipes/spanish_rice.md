+++
title = "Spanish rice"
author = ["James Hawley"]
date = 2013-09-14T00:00:00-04:00
lastmod = 2024-05-23T21:32:55-04:00
tags = ["vegetarian", "recipes", "sides", "entrees"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 5 min      |
| Cook Time | 20 min     |
| Yields    | 5 servings |


## Ingredients {#ingredients}

| Quantity | Item                                                        |
|----------|-------------------------------------------------------------|
| 2 Tbsp   | [olive oil]({{< relref "../items/olive_oil.md" >}})         |
| 2 Tbsp   | [onion]({{< relref "../items/onion.md" >}}), chopped        |
| 3/2 cups | [rice]({{< relref "../items/basmati_rice.md" >}}), uncooked |
| 2 cups   | [chicken broth]({{< relref "../items/chicken_broth.md" >}}) |
| 1 cup    | [salsa]({{< relref "../items/salmon.md" >}}), chunky        |


## Directions {#directions}

1.  Heat oil in large skillet over medium heat
2.  Stir in onion and cook till tender
3.  Mix rice into skillet and brown
4.  Add broth and salsa, and mix well
5.  Reduce heat, cover, and simmer until liquid is absorbed (~ 20min)
