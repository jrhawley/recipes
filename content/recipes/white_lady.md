+++
title = "White Lady"
author = ["James Hawley"]
date = 2021-02-17T00:00:00-05:00
lastmod = 2023-12-24T20:04:52-05:00
tags = ["recipes", "alcohols", "beverages"]
draft = false
+++

## Ingredients {#ingredients}

| Quantity | Item                                                      |
|----------|-----------------------------------------------------------|
| 5/3 oz   | [gin]({{< relref "../items/gin.md" >}})                   |
| 2/3 oz   | [lemon juice]({{< relref "../items/lemon_juice.md" >}})   |
| 2/3 oz   | [Triple Sec]({{< relref "../items/triple_sec.md" >}})     |
| 1        | [egg white]({{< relref "../items/eggs.md" >}}) (optional) |


## Directions {#directions}

1.  Add all ingredients to a cocktail shaker and shake well
2.  Add a few ice cubes, then shake again
3.  Strain into a chilled cocktail glass
4.  Garnish with a strip of lemon peel


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [The Periodic Table of Cocktails]({{< relref "../references/Periodic_Table_of_Cocktails.md" >}})
