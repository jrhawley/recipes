+++
title = "Tom Collins"
author = ["James Hawley"]
date = 2022-03-13T00:00:00-05:00
lastmod = 2023-12-24T20:01:55-05:00
tags = ["recipes", "alcohols", "beverages"]
draft = false
+++

## Ingredients {#ingredients}

| Quantity | Ingredient                                              |
|----------|---------------------------------------------------------|
| 2 oz     | [gin]({{< relref "../items/gin.md" >}})                 |
| 1 oz     | [lemon juice]({{< relref "../items/lemon_juice.md" >}}) |
| 2/3 oz   | [simple syrup](simple-syrup.md)                         |
|          | [soda water]({{< relref "../items/club_soda.md" >}})    |


## Directions {#directions}

1.  Fill a highball glass with ice.
2.  Add the gin, lemon juice, and simple syrup.
3.  Add more ice if necessary, and top with soda.
4.  Garnish with a lemon wedge or maraschino cherry.


## References {#references}

1.  Original recipe: [The Periodic Table of Cocktails]({{< relref "../references/Periodic_Table_of_Cocktails.md" >}})
