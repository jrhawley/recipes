+++
title = "Banana bread"
author = ["James Hawley"]
date = 2013-09-21T00:00:00-04:00
lastmod = 2024-06-12T19:12:03-04:00
tags = ["breads", "breakfasts", "recipes"]
draft = false
+++

| Info      | Amount |
|-----------|--------|
| Prep Time | 10 min |
| Cook Time | 1 h    |
| Yields    | 1 loaf |


## Ingredients {#ingredients}

| Quantity | Item                                                            |
|----------|-----------------------------------------------------------------|
| 1/4 cup  | [butter]({{< relref "../items/butter.md" >}})                   |
| 1/4 cup  | [white sugar]({{< relref "../items/white_sugar.md" >}})         |
| 1        | [egg]({{< relref "../items/eggs.md" >}}), beaten                |
| 2        | [bananas]({{< relref "../items/banana.md" >}}), crushed         |
| 3/2 cup  | [flour]({{< relref "../items/flour.md" >}})                     |
| 1 tsp    | [baking soda]({{< relref "../items/baking_soda.md" >}})         |
| 1/2 tsp  | [table salt]({{< relref "../items/table_salt.md" >}})           |
| 1/2 tsp  | [vanilla extract]({{< relref "../items/vanilla_extract.md" >}}) |


## Directions {#directions}

1.  Cream together butter and sugar
    -   Add eggs and crushed bananas
2.  Sift together flour, soda and salt
    -   Add to creamed mixture
    -   Add vanilla
3.  Pour into greased and floured loaf pan
4.  Bake at 350 F for 1h
