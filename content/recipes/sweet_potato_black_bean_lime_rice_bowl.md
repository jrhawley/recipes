+++
title = "Sweet potato, black bean, and lime rice bowl"
author = ["James Hawley"]
date = 2022-03-27T00:00:00-04:00
lastmod = 2024-05-23T21:14:56-04:00
tags = ["bowls", "recipes", "entrees", "vegetarian"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time |            |
| Cook Time |            |
| Yields    | 4 servings |


## Ingredients {#ingredients}

| Quantity | Item                                                            |
|----------|-----------------------------------------------------------------|
| 2        | [sweet potatoes]({{< relref "../items/yam.md" >}}), cubed       |
| 2 Tbsp   | [olive oil]({{< relref "../items/olive_oil.md" >}})             |
| 15 oz    | [black beans]({{< relref "../items/black_bean.md" >}})          |
| 2 tsp    | [cumin]({{< relref "../items/cumin.md" >}})                     |
| 1 cup    | [long grain rice]({{< relref "../items/basmati_rice.md" >}})    |
| 2 cloves | [garlic]({{< relref "../items/garlic.md" >}}), minced           |
| 3/2 cup  | [vegetable broth]({{< relref "../items/vegetable_broth.md" >}}) |
| 2 Tbsp   | [lime juice]({{< relref "../items/lime_juice.md" >}})           |
| 1 tsp    | [kosher salt]({{< relref "../items/kitchen_salt.md" >}})        |
| 2 tsp    | [lime zest]({{< relref "../items/lime.md" >}})                  |


## Directions {#directions}

1.  Preheat oven to 400 F.
    1.  Drizzle cubed potatoes with olive oil and toss to coat.
    2.  Place potatoes onto a baking sheet, bake for 15 - 20 min, until soft.
2.  In a medium saucepan over medium-high heat, add 1 Tbsp olive oil.
    1.  Add rice and garlic, cooking for 1 - 2 min.
    2.  Add broth, lime juice, and salt.
    3.  Reduce heat to low, cover, and simmer for 20 min.
    4.  Stir in lime zest and set aside.
3.  In a large, deep pot over medium heat, add 1 Tbsp oil.
    1.  Add roasted potatoes, black beans, and cumin.
    2.  Mix together evenly and cook for 3 - 5 min.
    3.  Add rice and remaining ingredients, mixing well.


## References {#references}

1.  Original recipe: [This Wife Cooks](https://thiswifecooks.com/wprm_print/9504)
