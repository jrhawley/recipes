+++
title = "Honey garlic chicken and veggies"
author = ["James Hawley"]
date = 2019-12-07T00:00:00-05:00
lastmod = 2024-05-23T21:38:45-04:00
tags = ["chicken", "recipes", "slow-cooker", "entrees"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 10 min     |
| Cook Time | 4 h        |
| Yields    | 4 servings |

{{< figure src="../assets/honey-garlic-chicken-veggies.jpg" caption="<span class=\"figure-number\">Figure 1: </span>Honey garlic chicken and veggies" >}}


## Ingredients {#ingredients}


### Main {#main}

| Quantity | Item                                                                    |
|----------|-------------------------------------------------------------------------|
| 4        | [chicken breasts]({{< relref "../items/chicken_breast.md" >}})          |
| 16 oz    | baby [red potatoes]({{< relref "../items/russet_potato.md" >}}), halved |
| 16 oz    | baby [carrots]({{< relref "../items/baby_carrot.md" >}})                |
| 16 oz    | [green beans]({{< relref "../items/green_bean.md" >}}), trimmed         |
| 2 Tbsp   | [parsley]({{< relref "../items/parsley.md" >}}), chopped                |


### Sauce {#sauce}

| Quantity | Item                                                               |
|----------|--------------------------------------------------------------------|
| 1/2 cup  | [soy sauce]({{< relref "../items/soy_sauce.md" >}})                |
| 1/2 cup  | [honey]({{< relref "../items/honey.md" >}})                        |
| 1/4 cup  | [ketchup]({{< relref "../items/ketchup.md" >}})                    |
| 2 cloves | [garlic]({{< relref "../items/garlic.md" >}}), minced              |
| 1 tsp    | [basil]({{< relref "../items/basil.md" >}})                        |
| 1/2 tsp  | [oregano]({{< relref "../items/oregano.md" >}})                    |
| 1/4 tsp  | [red pepper flakes]({{< relref "../items/red_pepper_flake.md" >}}) |
| 1/4 tsp  | [pepper]({{< relref "../items/black_pepper.md" >}})                |


## Directions {#directions}

1.  In a large bowl, combine soy sauce, honey, ketchup, garlic, basil, oregano, red pepper flakes and pepper.
2.  Place chicken thighs, potatoes, carrots and soy sauce mixture into slow cooker
    1.  Cover and cook on high heat for 4h or low for 8 h
    2.  Baste every hour
    3.  Add green beans during the last 30 minutes of cooking time
3.  OPTIONAL: Preheat oven to broil. Place chicken thighs onto a baking sheet, skin side up, and broil until crisp, about 3-4 min.
4.  Serve chicken immediately with potatoes, carrots and green beans, garnished with parsley, if desired.


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [Damn Delicious](https://damndelicious.net/2015/06/05/slow-cooker-honey-garlic-chicken-and-veggies/)
