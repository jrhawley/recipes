+++
title = "Broccoli, ham, macaroni, and cheese"
author = ["James Hawley"]
date = 2020-12-19T00:00:00-05:00
lastmod = 2024-05-23T21:23:23-04:00
tags = ["pastas", "recipes", "entrees"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time |            |
| Cook Time | 30 min     |
| Yields    | 4 servings |


## Ingredients {#ingredients}

| Quantity | Item                                                        |
|----------|-------------------------------------------------------------|
| 2 Tbsp   | [butter]({{< relref "../items/butter.md" >}}), divided      |
| 1        | small [onion]({{< relref "../items/onion.md" >}}), minced   |
| 1/4 cup  | [flour]({{< relref "../items/flour.md" >}})                 |
| 4/3 cup  | [dairy milk]({{< relref "../items/dairy_milk.md" >}})       |
| 1 cup    | [cheddar]({{< relref "../items/cheddar.md" >}}), shredded   |
| 1/3 cup  | [parmesan]({{< relref "../items/parmesan.md" >}})           |
| 1 tsp    | [dijon mustard]({{< relref "../items/dijon_mustard.md" >}}) |
| 1/2 tsp  | [table salt]({{< relref "../items/table_salt.md" >}})       |
| 1/2 tsp  | [pepper]({{< relref "../items/black_pepper.md" >}})         |
| 2 cup    | [macaroni]({{< relref "../items/macaroni.md" >}})           |
| 3 cup    | [broccoli florets]({{< relref "../items/broccoli.md" >}})   |
| 1 cup    | [ham]({{< relref "../items/ham.md" >}}), diced              |
| 1 cup    | [breadcrumbs]({{< relref "../items/breadcrumbs.md" >}})     |


## Directions {#directions}

1.  In a large saucepan, melt butter over medium heat.
    1.  Cook onion until translucent
2.  Add flour to milk and evaporated milk in a small bowl, and mix together
    1.  Mix into saucepan with the onion and stir until thickened
    2.  Stir in cheddar, 1/4 cup of parmesan, mustard, salt, and pepper
3.  Preheat oven to 375 F
4.  Meanwhile, in a large pot of boiling water, cook macaroni according to instructions
    1.  Add broccoli and cook for 1 min
    2.  Drain and add to saucepan with ham
    3.  Stir the entire mixture together to coat
5.  Scrape into greased 8" square baking dish
6.  In a small bowl, combine bread crumbs, remaining parmesan, and melted butter
    1.  Sprinkle over macaroni
    2.  Bake in oven for 20 min until bubbly, and a golden brown


## References &amp; Notes {#references-and-notes}

1.  Original recipe: Nana
