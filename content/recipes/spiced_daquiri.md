+++
title = "Spiced daquiri"
author = ["James Hawley"]
date = 2022-09-15T22:08:00-04:00
lastmod = 2023-12-22T01:12:56-05:00
tags = ["recipes", "beverages", "alcohols"]
draft = false
+++

| Info      | Amount             |
|-----------|--------------------|
| Prep Time | 5 min              |
| Cook Time | 0 min              |
| Yields    | 1 serving servings |


## Ingredients {#ingredients}

| Quantity | Item                                                         |
|----------|--------------------------------------------------------------|
| 1        | [lime]({{< relref "../items/lime.md" >}}) wedge              |
| 3        | whole [cloves]({{< relref "../items/clove.md" >}})           |
| 2 oz     | [aged rum]({{< relref "../items/aged_rum.md" >}})            |
| 1 oz     | [lime juice]({{< relref "../items/lime_juice.md" >}})        |
| 3/4 oz   | [spiced honey syrup]({{< relref "spiced_honey_syrup.md" >}}) |


## Directions {#directions}

1.  Poke 3 evenly-spaced holes in the lime wedge skin with a skewer.
    -   Stud each hole with a clove.
2.  Place rum, lime juice, and spiced honey syrup in a cocktail shaker.
    -   Fill 3/4 full with ice.
    -   Shake until ice-cold and strain into a chilled coupe glass.
3.  Garnish with lime wedge.


## References &amp; Notes {#references-and-notes}

1.  Original recipe: LCBO Food and Drink - Summer 2022
