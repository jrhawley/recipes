+++
title = "Pumpkin Waffles"
author = ["James Hawley"]
date = 2013-09-12T00:00:00-04:00
lastmod = 2024-06-12T19:15:48-04:00
tags = ["recipes", "breakfasts"]
draft = false
+++

| Info      | Amount    |
|-----------|-----------|
| Prep Time | 20 min    |
| Cook Time | 1 h       |
| Yield     | 2 waffles |


## Ingredients {#ingredients}

| Quantity  | Item                                                                |
|-----------|---------------------------------------------------------------------|
| 3 cup     | [all-purpose flour]({{< relref "../items/all-purpose_flour.md" >}}) |
| 1 tsp     | [ground cloves]({{< relref "../items/garlic.md" >}})                |
| 2 tsp     | [ground cinnamon]({{< relref "../items/cinnamon.md" >}})            |
| 1 tsp     | [ground nutmeg]({{< relref "../items/nutmeg.md" >}})                |
| 1/2 tsp   | [table salt]({{< relref "../items/table_salt.md" >}})               |
| 1 tsp     | [baking soda]({{< relref "../items/baking_soda.md" >}})             |
| 1/2 tsp   | [baking powder]({{< relref "../items/baking_powder.md" >}})         |
| 3 cup     | [white sugar]({{< relref "../items/white_sugar.md" >}})             |
| 1 cup     | soft [butter]({{< relref "../items/butter.md" >}})                  |
| 3         | large [eggs]({{< relref "../items/eggs.md" >}})                     |
| 16 oz can | [pumpkin puree]({{< relref "../items/pumpkin_puree.md" >}})         |
| 1/2 cup   | chopped [pecans]({{< relref "../items/pecan.md" >}}) (optional)     |


## Directions {#directions}

1.  Preheat oven to 350 F and spray two medium loaf pans with non-stick cooking spray
2.  In a medium bowl, mix the flour, spices, salt, baking soda, and baking powder and set aside
3.  In another bowl add the butter and put it in the microwave for about 30 seconds, so that it is half melted
    1.  Add the sugar, eggs
    2.  Cream these three ingredients together until fluffy
    3.  Add the pumpkin and combine well
4.  In three batches, add the dry ingredients to the butter, sugar mixture and mix gently until each batch is just incorporated. Scrape the sides between each batch
5.  Fold in the nuts
6.  Split the batter between the prepared pans.
7.  Bake side-by-side for about one hour or until an inserted knife comes out clean the top is golden
