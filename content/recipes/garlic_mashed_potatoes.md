+++
title = "Garlic mashed potatoes"
author = ["James Hawley"]
date = 2021-01-16T00:00:00-05:00
lastmod = 2024-05-23T21:22:01-04:00
tags = ["recipes", "vegetarian", "sides"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 0 min      |
| Cook Time | 1 h        |
| Yields    | 2 servings |


## Ingredients {#ingredients}

| Quantity | Item                                                                |
|----------|---------------------------------------------------------------------|
| 1        | [Yukon gold potato]({{< relref "../items/yukon_gold_potato.md" >}}) |
| 1 Tbsp   | [butter]({{< relref "../items/butter.md" >}})                       |
| 1 clove  | [garlic]({{< relref "../items/garlic.md" >}})                       |
|          | [milk]({{< relref "../items/milk.md" >}})                           |
|          | [olive oil]({{< relref "../items/olive_oil.md" >}})                 |
|          | [table salt]({{< relref "../items/table_salt.md" >}})               |
|          | [black pepper]({{< relref "../items/black_pepper.md" >}})           |


## Directions {#directions}

1.  Get a large pot of water to boil and preheat the oven to 180 C (350 F).
    1.  Boil potato(s) for 45 min, or until soft throughout
2.  While the potatoes are boiling, wrap the garlic in aluminium foil, drizzle in olive oil, and sprinkle with salt.
    1.  Wrap everything together and put on a baking sheet.
    2.  Roast in the oven for 20 min, or until garlic can be crushed with a spoon.&nbsp;[^fn:1]
3.  Drain water, add butter to the pot and garlic.
    1.  Mash with a potato masher, slowly adding milk until smooth and creamy.&nbsp;[^fn:2]
4.  Serve with salt and pepper

[^fn:1]: This works well if you're baking something in the oven to go with the potatoes.
[^fn:2]: Use an egg beater to get an extra creamy texture, if desired.
