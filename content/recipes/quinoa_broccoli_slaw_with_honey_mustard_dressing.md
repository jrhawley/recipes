+++
title = "Quinoa broccoli slaw with honey mustard dressing"
author = ["James Hawley"]
date = 2023-03-12T19:36:00-04:00
lastmod = 2024-05-23T20:47:41-04:00
tags = ["entrees", "vegetarian", "recipes"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 20 min     |
| Cook Time | 20 min     |
| Yields    | 4 servings |


## Ingredients {#ingredients}

| Quantity | Item                                                                          |
|----------|-------------------------------------------------------------------------------|
| 3/4 cup  | [quinoa]({{< relref "../items/quinoa.md" >}}), uncooked                       |
| 3/2 cup  | [water]({{< relref "../items/water.md" >}})                                   |
| 1/2 cup  | [almonds]({{< relref "../items/almond.md" >}}), slivered                      |
| 4 heads  | [broccoli]({{< relref "../items/broccoli.md" >}})                             |
| 1/3 cup  | [basil]({{< relref "../items/basil.md" >}}), chopped                          |
| 1/2 cup  | [olive oil]({{< relref "../items/olive_oil.md" >}})                           |
| 2 Tbsp   | [lemon juice]({{< relref "../items/lemon_juice.md" >}})                       |
| 2 Tbsp   | [Dijon mustard]({{< relref "../items/dijon_mustard.md" >}})                   |
| 1 Tbsp   | [apple cider vinegar]({{< relref "../items/apple_cider_vinegar.md" >}})       |
| 1 Tbsp   | [honey]({{< relref "../items/honey.md" >}})                                   |
| 2 cloves | [garlic]({{< relref "../items/garlic.md" >}}), minced                         |
| 1/2 tsp  | [sea salt]({{< relref "../items/sea_salt.md" >}})                             |
|          | [black pepper]({{< relref "../items/black_pepper.md" >}})                     |
|          | [red pepper flakes]({{< relref "../items/red_pepper_flake.md" >}}) (optional) |


## Directions {#directions}

1.  First, rinse the quinoa in a fine mesh colander under running water
    1.  In a medium-sized pot, combine the rinsed quinoa and 3/2 cups water
    2.  Bring the mixture to a gentle boil over medium heat, then reduce the heat to medium-low and gently simmer the quinoa until it has absorbed all of the water
    3.  Remove the quinoa from heat, cover the pot and let it rest for 5 min
    4.  Uncover the pot and fluff the quinoa with a fork
    5.  Set it aside to cool
2.  Meanwhile, toast the almonds in a small skillet over medium heat, until fragrant
    1.  Transfer to a large serving bowl to cool
3.  To prepare the broccoli, trim off any brown bits from the florets and stems, then slice the florets off the stems into manageable pieces
    1.  Use a paring knife to peel off the tough, woody perimeter of the broccoli stems and then discard
    2.  Feed the broccoli florets through your food processor using the slicing blade, then switch to the grating blade to shred the stems
4.  Combine all of the dressing ingredients in a liquid measuring cup and whisk until emulsified&nbsp;[^fn:1]
5.  Add the shredded broccoli slaw, cooked quinoa and chopped basil to your large serving bowl
    1.  Pour the dressing over the mixture and toss until well mixed
    2.  Let the slaw rest for about 20 min to let the flavors meld


## References {#references}

1.  Original recipe: [Cookie and Kate](https://cookieandkate.com/quinoa-broccoli-slaw-recipe/print/23831/)

[^fn:1]: The dressing should be pleasantly tangy and pack a punch.
    If it's overwhelmingly acidic, add a little more honey to balance out the flavors.
    If it needs more kick, add a bit more mustard or lemon juice.
