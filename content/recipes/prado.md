+++
title = "Prado"
author = ["James Hawley"]
date = 2023-07-30T13:19:00-04:00
lastmod = 2023-12-23T01:58:32-05:00
tags = ["alcohols", "recipes", "beverages"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 5 min      |
| Cook Time | 0 min      |
| Yields    | 1 cocktail |


## Ingredients {#ingredients}

| Quantity | Item                                                            |
|----------|-----------------------------------------------------------------|
| 1 oz     | [gin]({{< relref "../items/gin.md" >}})                         |
| 3/4 oz   | [Triple Sec]({{< relref "../items/triple_sec.md" >}})           |
| 2 oz     | [pineapple juice]({{< relref "../items/pineapple_juice.md" >}}) |
| 1/2 oz   | [lime juice]({{< relref "../items/lime_juice.md" >}})           |
| 1/4 oz   | [grenadine]({{< relref "../items/grenadine.md" >}})             |


## Directions {#directions}

1.  Combine the gin, Triple Sec, pineapple juice, and lime juice in a cocktail shaker with ice.
2.  Add large ice cubes to coupe glasses.
3.  Shake ingredients until chilled, then strain into cocktail glasses.
4.  Drizzle grenadine over top to finish with colour.


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [Elora Distilling Company]({{< relref "../companies/Elora_Distilling_Company.md" >}})
