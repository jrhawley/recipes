+++
title = "Egg bites"
author = ["James Hawley"]
date = 2024-07-24T18:13:00-04:00
lastmod = 2024-10-19T01:07:11-04:00
tags = ["recipes"]
draft = false
+++

| Info      | Amount  |
|-----------|---------|
| Prep Time | 10 min  |
| Cook Time | 30 min  |
| Yields    | 12 cups |


## Equipment {#equipment}

-   1 large mixing bowl
-   12 cup muffin tray
-   1 whisk
-   1 large cutting board
-   1 chef's knife


## Ingredients {#ingredients}

| Quantity | Item                                                                 |
|----------|----------------------------------------------------------------------|
| 12       | [tart pastry shells]({{< relref "../items/tart_pastry_shell.md" >}}) |
| 2        | [eggs]({{< relref "../items/eggs.md" >}})                            |
| 1/2 cup  | [milk]({{< relref "../items/milk.md" >}})                            |
| 1/4 cup  | [heavy cream]({{< relref "../items/heavy_cream.md" >}})              |
| 1/4 tsp  | [salt]({{< relref "../items/salt.md" >}})                            |
| 1/4 tsp  | [black pepper]({{< relref "../items/black_pepper.md" >}})            |
| dash     | [cayenne]({{< relref "../items/cayenne_pepper.md" >}})               |
| 1/2 cup  | [spinach]({{< relref "../items/spinach.md" >}}), finely chopped      |
| 1/4      | [white onion]({{< relref "../items/white_onion.md" >}}), diced       |
| 1/2 cup  | [cheddar]({{< relref "../items/cheddar.md" >}}), shredded            |


## Directions {#directions}

1.  If using frozen pastry shells, let thaw in the muffin tray while preparing other ingredients.
2.  Preheat the oven to 190 C (375 F).
3.  In a large bowl, whisk the eggs, milk, cream, salt, pepper, and cayenne.
    1.  Add in chopped spinach, onion, and shredded cheese.
    2.  Pour mixture into pastry shells, leaving a bit of room in each.
4.  Bake for 25 - 30 min, or until the centres are set and the edges begin to brown.
5.  Remove from the oven and let cool on the stovetop for 10 min before serving.


## References {#references}

1.  Original recipe: [If You Give a Blonde a Kitchen](https://www.ifyougiveablondeakitchen.com/wprm_print/muffin-tin-mini-quiche)
