+++
title = "Strawberry shortcake scones"
author = ["James Hawley"]
date = 2023-03-12T09:27:00-04:00
lastmod = 2024-06-12T19:15:57-04:00
tags = ["breakfasts", "snacks", "recipes"]
draft = false
+++

| Info      | Amount         |
|-----------|----------------|
| Prep Time | 45 min         |
| Cook Time | 20 min         |
| Yields    | 10 - 12 scones |


## Ingredients {#ingredients}

| Quantity | Item                                                                       |
|----------|----------------------------------------------------------------------------|
| 2 cup    | [all-purpose flour]({{< relref "../items/flour.md" >}})                    |
| 2 Tbsp   | [granular sugar]({{< relref "../items/white_sugar.md" >}})                 |
| 1 Tbsp   | [baking powder]({{< relref "../items/baking_powder.md" >}})                |
| 1 tsp    | [baking soda]({{< relref "../items/baking_soda.md" >}})                    |
| 3/2 tsp  | [Kosher salt]({{< relref "../items/kitchen_salt.md" >}})                   |
| 1/2 cup  | cold, unsalted [butter]({{< relref "../items/butter.md" >}}), cubed        |
| 3/4 cup  | [buttermilk]({{< relref "../items/buttermilk.md" >}})                      |
| 2 cup    | [strawberries]({{< relref "../items/strawberry.md" >}}), halved and hulled |
| 1 cup    | [heavy cream]({{< relref "../items/heavy_cream.md" >}}), whipped           |


## Directions {#directions}

1.  Sift the flour into a large mixing bowl
    1.  Mix in the sugar, baking powder, baking soda, and salt
    2.  Work the butter in by hand until the flour mixture forms a coarse meal
2.  Make a well in the centre of the flour
    1.  Using a fork, gently stir the buttermilk in just until the dough, which will be wet and sticky, comes together—no more, no less
    2.  Stop once everything is cohesive
    3.  Gently knead the dough, still in the bowl, with a light touch of your hands
    4.  Cover the dough with a clean tea towel and chill in the refrigerator for 30 min
3.  Preheat the oven to 190 C (375 F) and line a baking sheet with parchment paper
    1.  On a lightly floured work surface, roll out the dough to 2 cm (0.75 ") thick
    2.  Cut the dough with the rim of a tumbler or a sharp scone cutter (somewhere near the 6 cm / 2.5 " diameter mark) that has been lightly floured
    3.  Repeat until you have made as many dough rounds as possible
4.  Place the dough rounds on the prepared baking sheet

    1.  Bake until the scones are golden, about 20 min
    2.  Set aside to cool for 5 - 10 min before serving

    <!--listend-->

    3.  Best served immediately


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [Peak Season]({{< relref "../references/Peak_Season.md" >}})
2.  Any leftovers can be wrapped airtight as soon as they cool to room temperature and frozen for up to 2 months.
    Frozen scones can be reheated in a 175 C (350 F) oven for 5 - 10 min
