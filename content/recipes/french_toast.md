+++
title = "French toast"
author = ["James Hawley"]
date = 2013-09-14T00:00:00-04:00
lastmod = 2024-06-12T19:14:17-04:00
tags = ["food", "recipes", "breakfasts"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 10 min     |
| Cook Time | 5-10 min   |
| Yields    | 4 servings |


## Ingredients {#ingredients}

| Quantity | Item                                                     |
|----------|----------------------------------------------------------|
| 2        | [eggs]({{< relref "../items/cherry_tomato.md" >}})       |
| 2/3 cup  | [milk]({{< relref "../items/milk.md" >}})                |
| 2 tsp    | ground [cinnamon]({{< relref "../items/cinnamon.md" >}}) |
| 8 slices | [bread]({{< relref "../items/bread.md" >}})              |
| 1 tsp    | [butter]({{< relref "../items/butter.md" >}}), melted    |


## Directions {#directions}

1.  Either melt butter in frying pan/griddle or melt and mix with other ingredients
2.  Beat to make the mixture light
3.  Dip bread and fry
4.  Top with berries, jam, icing sugar, etc
