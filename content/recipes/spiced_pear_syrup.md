+++
title = "Spiced pear syrup"
author = ["James Hawley"]
date = 2023-03-11T00:17:00-05:00
lastmod = 2023-12-22T01:15:05-05:00
tags = ["recipes", "beverages"]
draft = false
+++

| Info      | Amount |
|-----------|--------|
| Prep Time | 5 min  |
| Cook Time | 45 min |
| Yields    | 2 cups |


## Ingredients {#ingredients}

| Quantity | Item                                                           |
|----------|----------------------------------------------------------------|
| 3/2 cup  | [water]({{< relref "../items/water.md" >}})                    |
| 1        | [pear]({{< relref "../items/pear.md" >}}), cubed               |
| 3/4 cup  | [white sugar]({{< relref "../items/white_sugar.md" >}})        |
| 3        | [cinnamon sticks]({{< relref "../items/cinnamon_stick.md" >}}) |


## Directions {#directions}

1.  Add water, pears, sugar, and cinnamon sticks to a large sauce pan over medium heat
    1.  Stir gently until sugar has completely dissolved
    2.  Allow to set for 25 min, gently simmering
2.  Turn heat to low, and simmer another 5 min
3.  Remove from heat, and allow to cool for 15 min
4.  Strain the syrup into a glass measuring cup and discard the pears and cinnamon sticks
5.  Strain the syrup a second time into a large glass bottle that tightly seals


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [A Flavor Journal](https://aflavorjournal.com/wprm_print/4511)
2.  Refrigerate for up to two weeks
