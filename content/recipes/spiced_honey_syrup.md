+++
title = "Spiced honey syrup"
author = ["James Hawley"]
date = 2022-09-15T22:14:00-04:00
lastmod = 2023-11-20T09:36:49-05:00
tags = ["sweeteners", "syrups", "recipes", "beverages"]
draft = false
+++

| Info      | Amount           |
|-----------|------------------|
| Prep Time | 5 min            |
| Cook Time | 5 min + 2 h      |
| Yields    | 3/4 cup servings |


## Ingredients {#ingredients}

| Quantity | Item                                                           |
|----------|----------------------------------------------------------------|
| 1/2 cup  | [honey]({{< relref "../items/honey.md" >}})                    |
| 1/4 cup  | [water]({{< relref "../items/water.md" >}})                    |
| 2        | [cinnamon sticks]({{< relref "../items/cinnamon_stick.md" >}}) |
| 1 tsp    | [whole cloves]({{< relref "../items/clove.md" >}})             |


## Directions {#directions}

1.  Combine honey, water, cinnamon, and cloves in a small saucepan.
    -   Place over medium heat.
    -   When mixture comes to a boil, remove from heat, cover, and let stand for 2 h.
2.  Strain into a bowl and discard solids.
    -   Transfer to a glass jar and store in the fridge.


## References &amp; Notes {#references-and-notes}

1.  Original recipe: LCBO Food &amp; Drink - Summer 2022.
2.  Can last in the fridge up to 1 month.
