+++
title = "Honey whisky roasted parsnips"
author = ["James Hawley"]
date = 2022-10-01T00:56:00-04:00
lastmod = 2023-12-21T22:05:14-05:00
tags = ["vegetarian", "vegan", "sides", "recipes"]
draft = false
+++

| Info      | Amount  |
|-----------|---------|
| Prep Time | 10 min  |
| Cook Time | 40 min  |
| Yields    | 4 sides |


## Ingredients {#ingredients}

| Quantity | Item                                                             |
|----------|------------------------------------------------------------------|
| 8        | small to medium [parsnips]({{< relref "../items/parsnip.md" >}}) |
| 1/4 cup  | [honey]({{< relref "../items/honey.md" >}})                      |
| 2 Tbsp   | [olive oil]({{< relref "../items/olive_oil.md" >}})              |
| 1 oz     | [whisky]({{< relref "../items/whiskey.md" >}})                   |
| 1 tsp    | [Kosher salt]({{< relref "../items/kitchen_salt.md" >}})         |
| 2 tsp    | [fennel seeds]({{< relref "../items/fennel.md" >}})              |


## Directions {#directions}

1.  Preheat the oven to 375 F and line a rimmed baking sheet with parchment paper.
2.  In a small skillet over medium-low heat, gently toast the fennel seeds until they are aromatic and evenly toasted.
3.  Peel the parsnips, leaving them whole and the stems attached, if possible.
4.  With a small bowl, whisk together the honey, oil, whisky, and salt.
    1.  Use a pastry brush to evenly coat the parsnips with the mixture.
    2.  Drizzle any remaining mixture over top.
5.  Lay the parsnips on the baking sheet and sprinkle the fennel seeds on top.
    1.  Bake in the centre of the oven for 35 - 40 min, until golden brown and tender to the fork.


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [A Rising Tide]({{< relref "../references/A_Rising_Tide.md" >}})
