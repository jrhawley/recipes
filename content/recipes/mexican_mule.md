+++
title = "Mexican mule"
author = ["James Hawley"]
date = 2023-09-08T11:18:00-04:00
lastmod = 2023-12-21T22:15:23-05:00
tags = ["alcohols", "recipes", "beverages"]
draft = false
+++

A twist on the classic [Moscow mule]({{< relref "moscow_mule.md" >}}).

| Info      | Amount     |
|-----------|------------|
| Prep Time | 2 min      |
| Cook Time | 0 min      |
| Yields    | 1 cocktail |


## Ingredients {#ingredients}

| Quantity | Item                                                    |
|----------|---------------------------------------------------------|
| 3/2 oz   | [tequila]({{< relref "../items/tequila.md" >}})         |
| 9/2 oz   | [ginger beer]({{< relref "../items/ginger_beer.md" >}}) |
| 1 oz     | [lime juice]({{< relref "../items/lime_juice.md" >}})   |
|          | ice cubes                                               |


## Directions {#directions}

1.  Fill copper mug with ice cubes
2.  Mix remaining ingredients in mug and stir
