+++
title = "Sunchoke and leek soup with Canadian gremolata"
author = ["James Hawley"]
date = 2023-03-12T09:46:00-04:00
lastmod = 2023-12-28T01:22:05-05:00
tags = ["entrees", "vegetarian", "soups", "recipes"]
draft = false
+++

| Info      | Amount         |
|-----------|----------------|
| Prep Time | 15 min         |
| Cook Time | 50 min         |
| Yields    | 4 - 6 servings |


## Ingredients {#ingredients}

| Quantity | Item                                                                 |
|----------|----------------------------------------------------------------------|
| 1 stalk  | [leeks]({{< relref "../items/leek.md" >}}), chopped                  |
| 1 Tbsp   | [butter]({{< relref "../items/butter.md" >}})                        |
| 4 cloves | [garlic]({{< relref "../items/garlic.md" >}}), minced                |
| 2 cup    | [sunchokes]({{< relref "../items/sunchoke.md" >}}), peeled and cubed |
| 2 cup    | [cauliflower florets]({{< relref "../items/cauliflower.md" >}})      |
|          | [Kosher salt]({{< relref "../items/kitchen_salt.md" >}})             |
| 1 L      | [vegetable broth]({{< relref "../items/vegetable_broth.md" >}})      |
| 2        | [bay leaves]({{< relref "../items/bay_leaf.md" >}})                  |
| 1 cup    | [parsley]({{< relref "../items/parsley.md" >}})                      |
|          | [black pepper]({{< relref "../items/black_pepper.md" >}})            |
| 1 Tbsp   | [sunflower seeds]({{< relref "../items/sunflower_seed.md" >}})       |
| 1 tsp    | [ground sumac]({{< relref "../items/sumac.md" >}})                   |
| 1/2 tsp  | [flaky sea salt]({{< relref "../items/sea_salt.md" >}})              |


## Directions {#directions}

1.  Trim the leek, remove the dark green tops, and chop
    1.  Place the chopped leeks in a colander and wash thoroughly
    2.  Use your hands to inspect the leeks to ensure the dirt between the layers has been removed
    3.  Dry and set aside
2.  Place the sunchokes in a bowl of cold water after peeling and cubing
3.  In a large pot over medium heat, melt the butter
    1.  Add the leeks and sauté until caramelized and translucent, about 10 min
    2.  Use a wooden spoon to stir often
    3.  Reduce the heat to medium-low and add half the garlic
    4.  Stirring frequently, cook until fragrant, 2 - 3 min
    5.  Remove the sunchokes from the cold-water bath and add them and the cauliflower to the leeks and garlic
    6.  Season with salt
    7.  Stir and cook for another 2 - 3 min
4.  Add the broth and increase the heat to high
    1.  Cover with a lid
    2.  Once the liquid reaches a boil, reduce the heat to a simmer
    3.  Add the bay leaves and parsley and let simmer until the vegetables are fork-tender, about 35 min
5.  While the soup is simmering, in a mortar, grind the sunflower seeds, sumac, thyme, and sea salt with a pestle
    1.  Add the remaining garlic and continue to grind until a crunchy paste forms
    2.  Add the parsley and continue to stamp, press, and muddle into a minced paste
    3.  Set aside for serving
6.  Remove the pot from the heat, take out the bay leaves, and set the soup aside to cool for 5 - 10 min before blending
    1.  Use an immersion blender or transfer the soup to a good-quality blender to purée into a thick, creamy soup
    2.  Season with salt and pepper to taste
    3.  Garnish with 1 to 2 Tbsp of the gremolata, and serve


## References {#references}

1.  Original recipe: [Peak Season]({{< relref "../references/Peak_Season.md" >}})
