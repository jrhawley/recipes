+++
title = "Spanakorizo rice"
author = ["James Hawley"]
date = 2020-04-16T00:00:00-04:00
lastmod = 2024-05-23T21:35:00-04:00
tags = ["recipes", "vegetarian", "entrees"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 5 min      |
| Cook Time | 35 min     |
| Yields    | 4 servings |

{{< figure src="../assets/spanakorizo-rice.png" caption="<span class=\"figure-number\">Figure 1: </span>Spanakorizo rice" >}}


## Ingredients {#ingredients}

| Quantity | Item                                                            |
|----------|-----------------------------------------------------------------|
| 3 Tbsp   | [olive oil]({{< relref "../items/olive_oil.md" >}})             |
| 1        | medium [onion]({{< relref "../items/onion.md" >}}), chopped     |
| 4 cloves | [garlic]({{< relref "../items/garlic.md" >}}), chopped          |
| 1/2 lbs  | baby [spinach]({{< relref "../items/spinach.md" >}})            |
| 1 cup    | [basmati rice]({{< relref "../items/basmati_rice.md" >}})       |
| 2 cup    | [vegetable broth]({{< relref "../items/vegetable_broth.md" >}}) |
| 3 Tbsp   | [butter]({{< relref "../items/butter.md" >}})                   |
| 2 Tbsp   | [lemon juice]({{< relref "../items/lemon_juice.md" >}})         |
| 2 tsp    | [lemon zest]({{< relref "../items/lemon.md" >}})                |
|          | [feta]({{< relref "../items/feta.md" >}})                       |


## Directions {#directions}

1.  Heat olive oil in large pot over medium-high heat
    1.  Saute onions
    2.  Add garlic, half the dill and chives
    3.  Saute for another minute
2.  Add spinach, cook until wilted
    1.  Add salt, rice, and broth
    2.  Stir, bring to a boil, cover, reduce heat to medium-low
    3.  Simmer for 20 min
3.  Stir in butter, lemon juice, lemon zest, and remaining chives and dill
4.  Garnish with feta, serve with lemon wedges


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [Cooktoria](https://cooktoria.com/spanakorizo-greek-spinach-rice/#wprm-recipe-container-5934)
