+++
title = "Gingersnaps"
author = ["James Hawley"]
date = 2018-12-18T00:00:00-05:00
lastmod = 2023-12-21T21:53:25-05:00
tags = ["baking", "recipes", "desserts"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 15 min     |
| Cook Time | 10 min     |
| Yields    | 36 cookies |

{{< figure src="../assets/gingersnaps.jpg" caption="<span class=\"figure-number\">Figure 1: </span>Gingersnaps" >}}


## Ingredients {#ingredients}

| Quantity | Item                                                                   |
|----------|------------------------------------------------------------------------|
| 1 cup    | [white sugar]({{< relref "../items/white_sugar.md" >}})                |
| 3/4 cup  | [shortening]({{< relref "../items/vegetable_shortening.md" >}})        |
| 1/4 cup  | Crosby's fancy [molasses]({{< relref "../items/fancy_molasses.md" >}}) |
| 1        | [egg]({{< relref "../items/eggs.md" >}})                               |
| 2 cup    | [flour]({{< relref "../items/flour.md" >}})                            |
| 2 tsp    | [baking soda]({{< relref "../items/baking_soda.md" >}})                |
| 1/2 tsp  | [table salt]({{< relref "../items/table_salt.md" >}})                  |
| 1/2 tsp  | [cinnamon]({{< relref "../items/cinnamon.md" >}})                      |
| 1/4 tsp  | ground [cloves]({{< relref "../items/garlic.md" >}})                   |
| 1/4 tsp  | ground [ginger]({{< relref "../items/ginger.md" >}})                   |


## Directions {#directions}

1.  Grease cookies sheets
2.  In large bowl, combine sugar, shortening, molasses, and egg, blending well
3.  Mix remaining ingredients together in a smaller bowl
    1.  Blend with creamed mixture
    2.  If desired, chill dough for easier handling
4.  Shape into 1" balls
    1.  Roll each in white sugar, if desired
    2.  Place 2" apart on cookie sheet
    3.  Press down lightly with a wet fork
5.  Bake for 10 min at 350 F or until edges are set
6.  Let sit on cookie sheet for 5 min before removing to cool on rack
