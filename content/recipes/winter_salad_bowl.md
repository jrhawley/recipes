+++
title = "Winter salad bowl"
author = ["James Hawley"]
date = 2020-02-01T00:00:00-05:00
lastmod = 2024-05-23T21:36:44-04:00
tags = ["bowls", "recipes", "vegetarian", "vegan", "entrees"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 15 min     |
| Cook Time | 30 min     |
| Yields    | 6 servings |


## Ingredients {#ingredients}


### Salad {#salad}

| Quantity | Item                                                                               |
|----------|------------------------------------------------------------------------------------|
| 2        | large [Yukon gold potatoes]({{< relref "../items/yukon_gold_potato.md" >}}), diced |
| 12 oz    | [green beans]({{< relref "../items/green_bean.md" >}}), trimmed and chopped        |
| 1 clove  | [garlic]({{< relref "../items/garlic.md" >}})                                      |
| 1 Tbsp   | [olive oil]({{< relref "../items/olive_oil.md" >}})                                |
| 1 cup    | [quinoa]({{< relref "../items/quinoa.md" >}}), uncooked                            |
| 1 cup    | [kale]({{< relref "../items/kale.md" >}}), stemmed, finely chopped                 |
| 3        | [green onions]({{< relref "../items/green_onion.md" >}}), sliced                   |
| 1/4 cup  | toasted [pumpkin seeds]({{< relref "../items/pumpkin_seed.md" >}})                 |
| 1        | large ripe [avocado]({{< relref "../items/avocado.md" >}})                         |
|          | [kosher salt]({{< relref "../items/kitchen_salt.md" >}})                           |
|          | [pepper]({{< relref "../items/black_pepper.md" >}})                                |


### Dressing {#dressing}

| Quantity | Item                                                              |
|----------|-------------------------------------------------------------------|
| 1/4 cup  | [red wine vinegar]({{< relref "../items/red_wine_vinegar.md" >}}) |
| 1/4 cup  | [olive oil]({{< relref "../items/olive_oil.md" >}})               |
| 2 tsp    | [dijon mustard]({{< relref "../items/dijon_mustard.md" >}})       |
| 1 Tbsp   | [maple syrup]({{< relref "../items/maple_syrup.md" >}})           |
|          | [kosher salt]({{< relref "../items/kitchen_salt.md" >}})          |
|          | [pepper]({{< relref "../items/black_pepper.md" >}})               |


## Directions {#directions}

1.  Preheat oven to 400 F
2.  Line extra-large baking sheet with parchment paper
    1.  Place chopped potatoes, green beans, and whole garlic cloves onto sheet
    2.  Toss with oil and season with salt and pepper
    3.  Spread into even layer, cook for 15 min
    4.  Flip and roast again for another 15 min
3.  Meanwhile, cook quinoa according to instructions, fluff with a fork
4.  Whisk all dressing ingredients together
    1.  Cut off end of garlic clove, push out cooked garlic into dressing, and whisk together
5.  Spoon potatoes and beans into a large serving bowl
    1.  Add quinoa, kale, green onions, dressing, and toss all together
6.  Season with salt and pepper, top with seeds and avocado


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [Oh She Glows]({{< relref "../references/Oh_She_Glows_Cookbook.md" >}})
