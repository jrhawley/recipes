+++
title = "Sun-dried tomato herb salad with chickpeas"
author = ["James Hawley"]
date = 2023-03-01T09:12:00-05:00
lastmod = 2024-05-23T20:57:39-04:00
tags = ["vegetarian", "entrees", "salads", "recipes"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 20 min     |
| Cook Time | 10 min     |
| Yields    | 4 servings |


## Ingredients {#ingredients}


### Salad {#salad}

| Quantity | Item                                                                      |
|----------|---------------------------------------------------------------------------|
| 3 Tbsp   | [pine nuts]({{< relref "../items/pine_nut.md" >}})                        |
| 1 Tbsp   | [butter]({{< relref "../items/butter.md" >}})                             |
| 15 oz    | [chickpeas]({{< relref "../items/chickpea.md" >}}), rinsed and dried      |
|          | [Kosher salt]({{< relref "../items/kitchen_salt.md" >}})                  |
|          | [black pepper]({{< relref "../items/black_pepper.md" >}})                 |
| 1 head   | [kale]({{< relref "../items/kale.md" >}})                                 |
| 4 cup    | [arugula]({{< relref "../items/arugula.md" >}})                           |
| 4        | [carrots]({{< relref "../items/carrot.md" >}}), thinly sliced into rounds |
| 8 oz     | [sun-dried tomatoes]({{< relref "../items/sun-dried_tomato.md" >}})       |
| 1/4 cup  | [basil]({{< relref "../items/basil.md" >}}), roughly chopped              |
| 6 oz     | [feta]({{< relref "../items/feta.md" >}}), crumbled                       |
| 2        | [avocadoes]({{< relref "../items/avocado.md" >}})                         |


### Vinaigrette {#vinaigrette}

| Quantity | Item                                                                    |
|----------|-------------------------------------------------------------------------|
| 1        | [lemon]({{< relref "../items/lemon.md" >}})                             |
| 2 Ybsp   | [apple cider vinegar]({{< relref "../items/apple_cider_vinegar.md" >}}) |
| 1 tsp    | [honey]({{< relref "../items/honey.md" >}})                             |
| 1 pinch  | [red pepper flakes]({{< relref "../items/red_pepper_flake.md" >}})      |


## Directions {#directions}

1.  Heat a skillet over medium heat.
    1.  Add the pine nuts and toast for 2 min, then set aside.
    2.  Add the butter, chickpeas, oregano, and salt and pepper, cooking for 5 min, then set aside.
2.  While the other stuff is cooking, combined the kale, arugula carrots, tomatoes, and basil in a large salad bowl.
3.  In a small bowl, whisk together the vinaigrette ingredients, seasoning with salt, pepper, and red pepper flakes.
4.  Pour the vinaigrette over the salad and toss to combine.
5.  Sprinkle with crumbled feta, chickpeas, and avocado to serve.


## References {#references}

1.  Original recipe: [Half-Baked Harvest](https://halfbakedharvest.com/wprm_print/72900)
