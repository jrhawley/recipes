+++
title = "Creamy quinoa and tomato soup"
author = ["James Hawley"]
date = 2024-04-15T09:45:00-04:00
lastmod = 2024-05-23T20:36:37-04:00
tags = ["entrees", "soups", "vegetarian", "recipes"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 10 min     |
| Cook Time | 35 min     |
| Yields    | 6 servings |


## Equipment {#equipment}

-   1 prep knife
-   1 cutting board
-   1 garlic press
-   1 large soup pot
-   1 cheese grater
-   1 collander
-   1 can opener
-   1 wooden spoon
-   1 soup ladle


## Ingredients {#ingredients}

| Quantity   | Item                                                                   |
|------------|------------------------------------------------------------------------|
| 2 Tbsp     | [olive oil]({{< relref "../items/olive_oil.md" >}})                    |
| 1/2        | [white onion]({{< relref "../items/white_onion.md" >}}), diced         |
| 3          | [carrots]({{< relref "../items/carrot.md" >}}), diced                  |
| 2 cloves   | [garlic]({{< relref "../items/garlic.md" >}}), minced                  |
| 1 L        | [vegetable broth]({{< relref "../items/vegetable_broth.md" >}})        |
| 5/4 cup    | [quinoa]({{< relref "../items/quinoa.md" >}}), uncooked, rinsed        |
| 15 oz      | [chickpeas]({{< relref "../items/chickpea.md" >}}), drained and rinsed |
| 15 oz      | [diced tomatoes]({{< relref "../items/diced_tomatoes.md" >}})          |
| 8 oz       | [tomato sauce]({{< relref "../items/tomato_sauce.md" >}})              |
| 1 Tbsp     | [Italian seasoning]({{< relref "../items/italian_seasoning.md" >}})    |
| 3 handfuls | [spinach]({{< relref "../items/spinach.md" >}})                        |
| 1 cup      | [Greek yogurt]({{< relref "../items/greek_yogurt.md" >}})              |
|            | [Kosher salt]({{< relref "../items/kitchen_salt.md" >}})               |
|            | [lemon juice]({{< relref "../items/lemon_juice.md" >}})                |
|            | [table salt]({{< relref "../items/table_salt.md" >}})                  |
|            | [black pepper]({{< relref "../items/black_pepper.md" >}})              |
|            | [Parmesan]({{< relref "../items/parmesan.md" >}})                      |
|            | [red pepper flakes]({{< relref "../items/red_pepper_flake.md" >}})     |


## Directions {#directions}

1.  Heat olive oil in the pot over medium-high heat.
    1.  Add the diced onion, and a pinch of Kosher salt, and saute for 3 min.
    2.  Add the minced garlic and saute for another 1 - 2 min.
    3.  Add the carrots and saute for another 3 - 5 min, until soft.
2.  Braise the bottom of the soup pot with a splash of vegetable broth.
    1.  Once the fond has been scraped up, pour in the rest of the broth, and add the quinoa, chickpeas, tomatoes, tomato sauce, and seasonings.
    2.  Bring to a boil, reduce to low heat, then cover and let simmer for 20 - 25 min.
3.  Season with lemon juice, salt, pepper, and Italian seasoning to taste.
    1.  Stir in spinach until wilted, then Greek yogurt.
    2.  Season to taste, then serve with grated Parmesan.


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [Platings &amp; Pairings](https://www.platingsandpairings.com/wprm_print/14963)
2.  Lasts in the fridge for ~ 3 days
