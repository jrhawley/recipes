+++
title = "One-skillet lemon chicken orzo"
author = ["James Hawley"]
date = 2023-02-24T19:14:00-05:00
lastmod = 2024-05-23T20:58:10-04:00
tags = ["chicken", "pastas", "entrees", "recipes"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 5 min      |
| Cook Time | 30 min     |
| Yields    | 4 servings |


## Ingredients {#ingredients}

| Quantity  | Item                                                            |
|-----------|-----------------------------------------------------------------|
| 2 Tbsp    | [olive oil]({{< relref "../items/olive_oil.md" >}})             |
| 3         | [chicken breasts]({{< relref "../items/chicken_breast.md" >}})  |
| 1         | [lemon]({{< relref "../items/lemon.md" >}}), sliced             |
| 2 Tbsp    | [butter]({{< relref "../items/butter.md" >}})                   |
| 1 clove   | [garlic]({{< relref "../items/garlic.md" >}}), minced           |
| 1 cup     | [orzo]({{< relref "../items/orzo.md" >}})                       |
| 1/3 cup   | [white wine]({{< relref "../items/white_wine.md" >}})           |
| 5/2 cup   | [vegetable broth]({{< relref "../items/vegetable_broth.md" >}}) |
| 1/2 bunch | [kale]({{< relref "../items/kale.md" >}}), roughly torn         |
|           | [Kosher salt]({{< relref "../items/kitchen_salt.md" >}})        |
|           | [black pepper]({{< relref "../items/black_pepper.md" >}})       |


## Directions {#directions}

1.  Preheat the oven to 200 C (400 F).
2.  In a Dutch oven, heat 1 Tbsp of olive oil.
    1.  Season the chicken with salt and pepper, then add to the dutch oven when oil is hot.
    2.  Fry chicken until almost cooked all the way through (~ 3 - 5 min per side, if fresh).
    3.  Remove the chicken from the dutch oven.
3.  Add the butter and juice from the 1 lemon to the dutch oven.
    1.  Add the lemon slices and sear them for 1 min.
    2.  Remove the slices and place them on the plate with the chicken.
4.  Add the last Tbsp of oil, the garlic, and orzo to the dutch oven.
    1.  Cook for 2 min, until fragrant.
    2.  De-glaze the pan with the wine, add the chicken broth, kale, and any remaining lemon juice.
    3.  Bring to a boil, then add chicken and lemons, turn off heat, and place the dutch oven in oven, uncovered.
    4.  Cook for 15 min, or until the broth has evaporated.
5.  Serve with fresh dill or other seasoning.


## References {#references}

1.  Original recipe: [Half-Baked Harvest](https://www.halfbakedharvest.com/wprm_print/58017)
