+++
title = "Greek tortellini salad"
author = ["James Hawley"]
date = 2017-01-21T00:00:00-05:00
lastmod = 2024-05-23T21:43:54-04:00
tags = ["salads", "recipes", "vegetarian", "entrees"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 15 min     |
| Cook Time | 0 min      |
| Yields    | 4 servings |

{{< figure src="../assets/greek-tortellini.jpg" caption="<span class=\"figure-number\">Figure 1: </span>Greek tortellini salad" >}}


## Ingredients {#ingredients}


### Salad {#salad}

| Quantity | Item                                                                                    |
|----------|-----------------------------------------------------------------------------------------|
| 20 oz    | [tortellini]({{< relref "../items/tortellini.md" >}})                                   |
| 3/2 cups | [cherry tomatoes]({{< relref "../items/cherry_tomato.md" >}}), cut in half              |
| 1        | large [cucumber]({{< relref "../items/cucumber.md" >}}), diced                          |
| 1 cup    | [kalamata olives]({{< relref "../items/kalamata_olive.md" >}}), pit removed and chopped |
| 1/2      | [red onion]({{< relref "../items/red_onion.md" >}})                                     |
| 3/4 cup  | [feta]({{< relref "../items/feta.md" >}})                                               |


### Dressing {#dressing}

| Quantity | Item                                                              |
|----------|-------------------------------------------------------------------|
| 1/4 cup  | [olive oil]({{< relref "../items/olive_oil.md" >}})               |
| 3 Tbsp   | [red-wine-vinegar]({{< relref "../items/red_wine_vinegar.md" >}}) |
| 1 clove  | [garlic]({{< relref "../items/garlic.md" >}}), minced             |
| 1/2 tsp  | dried [oregano]({{< relref "../items/oregano.md" >}})             |
|          | [kosher salt]({{< relref "../items/kitchen_salt.md" >}})          |
|          | [pepper]({{< relref "../items/black_pepper.md" >}})               |


## Directions {#directions}

1.  Bring a large pot of salted water to a boil
    1.  Cook the tortellini according to the package directions
    2.  Drain the tortellini and rinse with cold water
2.  Place the tortellini in a large bowl
    1.  Add the tomatoes, cucumber, olives, red onion, and feta cheese
3.  In a small bowl, whisk together the olive oil, vinegar, garlic, oregano, salt, and pepper
    1.  Pour the dressing over the salad and stir until salad is well coated
    2.  Serve immediately or place in the refrigerator


## References &amp; Notes {#references-and-notes}

1.  This will keep in the fridge for up to 3 days
