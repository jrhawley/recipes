+++
title = "Chicken tetrazzini"
author = ["James Hawley"]
date = 2020-09-09T00:00:00-04:00
lastmod = 2024-05-23T21:30:20-04:00
tags = ["pastas", "recipes", "entrees"]
draft = false
+++

| Info      | Amount         |
|-----------|----------------|
| Prep time | 20 min         |
| Cook time | 30 min         |
| Yields    | 4 - 6 servings |


## Ingredients {#ingredients}

| Quantity | Item                                                                      |
|----------|---------------------------------------------------------------------------|
| 6 Tbsp   | [butter]({{< relref "../items/butter.md" >}})                             |
| 1        | [onion]({{< relref "../items/onion.md" >}}), chopped                      |
| 1 stalk  | [celery]({{< relref "../items/celery.md" >}})                             |
| 3 Tbsp   | [flour]({{< relref "../items/flour.md" >}})                               |
| 7/2 cup  | [milk]({{< relref "../items/milk.md" >}})                                 |
| 3/2 tsp  | [table salt]({{< relref "../items/table_salt.md" >}})                     |
| 1/4 tsp  | [pepper]({{< relref "../items/black_pepper.md" >}})                       |
| 1 Tbsp   | [worcestershire sauce]({{< relref "../items/worcestershire_sauce.md" >}}) |
| 1/2 cup  | [cream cheese]({{< relref "../items/cream_cheese.md" >}})                 |
| 3 cup    | [chicken]({{< relref "../items/chicken_breast.md" >}}), cooked            |
| 1 pkg    | [egg noodles]({{< relref "../items/egg_noodle.md" >}}), cooked            |
| 1/2 cup  | [parmesan]({{< relref "../items/parmesan.md" >}}), grated                 |
| 1/2 cup  | [breadcrumbs]({{< relref "../items/breadcrumbs.md" >}})                   |


## Directions {#directions}

1.  Preheat oven to 350 F, cook chicken in a large frying pan, and cook egg noodles according to instructions
2.  Heat half the butter in a large saucepan
    -   Add onion and celery
    -   Cook until tender and liquid has evaporated
3.  Sprinkle with flour, cook for 1 min
4.  Add milk gradually and bring to a boil
    -   Add salt, pepper, worcestershire sauce
    -   Whisk in cream cheese small bits at a time
    -   Cook gently for ~ 2 min until melted and thickened
5.  Add chicken and noodles
    -   Place in buttered 9"x13" pan
6.  Melt remaining butter
    -   Combine with parmesan and bread crumbs
    -   Sprinkle over the casserole
7.  Place entire dish on a baking sheet to catch any spills
    -   Cook in oven for 30 min


## References &amp; Notes {#references-and-notes}

1.  Original recipe: Wendy McNeil
2.  Be sure not to brown the vegetables after adding flour
