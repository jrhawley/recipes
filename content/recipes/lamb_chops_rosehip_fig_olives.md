+++
title = "Lamb chops with rosehip, fig, and green olives"
author = ["James Hawley"]
date = 2021-09-25T00:00:00-04:00
lastmod = 2024-05-23T21:19:23-04:00
tags = ["lamb", "recipes", "entrees"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 5 min      |
| Cook Time | 2 h 20 min |
| Yields    | 4 servings |


## Ingredients {#ingredients}

| Quantity | Item                                                                  |
|----------|-----------------------------------------------------------------------|
| 1/4 cup  | [rosehip tea]({{< relref "../items/rosehip_tea.md" >}})               |
| 3 Tbsp   | [olive oil]({{< relref "../items/olive_oil.md" >}})                   |
| 1 kg     | [lamb shoulder chops]({{< relref "../items/lamb_chop.md" >}})         |
| 2 cup    | [white onion]({{< relref "../items/onion.md" >}}), coarsely chopped   |
| 1/2 tsp  | [red pepper flakes]({{< relref "../items/red_pepper_flake.md" >}})    |
| 1 clove  | [garlic]({{< relref "../items/garlic.md" >}}), finely chopped         |
| 2 tsp    | [tomato paste]({{< relref "../items/tomato_paste.md" >}})             |
| 1 cup    | [dry red wine]({{< relref "../items/red_wine.md" >}})                 |
| 1/2 cup  | [whole pitted green olives]({{< relref "../items/green_olive.md" >}}) |
| 4        | [dried figs]({{< relref "../items/fig.md" >}}), quartered             |
| 5        | [cardamom pods]({{< relref "../items/cardamom.md" >}}) (optional)     |


## Directions {#directions}

1.  Preheat the over to 325 F.
2.  Make rosehip tea with 2 cups of boiling water
    1.  Let steep for 10 min.
3.  In a large ovenproof skillet, heat 2 Tbsp of olive oil over medium-high heat.
    1.  Season lamb with salt, to taste.
    2.  Add to skillet and cook until browned on both sides, ~10 min.
4.  Remove lamb to set aside and discard the oil.
    1.  Heat remaining oil over medium-high heat and add onion and pepper flakes.
    2.  Saute for 5 min.
    3.  Add garlic and tomato paste, then saute for another 5 min until tender.
5.  Tie the cardamom pods in a small piece of cheesecloth, if using.
    1.  Add red wine, olives, figs, cardamom, strained tea, and lamb back into the skillet.
    2.  If needed, add water to fill the skillet halfway up the lamb.
    3.  Bring to a boil over high heat, cover, then place in the oven.
6.  Cook for 2 h or until the lamb is very tender&nbsp;[^fn:1].
7.  Remove from oven and remove the cardamom pods before serving&nbsp;[^fn:2].


## References {#references}

1.  Original recipe: [LCBO](https://www.lcbo.com/webapp/wcs/stores/servlet/en/lcbo/recipe/lamb-shoulder-chops-with-rosehip,-fig-green-olives/F202105013)

[^fn:1]: Check the oven occasionally to ensure the liquid hasn't fully reduced.
    If so, add 1 cup of water and finish cooking.
[^fn:2]: If the sauce is too thin, place skillet over medium heat and reduce.
    If too thick, add water until the desired consistency.
