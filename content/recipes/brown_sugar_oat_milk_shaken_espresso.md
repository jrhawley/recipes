+++
title = "Brown sugar oat milk shaken espresso"
author = ["James Hawley"]
date = 2022-11-16T14:57:00-05:00
lastmod = 2023-12-19T22:10:15-05:00
tags = ["coffee", "recipes", "beverages"]
draft = false
+++

| Info      | Amount  |
|-----------|---------|
| Prep Time | 5 min   |
| Cook Time | 0 min   |
| Yields    | 1 drink |


## Ingredients {#ingredients}

| Quantity | Item                                                                |
|----------|---------------------------------------------------------------------|
| 3/2 Tbsp | [brown sugar simple syrup]({{< relref "simple_syrup.md" >}})        |
| 1 oz     | espresso                                                            |
| 4 oz     | [oat milk]({{< relref "../items/oat_milk.md" >}})                   |
|          | [ground cinnamon]({{< relref "../items/cinnamon.md" >}}) (optional) |


## Directions {#directions}

1.  Pour brown sugar simple syrup and espresso in a cocktail shaker with ice and shake vigorously.
2.  Strain into a highball glass with ice.
3.  Top with oat milk and garnish with a sprinkle of cinnamon.


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [Sugar&amp;Soul](https://sugarandsoul.co/wprm_print/53887)
2.  Starbucks: [Iced brown sugar oat milk shaken espresso](https://www.starbucks.com/menu/product/2123431/iced?parent=%2Fdrinks%2Fcold-coffees%2Ficed-shaken-espresso)
