+++
title = "Sunrise scramble with homefries and avocado toast"
author = ["James Hawley"]
date = 2023-03-18T12:43:00-04:00
lastmod = 2024-06-12T19:16:29-04:00
tags = ["breakfasts", "recipes"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 25 min     |
| Cook Time | 30 min     |
| Yields    | 4 servings |


## Ingredients {#ingredients}


### Home fries {#home-fries}

| Quantity | Item                                                              |
|----------|-------------------------------------------------------------------|
| 1        | large [russet potato]({{< relref "../items/russet_potato.md" >}}) |
| 1        | [sweet potato]({{< relref "../items/yam.md" >}})                  |
| 1 Tbsp   | [cornstarch]({{< relref "../items/corn_starch.md" >}})            |
| 1/4 tsp  | [fine sea salt]({{< relref "../items/sea_salt.md" >}})            |
| 3/2 tsp  | [coconut oil]({{< relref "../items/coconut_oil.md" >}})           |


### Tofu scramble {#tofu-scramble}

| Quantity | Item                                                                                                         |
|----------|--------------------------------------------------------------------------------------------------------------|
| 2 tsp    | [olive oil]({{< relref "../items/olive_oil.md" >}})                                                          |
| 2 cloves | [garlic]({{< relref "../items/garlic.md" >}}), minced                                                        |
| 2        | [shallots]({{< relref "../items/shallot.md" >}})                                                             |
| 1        | [bell pepper]({{< relref "../items/bell_pepper.md" >}}), finely chopped                                      |
| 2 cup    | [kale]({{< relref "../items/kale.md" >}}) or [spinach]({{< relref "../items/spinach.md" >}}), finely chopped |
| 1 Tbsp   | [nutritional yeast]({{< relref "../items/nutritional_yeast.md" >}})                                          |
| 1/4 tsp  | [paprika]({{< relref "../items/paprika.md" >}})                                                              |
| 450 g    | [tofu]({{< relref "../items/tofu.md" >}}), pressed                                                           |
| 1/2 tsp  | [fine sea salt]({{< relref "../items/sea_salt.md" >}})                                                       |
|          | [black pepper]({{< relref "../items/black_pepper.md" >}})                                                    |
|          | [red pepper flakes]({{< relref "../items/red_pepper_flake.md" >}})                                           |


### Avocado toast {#avocado-toast}

| Quantity | Item                                                      |
|----------|-----------------------------------------------------------|
| 1        | [avocado]({{< relref "../items/avocado.md" >}}), mashed   |
| 1 slice  | [bread]({{< relref "../items/bread.md" >}}), toasted      |
|          | [olive oil]({{< relref "../items/olive_oil.md" >}})       |
|          | [fine sea salt]({{< relref "../items/sea_salt.md" >}})    |
|          | [black pepper]({{< relref "../items/black_pepper.md" >}}) |


## Directions {#directions}

1.  Preheat oven to 220 C (425 F) and line a large rimmed baking sheet with parchment paper.
2.  Dice the potatoes into 1 cm (1/2") chunks, or smaller pieces
3.  In a large bowl, combine the diced potatoes, the cornstarch, and the salt and stir to combine
    1.  Stir in the coconut oil and mix until thoroughly combined
    2.  Spread the potatoes into an even layer on the prepared baking sheet
    3.  Bake for 15 min, then flip the potatoes and bake for an additional 15 to 25 min
4.  In a large wok, combine the oil, garlic, and shallots and sauté over medium-high heat for 5 - 10 min
    1.  Add the bell pepper, kale, nutritional yeast, and smoked paprika
    2.  Stir well and continue cooking over medium-high heat
    3.  Crumble or finely chop the tofu and add it to the wok, stirring to combine
    4.  Reduce the heat to medium and sauté for about 10 min
    5.  Season with salt, black pepper, and red pepper flakes, if desired
5.  Spread the mashed avocado on the toast
    1.  Top with a drizzle of flaxseed oil and a sprinkle of salt, black pepper, and red pepper flakes
6.  Plate and serve


## References {#references}

1.  Original recipe: [The Oh She Glows Cookbook]({{< relref "../references/Oh_She_Glows_Cookbook.md" >}})
