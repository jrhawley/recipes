+++
title = "Garlic butter smashed sweet potatoes"
author = ["James Hawley"]
date = 2020-09-12T00:00:00-04:00
lastmod = 2024-05-23T21:29:33-04:00
tags = ["recipes", "entrees", "vegetarian"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 2 min      |
| Cook Time | 25 min     |
| Yields    | 4 servings |


## Ingredients {#ingredients}

| Quantity | Item                                                               |
|----------|--------------------------------------------------------------------|
| 4        | medium [sweet potatoes]({{< relref "../items/yam.md" >}})          |
| 3 Tbsp   | [butter]({{< relref "../items/butter.md" >}}), melted              |
| 4 cloves | [garlic]({{< relref "../items/garlic.md" >}})                      |
| 1 Tbsp   | fresh [parsley]({{< relref "../items/parsley.md" >}}), chopped     |
| 2 Tbsp   | [parmesan]({{< relref "../items/parmesan.md" >}})                  |
|          | [kosher salt]({{< relref "../items/kitchen_salt.md" >}}), to taste |
|          | [pepper]({{< relref "../items/black_pepper.md" >}}), to taste      |


## Directions {#directions}

1.  Pre-heat oven to broil
2.  Time the ends of the sweet potatoes and slice into chunks about 1" in thickness
    -   Place on baking sheet, lightly drizzle with olive or vegetable oil, and bake for 20-25 min, until soft
3.  Lightly smash


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [Cafe Delites](https://cafedelites.com/wprm_print/40378)
