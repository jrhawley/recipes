+++
title = "Custard and Berry Cake"
author = ["James Hawley"]
date = 2013-09-22T00:00:00-04:00
lastmod = 2023-12-21T21:38:26-05:00
tags = ["cakes", "recipes", "desserts"]
draft = false
+++

| Info      | Amount |
|-----------|--------|
| Prep Time | 10 min |
| Cook Time | 40 min |
| Yields    |        |


## Ingredients {#ingredients}

| Quantity | Item                                                                     |
|----------|--------------------------------------------------------------------------|
| 2 cup    | frozen [raspberries]({{< relref "../items/raspberries.md" >}})           |
| 2        | [eggs]({{< relref "../items/eggs.md" >}})                                |
| 1 cup    | [milk]({{< relref "../items/milk.md" >}})                                |
| 1 cup    | [flour]({{< relref "../items/flour.md" >}})                              |
| 3/4 cup  | [white sugar]({{< relref "../items/white_sugar.md" >}})                  |
| 3 tbsp   | [butter]({{< relref "../items/butter.md" >}}), melted                    |
| 1/2 tsp  | [table salt]({{< relref "../items/table_salt.md" >}})                    |
| 1 tbsp   | [vanilla extract]({{< relref "../items/vanilla_extract.md" >}})          |
|          | [icing sugar]({{< relref "../items/icing_sugar.md" >}}) (optional)       |
|          | low fat vanilla [yogurt]({{< relref "../items/yogurt.md" >}}) (optional) |


## Directions {#directions}

1.  Preheat oven to 350 F
2.  Scatter raspberries in greased 11" shallow baking dish
3.  Combine eggs, milk, flour, sugar, butter, vanilla, and salt in a blender
    1.  Blend on medium until smooth
    2.  Could also combine in bowl and whisk
4.  Pour batter evenly over raspberries
5.  Bake for 40 min or until set
6.  Dust with icing sugar and serve warm with a dollop of yogurt
