+++
title = "El Diablo"
author = ["James Hawley"]
date = 2022-11-12T23:30:00-05:00
lastmod = 2023-12-21T21:43:47-05:00
tags = ["alcohols", "recipes", "beverages"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 2 min      |
| Cook Time | 0 min      |
| Yields    | 1 cocktail |


## Ingredients {#ingredients}

| Quantity | Item                                                            |
|----------|-----------------------------------------------------------------|
| 2 oz     | [tequila]({{< relref "../items/tequila.md" >}})                 |
| 1 oz     | [creme de cassis]({{< relref "../items/creme_de_cassis.md" >}}) |
| 1 oz     | [lime juice]({{< relref "../items/lime_juice.md" >}})           |
|          | [ginger ale]({{< relref "../items/ginger_ale.md" >}})           |
| 1        | [lime wedge]({{< relref "../items/lime.md" >}})                 |
|          | ice                                                             |


## Directions {#directions}

1.  Add the first 3 ingredients to a highball glass filled with ice.
2.  Top with ginger ale and garnish with a lime wedge.


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [The Periodic Table of Cocktails]({{< relref "../references/Periodic_Table_of_Cocktails.md" >}})
