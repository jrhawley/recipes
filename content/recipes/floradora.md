+++
title = "Floradora"
author = ["James Hawley"]
date = 2021-07-11T00:00:00-04:00
lastmod = 2023-12-21T21:46:08-05:00
tags = ["recipes", "beverages", "alcohols"]
draft = false
+++

## Ingredients {#ingredients}

| Quantity | Item                                                  |
|----------|-------------------------------------------------------|
| 3/2 oz   | [gin]({{< relref "../items/gin.md" >}})               |
| 1/2 oz   | [grenadine]({{< relref "../items/grenadine.md" >}})   |
| 1/2 oz   | [lime juice]({{< relref "../items/lime_juice.md" >}}) |
| 4 oz     | [ginger ale]({{< relref "../items/ginger_ale.md" >}}) |


## Directions {#directions}

1.  Combine all ingredients and stir in a cocktail glass
2.  Garnish with a lime wedge


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [The Spruce Eats](https://thespruceeats.com/floradora-cocktail-recipe-760079)
