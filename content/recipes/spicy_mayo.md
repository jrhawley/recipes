+++
title = "Spicy mayo"
author = ["James Hawley"]
date = 2013-09-13T00:00:00-04:00
lastmod = 2023-12-22T01:15:40-05:00
tags = ["recipes", "sauces"]
draft = false
+++

| Info      | Amount  |
|-----------|---------|
| Prep Time | 5 min   |
| Cook Time | 0 min   |
| Yields    | 1/2 cup |


## Ingredients {#ingredients}

| Quantity | Item                                                          |
|----------|---------------------------------------------------------------|
| 1/2 cup  | [mayonnaise]({{< relref "../items/mayonnaise.md" >}})         |
| 1 tsp    | [Sriracha sauce]({{< relref "../items/sriracha.md" >}})       |
| 1/4 tsp  | [cayenne pepper]({{< relref "../items/cayenne_pepper.md" >}}) |


## Directions {#directions}

1.  Mix all ingredients together in a small bowl or container


## References &amp; Notes {#references-and-notes}
