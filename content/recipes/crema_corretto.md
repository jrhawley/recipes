+++
title = "Crema corretto"
author = ["James Hawley"]
date = 2021-09-25T00:00:00-04:00
lastmod = 2023-12-21T21:36:32-05:00
tags = ["recipes", "alcohols", "beverages"]
draft = false
+++

## Ingredients {#ingredients}

| Quantity | Item                                                            |
|----------|-----------------------------------------------------------------|
| 2 oz     | [amaro]({{< relref "../items/lucano_amaro.md" >}})              |
| 1 oz     | [whipping cream]({{< relref "../items/whipping_cream.md" >}})   |
| 3/4 oz   | [espresso]({{< relref "../items/espresso.md" >}})               |
| 2        | [chocolate sticks]({{< relref "../items/chocolate_chip.md" >}}) |


## Directions {#directions}

1.  Shake amaro, cream, and espresso over ice until cold.
2.  Strain into an ice-filled rocks glass and garnish with chocolate sticks.


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [LCBO](https://www.lcbo.com/webapp/wcs/stores/servlet/en/lcbo/recipe/crema-corretto/F202105043)
