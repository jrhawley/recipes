+++
title = "Shallow-fried beer-battered fish and chips"
author = ["James Hawley"]
date = 2020-04-26T00:00:00-04:00
lastmod = 2024-05-23T21:34:46-04:00
tags = ["fish", "recipes", "entrees"]
draft = false
+++

| Info      | Amount   |
|-----------|----------|
| Prep Time | 5 min    |
| Cook Time | 15 min   |
| Yields    | 2 filets |


## Ingredients {#ingredients}

| Quantity | Item                                                                           |
|----------|--------------------------------------------------------------------------------|
| 2 filets | [cod]({{< relref "../items/cod.md" >}})                                        |
| 1        | [egg]({{< relref "../items/eggs.md" >}})                                       |
| 1 cup    | [all-purpose flour]({{< relref "../items/all-purpose_flour.md" >}}), separated |
| 1 tsp    | [baking powder]({{< relref "../items/baking_powder.md" >}})                    |
| 3/2 tsp  | [curry powder]({{< relref "../items/curry_powder.md" >}}), optional            |
| 150 mL   | beer                                                                           |
| 2        | yukon gold [potatoes]({{< relref "../items/russet_potato.md" >}})              |
| 1/2 cup  | [sunflower oil]({{< relref "../items/sunflower_oil.md" >}})                    |
| 2        | [shallots]({{< relref "../items/shallot.md" >}})                               |
| 1        | [lemon]({{< relref "../items/lemon.md" >}})                                    |
|          | [kosher salt]({{< relref "../items/kitchen_salt.md" >}})                       |
|          | [creme fraiche]({{< relref "../items/creme_fraiche.md" >}})                    |
|          | [mayonnaise]({{< relref "../items/mayonnaise.md" >}})                          |
|          | [gherkin pickles]({{< relref "../items/gherkin_pickle.md" >}})                 |
|          | [capers]({{< relref "../items/caper.md" >}})                                   |
|          | [parsley]({{< relref "../items/parsley.md" >}})                                |
|          | [tabasco sauce]({{< relref "../items/tabasco.md" >}}), optional                |


## Directions {#directions}

1.  Pour sunflower oil into a large saucepan
    1.  Heat to just below the smoking point
    2.  Preheat oven to 350 F
2.  Mix 1/2 cup of flour with baking soda and 1 tsp of curry powder
    1.  Add beer and whish together until smooth and runny
3.  Separate egg white into a small bowl
    1.  Whisk until peaks are stiff
    2.  Add a pinch of salt
    3.  Pour into batter, whisk together
4.  Mix 1/2 cup of flour with 1/2 tsp of curry powder, whisk in a large flat saucepan or bowl
    1.  Salt both sides of the filets
    2.  Dredge both side of the fish in the flour
    3.  Toss back and forth to remove excess flour
5.  Drop fish into the batter
    1.  Lift by the corners, flip from side to side to completely cover in batter
    2.  Lay into the hot pan with oil, away from you
    3.  Reduce heat, baste top of fish using a metal spoon, cooking for 2 min
6.  While fish is frying, square top and bottom of potatoes
    1.  Thinly slice into fries
    2.  Dry with paper towel to remove starch and moisture
    3.  Season with salt and pepper, set aside
7.  Lift fish to edge of the pan to gently flip over
    1.  Baste again, let cook for another 2 min
8.  Remove fish from oil, set aside on over-safe dish covered in paper towel
    1.  Place on top rack in the oven while potatoes fry
9.  Place first into oil, raise heat, cook for 4 min
    1.  Mix creme fraiche and mayo in a small bowl
    2.  Dice and add shallots, gherkins, and capers
    3.  Add a splash of tabasco sauce
    4.  Cut a slice of lemon, squeeze juice into tartar sauce
10. Remove fries from oil, place onto paper towel and rub dry
    1.  Remove fish from oven, salt with fries, and serve
    2.  Garnish with lemon wedge


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [Gordon Ramsay](https://www.youtube.com/watch?v=HrNLvCO2tE4)
2.  If the oil gets too hot, add a splash of room temperature oil, then remove from heat
