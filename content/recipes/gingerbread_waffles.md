+++
title = "Gingerbread waffles"
author = ["James Hawley"]
date = 2024-01-06T22:55:00-05:00
lastmod = 2024-06-12T19:14:27-04:00
tags = ["breakfasts", "recipes"]
draft = false
+++

| Info      | Amount    |
|-----------|-----------|
| Prep Time | 15 min    |
| Cook Time | 15 min    |
| Yields    | 2 waffles |


## Equipment {#equipment}

-   2 medium mixing bowls
-   1 small microwaveable bowl
-   1 whisk
-   1 waffle iron
-   measuring spoons
-   measuring cup


## Ingredients {#ingredients}

| Quantity | Item                                                        |
|----------|-------------------------------------------------------------|
| 1 cup    | [flour]({{< relref "../items/flour.md" >}})                 |
| 1 tsp    | [baking powder]({{< relref "../items/baking_powder.md" >}}) |
| 1/2 tsp  | [baking soda]({{< relref "../items/baking_soda.md" >}})     |
| 1/4 tsp  | [table salt]({{< relref "../items/table_salt.md" >}})       |
| 2 tsp    | [ground ginger]({{< relref "../items/ginger_powder.md" >}}) |
| 2 tsp    | [ground cinnamon]({{< relref "../items/cinnamon.md" >}})    |
| 1/2 tsp  | [allspice]({{< relref "../items/allspice.md" >}})           |
| 1/4 tsp  | [ground cloves]({{< relref "../items/clove.md" >}})         |
| 1/4 tsp  | [nutmeg]({{< relref "../items/nutmeg.md" >}})               |
| 1 cup    | [milk]({{< relref "../items/milk.md" >}})                   |
| 1        | [egg]({{< relref "../items/eggs.md" >}})                    |
| 3/2 Tbsp | [butter]({{< relref "../items/butter.md" >}}), melted       |


## Directions {#directions}

1.  Heat lightly oiled pan or griddle to medium-high
2.  In separate bowls, combine dry ingredients and wet ingredients, respectively
    1.  Add the dry bowl into the wet bowl, stirring slowly
    2.  Whisk together until batter is fully mixed and slightly lumpy
3.  Cook for ~ 3 min, until golden brown
4.  Add fruit, cinnamon, icing sugar, etc as toppings


## References &amp; Notes {#references-and-notes}

1.  Same recipe as this [pancakes and waffles]({{< relref "buttermilk_pancakes_waffles.md" >}}) recipe, with extra spices to create the gingerbread flavour
