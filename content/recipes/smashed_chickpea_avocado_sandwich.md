+++
title = "Smashed chickpea avocado sandwich"
author = ["James Hawley"]
date = 2019-12-07T00:00:00-05:00
lastmod = 2024-05-23T21:39:12-04:00
tags = ["sandwiches", "recipes", "vegetarian", "lunch", "entrees"]
draft = false
+++

| Info      | Amount       |
|-----------|--------------|
| Prep Time | 10 min       |
| Cook Time | 0 min        |
| Yields    | 2 sandwiches |


## Ingredients {#ingredients}

| Quantity  | Item                                                                |
|-----------|---------------------------------------------------------------------|
| 15 oz     | [chickpeas]({{< relref "../items/chickpea.md" >}}), drained, rinsed |
| 1         | large ripe [avocado]({{< relref "../items/avocado.md" >}})          |
| 2 tsp     | [lemon juice]({{< relref "../items/lemon_juice.md" >}})             |
| 1/4 cup   | [dried cranberries]({{< relref "../items/dried_cranberry.md" >}})   |
| 1 handful | [arugula]({{< relref "../items/arugula.md" >}})                     |
| 1/8       | [red onion]({{< relref "../items/red_onion.md" >}}), sliced         |
| 4 slices  | [French bread]({{< relref "../items/french_bread.md" >}})           |
|           | [kosher salt]({{< relref "../items/kitchen_salt.md" >}})            |
|           | [pepper]({{< relref "../items/black_pepper.md" >}})                 |


## Directions {#directions}

1.  In a medium bowl, smash chickpeas with a fork
    1.  Add in avocado, lemon juice, cranberries, and spinach and us a fork to mash everything together
    2.  Place in refrigerator until ready to serve
2.  Toast bread, spoon chickpea mix into each sandwich
3.  Top with red onion, cut sandwiches in half


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [Ambitious Kitchen](https://www.ambitiouskitchen.com/smashed-chickpea-avocado-salad-sandwich-with-cranberries-lemon/)
