+++
title = "Mediterranean salad"
author = ["James Hawley"]
date = 2017-03-18T00:00:00-04:00
lastmod = 2024-05-23T21:43:17-04:00
tags = ["salads", "recipes", "vegetarian", "entrees"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 10 min     |
| Cook Time | 0 min      |
| Yields    | 4 servings |


## Ingredients {#ingredients}

| Quantity | Item                                                                             |
|----------|----------------------------------------------------------------------------------|
| 1        | [cucumber]({{< relref "../items/cucumber.md" >}}), diced                         |
| 1/2      | [red onion]({{< relref "../items/red_onion.md" >}}), diced                       |
| 15 oz    | [bean medley]({{< relref "../items/bean_medley.md" >}})                          |
| 1        | [tomato]({{< relref "../items/beefsteak_tomato.md" >}}), diced                   |
| 1/4 cup  | [kalamata olives]({{< relref "../items/kalamata_olive.md" >}}), pitted and diced |
| 50 g     | [feta]({{< relref "../items/feta.md" >}})                                        |
| 1/2      | [lemon]({{< relref "../items/lemon.md" >}})                                      |
| 2 Tbsp   | [olive oil]({{< relref "../items/olive_oil.md" >}})                              |
| 1 Tbsp   | [balsamic vinegar]({{< relref "../items/balsamic_vinegar.md" >}})                |


## Directions {#directions}

1.  Mix all ingredients except olive oil and vinegar in a medium sized bowl
2.  Zest and juice the lemon
3.  Dress with olive oil and vinegar before serving
