+++
title = "Coleslaw dressing"
author = ["James Hawley"]
date = 2024-03-02T16:55:00-05:00
lastmod = 2024-04-02T00:07:56-04:00
tags = ["sauces", "recipes"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 5 min      |
| Cook Time | 0 min      |
| Yields    | 4 servings |


## Equipment {#equipment}

-   small mixing bowl or container
-   small whisk
-   measuring cups and spoons


## Ingredients {#ingredients}

| Quantity | Item                                                        |
|----------|-------------------------------------------------------------|
| 1/2 cup  | [mayonnaise]({{< relref "../items/mayonnaise.md" >}})       |
| 1 Tbsp   | [honey]({{< relref "../items/honey.md" >}})                 |
| 1 Tbsp   | [lemon juice]({{< relref "../items/lemon_juice.md" >}})     |
| 1 Tbsp   | [Dijon mustard]({{< relref "../items/dijon_mustard.md" >}}) |
|          | [table salt]({{< relref "../items/table_salt.md" >}})       |
|          | [black pepper]({{< relref "../items/black_pepper.md" >}})   |


## Directions {#directions}

1.  Whisk all ingredients together in a small bowl, seasoning to taste.
2.  Drizzle over coleslaw before serving, and leave covered in the fridge.


## References {#references}

1.  Original recipe: [Kristine's Kitchen Blog](https://kristineskitchenblog.com/wprm_print/37429)
