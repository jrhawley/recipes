+++
title = "Farfalle, tomato, feta, and balsamic"
author = ["James Hawley"]
date = 2013-09-22T00:00:00-04:00
lastmod = 2024-05-23T21:46:11-04:00
tags = ["pastas", "recipes", "vegetarian", "entrees"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 10 min     |
| Cook Time | 15 min     |
| Yields    | 4 servings |


## Ingredients {#ingredients}

| Quantity | Item                                                                |
|----------|---------------------------------------------------------------------|
| 6 oz     | [farfalle]({{< relref "../items/farfalle.md" >}}), uncooked         |
| 2 cups   | [cherry tomato]({{< relref "../items/cherry_tomato.md" >}}), halved |
| 1 cup    | seedless green [grapes]({{< relref "../items/grape.md" >}}), halved |
| 1/3 cup  | fresh [basil]({{< relref "../items/basil.md" >}}), thinly sliced    |
| 2 Tbsp   | [balsamic vinegar]({{< relref "../items/balsamic_vinegar.md" >}})   |
| 2 Tbsp   | [shallots]({{< relref "../items/shallot.md" >}}), chopped           |
| 2 tsp    | [capers]({{< relref "../items/caper.md" >}})                        |
| 1 tsp    | [dijon mustard]({{< relref "../items/dijon_mustard.md" >}})         |
| 1 clove  | [garlic]({{< relref "../items/garlic.md" >}}). minced               |
| 4 oz     | [feta]({{< relref "../items/feta.md" >}}), crumbled                 |
|          | [kosher salt]({{< relref "../items/kitchen_salt.md" >}})            |
|          | [pepper]({{< relref "../items/black_pepper.md" >}})                 |
|          | [olive oil]({{< relref "../items/olive_oil.md" >}})                 |


## Directions {#directions}

1.  Cook pasta according to directions, omitting salt and fat
    1.  Drain, combine with tomatoes, grapes, and basil in large bowl
2.  While pasta cooks, combine vinegar and following 6 ingredients in small bowl
3.  Gradually add oil to vinegar, stirring constantly
4.  Drizzle vinaigrette over pasta, and toss well to coat
