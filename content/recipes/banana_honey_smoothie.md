+++
title = "Banana honey smoothie"
author = ["James Hawley"]
date = 2016-02-28T00:00:00-05:00
lastmod = 2023-11-12T20:59:13-05:00
tags = ["smoothies", "recipes", "beverages"]
draft = false
+++

| Info      | Amount |
|-----------|--------|
| Prep Time | 5 min  |
| Cook Time | 2 min  |
| Serves    | 4      |


## Ingredients {#ingredients}

| Quantity | Item                                                                    |
|----------|-------------------------------------------------------------------------|
| 2        | [bananas]({{< relref "../items/banana.md" >}}) (or 3/2 cups of berries) |
| 1 cup    | vanilla [yogurt]({{< relref "../items/yogurt.md" >}})                   |
| 3/2 cup  | [milk]({{< relref "../items/milk.md" >}})                               |
| 1/2 Tbsp | ground [cinnamon]({{< relref "../items/cinnamon.md" >}})                |
| 1/2 Tbsp | ground [nutmeg]({{< relref "../items/nutmeg.md" >}})                    |
| 4 Tbsp   | [honey]({{< relref "../items/honey.md" >}})                             |
| 1 cup    | ice cubes                                                               |
| 1 tsp    | [vanilla extract]({{< relref "../items/vanilla_extract.md" >}})         |


## Directions {#directions}

1.  Blend all ingredients, serve
