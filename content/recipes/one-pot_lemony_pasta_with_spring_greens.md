+++
title = "One-pot lemony pasta with spring greens"
author = ["James Hawley"]
date = 2023-04-15T12:17:00-04:00
lastmod = 2023-12-28T01:20:38-05:00
tags = ["entrees", "pastas", "vegetarian", "recipes"]
draft = false
+++

| Info      | Amount         |
|-----------|----------------|
| Prep Time | 10 min         |
| Cook Time | 10 min         |
| Yields    | 4 - 6 servings |


## Ingredients {#ingredients}

| Quantity | Item                                                                     |
|----------|--------------------------------------------------------------------------|
| 454 g    | rigatone or other [pasta]({{< relref "../items/pasta.md" >}})            |
| 4 cup    | [spinach]({{< relref "../items/spinach.md" >}})                          |
| 4 cup    | [kale]({{< relref "../items/kale.md" >}}), stemmed                       |
| 1 bunch  | [asparagus]({{< relref "../items/asparagus.md" >}}), trimmed and chopped |
| 1/4 cup  | [olive oil]({{< relref "../items/olive_oil.md" >}})                      |
| 4 Tbsp   | [butter]({{< relref "../items/butter.md" >}})                            |
| 4 cloves | [garlic]({{< relref "../items/garlic.md" >}}), minced                    |
| 2 Tbsp   | [lemon zest]({{< relref "../items/lemon.md" >}})                         |
| 1 tsp    | [red pepper flakes]({{< relref "../items/red_pepper_flake.md" >}})       |
|          | [Parmesan]({{< relref "../items/parmesan.md" >}})                        |


## Directions {#directions}

1.  Bring a medium pot of salted water to a boil and cook the pasta for 5 min.
    1.  Add spinach, kale, and asparagus, cooking for another 1 min.
    2.  Reserve 3/4 cup of the pasta water, then drain pasta and greens.
2.  Place pot back on the stove over medium heat.
    1.  Add olive oil and butter, and garlic once hot.
    2.  Cook garlic until golden brown, about 2 min.
    3.  Add 1/2 cup pasta water, lemon zest, chili flakes, pasta, and greens, and stir to combine.
    4.  Increase heat to high and cool until sauce has thickened slightly and coats pasta.
    5.  Add remaining past water as needed to keep the sauce smooth.
3.  Remove pasta from heat, stir in lemon juice, season with salt and top with grated Parmesan.


## References {#references}

1.  Original recipe: LCBO
