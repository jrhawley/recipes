+++
title = "Date balls"
author = ["James Hawley"]
date = 2023-02-04T11:42:00-05:00
lastmod = 2023-11-05T16:16:40-05:00
tags = ["vegan", "snacks", "recipes"]
draft = false
+++

| Info      | Amount   |
|-----------|----------|
| Prep Time | 20 min   |
| Cook Time | 0 min    |
| Yields    | 16 bites |


## Ingredients {#ingredients}

| Quantity | Item                                                                        |
|----------|-----------------------------------------------------------------------------|
| 6        | [Medjool dates]({{< relref "../items/medjool_date.md" >}})                  |
| 1/2 cup  | [natural peanut butter]({{< relref "../items/natural_peanut_butter.md" >}}) |
| 1/3 cup  | [oats]({{< relref "../items/oat.md" >}})                                    |
| 2 Tbsp   | [chia seeds]({{< relref "../items/chia_seed.md" >}})                        |
| 3 Tbsp   | [maple syrup]({{< relref "../items/maple_syrup.md" >}})                     |
| 1/2 tsp  | [ground cinnamon]({{< relref "../items/cinnamon.md" >}})                    |
| pinch    | [Kosher salt]({{< relref "../items/kitchen_salt.md" >}})                    |
| 2 Tbsp   | hot [water]({{< relref "../items/water.md" >}})                             |


## Directions {#directions}

1.  Remove seeds from dates and chop into small pieces.
2.  Place dates in a large bowl with all other ingredients and mix with a wooden spoon.
3.  Using your hands, press into a crumbly dough&nbsp;[^fn:1], and shape the mixture into small balls.
4.  Place into the fridge for 10 min to set, then store in the fridge.


## References {#references}

1.  Original recipe: [Nourish Everyday](https://nourisheveryday.com/wprm_print/10184)

[^fn:1]: If the mixture seems too wet, add more protein powder.