+++
title = "Crispy salmon"
author = ["James Hawley"]
date = 2022-03-12T00:00:00-05:00
lastmod = 2024-05-23T21:15:23-04:00
tags = ["fish", "recipes", "entrees"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 0 min      |
| Cook Time | 10 min     |
| Yields    | 2 servings |


## Ingredients {#ingredients}

| Quantity | Item                                                |
|----------|-----------------------------------------------------|
| 1 filet  | [salmon]({{< relref "../items/salmon.md" >}})       |
|          | [olive oil]({{< relref "../items/olive_oil.md" >}}) |


## Directions {#directions}

1.  Pat filet dry, thoroughly, and salt generously.
2.  In an oven-safe skillet at medium-high heat, add 1 - 2 Tbsp.
3.  Place flesh side down into the pan, cook for 5 min.
    1.  Flip, cook for another 1 - 2 min.
4.  Transfer to oven on low broil, placing on the lowest oven rack.
    1.  Cook for 6 min.
