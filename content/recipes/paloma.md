+++
title = "Paloma"
author = ["James Hawley"]
date = 2023-04-25T10:02:00-04:00
lastmod = 2023-12-21T22:19:38-05:00
tags = ["alcohols", "recipes", "beverages"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 2 min      |
| Cook Time | 0 min      |
| Yields    | 1 cocktail |


## Ingredients {#ingredients}

| Quantity | Item                                                              |
|----------|-------------------------------------------------------------------|
| 2 oz     | [tequila]({{< relref "../items/tequila.md" >}})                   |
| 2 oz     | [grapefruit juice]({{< relref "../items/grapefruit_juice.md" >}}) |
| 2 oz     | [sparkling water]({{< relref "../items/sparkling_water.md" >}})   |
| 1/2 oz   | [lime juice]({{< relref "../items/lime_juice.md" >}})             |
| 1/4 oz   | [simple syrup]({{< relref "simple_syrup.md" >}})                  |
|          | ice                                                               |
|          | [Kosher salt]({{< relref "../items/kitchen_salt.md" >}})          |


## Directions {#directions}

1.  Salt the rim of the glass by rubbing a grapefruit wedge (or other citrus) around the rim and running the glass through a plate of salt.
2.  Add all ingredients to a tumbler and shake to combine.
3.  Pour the drink into a cocktail glass and fill with ice.
4.  Add sugar/simple syrup to desired sweetness and serve with the grapefruit wedge as a garnish.


## References {#references}

1.  Original recipe: [Love and Lemons](https://loveandlemons.com/wprm_print/recipe/47109)
