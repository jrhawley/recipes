+++
title = "Roasted chickpea stuffed avocadoes"
author = ["James Hawley"]
date = 2017-08-22T00:00:00-04:00
lastmod = 2024-05-23T21:42:43-04:00
tags = ["bowls", "recipes", "vegetarian", "entrees"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 10 min     |
| Cook Time | 20 min     |
| Yields    | 2 servings |

{{< figure src="../assets/roasted-chickpeas-stuff-avocado.jpg" caption="<span class=\"figure-number\">Figure 1: </span>Roasted chickpeas stuff avocado" >}}


## Ingredients {#ingredients}


### Roasted chickpeas {#roasted-chickpeas}

| Quantity | Item                                                                |
|----------|---------------------------------------------------------------------|
| 15 oz    | [chickpeas]({{< relref "../items/chickpea.md" >}}), drained, rinsed |
| 1 Tbsp   | [olive oil]({{< relref "../items/olive_oil.md" >}})                 |
| 2 tsp    | smoked [paprika]({{< relref "../items/paprika.md" >}})              |
| 1 tsp    | [pepper]({{< relref "../items/black_pepper.md" >}})                 |
| 1/2 tsp  | [cayenne pepper]({{< relref "../items/cayenne_pepper.md" >}})       |
| 1/2 tsp  | [table salt]({{< relref "../items/table_salt.md" >}})               |
| 4        | ripe [avocadoes]({{< relref "../items/avocado.md" >}})              |
| 1        | [lemon]({{< relref "../items/lemon.md" >}})                         |


### Yogurt sauce {#yogurt-sauce}

| Quantity | Item                                                            |
|----------|-----------------------------------------------------------------|
| 1/4 cup  | plain [Greek yogurt]({{< relref "../items/greek_yogurt.md" >}}) |
| 1 clove  | [garlic]({{< relref "../items/garlic.md" >}}), minced           |
| 2 Tbsp   | [parsley]({{< relref "../items/parsley.md" >}}), chopped        |
|          | [pepper]({{< relref "../items/black_pepper.md" >}})             |


## Directions {#directions}

1.  Preheat oven to 400 F
2.  Pour chickpeas onto paper towels to pat dry, rolling to remove skins
    1.  In a medium sized bowl, mix chickpeas, olive oil, and spices
    2.  Spread chickpeas onto parchment lined baking sheet
    3.  Cook for 20 min in the oven
3.  Combine all dressing ingredients in a small bowl
4.  When chickpeas have finished cooking, slice avocados in half, fill with chickpeas, drizzle with lemon juice, and sprinkle with salt and pepper
5.  Serve in a bowl with diced tomato and a dollop of the yogurt sauce
