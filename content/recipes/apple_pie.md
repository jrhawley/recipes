+++
title = "Apple pie"
author = ["James Hawley"]
date = 2018-12-18T00:00:00-05:00
lastmod = 2023-12-19T21:43:06-05:00
tags = ["pies", "recipes", "desserts"]
draft = false
+++

| Info      | Amount  |
|-----------|---------|
| Prep Time | 1 h     |
| Bake Time | 50 min  |
| Yields    | 13" pie |

{{< figure src="../assets/apple-pie.jpg" caption="<span class=\"figure-number\">Figure 1: </span>Apple pie" >}}


## Ingredients {#ingredients}


### Pastry {#pastry}

| Quantity | Item                                                            |
|----------|-----------------------------------------------------------------|
| 2 cup    | [shortening]({{< relref "../items/vegetable_shortening.md" >}}) |
| 8 cup    | [flour]({{< relref "../items/flour.md" >}})                     |
| 4 tsp    | [table salt]({{< relref "../items/table_salt.md" >}})           |


### Filling {#filling}

| Quantity | Item                                                      |
|----------|-----------------------------------------------------------|
| 6        | granny smith [apples]({{< relref "../items/apple.md" >}}) |
| 1 cup    | [sugar]({{< relref "../items/white_sugar.md" >}})         |
| 2 Tbsp   | [flour]({{< relref "../items/flour.md" >}})               |
| 1 tsp    | [cinnamon]({{< relref "../items/cinnamon.md" >}})         |
| 1/4 tsp  | [nutmeg]({{< relref "../items/nutmeg.md" >}})             |
| 1/4 tsp  | [allspice]({{< relref "../items/allspice.md" >}})         |
| 1/4 tsp  | ground [cloves]({{< relref "../items/garlic.md" >}})      |
| dash     | [table salt]({{< relref "../items/table_salt.md" >}})     |
| 2 Tbsp   | [butter]({{< relref "../items/butter.md" >}})             |


## Directions {#directions}

1.  Sift flour and salt
    1.  Cut shortening into small chunks, blend into mixture
    2.  Can store in covered container in fridge for months
    3.  Makes enough for 4 pie crusts
2.  Take 4 cups of pastry mix and add 5-6 Tbsp of water
    1.  Blend until dry spots disappear and will hold into a large ball
    2.  Knead dough
3.  Make into 2 even balls, roll out on floured surface to the size of your pie plate
    1.  Generously flour the pie plate to stop the dough from binding while baking
    2.  Press first pie crust into the plate, slowly
4.  Pare apples and thinly slice
5.  Combine sugar, flour, spices, and salt
    1.  Mix with sliced apples and pour into pie plate
    2.  Dot with butter
6.  Place top pie crust on and crimp edges with fingers
    1.  Place small slits into the top and sprinkle with sugar
7.  Bake at 400 F for 50 min, or until nicely browned


## References &amp; Notes {#references-and-notes}

1.  Be sure to make pie crust first, so apple mixture doesn't become runny before you place it in the pie
2.  You can make a weave top instead of a pull top by slicing the top crust into strips, then weaving and crimping around the edges
3.  Takes around 1h prep time if starting from scratch, 10-20 min less if pastry mix is already made
4.  Don't butter pan, put flour around to make it non-stick
