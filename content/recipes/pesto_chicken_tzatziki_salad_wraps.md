+++
title = "Pesto chicken tzatziki salad wraps"
author = ["James Hawley"]
date = 2023-03-28T18:18:00-04:00
lastmod = 2023-12-28T01:20:56-05:00
tags = ["entrees", "chicken", "wraps", "recipes"]
draft = false
+++

| Info      | Amount  |
|-----------|---------|
| Prep Time | 30 min  |
| Cook Time | 30 min  |
| Yields    | 6 wraps |


## Ingredients {#ingredients}


### Tzatziki marinade and chicken {#tzatziki-marinade-and-chicken}

| Quantity | Item                                                               |
|----------|--------------------------------------------------------------------|
| 1/3 cup  | [Greek yogurt]({{< relref "../items/greek_yogurt.md" >}})          |
| 1/4 cup  | [olive oil]({{< relref "../items/olive_oil.md" >}})                |
| 3/2 lbs  | [chicken breasts]({{< relref "../items/chicken_breast.md" >}})     |
| 4 cloves | [garlic]({{< relref "../items/garlic.md" >}})                      |
| 1 Tbsp   | [paprika]({{< relref "../items/paprika.md" >}})                    |
| 1 Tbsp   | [parsley]({{< relref "../items/parsley.md" >}}), chopped           |
|          | [red pepper flakes]({{< relref "../items/red_pepper_flake.md" >}}) |
|          | [Kosher salt]({{< relref "../items/kitchen_salt.md" >}})           |
|          | [black pepper]({{< relref "../items/black_pepper.md" >}})          |


### Wraps {#wraps}

| Quantity | Item                                                            |
|----------|-----------------------------------------------------------------|
| 4 cup    | [romaine lettuce]({{< relref "../items/romaine_lettuce.md" >}}) |
| 2        | [avocadoes]({{< relref "../items/avocado.md" >}})               |
| 1/2 cup  | [feta]({{< relref "../items/feta.md" >}})                       |
| 4        | [tortillas]({{< relref "../items/tortilla.md" >}})              |
| 1/4 cup  | [pesto]({{< relref "../items/pesto.md" >}})                     |


## Directions {#directions}

1.  Preheat the oven to 220 C (425 F).
2.  In a bowl, combine the chicken and marinade ingredients and let marinade for 15 min at room temperature.
3.  Arrange chicken breasts on a baking sheet and cook for 25 min, or until cooked through.
4.  Mix lettuce, avocadoes, feta in a bowl while the chicken is cooking.
5.  Turn on the broiler and broil for 1 - 2 min, or until the chicken edges start to char.
6.  Remove the chicken and chop into large chunks.
7.  Spread pesto and lettuce mixture on each tortilla, and add the chicken and tzatziki.
8.  If desired, wrap in alumium foil and place in the oven or in a panini press to sear the outside of the wraps.


## References {#references}

1.  Original recipe: [Half-Baked Harvest](https://www.halfbakedharvest.com/wprm_print/143715)
