+++
title = "Black bean quinoa falafel"
author = ["James Hawley"]
date = 2020-09-24T00:00:00-04:00
lastmod = 2024-05-23T21:28:43-04:00
tags = ["recipes", "vegetarian", "entrees"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 15 min     |
| Cook Time | 45 min     |
| Yields    | 12 falafel |


## Ingredients {#ingredients}

| Quantity | Item                                                                           |
|----------|--------------------------------------------------------------------------------|
| 1 cup    | cooked [quinoa]({{< relref "../items/quinoa.md" >}}), cooled                   |
| 15 oz    | [black beans]({{< relref "../items/black_bean.md" >}}), rinsed, drained, dried |
| 1/4 cup  | [pumpkin seeds]({{< relref "../items/pumpkin_seed.md" >}})                     |
| 5 cloves | [garlic]({{< relref "../items/garlic.md" >}}), minced                          |
| 1/2 tsp  | [table salt]({{< relref "../items/table_salt.md" >}})                          |
| 1 tsp    | ground [cumin]({{< relref "../items/cumin.md" >}})                             |
| 2 Tbsp   | [tomato paste]({{< relref "../items/tomato_paste.md" >}})                      |


## Directions {#directions}

1.  Prepare quinoa according to instructions, preheat oven to 350 F
2.  Add black beans to parchment-lined baking sheet
    1.  Bake for 15 min until beans crack
    2.  Remove from oven, increase temperature to 375 F
3.  Add black beans to food processor with pumpkin seeds and garlic
    1.  Pulse into a loose meal
    2.  Add quinoa and all remaining ingredients
    3.  Blend to combine until a textured dough forms
4.  Scoop 3/2 Tbsp at a time, form into small discs
    1.  Place on parchment-lined baking sheet
    2.  Repeat for entire dough mixture
5.  Bake for 15 min
    1.  Flip and back for another 10-15 min
6.  Garnish with chili garlic (optional) and serve with hummus or baba ghanoush


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [Minimalist Baker](https://minimalistbaker.com/baked-quinoa-black-bean-falafel/)
