+++
title = "Elderflower gin sour"
author = ["James Hawley"]
date = 2023-04-22T13:54:00-04:00
lastmod = 2023-12-21T21:43:06-05:00
tags = ["alcohols", "recipes", "beverages"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 2 min      |
| Cook Time | 0 min      |
| Yields    | 1 cocktail |


## Ingredients {#ingredients}

| Quantity | Item                                                    |
|----------|---------------------------------------------------------|
| 2 oz     | [gin]({{< relref "../items/gin.md" >}})                 |
| 3/2 oz   | elderflower liqueur or elderflower syrup                |
| 1 oz     | [lemon juice]({{< relref "../items/lemon_juice.md" >}}) |
|          | ice                                                     |


## Directions {#directions}

1.  Combine all ingredients in a tumbler with ice and shake.
2.  Pour into a chilled cocktail glass and serve.


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [Mint and Twist](https://mintandtwist.com/wp-json/mv-create/v1/creations/15/print)
