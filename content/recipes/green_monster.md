+++
title = "Green monster"
author = ["James Hawley"]
date = 2023-03-18T13:32:00-04:00
lastmod = 2023-12-21T22:00:06-05:00
tags = ["smoothies", "recipes", "beverages"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 25 min     |
| Cook Time | 30 min     |
| Yields    | 1 smoothie |


## Ingredients {#ingredients}

| Quantity | Item                                                                                                    |
|----------|---------------------------------------------------------------------------------------------------------|
| 1 cup    | [almond milk]({{< relref "../items/almond_milk.md" >}})                                                 |
| 1 cup    | [kale]({{< relref "../items/kale.md" >}}) or [spinach]({{< relref "../items/spinach.md" >}}), destemmed |
| 1        | [banana]({{< relref "../items/banana.md" >}})                                                           |
| 2 - 3    | ice cubes                                                                                               |
| 1 Tbsp   | [peanut butter]({{< relref "../items/peanut_butter.md" >}})                                             |
| 1 Tbsp   | [chia seeds]({{< relref "../items/chia_seed.md" >}})                                                    |
| 1/4 tsp  | [vanilla extract]({{< relref "../items/vanilla_extract.md" >}})                                         |
|          | [ground cinnamon]({{< relref "../items/cinnamon.md" >}})                                                |


## Directions {#directions}

1.  Blend all ingredients together in a blender until smooth


## References {#references}

1.  Original recipe: [The Oh She Glows Cookbook]({{< relref "../references/Oh_She_Glows_Cookbook.md" >}})
