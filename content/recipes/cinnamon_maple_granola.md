+++
title = "Cinnamon maple granola"
author = ["James Hawley"]
date = 2019-05-21T00:00:00-04:00
lastmod = 2024-06-12T19:13:38-04:00
tags = ["breakfasts", "recipes", "cereal", "granola"]
draft = false
+++

| Info      | Amount  |
|-----------|---------|
| Prep Time | 5 mins  |
| Cook Time | 35 mins |
| Yields    | 4 cups  |

{{< figure src="../assets/cinnamon-maple-granola.jpg" caption="<span class=\"figure-number\">Figure 1: </span>Cinnamon maple granola" >}}


## Ingredients {#ingredients}

| Quantity | Item                                                            |
|----------|-----------------------------------------------------------------|
| 4 cups   | [oats]({{< relref "../items/oat.md" >}})                        |
| 2 tsp    | [cinnamon]({{< relref "../items/cinnamon.md" >}})               |
| 1/2 cup  | [pure maple syrup]({{< relref "../items/maple_syrup.md" >}})    |
| 1/3 cup  | [olive oil]({{< relref "../items/olive_oil.md" >}})             |
| 3/2 tsp  | [vanilla extract]({{< relref "../items/vanilla_extract.md" >}}) |


## Directions {#directions}

1.  Preheat oven to 325 F
    -   Prepare a large rimmed baking sheet with parchment paper or a silicone baking mat.
2.  In a large bowl, combine all ingredients. Stir to coat well.
3.  Transfer oatmeal mixture to prepared baking sheet.
    -   Spread out into an even layer, pressing down to compress the mixture.
4.  Bake for 35-40 minutes
    -   Remove from oven and cool, untouched, for 45 minutes
    -   Break into pieces
    -   Store in an airtight container for up to 2 weeks.


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [Baked By Rachel](https://www.bakedbyrachel.com/cinnamon-maple-granola/)
