+++
title = "Dark and stormy"
author = ["James Hawley"]
date = 2022-03-13T00:00:00-05:00
lastmod = 2023-11-20T09:31:41-05:00
tags = ["recipes", "alcohols", "beverages"]
draft = false
+++

## Ingredients {#ingredients}

| Quantity | Ingredient                                                         |
|----------|--------------------------------------------------------------------|
| 5/3 oz   | [black rum]({{< relref "../items/black_rum.md" >}})                |
| 1/2 oz   | [lime juice]({{< relref "../items/lime_juice.md" >}})              |
| 5 oz     | [ginger beer]({{< relref "../items/fever_tree_ginger_beer.md" >}}) |


## Directions {#directions}

1.  Fill a highball glass with ice.
2.  Pour the ginger beer, leaving a small window for the lime and rum.
3.  Squeeze in the lime juice, then slowly pour over the dark rum to float it on top.


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [The Periodic Table of Cocktails]({{< relref "../references/Periodic_Table_of_Cocktails.md" >}})
