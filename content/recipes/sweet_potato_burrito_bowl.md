+++
title = "Sweet potato burrito bowl"
author = ["James Hawley"]
date = 2021-03-03T00:00:00-05:00
lastmod = 2024-05-23T21:20:21-04:00
tags = ["vegetarian", "bowls", "recipes", "entrees", "vegan"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 10 min     |
| Cook Time | 30 min     |
| Yields    | 4 servings |

{{< figure src="../assets/sweet-potato-burrito-bowl.jpg" caption="<span class=\"figure-number\">Figure 1: </span>Sweet potato burrito bowl" >}}


## Ingredients {#ingredients}

| Quantity | Item                                                      |
|----------|-----------------------------------------------------------|
| 1        | large [sweet potato]({{< relref "../items/yam.md" >}})    |
| 2        | [bell peppers]({{< relref "../items/bell_pepper.md" >}})  |
| 1        | small [red onion]({{< relref "../items/red_onion.md" >}}) |
| 1 tsp    | [paprika]({{< relref "../items/paprika.md" >}})           |
| 2 Tbsp   | [olive oil]({{< relref "../items/olive_oil.md" >}})       |
| 1/2 cup  | [rice]({{< relref "../items/basmati_rice.md" >}})         |
| 14 oz    | [black beans]({{< relref "../items/black_bean.md" >}})    |
| 1 cup    | cooked [corn]({{< relref "../items/corn_niblet.md" >}})   |
| 1        | [avocado]({{< relref "../items/avocado.md" >}})           |
|          | [kosher salt]({{< relref "../items/kitchen_salt.md" >}})  |
|          | [pepper]({{< relref "../items/black_pepper.md" >}})       |


## Directions {#directions}

1.  Preheat oven to 400 F
2.  Chop sweet potato into chunks
    1.  Slice peppers and onion into long, thin strips
    2.  Toss vegetables with olive oil, paprika, salt, and pepper
    3.  Place on a lined baking sheet and cook for 25 - 30 min, until potatoes are tender
3.  While the vegetables are roasting, cook the rice
4.  With 5 min left on the vegetables, drain and rinse black beans
    1.  Transfer to a pan, add salt, pepper, paprika, corn, and olive oil to heat up
5.  Mix black beans with roasted vegetables and serve on a bed of rice in each bowl
6.  Garnish with sliced avocado and a dressing of olive oil, lemon juice, and minced garlic


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [Eat With Clarity](https://eatwithclarity.com/wprm_print/7645)
