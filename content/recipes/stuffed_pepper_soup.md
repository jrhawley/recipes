+++
title = "Stuffed pepper soup"
author = ["James Hawley"]
date = 2023-03-25T21:15:00-04:00
lastmod = 2023-12-28T01:00:37-05:00
tags = ["entrees", "vegetarian", "vegan", "soups", "recipes"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 10 min     |
| Cook Time | 15 min     |
| Yields    | 6 servings |


## Equipment {#equipment}

-   large cutting board
-   prep knife
-   Dutch oven
-   spatula
-   soup spoon


## Ingredients {#ingredients}

| Quantity | Item                                                                                                                               |
|----------|------------------------------------------------------------------------------------------------------------------------------------|
| 15 oz    | [black beans]({{< relref "../items/black_bean.md" >}}) [^fn:1]                                                                     |
| 1        | [yellow onion]({{< relref "../items/sweet_onion.md" >}})                                                                           |
| 2 cloves | [garlic]({{< relref "../items/garlic.md" >}}), minced                                                                              |
| 4 cup    | [vegetable broth]({{< relref "../items/vegetable_broth.md" >}})                                                                    |
| 3 - 4    | [bell peppers]({{< relref "../items/bell_pepper.md" >}}), diced                                                                    |
| 15 oz    | [diced tomatoes]({{< relref "../items/diced_tomatoes.md" >}})                                                                      |
| 5 oz     | [tomato paste]({{< relref "../items/tomato_paste.md" >}})                                                                          |
| 1 Tbsp   | [Italian seasoning]({{< relref "../items/italian_seasoning.md" >}})                                                                |
| 1 tsp    | [Kosher salt]({{< relref "../items/kitchen_salt.md" >}})                                                                           |
| 1/2 cup  | [Jasmine rice]({{< relref "../items/jasmine_rice.md" >}})                                                                          |
| 1 tsp    | [apple cider vinegar]({{< relref "../items/apple_cider_vinegar.md" >}}) or [lemon juice]({{< relref "../items/lemon_juice.md" >}}) |


## Directions {#directions}

1.  Heat a large skillet or Dutch oven on medium high heat.
    1.  Add the black beans, onion, and garlic and sauté until onion is transluscent.
2.  Deglaze the pan by adding about 1/4 cup of broth and use a spatula to scrape off any brown bits stuck to the bottom.
3.  Add the rest of the broth, then layer on the bell peppers, diced tomatoes, tomato paste, Italian seasoning and salt.
    1.  Add the rice and stir to combine
4.  Cover, reduce heat, and simmer for 25 - 30 min.
5.  Remove from heat and let cool for 5 min
    1.  Mix in the apple cider vinegar/lemon juice and season with more salt to taste
    2.  Serve topped with chopped parsley and/or finely grated parmesan


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [The Recipe Well](https://therecipewell.com/wprm_print/1029)
2.  Store any leftovers in a sealed container in the refrigerator and eat within 4 days.

[^fn:1]: You can replace this with 454 g of [ground beef]({{< relref "../items/ground_beef.md" >}}) if you'd like to increase the protein and fat content and don't need this to be vegetarian/vegan.