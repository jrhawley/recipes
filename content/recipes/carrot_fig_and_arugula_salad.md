+++
title = "Carrot fig and arugula salad"
author = ["James Hawley"]
date = 2024-06-07T17:50:00-04:00
lastmod = 2024-06-07T18:46:53-04:00
tags = ["sides", "entrees", "salads", "recipes"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 20 min     |
| Cook Time | 20 min     |
| Yields    | 4 servings |


## Equipment {#equipment}


## Ingredients {#ingredients}


### Salad {#salad}

| Quantity | Item                                                                                                                                                                                                 |
|----------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 4        | large [carrots]({{< relref "../items/carrot.md" >}}), cut vertically in half                                                                              |
| 1 Tbsp   | [avocado oil]({{< relref "../items/avocado_oil.md" >}}) or [olive oil]({{< relref "../items/olive_oil.md" >}}) |
|          | [kosher salt]({{< relref "../items/kitchen_salt.md" >}})                                                                                                  |
|          | [black pepper]({{< relref "../items/black_pepper.md" >}})                                                                                                 |
| 5 oz     | [arugula]({{< relref "../items/arugula.md" >}})                                                                                                           |
| 3/4 cup  | dried [fig]({{< relref "../items/fig.md" >}}) halves                                                                                                      |
| 1/3 cup  | [walnut]({{< relref "../items/walnut.md" >}}) halves                                                                                                      |
| 1/3 cup  | [feta]({{< relref "../items/feta.md" >}})                                                                                                                 |
| 1        | [avocado]({{< relref "../items/avocado.md" >}}), sliced                                                                                                   |
| 15 oz    | [chickpeas]({{< relref "../items/chickpea.md" >}})                                                                                                        |


### Dressing {#dressing}

| Quantity | Item                                                                                                   |
|----------|--------------------------------------------------------------------------------------------------------|
| 1/4 cup  | [tahini]({{< relref "../items/tahini.md" >}})               |
| 2 Tbsp   | [lemon juice]({{< relref "../items/lemon_juice.md" >}})     |
| 1 tsp    | [maple syrup]({{< relref "../items/maple_syrup.md" >}})     |
| 1 tsp    | [Dijon mustard]({{< relref "../items/dijon_mustard.md" >}}) |
|          | [table salt]({{< relref "../items/table_salt.md" >}})       |
|          | [black pepper]({{< relref "../items/black_pepper.md" >}})   |


## Directions {#directions}

1.  Preheat a grill to medium heat the oven to 400 F.
    1.  Grill or roast the carrots for 15 - 25 min, until tender and charring.
2.  Toast the walnuts in a dry skillet over medium heat until fragrant and set aside.
    1.  Add the rinsed and drained chickpeas with olive oil and any seasonings, then fry on low heat.
3.  Mix all dressing ingredients together in a small bowl and season to taste.
4.  When the carrots have finished roasting. cut them into large chunks, then assemble everything on a large plate and drizzle with the dressing.


## References {#references}

1.  Original recipe: [Ambitious Kitchen](https://www.ambitiouskitchen.com/wprm_print/73310)
