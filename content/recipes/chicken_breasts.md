+++
title = "Chicken breasts"
author = ["James Hawley"]
date = 2013-09-14T00:00:00-04:00
lastmod = 2024-05-23T21:52:03-04:00
tags = ["entrees", "chicken", "recipes"]
draft = false
+++

| Info      | Amount      |
|-----------|-------------|
| Prep Time | 5 min       |
| Cook Time | 20 min      |
| Yields    | 1-4 breasts |


## Ingredients {#ingredients}

| Quantity | Item                                                                              |
|----------|-----------------------------------------------------------------------------------|
| 4        | [chicken breasts]({{< relref "../items/chicken_breast.md" >}}), fresh, not frozen |
| 1/4 cup  | [flour]({{< relref "../items/flour.md" >}})                                       |
| 1 Tbsp   | [olive oil]({{< relref "../items/olive_oil.md" >}})                               |
| 1 tsp    | [butter]({{< relref "../items/butter.md" >}})                                     |
|          | [kosher salt]({{< relref "../items/kitchen_salt.md" >}})                          |
|          | [pepper]({{< relref "../items/black_pepper.md" >}})                               |


## Directions {#directions}

1.  Pound chicken breasts into an even thickness with the handle or flat of a knife
2.  Mix flour with a desired sprinkle of salt and pepper and the herbs, if available
    1.  Dredge the chicken breasts in the flour
3.  Heat frying pan over medium-high heat
    1.  When hot, add olive oil and butter, allow to melt and mix
4.  Reduce heat to medium and add dredged chicken breasts
    1.  Cook for 1 min until golden on one side, then flip
5.  Reduce heat to low, cover pan with a lid
    1.  Set a timer for 10 min and leave the chicken alone
6.  Flip chicken breasts, turn off heat, cover again with the lid and let rest for another 10 min
