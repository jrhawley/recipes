+++
title = "Bramble"
author = ["James Hawley"]
date = 2021-12-05T00:00:00-05:00
lastmod = 2023-12-19T22:07:33-05:00
tags = ["recipes", "beverages", "alcohols"]
draft = false
+++

## Ingredients {#ingredients}

| Quantity | Item                                                        |
|----------|-------------------------------------------------------------|
| 2 oz     | [gin]({{< relref "../items/gin.md" >}})                     |
| 1 oz     | [lemon juice]({{< relref "../items/lemon_juice.md" >}})     |
| 2 tsp    | [simple syrup](../_recipes/simple-syrup.md)                 |
| 1/2 oz   | [creme de mure]({{< relref "../items/creme_de_mure.md" >}}) |
|          | [lemon wedge]({{< relref "../items/lemon.md" >}})           |
|          | [blackberry]({{< relref "../items/blackberry.md" >}})       |


## Directions {#directions}

1.  Add gin, lemon juice, and simple syrup into a cocktail shaker with ice.
2.  Shake until well-chilled.
3.  Fill a rocks glass with ice and strain the cocktail shaker into the glass.
4.  Pour the creme de mure over top.
5.  Garnish with a lemon wedge and blackberry.
