+++
title = "Tomato soup with crispy chickpea croutons"
author = ["James Hawley"]
date = 2018-04-07T00:00:00-04:00
lastmod = 2024-05-23T21:41:58-04:00
tags = ["soups", "recipes", "vegetarian", "vegan", "lunch", "entrees"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 30 min     |
| Cook Time | 15 min     |
| Yields    | 4 servings |

{{< figure src="/ox-hugo/cream-tomato.jpg" caption="<span class=\"figure-number\">Figure 1: </span>Tomato soup with crispy chickpea croutons" >}}


## Ingredients {#ingredients}


### Chickpea croutons {#chickpea-croutons}

| Quantity | Item                                                                |
|----------|---------------------------------------------------------------------|
| 15 oz    | [chickpeas]({{< relref "../items/chickpea.md" >}}), drained, rinsed |
| 1 tsp    | [olive oil]({{< relref "../items/olive_oil.md" >}})                 |
| 1/2 tsp  | dried [oregano]({{< relref "../items/oregano.md" >}})               |
| 1/8 tsp  | [paprika]({{< relref "../items/paprika.md" >}})                     |
| 1 tsp    | [table salt]({{< relref "../items/table_salt.md" >}})               |


### Tomato soup {#tomato-soup}

| Quantity | Item                                                                                  |
|----------|---------------------------------------------------------------------------------------|
| 1 Tbsp   | [olive oil]({{< relref "../items/olive_oil.md" >}})                                   |
| 1        | [yellow onion]({{< relref "../items/sweet_onion.md" >}}), diced                       |
| 2 cloves | [garlic]({{< relref "../items/garlic.md" >}}), minced                                 |
| 1/2 cup  | [cashews]({{< relref "../items/cashew.md" >}}), soaked                                |
| 2 cups   | [vegetable broth]({{< relref "../items/vegetable_broth.md" >}})                       |
| 28 oz    | [whole peeled tomatoes]({{< relref "../items/whole_peeled_tomato.md" >}}) with juices |
| 1/4 cup  | oil-packed [sun-dried-tomatoes]({{< relref "../items/sun-dried_tomato.md" >}})        |
| 2 oz     | [tomato paste]({{< relref "../items/tomato_paste.md" >}})                             |
| 1 tsp    | dried [oregano]({{< relref "../items/oregano.md" >}})                                 |
| 1 tsp    | [table salt]({{< relref "../items/table_salt.md" >}})                                 |
| 1 tsp    | [pepper]({{< relref "../items/black_pepper.md" >}})                                   |
| 1/2 tsp  | dried [thyme]({{< relref "../items/thyme.md" >}})                                     |


### Garnish {#garnish}

-   fresh [basil]({{< relref "../items/basil.md" >}})
-   [olive oil]({{< relref "../items/olive_oil.md" >}})
-   [pepper]({{< relref "../items/black_pepper.md" >}})


## Directions {#directions}

1.  Preheat the oven to 220 C (425 F)
2.  Remove skins from the chickpeas
    1.  Line a large rimmed baking sheet with paper towels
    2.  Place the chickpeas on the paper towels and place a couple paper towels on top
    3.  Roll them around until any liquid on them has been absorbed
    4.  Discard paper towels
3.  Transfer the chickpeas to a large bowl and stir in the grapeseed oil, oregano, cayenne, garlic powder, onion powder, and salt
    1.  Line the baking sheet with parchment paper and then spread the chickpeas in an even layer
4.  Bake for 15 minutes
    1.  Give the pan a shake and cook for 15 to 20 minutes more until the chickpeas are lightly charred and golden
5.  Let cool on the baking sheet for at least 5 minutes. They will crisp up as they cool
6.  While chickpeas are baking, brown minced garlic and onion
    1.  In a large saucepan, heat the olive oil over medium heat
    2.  Add the onion and garlic and saute for 5 to 6 minutes, or until the onion is translucent
7.  In a blender, combine the soaked cashews and the broth and blend on high speed until creamy and smooth
    1.  Add the garlic-onion mixture, the tomatoes and their juices, sun-dried tomatoes, and tomato paste and blend on high until smooth
8.  Pour the tomato mixture into the saucepan in which you cooked the onions and set the pan over med-high heat
    1.  Bring the mixture to a simmer, then stir in the oregano, salt, pepper to taste, and thyme
9.  Gently simmer over medium heat, uncovered, for 20 to 30 minutes, until the flavors have developed
10. Ladle the soup into bowls and top with 1/3 - 1/2 cup of the chickpea croutons
    1.  Garnish with minced fresh basil leaves, a drizzle of olive oil, and freshly ground black pepper


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [Style at Home](http://www.styleathome.com/food-and-drink/recipes/article/recipe-cream-of-tomato-soup-with-roasted-italian-chickpea-croutons)
