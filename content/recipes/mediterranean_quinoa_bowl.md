+++
title = "Mediterranean quinoa bowl"
author = ["James Hawley"]
date = 2023-04-15T11:13:00-04:00
lastmod = 2024-05-23T20:46:22-04:00
tags = ["entrees", "vegetarian", "recipes"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 10 min     |
| Cook Time | 10 min     |
| Yields    | 8 servings |


## Ingredients {#ingredients}


### Roasted red pepper sauce {#roasted-red-pepper-sauce}

| Quantity    | Item                                                                                             |
|-------------|--------------------------------------------------------------------------------------------------|
| 1 16 oz jar | [roasted red peppers]({{< relref "../items/roasted_red_pepper.md" >}}), drained                  |
| 1 clove     | [garlic]({{< relref "../items/garlic.md" >}})                                                    |
| 1/2 tsp     | [fine sea salt]({{< relref "../items/sea_salt.md" >}})                                           |
| 1           | [lemon]({{< relref "../items/lemon.md" >}}), juiced                                              |
| 1/2 cup     | [olive oil]({{< relref "../items/olive_oil.md" >}})                                              |
| 1/2 cup     | [almonds]({{< relref "../items/almond.md" >}}) or [cashews]({{< relref "../items/cashew.md" >}}) |


### Quinoa bowl {#quinoa-bowl}

| Quantity   | Item                                                               |
|------------|--------------------------------------------------------------------|
| 5/4 cup    | [quinoa]({{< relref "../items/quinoa.md" >}}), uncooked            |
| 2 handfuls | [spinach]({{< relref "../items/spinach.md" >}})                    |
| 1          | [cucumber]({{< relref "../items/cucumber.md" >}})                  |
|            | [feta]({{< relref "../items/feta.md" >}})                          |
|            | [kalamata olives]({{< relref "../items/kalamata_olive.md" >}})     |
|            | [red onion]({{< relref "../items/red_onion.md" >}}), thinly sliced |
|            | [basil]({{< relref "../items/basil.md" >}})                        |
|            | [parsley]({{< relref "../items/parsley.md" >}})                    |


## Directions {#directions}

1.  Cook quinoa according to instructions.
2.  Mix all red pepper sauce ingredients in a blender and blend until smooth.
3.  Combine remaining ingredients as desired in each bowl and serve immediately.


## References {#references}

1.  Original recipe: [Pinch of Yum](https://pinchofyum.com/mediterranean-quinoa-bowls-with-roasted-red-pepper-sauce/print/41658)
