+++
title = "Crab rangoons"
author = ["James Hawley"]
date = 2023-11-25T08:59:00-05:00
lastmod = 2023-12-28T01:18:50-05:00
tags = ["entrees", "fish", "recipes"]
draft = false
+++

| Info      | Amount      |
|-----------|-------------|
| Prep Time | 1 h         |
| Cook Time | 20 min      |
| Yields    | 8 dumplings |


## Equipment {#equipment}

-   1 medium mixing bowl
-   1 spatula for folding and spooning
-   2 small mixing bowls
-   1 small whisk


## Ingredients {#ingredients}


### Filling {#filling}

| Quantity    | Item                                                                                                             |
|-------------|------------------------------------------------------------------------------------------------------------------|
| 225 g       | [cream cheese]({{< relref "../items/cream_cheese.md" >}})                                                        |
| 1/2 tsp     | [table salt]({{< relref "../items/table_salt.md" >}})                                                            |
| 1/4 tsp     | [MSG]({{< relref "../items/monosodium_glutamate.md" >}})                                                         |
| 1 tsp       | [white sugar]({{< relref "../items/white_sugar.md" >}}) or [cane sugar]({{< relref "../items/cane_sugar.md" >}}) |
| 1/4 tsp     | [white pepper]({{< relref "../items/white_pepper.md" >}})                                                        |
| 1/8 tsp     | [garlic powder]({{< relref "../items/garlic_powder.md" >}})                                                      |
| 1 tsp       | [soy sauce]({{< relref "../items/soy_sauce.md" >}})                                                              |
| 2 - 3 drops | [sesame oil]({{< relref "../items/sesame_oil.md" >}})                                                            |
| 1/4 cup     | [green onion]({{< relref "../items/green_onion.md" >}}), finely diced                                            |
|             | [Crab stick]({{< relref "../items/crab_stick.md" >}})                                                            |
|             | [wonton wrappers]({{< relref "../items/wonton_wrapper.md" >}})                                                   |
|             | neutral oil (for frying)                                                                                         |
| 1           | [egg]({{< relref "../items/eggs.md" >}})                                                                         |
|             | [water chestnuts]({{< relref "../items/water_chestnut.md" >}}), finely diced                                     |


### Dipping sauce {#dipping-sauce}

| Quantity | Item                                                                |
|----------|---------------------------------------------------------------------|
| 1/4 cup  | [sweet chili sauce]({{< relref "../items/sweet_chili_sauce.md" >}}) |
| 1 Tbsp   | [rice vinegar]({{< relref "../items/rice_vinegar.md" >}})           |
| 1 tsp    | [soy sauce]({{< relref "../items/soy_sauce.md" >}})                 |


## Directions {#directions}

1.  Let the block of cream chese rest at room temperature for ~ 30 min to soften.
2.  Add salt, MSG, sugar, pepper, and garlic powder to cream cheese and stir until smooth.
    1.  Add soy sauce and sesame oil, mixing until smooth.
    2.  Add green onion and water chestnuts, and mix again.
3.  Gently twist the crab sticks back and forth until they separate into strands, then finely dice.
    1.  Fold in diced crab meat into the cream cheese mixture.
    2.  Cover the filling and cool in the fridge for &gt; 30 min.
4.  Fill a pot with a neutral oil and heat it to &gt; 175 C (350 F)&nbsp;[^fn:1].
5.  Make an egg wash in a small mixing bowl with a bit of water to thin.
    1.  Lay out each wonton wrapper and wet all 4 edges with the egg wash.
    2.  Add 1 Tbsp of cream cheese filling to the wrappers, then fold up.
    3.  With all edges sealed, gently push down on a flat surface to spread out the filling&nbsp;[^fn:2].
6.  Gently place the rangoons in the oil in small batches so each has room to move.
    1.  Cook for 3 - 4 min each, using a metal spoon to gently nudge the rangoons and ensure the tops are basted.
    2.  When all sides are evenly bubbly, remove and place on a wire rack to let drain and cool for 5 - 10 min.
7.  Stir all dipping sauce ingredients in a small mixing bowl.
8.  When rangoons are cooled, serve immediately with the dipping sauce.


## References {#references}

1.  Original recipe: [Jason Farmer](https://www.youtube.com/watch?v=ZrMC0wnQe-4)

[^fn:1]: There should be enough oil so that each rangoon is fully submerged when frying.
[^fn:2]: It's important that every edge is sealed, or else the wontons can pop open when frying.