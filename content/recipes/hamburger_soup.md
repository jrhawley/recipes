+++
title = "Hamburger soup"
author = ["James Hawley"]
date = 2014-01-02T00:00:00-05:00
lastmod = 2023-12-28T01:32:06-05:00
tags = ["entrees", "soups", "recipes"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 30 min     |
| Cook Time | 2 h        |
| Yields    | 8 servings |


## Ingredients {#ingredients}

| Quantity | Item                                                                      |
|----------|---------------------------------------------------------------------------|
| 3/2 lbs  | [ground beef]({{< relref "../items/ground_beef.md" >}})                   |
| 1        | medium [onion]({{< relref "../items/onion.md" >}}), chopped               |
| 28 oz    | [whole peeled tomatoes]({{< relref "../items/whole_peeled_tomato.md" >}}) |
| 2 cup    | [water]({{< relref "../items/water.md" >}})                               |
| 852 mL   | [consomme soup]({{< relref "../items/consomme.md" >}})                    |
| 284 mL   | [tomato soup]({{< relref "../items/tomato_soup.md" >}})                   |
| 4        | [carrots]({{< relref "../items/baby_carrot.md" >}}), chopped              |
| 1        | [bay leaf]({{< relref "../items/bay_leaf.md" >}})                         |
| 3 stalks | [celery]({{< relref "../items/celery.md" >}})                             |
|          | [parsley]({{< relref "../items/parsley.md" >}})                           |
| 1/2 tsp  | [thyme]({{< relref "../items/thyme.md" >}})                               |
| 8 tbsp   | [barley]({{< relref "../items/barley.md" >}})                             |
|          | [pepper]({{< relref "../items/black_pepper.md" >}})                       |


## Directions {#directions}

1.  Brown meat and onion, then drain off fat
2.  Add all other ingredients and simmer for 2 h
3.  Remove bay leaf before serving


## References &amp; Notes {#references-and-notes}

1.  Original recipe: Nana
