+++
title = "Sundowner"
author = ["James Hawley"]
date = 2022-11-13T00:30:00-05:00
lastmod = 2023-12-05T22:27:41-05:00
tags = ["alcohols", "recipes", "beverages"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 2 min      |
| Cook Time | 0 min      |
| Yields    | 1 cocktail |


## Ingredients {#ingredients}

| Quantity | Item                                                        |
|----------|-------------------------------------------------------------|
| 5/3 oz   | [cognac]({{< relref "../items/cognac.md" >}})               |
| 1/2 oz   | [Grand Marnier]({{< relref "../items/grand_marnier.md" >}}) |
| 1/2 oz   | [orange juice]({{< relref "../items/orange_juice.md" >}})   |
| 1/2 oz   | [lemon juice]({{< relref "../items/lemon_juice.md" >}})     |
| 1        | [lemon wheel]({{< relref "../items/lemon.md" >}})           |


## Directions {#directions}

1.  Add all ingredients to a cocktail shaker and shake hard.
2.  Strain into a chilled coupe glass and garnish with a lemon wedge or wheel.


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [The Periodic Table of Cocktails]({{< relref "../references/Periodic_Table_of_Cocktails.md" >}})
