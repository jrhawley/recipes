+++
title = "Apricot BBQ sticky ribs"
author = ["James Hawley"]
date = 2023-03-12T09:36:00-04:00
lastmod = 2024-05-23T20:55:01-04:00
tags = ["beef", "entrees", "recipes"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 1 day      |
| Cook Time | 1 h 20 min |
| Yields    | 6 servings |


## Ingredients {#ingredients}

| Quantity | Item                                                                         |
|----------|------------------------------------------------------------------------------|
| 1.8 kg   | [back ribs]({{< relref "../items/beef_back_ribs.md" >}})                     |
| 4        | [apricots]({{< relref "../items/apricot.md" >}}), stoned and roughly chopped |
| 1/2 cup  | [olive oil]({{< relref "../items/olive_oil.md" >}})                          |
| 1/4 cup  | [canola oil]({{< relref "../items/canola_oil.md" >}})                        |
| 2 cloves | [garlic]({{< relref "../items/garlic.md" >}}), minced                        |
| 1/4 cup  | [honey]({{< relref "../items/honey.md" >}})                                  |
| 3 Tbsp   | [Dijon mustard]({{< relref "../items/dijon_mustard.md" >}})                  |
| 3 Tbsp   | [balsamic vinegar]({{< relref "../items/balsamic_vinegar.md" >}})            |
|          | [Kosher salt]({{< relref "../items/kitchen_salt.md" >}})                     |
|          | [black pepper]({{< relref "../items/black_pepper.md" >}})                    |


## Directions {#directions}

1.  Season the ribs with salt and place in a plastic bag
    1.  Place the apricots, olive and canola oils, garlic, honey, mustard, balsamic vinegar, salt, and pepper into a blender and blend on high until a smooth sauce has formed
    2.  Pour the sauce over the ribs in the plastic bag, seal, and rub to coat the ribs in the sauce
    3.  Marinate the ribs for 6 - 24 h in the refrigerator
2.  Preheat a well-oiled grill to 120 C (250 F)
    1.  Wipe excess marinade off the ribs
    2.  Place ribs on the grill, then close the grill and cook the ribs for 1 h 20 min, flipping every 20 min
    3.  Cook until the ribs have an internal temperature of at least 71 C (160 F), or 88 C (190 F) if you want fall-off-the-bone
3.  Transfer the ribs to a cutting board, tent with foil, and let rest for 5 min before serving


## References {#references}

1.  Original recipe: Peak Season
