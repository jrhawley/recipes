+++
title = "Mexican Firing Squad"
author = ["James Hawley"]
date = 2023-04-20T20:19:00-04:00
lastmod = 2023-12-21T22:15:13-05:00
tags = ["alcohols", "recipes", "beverages"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 2 min      |
| Cook Time | 0 min      |
| Yields    | 1 cocktail |


## Ingredients {#ingredients}

| Quantity | Item                                                                |
|----------|---------------------------------------------------------------------|
| 2 oz     | [tequila]({{< relref "../items/tequila.md" >}})                     |
| 3/4 oz   | [lime juice]({{< relref "../items/lime_juice.md" >}})               |
| 3/4 oz   | [grenadine]({{< relref "../items/grenadine.md" >}})                 |
| 4 dashes | [Angostura bitters]({{< relref "../items/angostura_bitters.md" >}}) |
|          | ice                                                                 |


## Directions {#directions}

1.  Combine all ingredients in a tumbler and shake to combine.
2.  Pour into a chilled cocktail glass over ice.


## References {#references}

1.  Original recipe: [Liquor.com](https://www.liquor.com/mexican-firing-squad-cocktail-recipe-5270686)
