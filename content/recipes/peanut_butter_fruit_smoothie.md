+++
title = "Peanut butter fruit smoothie"
author = ["James Hawley"]
date = 2017-05-28T00:00:00-04:00
lastmod = 2023-12-21T22:26:06-05:00
tags = ["smoothies", "recipes", "beverages"]
draft = false
+++

| Info      | Amount |
|-----------|--------|
| Prep Time | 2 min  |

{{< figure src="../assets/peanut-butter-fruit-smoothie.jpg" caption="<span class=\"figure-number\">Figure 1: </span>Peanut butter fruit smoothie" >}}


## Ingredients {#ingredients}

| Quantity | Item                                                           |
|----------|----------------------------------------------------------------|
| 1/4 cup  | frozen [blueberries]({{< relref "../items/blueberry.md" >}})   |
| 1/4 cup  | frozen [blackberries]({{< relref "../items/blackberry.md" >}}) |
| 1/4 cup  | frozen [raspberry]({{< relref "../items/raspberries.md" >}})   |
| 1/3 cup  | [Greek yogurt]({{< relref "../items/greek_yogurt.md" >}})      |
| 1 Tbsp   | [peanut butter]({{< relref "../items/peanut_butter.md" >}})    |
| 3/4 cup  | almond [milk]({{< relref "../items/milk.md" >}})               |
| 1        | [dates]({{< relref "../items/medjool_date.md" >}})             |


## Directions {#directions}

1.  Place the date at the bottom of your blender and then all of the other ingredients
2.  Blend until you get a smooth consistency
