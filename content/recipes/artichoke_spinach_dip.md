+++
title = "Artichoke and spinach dip"
author = ["James Hawley"]
date = 2015-01-02T00:00:00-05:00
lastmod = 2023-12-28T00:56:43-05:00
tags = ["recipes", "sauces"]
draft = false
+++

| Info      | Amount      |
|-----------|-------------|
| Prep Time | 15 min      |
| Cook Time | 25 min      |
| Yields    | 12 servings |


## Ingredients {#ingredients}

| Quantity | Item                                                              |
|----------|-------------------------------------------------------------------|
| 8 oz     | [cream cheese]({{< relref "../items/cream_cheese.md" >}})         |
| 1/4 cup  | [sour cream]({{< relref "../items/sour_cream.md" >}})             |
| 1/4 cup  | [Parmesan]({{< relref "../items/parmesan.md" >}}), grated         |
| 1/4 cup  | [Romano]({{< relref "../items/romano.md" >}}), grated             |
| 1 clove  | [garlic]({{< relref "../items/garlic.md" >}}), minced             |
| 1/2 tsp  | dried [basil]({{< relref "../items/basil.md" >}})                 |
| 1/4 tsp  | [garlic salt]({{< relref "../items/garlic_salt.md" >}})           |
|          | [kosher salt]({{< relref "../items/kitchen_salt.md" >}})          |
|          | [black pepper]({{< relref "../items/black_pepper.md" >}})         |
| 14 oz    | [artichoke hearts]({{< relref "../items/artichoke_hearts.md" >}}) |
| 1/2 cup  | [spinach]({{< relref "../items/spinach.md" >}}), chopped          |
| 1/4 cup  | [mozzarella]({{< relref "../items/mozzarella.md" >}}), grated     |


## Directions {#directions}

1.  Preheat over to 350 F
    1.  Lightly grease a small baking dish
2.  In a medium bowl, mix together cream cheese, sour cream, parmesan, romano, garlic, basil, garlic salt, salt, and pepper
    1.  Once well mixed, gently stir in artichoke hearts and spinach
3.  Transfer mixture to baking dish
    1.  Top with mozzarella
4.  Bake for 25 min, until bubbly and lightly browned
