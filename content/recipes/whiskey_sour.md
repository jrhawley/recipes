+++
title = "Whiskey sour"
author = ["James Hawley"]
date = 2020-05-18T00:00:00-04:00
lastmod = 2023-12-24T20:04:28-05:00
tags = ["recipes", "alcohols", "beverages"]
draft = false
+++

## Ingredients {#ingredients}

| Quantity | Item                                                  |
|----------|-------------------------------------------------------|
| 1.5 oz   | [whiskey]({{< relref "../items/whiskey.md" >}})       |
| 1.5 oz   | [Simple syrup](./simple-syrup.md)                     |
| 1 oz     | [lime juice]({{< relref "../items/lime_juice.md" >}}) |
|          | ice cubes                                             |


## Directions {#directions}

1.  Fill lowball glass with ice cubes
2.  Combine other ingredients and stir
