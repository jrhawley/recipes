+++
title = "Lemon garlic chicken"
author = ["James Hawley"]
date = 2020-11-10T00:00:00-05:00
lastmod = 2024-05-23T21:24:15-04:00
tags = ["chicken", "recipes", "entrees"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 20 min     |
| Cook Time | 40 min     |
| Yields    | 6 servings |


## Ingredients {#ingredients}

| Quantity | Item                                                                    |
|----------|-------------------------------------------------------------------------|
| 1/4 cup  | [olive oil]({{< relref "../items/olive_oil.md" >}})                     |
| 2 Tbsp   | [lemon juice]({{< relref "../items/lemon_juice.md" >}})                 |
| 3 cloves | [garlic]({{< relref "../items/garlic.md" >}}), minced                   |
| 3/2 tsp  | fresh [thyme]({{< relref "../items/thyme.md" >}}), minced               |
| 1 tsp    | [table salt]({{< relref "../items/table_salt.md" >}})                   |
| 1/2 tsp  | fresh [rosemary]({{< relref "../items/rosemary.md" >}}), minced         |
| 1/4 tsp  | [pepper]({{< relref "../items/black_pepper.md" >}})                     |
| 6        | bone-in [chicken thighs]({{< relref "../items/chicken_broth.md" >}})    |
| 6        | [chicken drumsticks]({{< relref "../items/chicken_breast.md" >}})       |
| 1 lb     | baby [red potatoes]({{< relref "../items/russet_potato.md" >}}), halved |
| 1        | [lemon]({{< relref "../items/lemon.md" >}}), sliced                     |
| 2 Tbsp   | [parsley]({{< relref "../items/parsley.md" >}}), minced                 |


## Directions {#directions}

1.  Preheat oven to 425 F
2.  In a small bowl, whisk the first 7 ingredients until blended.
    1.  Pour 1/4 cup marinade into a large bowl or shallow dish
    2.  Add chicken and turn to coat
    3.  Refrigerate 30 min
    4.  Cover and refrigerate remaining marinade
3.  Drain chicken, discarding any remaining marinade in bowl
4.  Place chicken in a 15" x 10" x 1" baking pan
    1.  Add potatoes in a single layer
    2.  Drizzle reserved marinade over potatoes
    3.  Top with lemon slices
5.  Bake until a thermometer inserted in chicken reads 170 F - 175 F and potatoes are tender (40-45 min)
6.  If desired, broil chicken 3 - 4" from heat until deep golden brown, about 3-4 min
7.  Sprinkle with parsley before serving


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [Taste of Home](https://www.tasteofhome.com/recipes/sheet-pan-lemon-garlic-chicken/)
