+++
title = "Cod and tangy tartar sauce"
author = ["James Hawley"]
date = 2013-09-14T00:00:00-04:00
lastmod = 2024-05-23T21:33:25-04:00
tags = ["fish", "recipes", "entrees"]
draft = false
+++

| Info      | Amount   |
|-----------|----------|
| Prep Time | 15 min   |
| Cook Time | 10 min   |
| Yields    | 4 filets |


## Ingredients {#ingredients}


### Cod {#cod}

| Quantity | Item                                                  |
|----------|-------------------------------------------------------|
| 1/4 cup  | [flour]({{< relref "../items/flour.md" >}})           |
| 1 tsp    | [table salt]({{< relref "../items/table_salt.md" >}}) |
| 1/2 tsp  | [pepper]({{< relref "../items/black_pepper.md" >}})   |
| 1/4 tsp  | [paprika]({{< relref "../items/paprika.md" >}})       |
| 4        | [cod]({{< relref "../items/cod.md" >}}) filets        |


### Tartar sauce {#tartar-sauce}

| Quantity | Item                                                        |
|----------|-------------------------------------------------------------|
| 1/3 cup  | [sour cream]({{< relref "../items/sour_cream.md" >}})       |
| 1/4 cup  | [mayonnaise]({{< relref "../items/mayonnaise.md" >}})       |
| 2 Tbsp   | [relish]({{< relref "../items/relish.md" >}})               |
| 1 tsp    | [parsley]({{< relref "../items/parsley.md" >}}), chopped    |
| 1 tsp    | [capers]({{< relref "../items/caper.md" >}}), drained       |
| 1/2 tsp  | [lemon zest]({{< relref "../items/lemon.md" >}})            |
| 1 tsp    | [lemon juice]({{< relref "../items/lemon_juice.md" >}})     |
| 1/4 tsp  | [garlic powder]({{< relref "../items/garlic_powder.md" >}}) |


## Directions {#directions}

1.  Mix flour, salt, pepper, and paprika together in a wide bowl
    1.  Dredge both sides of each filet, shaking off excess flour
2.  Heat a large non-stick skillet to medium-high heat
    1.  Add a drizzle of olive oil to the pan
    2.  Cook fish for 10 min until flaky, flipping halfway through
3.  Combine all tartar sauce ingredients in a small bowl and set aside
