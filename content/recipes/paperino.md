+++
title = "Paperino"
author = ["James Hawley"]
date = 2021-09-25T00:00:00-04:00
lastmod = 2023-12-21T22:20:08-05:00
tags = ["recipes", "alcohols", "beverages"]
draft = false
+++

## Ingredients {#ingredients}

| Quantity | Item                                                         |
|----------|--------------------------------------------------------------|
| 3/2 oz   | [amaro]({{< relref "../items/lucano_amaro.md" >}})           |
| 1/2 oz   | [brandy]({{< relref "../items/brandy.md" >}})                |
| 1 oz     | [lemon juice]({{< relref "../items/lemon_juice.md" >}})      |
| 1        | [egg white]({{< relref "../items/eggs.md" >}})               |
| 1        | [lemon twist]({{< relref "../items/lemon.md" >}}) (optional) |


## Directions {#directions}

1.  Add all ingredients, except the lemon twist, to an ice-filled cocktail shaker and shake for ~ 1 min.
2.  Stain into a chilled coupe.
3.  Garnish with the lemon twist.


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [LCBO](https://www.lcbo.com/webapp/wcs/stores/servlet/en/lcbo/recipe/paperino/F202105051)
