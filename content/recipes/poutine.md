+++
title = "Poutine"
author = ["James Hawley"]
date = 2023-03-12T10:13:00-04:00
lastmod = 2023-12-28T01:11:22-05:00
tags = ["vegetarian", "sides", "recipes"]
draft = false
+++

| Info      | Amount         |
|-----------|----------------|
| Prep Time | 5 min          |
| Cook Time | 0 min          |
| Yields    | 2 - 4 servings |


## Ingredients {#ingredients}


### Fries {#fries}

| Quantity | Item                                                                                 |
|----------|--------------------------------------------------------------------------------------|
| 6        | [Yukon gold potatoes]({{< relref "../items/yukon_gold_potato.md" >}}), peeled        |
|          | [sunflower oil]({{< relref "../items/sunflower_oil.md" >}}) (or another neutral oil) |
| 3/4 tsp  | [Kosher salt]({{< relref "../items/kitchen_salt.md" >}})                             |


### Gravy {#gravy}

| Quantity | Item                                                                            |
|----------|---------------------------------------------------------------------------------|
| 3 Tbsp   | [potato starch]({{< relref "../items/potato_starch.md" >}})                     |
| 580 mL   | [bone broth]({{< relref "../items/bone_broth.md" >}})                           |
| 3 cloves | [garlic]({{< relref "../items/garlic.md" >}}), minced                           |
| 1        | large [yellow onion]({{< relref "../items/sweet_onion.md" >}}), roughly chopped |
|          | [Kosher salt]({{< relref "../items/kitchen_salt.md" >}})                        |
|          | [black pepper]({{< relref "../items/black_pepper.md" >}})                       |
| 675 g    | [cheese curds]({{< relref "../items/cheese_curd.md" >}})                        |


## Directions {#directions}

1.  Using a fry cutter or knife, cut the potatoes into evenly sized 1 cm fries
2.  Pour the sunflower oil into a deep pot and heat over medium
    1.  Add as many potatoes as will fit in the oil and cook until crispy, ~ 15 - 20 min
    2.  Remove potatoes with a slotted spoon&nbsp;[^fn:1]
    3.  Spread the fries in a single layer on a baking sheet and set aside
    4.  Repeat with the remaining potatoes, cycling as needed
3.  Once all the fries have cooked, toss with extra salt, to taste
4.  In a small bowl, whisk the potato starch with 100 mL of the bone broth until well combined, and set aside
5.  Meanwhile, in a medium-size saucepan over high heat, heat the garlic, onions, and remaining bone broth
    1.  Continually stir with a whisk until the broth comes to a boil
    2.  Reduce heat to low
    3.  Stir in the potato starch mixture and let simmer, continuing to stir, for 10 min or until the gravy thickens
    4.  Season with salt and pepper
6.  Transfer the gravy to a blender and blend on low speed until smooth
    1.  Pour the blended gravy back into the saucepan to reheat when ready for serving
7.  To serve, plate the fries on four wide plates or shallow bowls
    1.  Crumble the cheese curds evenly across all four plates and pour the gravy on top while it is still hot; this will allow the cheese to become melty and stringy


## References {#references}

1.  Original recipe: [Peak Season]({{< relref "../references/Peak_Season.md" >}})

[^fn:1]: If the fries fall apart when you go to remove them, they need another 3 - 5 min