+++
title = "Rum cocktail"
author = ["James Hawley"]
date = 2021-06-01T00:00:00-04:00
lastmod = 2023-11-05T16:35:41-05:00
tags = ["recipes", "beverages", "alcohols"]
draft = false
+++

## Ingredients {#ingredients}

| Quantity | Item                                                  |
|----------|-------------------------------------------------------|
| 2 oz     | [white rum]({{< relref "../items/white_rum.md" >}})   |
| 1/2 oz   | [lime juice]({{< relref "../items/lime_juice.md" >}}) |
| 1/2 oz   | [grenadine]({{< relref "../items/grenadine.md" >}})   |


## Directions {#directions}

1.  Combine all ingredients in a cocktail shaker with ice
2.  Shake well and strain into a chilled cocktail glass


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [The Spruce Eats](https://www.thespruceeats.com/bacardi-cocktail-recipe-759282)
