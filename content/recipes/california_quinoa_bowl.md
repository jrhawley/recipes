+++
title = "California quinoa bowl"
author = ["James Hawley"]
date = 2022-10-02T22:53:00-04:00
lastmod = 2024-05-23T21:13:01-04:00
tags = ["chicken", "bowls", "entrees", "recipes"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 20 min     |
| Cook Time | 15 min     |
| Yields    | 4 servings |


## Ingredients {#ingredients}

| Quantity | Item                                                                                                 |
|----------|------------------------------------------------------------------------------------------------------|
| 2        | [chicken breasts]({{< relref "../items/chicken_breast.md" >}})                                       |
| 1 tsp    | [paprika]({{< relref "../items/paprika.md" >}})                                                      |
| 1 Tbsp   | dried [parsley]({{< relref "../items/parsley.md" >}}) or [basil]({{< relref "../items/basil.md" >}}) |
| 1 - 2    | [bell peppers]({{< relref "../items/bell_pepper.md" >}})                                             |
| 1 - 2    | [avocadoes​]({{< relref "../items/avocado.md" >}}), mashed                                            |
| 2 cup    | [quinoa]({{< relref "../items/quinoa.md" >}}), cooked                                                |
| 1 cup    | [cherry tomatoes]({{< relref "../items/cherry_tomato.md" >}}), halved                                |
| 1/2 cup  | [feta]({{< relref "../items/feta.md" >}})                                                            |
|          | [olive oil]({{< relref "../items/olive_oil.md" >}})                                                  |
|          | [lemon juice]({{< relref "../items/lemon_juice.md" >}})                                              |
|          | [table salt]({{< relref "../items/table_salt.md" >}})                                                |
|          | [black pepper]({{< relref "../items/black_pepper.md" >}})                                            |


## Directions {#directions}

1.  Cook the quinoa according to the instructions.
2.  In a frying pan, add olive oil and cook the chicken.
3.  Meanwhile, slice the bell pepper.
    -   Once the chicken is fully cooked, swap the peppers with the chicken.
    -   Cube the chicken into bite sized chunks and sautee the peppers.
    -   Add the chicken back in after a few minutes.
4.  Combine everything in the bowl, mash the avocado, and top with lemon juice and olive oil.
