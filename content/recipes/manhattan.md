+++
title = "Manhattan"
author = ["James Hawley"]
date = 2021-02-17T00:00:00-05:00
lastmod = 2023-12-22T00:47:43-05:00
tags = ["recipes", "alcohols", "beverages"]
draft = false
+++

## Ingredients {#ingredients}

| Quantity | Item                                                                |
|----------|---------------------------------------------------------------------|
| 2 oz     | [whiskey]({{< relref "../items/whiskey.md" >}})                     |
| 2/3 oz   | [vermouth]({{< relref "../items/dry_vermouth.md" >}})               |
| 3 dashes | [Angostura bitters]({{< relref "../items/angostura_bitters.md" >}}) |


## Directions {#directions}

1.  Fill a mixing glass with ice
2.  Add all ingredients, stir until chilled
3.  Strain into a cocktail glass
4.  Garnish with a maraschino cherry (with sweet vermouth) or a strip of lemon peel (with dry vermouth)


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [The Periodic Table of Cocktails]({{< relref "../references/Periodic_Table_of_Cocktails.md" >}})
2.  The Rob Roy is made the same way as a Manhattan, just with scotch whiskey instead of bourbon or rye
