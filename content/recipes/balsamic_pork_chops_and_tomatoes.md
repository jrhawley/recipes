+++
title = "Balsamic pork chops with tomatoes"
author = ["James Hawley"]
date = 2022-10-11T19:54:00-04:00
lastmod = 2024-05-23T21:12:41-04:00
tags = ["pork", "entrees", "recipes"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 10 min     |
| Cook Time | 15 min     |
| Yields    | 2 servings |


## Ingredients {#ingredients}

| Quantity | Item                                                              |
|----------|-------------------------------------------------------------------|
| 2        | [pork chops]({{< relref "../items/pork_chop.md" >}})              |
| 1/2      | [red onion]({{< relref "../items/red_onion.md" >}})               |
| 1 cup    | [cherry tomatoes]({{< relref "../items/cherry_tomato.md" >}})     |
| 1 clove  | [garlic]({{< relref "../items/garlic.md" >}})                     |
| 1/2 cup  | [vegetable broth]({{< relref "../items/vegetable_broth.md" >}})   |
| 1/4 cup  | [white wine]({{< relref "../items/white_wine.md" >}})             |
| 1 Tbsp   | [balsamic vinegar]({{< relref "../items/balsamic_vinegar.md" >}}) |
| 1 tsp    | [lemon juice]({{< relref "../items/lemon_juice.md" >}})           |
|          | [Kosher salt]({{< relref "../items/kitchen_salt.md" >}})          |
|          | [black pepper]({{< relref "../items/black_pepper.md" >}})         |
|          | [olive oil]({{< relref "../items/olive_oil.md" >}})               |
|          | [butter]({{< relref "../items/butter.md" >}})                     |


## Directions {#directions}

1.  In a large skillet, heat olive oil over medium high heat.
2.  Pat pork chops dry and season with salt and pepper on both sides.
    1.  Add chops to skillet and cook 2-3 min per side.
    2.  Remove to a plate to rest and cover with foil.
3.  Still over medium high heat, pour half of the white wine into the skillet and scrape up any browned bits on the bottom of the pan.
    1.  Melt butter in skillet.
    2.  Add garlic and cook 30 seconds until fragrant.
    3.  Add red onion, tomatoes, and herbs and cook for 2 min, stirring occasionally.
4.  Pour in the broth, lemon, juice, and the rest of the wine and cook another 2 min.
5.  Remove skillet from heat and add chops back to the pan.
    Drizzle chops with with balsamic vinegar and serve immediately.


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [Aberdeen's Kitchen](https://www.aberdeenskitchen.com/2020/04/skillet-balsamic-pork-chops-with-tomatoes-and-gremolata/print/16406/)
