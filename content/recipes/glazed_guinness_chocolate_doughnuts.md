+++
title = "Glazed Guinness chocolate doughnuts"
author = ["James Hawley"]
date = 2022-03-16T00:00:00-04:00
lastmod = 2023-12-21T21:55:11-05:00
tags = ["doughnuts", "recipes", "desserts"]
draft = false
+++

| Info      | Amount      |
|-----------|-------------|
| Prep Time | 5 min       |
| Cook Time | 15 min      |
| Yields    | 6 doughnuts |


## Ingredients {#ingredients}


### Doughnuts {#doughnuts}

| Quantity | Ingredient                                                  |
|----------|-------------------------------------------------------------|
| 1 cup    | [flour]({{< relref "../items/flour.md" >}})                 |
| 1/3 cup  | [cocoa powder]({{< relref "../items/cocoa_powder.md" >}})   |
| 1/2 tsp  | [baking powder]({{< relref "../items/baking_powder.md" >}}) |
| 1/4 tsp  | [salt]({{< relref "../items/table_salt.md" >}})             |
| 1/2 cup  | [Guinness]({{< relref "../items/guinness.md" >}})           |
| 1        | large [egg]({{< relref "../items/eggs.md" >}})              |
| 1/2 cup  | [white sugar]({{< relref "../items/white_sugar.md" >}})     |
| 1/3 cup  | unsalted [butter]({{< relref "../items/butter.md" >}})      |


### Glaze {#glaze}

| Quantity | Ingredient                                                    |
|----------|---------------------------------------------------------------|
| 1/2 cup  | [icing sugar]({{< relref "../items/icing_sugar.md" >}})       |
| 1 Tbsp   | [Bailey's Irish Cream]({{< relref "../items/bailey's.md" >}}) |
| 1 tsp    | [water]({{< relref "../items/water.md" >}})                   |


## Directions {#directions}

1.  Preheat oven to 325 F.
    1.  In a medium bowl, whisk together all the dry doughnut ingredients.
    2.  In a large bowl, whisk together all the wet doughnut ingredients, then mix in the dry.
2.  Place the batter in a plastic bag and squeeze the air out.
    1.  Spray a doughnut pan with cooking spray, or line with butter.
    2.  Pipe the batter into the pan.
3.  Bake for 15 - 18 min.
    1.  Let cool in the pan for 5 min, then remove and place on a cooling rack for 5 - 10 min.
4.  While the doughnuts are baking, mix the glaze ingredients in a bowl wide enough to dip the doughnuts in.
    1.  Dip each of the doughnuts to glaze, then let rest for another few minutes.


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [Chisel and Fork](https://www.chiselandfork.com/wprm_print/2027)
