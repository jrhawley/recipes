+++
title = "Spicy rice"
author = ["James Hawley"]
date = 2013-10-15T00:00:00-04:00
lastmod = 2023-12-28T01:12:26-05:00
tags = ["recipes", "sides"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 10 min     |
| Cook Time | 50 min     |
| Yields    | 2 servings |


## Ingredients {#ingredients}

| Quantity  | Item                                                                                |
|-----------|-------------------------------------------------------------------------------------|
| 1 Tbsp    | [butter]({{< relref "../items/butter.md" >}})                                       |
| 1/2 cup   | [onion]({{< relref "../items/onion.md" >}}), chopped finely                         |
| 1/2 stalk | [celery]({{< relref "../items/celery.md" >}}), chopped finely                       |
| 1 clove   | [garlic]({{< relref "../items/garlic.md" >}})                                       |
| 1         | [Thai chili]({{< relref "../items/Thai_chili.md" >}}), seeded and minced (optional) |
| 1/2       | [carrot]({{< relref "../items/baby_carrot.md" >}}), peeled and chopped finely       |
| 3/4 cup   | long grain [rice]({{< relref "../items/basmati_rice.md" >}})                        |
| 3/2 cup   | [chicken broth]({{< relref "../items/chicken_broth.md" >}})                         |
| 1         | [lemon]({{< relref "../items/lemon.md" >}}), zested                                 |
| 1/3 cup   | frozen [peas]({{< relref "../items/pea.md" >}})                                     |
| 2 tbsp    | [parsley]({{< relref "../items/parsley.md" >}}), chopped                            |
| 2 tsp     | [thyme]({{< relref "../items/thyme.md" >}}), chopped                                |
|           | [kosher salt]({{< relref "../items/kitchen_salt.md" >}})                            |
|           | [pepper]({{< relref "../items/black_pepper.md" >}})                                 |


## Directions {#directions}

1.  Preheat oven to 350 F
2.  In oven-proof Dutch oven on medium heat, melt 1 Tbsp butter
    1.  Add onions, celery, garlic, and chili, and saute until softened (~ 5 min)
    2.  Stir in carrot and continue for another 2 min
3.  Add rice to the pot, stirring and cooking for 1 min
    1.  Pour chicken broth and zest over top
    2.  Bring to boil before covering with lid
4.  Place in oven and bake for 40 min
    1.  Remove from oven, stir frozen peas, parsley, and thyme
    2.  Put lid back on, sit for a few minutes
5.  Season with salt and pepper before serving
