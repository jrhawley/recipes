+++
title = "Apple cider chicken and sweet potatoes"
author = ["James Hawley"]
date = 2021-11-08T00:00:00-05:00
lastmod = 2024-05-23T21:18:00-04:00
tags = ["chicken", "recipes", "entrees"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 10 min     |
| Cook Time | 20 min     |
| Yields    | 4 servings |


## Ingredients {#ingredients}

| Quantity | Item                                                             |
|----------|------------------------------------------------------------------|
| 4        | [chicken breasts]({{< relref "../items/chicken_breast.md" >}})   |
| 3/2 Tbsp | [olive oil]({{< relref "../items/olive_oil.md" >}})              |
| 3/4 cup  | [apple cider](../_recipes/apple-cider.md)                        |
| 2        | [apples]({{< relref "../items/apple.md" >}}), sliced             |
| 2 tsp    | [thyme]({{< relref "../items/thyme.md" >}})                      |
| 1        | [sweet onion]({{< relref "../items/sweet_onion.md" >}}), chopped |
| 1 tsp    | [garlic powder]({{< relref "../items/garlic_powder.md" >}})      |
| 1        | [sweet potato]({{< relref "../items/yam.md" >}})                 |
| 1 Tbsp   | [dijon mustard]({{< relref "../items/dijon_mustard.md" >}})      |
| 1 tsp    | [cornstarch]({{< relref "../items/corn_starch.md" >}})           |


## Directions {#directions}

1.  In a large skillet, heat oil over medium-high heat.
    -   Add chicken and brown for 5 - 7 min, each side.
    -   Remove from skillet and place on a plate covered with foil.
2.  Reduce heat to medium and add the sweet potato chunks.
    -   Cook for 3 min.
3.  Add apple slices and cook for an additional 3 min.
4.  Add onion, thyme, and garlic powder.
    -   Stir to combine and cook for an additional few minutes.
5.  In a bowl, mix the apple cider and mustard.
    -   Make a little well in the vegetables, add back the chicken.
    -   Pour the apple cider mix into the skillet, cover, and cook for a few minutes.
6.  Remove chicken, then add cornstarch to thicken the apple cider sauce.
    -   Mix thoroughly, then let stand for a few minutes before serving.


## References {#references}

1.  Original recipe: [Food with Feeling](https://foodwithfeeling.com/one-skillet-apple-cider-chicken/print/10059/)
