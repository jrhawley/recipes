+++
title = "Gym rat"
author = ["James Hawley"]
date = 2023-03-18T13:38:00-04:00
lastmod = 2023-12-21T22:01:55-05:00
tags = ["smoothies", "recipes", "beverages"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 5 min      |
| Cook Time | 0 min      |
| Yields    | 1 smoothie |


## Ingredients {#ingredients}

| Quantity | Item                                                               |
|----------|--------------------------------------------------------------------|
| 1 cup    | [almond milk]({{< relref "../items/almond_milk.md" >}})            |
| 2 Tbsp   | [oats]({{< relref "../items/oat.md" >}})                           |
| 2 - 3    | [Medjool dates]({{< relref "../items/medjool_date.md" >}}), pitted |
| 1 Tbsp   | [chia seeds]({{< relref "../items/chia_seed.md" >}})               |
| 1 Tbsp   | [peanut butter]({{< relref "../items/peanut_butter.md" >}})        |
| 1/2 tsp  | [ground cinnamon]({{< relref "../items/cinnamon.md" >}})           |
| 1/4 tsp  | [vanilla extract]({{< relref "../items/vanilla_extract.md" >}})    |
| 4 - 5    | ice cubes                                                          |


## Directions {#directions}

1.  Blend all ingredients together in a blender until smooth


## References {#references}

1.  Original recipe: [The Oh She Glows Cookbook]({{< relref "../references/Oh_She_Glows_Cookbook.md" >}})
