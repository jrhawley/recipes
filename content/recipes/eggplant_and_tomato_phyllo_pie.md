+++
title = "Eggplant and tomato phyllo pie"
author = ["James Hawley"]
date = 2023-01-14T14:59:00-05:00
lastmod = 2023-12-21T21:42:35-05:00
tags = ["appetizers", "vegetarian", "recipes"]
draft = false
+++

| Info      | Amount         |
|-----------|----------------|
| Prep Time | 15 min         |
| Cook Time | 2h 30 min      |
| Yields    | 4 - 6 servings |


## Ingredients {#ingredients}

| Quantity | Item                                                                                             |
|----------|--------------------------------------------------------------------------------------------------|
| 454 g    | [beefsteak tomatoes]({{< relref "../items/beefsteak_tomato.md" >}}), cored and sliced 1/4" thick |
| 5/4 tsp  | [table salt]({{< relref "../items/table_salt.md" >}}), divided                                   |
| 454 g    | [eggplant]({{< relref "../items/eggplant.md" >}}), sliced into 1/4" thick rounds                 |
| 1/2 cup  | [olive oil]({{< relref "../items/olive_oil.md" >}}), divided                                     |
| 12       | [pastry sheets]({{< relref "../items/pastry_sheet.md" >}})                                       |
| 3 cloves | [garlic]({{< relref "../items/garlic.md" >}}), minced                                            |
| 2 tsp    | [oregano]({{< relref "../items/oregano.md" >}})                                                  |
| 1/4 tsp  | [black pepper]({{< relref "../items/black_pepper.md" >}})                                        |
| 3/2 cup  | [mozzarella]({{< relref "../items/mozzarella.md" >}}), shredded                                  |
| 2 Tbsp   | [parmesan]({{< relref "../items/parmesan.md" >}}), grated                                        |
| 1 TBsp   | [basil]({{< relref "../items/basil.md" >}}), chopped                                             |


## Directions {#directions}

1.  Toss tomatos and 1/4 tsp salt together in a colander and set aside to drain for 30 min.
2.  Adjust the top oven rack to be 15 cm from the broiler.
    1.  Turn on the broiler while slicing the eggplant.
3.  Line a rimmed baking sheet with aluminium foil.
    1.  Arrange eggplant in single layer on the prepared baking sheet and brush both
    2.  Brush both sides of each round with oil.
    3.  Broil eggplant until softened and beginning to brown, 10 - 12 min, flipping halfway through.
    4.  Set aside to cool for 10 min.
4.  Heat the oven to 190 C (375 F).
    1.  Line a second rimmed baking sheet with parchment paper.
    2.  Place 1/4 cup of oil in a small bowl, for brushing.
    3.  Place 1 phyllo sheet on the parchment paper and lightly brush with oil.
    4.  Rotate the sheet 30 deg, then repeat with the next sheet.
    5.  Repeat with all sheets until the pinwheel pattern is complete.
5.  Shake the colander to rid the tomatoes of any remaining water.
    1.  Combine tomatoes, garlic, oregano, pepper, 1 Tbsp oil, and remaining 1/2 tsp salt in a bowl.
    2.  Sprinkle mozzarella evenly in the centre of the phyllo in a 9" circle.
    3.  Shingle tomatoes and eggplant on top of the mozzalrella in concentric circles, alternating tomatoes and eggplant.
    4.  Sprinkle parmesan over top
6.  Gently fold edges of the pastry up, pleating every 2 - 3 " as needed.
    1.  Lightly brusg edges with oil to stick.
    2.  Bake phyllo until golden brown, 30 - 35 min.
    3.  Let pie cool for 15 min, then sprinkle with basil.
7.  Slice and serve.


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [The New Cooking School Cookbook]({{< relref "../references/The_New_Cooking_School_Cookbook.md" >}})
