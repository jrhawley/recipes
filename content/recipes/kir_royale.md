+++
title = "Kir Royale"
author = ["James Hawley"]
date = 2022-10-06T22:26:00-04:00
lastmod = 2023-12-21T22:08:58-05:00
tags = ["alcohols", "recipes", "beverages"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 1 min      |
| Cook Time | 0 min      |
| Yields    | 1 cocktail |


## Ingredients {#ingredients}

| Quantity | Item                                                                                                     |
|----------|----------------------------------------------------------------------------------------------------------|
| 1 oz     | [creme de cassis]({{< relref "../items/creme_de_cassis.md" >}})                                          |
| 4 oz     | [prosecco]({{< relref "../items/prosecco.md" >}}) or [champagne]({{< relref "../items/champagne.md" >}}) |


## Directions {#directions}

1.  Pour the creme de cassis into a champagne flute and top with champagne.
2.  Garnish with raspberries, cranberries, or other fruit.
