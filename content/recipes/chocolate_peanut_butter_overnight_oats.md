+++
title = "Chocolate peanut butter overnight oats"
author = ["James Hawley"]
date = 2023-03-21T23:16:00-04:00
lastmod = 2024-06-12T19:13:26-04:00
tags = ["breakfasts", "recipes"]
draft = false
+++

| Info       | Amount    |
|------------|-----------|
| Prep Time  | 5 min     |
| Chill Time | 4 h       |
| Yields     | 1 serving |


## Ingredients {#ingredients}

| Quantity | Item                                                                                                   |
|----------|--------------------------------------------------------------------------------------------------------|
| 1/2 cup  | [oats]({{< relref "../items/oat.md" >}})                                                               |
| 2-3 tsp  | [peanut butter]({{< relref "../items/peanut_butter.md" >}})                                            |
| 1 tsp    | [cocoa powder]({{< relref "../items/cocoa_powder.md" >}})                                              |
| 1 tsp    | [vanilla extract]({{< relref "../items/vanilla_extract.md" >}})                                        |
| 2 tsp    | [maple syrup]({{< relref "../items/maple_syrup.md" >}}) or [honey]({{< relref "../items/honey.md" >}}) |
| 1/2 cup  | [milk]({{< relref "../items/milk.md" >}})                                                              |
| 1 tsp    | [chia seeds]({{< relref "../items/chia_seed.md" >}}) (optional)                                        |


## Directions {#directions}

1.  Combine all ingredients in small jar or bowl that can be sealed
2.  Stir well and then cover
3.  Refrigerate overnight


## References {#references}

1.  Original recipe: [Lemons and Zest](https://lemonsandzest.com/wprm_print/9801)
