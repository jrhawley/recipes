+++
title = "Pot roast"
author = ["James Hawley"]
date = 2019-03-23T00:00:00-04:00
lastmod = 2024-05-23T21:40:57-04:00
tags = ["beef", "recipes", "slow-cooker", "entrees"]
draft = false
+++

| Info      | Amount      |
|-----------|-------------|
| Prep Time | 15 min      |
| Cook Time | 5 h         |
| Yields    | 12 servings |


## Ingredients {#ingredients}

| Quantity | Item                                                            |
|----------|-----------------------------------------------------------------|
| 3 lbs    | [chuck roast]({{< relref "../items/beef_chuck.md" >}})          |
| 5        | [potatoes]({{< relref "../items/russet_potato.md" >}}), chunked |
| 1        | [onion]({{< relref "../items/onion.md" >}}), chunked            |
| 4        | [carrots]({{< relref "../items/baby_carrot.md" >}}), chunked    |
| 2 cups   | [beef broth]({{< relref "../items/beef_broth.md" >}})           |
| 2 cups   | [water]({{< relref "../items/water.md" >}})                     |
| 1/2 tsp  | [garlic powder]({{< relref "../items/garlic_powder.md" >}})     |
| 1/2 tsp  | [onion powder]({{< relref "../items/onion_powder.md" >}})       |
|          | [kosher salt]({{< relref "../items/kitchen_salt.md" >}})        |
|          | [pepper]({{< relref "../items/black_pepper.md" >}})             |


## Directions {#directions}

1.  Cut all potatoes and onions into large chunks, around 2" square
2.  Add the roast to the slow cooker, then the potatoes and onions and carrots.
    1.  Top with seasonings
3.  Add the beef broth
    1.  The roast should be covered in liquid so add water until it is covered.
4.  Cover, and set the slow cooker to 5h on high or 8h on low
5.  Use the leftover juice to make a gravy by boiling in a small pot while whisking in flour to the desired texture


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [My Natural Family](https://www.mynaturalfamily.com/paleo-pot-roast-crock-pot/)
