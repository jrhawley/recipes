+++
title = "Apple butter"
author = ["James Hawley"]
date = 2023-09-10T09:14:00-04:00
publishDate = 2023-09-10
lastmod = 2023-09-10T21:05:48-04:00
tags = ["spreads", "recipes"]
draft = false
+++

| Info      | Amount |
|-----------|--------|
| Prep Time | 1 h    |
| Cook Time | 7 h    |
| Yields    | 7 cups |


## Ingredients {#ingredients}

| Quantity | Item                                                     |
|----------|----------------------------------------------------------|
| 2.5 kg   | [crabapples]({{< relref "../items/crabapple.md" >}})     |
| 3 cup    | [water]({{< relref "../items/water.md" >}})              |
| 3/2 cup  | [sugar]({{< relref "../items/sugar.md" >}}) [^fn:1]      |
| 2        | [lemons]({{< relref "../items/lemon.md" >}}), juiced     |
| 2 tsp    | [ground cinnamon]({{< relref "../items/cinnamon.md" >}}) |


## Directions {#directions}

1.  Quarter and core the apples.
2.  Add apples, water, sugar, and lemon juice to a slow cooker, cooking on high for 2 h.
3.  Use an immersion blender and blend the sauce together.
4.  Continue to cook on high for another 5 - 6 h, until the sauce is thickened and golden brown.
5.  Add cinnamon and extra sugar, if desired, to taste, stirring altogether.


## References {#references}

1.  Original recipe: [Champagne Tastes](https://champagne-tastes.com/crabapple-recipe/)
2.  Apple butter will last in the fridge in an airtight container for ~ 1 month, or the freezer for ~ 6 months.

[^fn:1]: [White sugar]({{< relref "../items/white_sugar.md" >}}), [brown sugar]({{< relref "../items/brown_sugar.md" >}}), or another kind of sugar are all acceptable, depending on the desired flavour.