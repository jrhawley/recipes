+++
title = "Aperol spritz"
author = ["James Hawley"]
date = 2022-03-13T00:00:00-05:00
publishDate = 2022-03-13
lastmod = 2023-08-22T21:52:42-04:00
tags = ["recipes", "alcohols", "beverages"]
draft = false
+++

## Ingredients {#ingredients}

| Quantity | Ingredients                                         |
|----------|-----------------------------------------------------|
| 3 oz     | [prosecco]({{< relref "../items/prosecco.md" >}})   |
| 2 oz     | [Aperol]({{< relref "../items/aperol.md" >}})       |
| 1 oz     | [club soda]({{< relref "../items/club_soda.md" >}}) |


## Directions {#directions}

1.  Add all ingredients to an ice-filled glass.
2.  Stir and garnish with an orange wedge or slice.


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [The Periodic Table of Cocktails]({{< relref "../references/Periodic_Table_of_Cocktails.md" >}})
