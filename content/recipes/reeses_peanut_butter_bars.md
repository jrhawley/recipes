+++
title = "Reese's Peanut Butter Bars"
author = ["James Hawley"]
date = 2013-11-09T00:00:00-05:00
lastmod = 2023-12-21T22:32:47-05:00
tags = ["recipes", "desserts"]
draft = false
+++

| Info      | Amount |
|-----------|--------|
| Prep Time |        |
| Cook Time |        |
| Yields    |        |


## Ingredients {#ingredients}

| Quantity | Item                                                                     |
|----------|--------------------------------------------------------------------------|
| 1 cup    | [butter]({{< relref "../items/butter.md" >}}), melted                    |
| 2 cup    | [graham cracker]({{< relref "../items/graham_cracker.md" >}}) crumbs     |
| 2 cup    | [icing sugar]({{< relref "../items/icing_sugar.md" >}})                  |
| 5/4 cup  | [peanut butter]({{< relref "../items/peanut_butter.md" >}})              |
| 3/2 cup  | semisweet [chocolate chips]({{< relref "../items/chocolate_chip.md" >}}) |


## Directions {#directions}

1.  In a medium bowl, mix together the melted butter, graham cracker crumbs, powdered sugar, and 1 cup peanut butter
    1.  Blend well
    2.  Press evenly into the bottom of an ungreased 9" x 13" pan
2.  In a metal bowl over simmering water, or in the microwave, melt the chocolate chips with the peanut butter
    1.  Stir occasionally until smooth
    2.  Spread over the peanut butter layer
    3.  Refrigerate for at least one hour before cutting into squares
