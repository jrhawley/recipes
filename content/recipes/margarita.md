+++
title = "Margarita"
author = ["James Hawley"]
date = 2022-03-13T00:00:00-05:00
lastmod = 2023-12-21T22:12:45-05:00
tags = ["recipes", "alcohols", "beverages"]
draft = false
+++

## Ingredients {#ingredients}

| Quantity | Item                                                  |
|----------|-------------------------------------------------------|
| 5/3 oz   | [tequila]({{< relref "../items/tequila.md" >}})       |
| 1 oz     | [lime juice]({{< relref "../items/lime_juice.md" >}}) |
| 5/6 oz   | [Triple Sec]({{< relref "../items/triple_sec.md" >}}) |


## Directions {#directions}

1.  Salt the rim of a martini glass or champagne coupe.
2.  Shake all ingredients with ice.
3.  Double strain into the glass.


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [The Periodic Table of Cocktails]({{< relref "../references/Periodic_Table_of_Cocktails.md" >}})
