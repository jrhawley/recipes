+++
title = "Peanut butter fudge"
author = ["James Hawley"]
date = 2014-04-24T00:00:00-04:00
lastmod = 2024-05-23T21:31:55-04:00
tags = ["recipes", "desserts"]
draft = false
+++

| Info      | Amount |
|-----------|--------|
| Prep Time |        |
| Cook Time |        |
| Yields    |        |


## Ingredients {#ingredients}

| Quantity | Item                                                                                    |
|----------|-----------------------------------------------------------------------------------------|
| 1/2 cup  | [butter]({{< relref "../items/butter.md" >}}), unsalted                                 |
| 1/2 tsp  | [nutmeg]({{< relref "../items/nutmeg.md" >}})                                           |
| 1 lb     | light [muscovado sugar]({{< relref "../items/muscovado_sugar.md" >}}) (soft dark brown) |
| 1/2 cup  | [milk]({{< relref "../items/milk.md" >}})                                               |
| 1 Tbsp   | [vanilla extract]({{< relref "../items/vanilla_extract.md" >}})                         |
| 1 cup    | extra crunchy [peanut butter]({{< relref "../items/peanut_butter.md" >}})               |
| 5/2 cup  | [icing sugar]({{< relref "../items/icing_sugar.md" >}})                                 |
| 1/2 cup  | semi-sweet [chocolate chips]({{< relref "../items/chocolate_chip.md" >}}) (optional)    |


## Directions {#directions}

1.  Over medium heat, melt butter in large saucepan with butter
    1.  Stir in brown sugar and milk with large spoon
    2.  Remove any lumps, bring to boil for 2-3 min without stirring
2.  Remove from heat, stir in vanilla and peanut butter
3.  Place powdered sugar in large heatproof mixing bowl
    1.  Pour hot peanut butter mixture on top of sugar
    2.  Beat with wooden spoon until smooth
4.  Pour into 8" square pan, cover the top with parchment
    1.  Press down to smooth out top (maybe with another 8" pan)
5.  Set aside to cool for 10 min
    1.  Place fridge in to cool completely
6.  Take out fudge onto parchment, cut into 36 pieces
7.  Melt chocolate, add thin layer to the top of each piece
    1.  Allow chocolate to set, sprinkle with a few grains of salt
8.  Store in airtight container
