+++
title = "Black currant buttermilk doughnuts"
author = ["James Hawley"]
date = 2023-04-08T11:41:00-04:00
lastmod = 2023-12-19T22:32:34-05:00
tags = ["desserts", "recipes"]
draft = false
+++

| Info      | Amount      |
|-----------|-------------|
| Prep Time | 10 min      |
| Cook Time | 10 min      |
| Yields    | 8 doughnuts |


## Ingredients {#ingredients}


### Doughnuts {#doughnuts}

| Quantity | Item                                                        |
|----------|-------------------------------------------------------------|
| 2        | [eggs]({{< relref "../items/eggs.md" >}})                   |
| 1/2 cup  | [buttermilk]({{< relref "../items/buttermilk.md" >}})       |
| 1/3 cup  | [honey]({{< relref "../items/honey.md" >}})                 |
| 1 Tbsp   | [baking powder]({{< relref "../items/baking_powder.md" >}}) |
| 1/2 tsp  | [baking soda]({{< relref "../items/baking_soda.md" >}})     |
| 5/2 cup  | [flour]({{< relref "../items/flour.md" >}})                 |
|          | [vegetable oil]({{< relref "../items/vegetable_oil.md" >}}) |


### Glaze {#glaze}

| Quantity | Item                                                                |
|----------|---------------------------------------------------------------------|
| 2 cup    | [icing sugar]({{< relref "../items/icing_sugar.md" >}})             |
| 1/2 cup  | [black currant jam]({{< relref "../items/black_currant_jam.md" >}}) |
| 1 Tbsp   | [whipping cream]({{< relref "../items/whipping_cream.md" >}})       |


## Directions {#directions}

1.  In a mixing bowl, whisk together the eggs, buttermilk, and honey until fully combined.
    1.  Add baking powder and soda and mix.
    2.  Add the flour and fold 2 - 3 times, until just mixed and a soft dough forms.
2.  Turn the dough out onto a well-floured surface and shape it into a disc, about 1/2 cm thick.
    1.  Using a 7.5 cm (3") cookie cutter, cut out rounds from the dough.
    2.  Using a small cookie cutter or icing tip, cut out a centre hole in each round, about 1 cm (1/2 ") thick.
    3.  Reform the dough and repeat until all the dough is used.
3.  In a heavy-bottomed skillet or pot over medium-high heat, heat 2.5 cm (1 ") of oil to 190 C (375 F).
    1.  Turn down the heat to medium once the oil reaches the desired temperature.
    2.  Using tongs, slip 2 - 3 doughnuts in at a time into the oil&nbsp;[^fn:1].
    3.  Flip after 1 min&nbsp;[^fn:2].
    4.  Cook for another minute, then transfer to a wire rack with parchment paper underneath to catch and dripping oil.
    5.  Give the oil 1 - 2 min to return to temperature, then repeat with the remaining doughnuts.
4.  In a shallow bowl, whisk together the glaze ingredients.
    1.  When the doughnuts are cool enough to touch, dip each one in the glaze and place back on the wire rack for 10 min.
5.  Serve as soon as the glaze has set.


## References {#references}

1.  Original recipe: [A Rising Tide]({{< relref "../references/A_Rising_Tide.md" >}})

[^fn:1]: Ensure the doughnuts have space to move, aren't over-crowded, and aren't sticking to the bottom of the pan.
[^fn:2]: They will be golden brown and may have cracked open a bit, which is perfect.
    The cracks allow the glaze to seep into the doughnuts.