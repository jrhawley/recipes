+++
title = "Aglio, olio e peperoncino"
author = ["James Hawley"]
date = 2020-11-01T00:00:00-04:00
lastmod = 2024-05-23T21:24:28-04:00
tags = ["pastas", "recipes", "entrees"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 5 min      |
| Cook Time | 15 min     |
| Yields    | 4 servings |


## Ingredients {#ingredients}

| Quantity | Item                                                                  |
|----------|-----------------------------------------------------------------------|
| 4 cloves | [garlic]({{< relref "../items/garlic.md" >}}), finely diced           |
| 3/4 cup  | [olive oil]({{< relref "../items/olive_oil.md" >}})                   |
| 20       | [anchovies]({{< relref "../items/anchovy.md" >}})                     |
| 30       | [cherry tomatoes]({{< relref "../items/cherry_tomato.md" >}}), halved |
| 500 g    | [spaghetti]({{< relref "../items/spaghetti.md" >}})                   |
|          | [pepper]({{< relref "../items/black_pepper.md" >}})                   |
|          | dry [chili flakes]({{< relref "../items/red_pepper_flake.md" >}})     |


## Directions {#directions}

1.  In a big non-stick pan add the olive oil, chili peppers and the garlic
    1.  Cook the garlic until it is nice and golden (white: too early, brown: too late)
2.  Remove from heat and add anchovies
3.  Once searing, return back to the heat and add cherry tomatoes
4.  Add pepper to taste, no need for salt
    1.  Fry on medium heat and have the tomatoes blister
5.  Reduce heat and let simmer while the pasta cooks until _al dente_
6.  Drain pasta, then toss with mixture in the pan before serving


## References &amp; Notes {#references-and-notes}

1.  Original recipe: Giacomo Grillo

> This recipe is the favorite of my dad.
> After a long and hard day at work, this pasta dish is all he needs to be in peace and happy.
> It is super-easy and straightforward but incredibly delicious.

1.  The sauce takes minutes to prepare so bring the water to the boil.

Do not add a lot of salt to the water since the sauce is a little strong on seasoning.

1.  **The reaction after adding the anchovies is pretty violent, so add them and with a wooden spoon and melt them with the heat off**

2.  You can prepare a little fancy garnish by toasting some breadcrumbs and fresh parsley in a non-stick pan until breadcrumbs are golden brown.

Once you serve the pasta sprinkle some breadcrumbs on the pasta.
