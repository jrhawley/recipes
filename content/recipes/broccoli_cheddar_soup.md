+++
title = "Broccoli and cheddar soup"
author = ["James Hawley"]
date = 2019-11-13T00:00:00-05:00
lastmod = 2024-05-23T21:39:32-04:00
tags = ["soups", "recipes", "vegetarian", "entrees"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 10 min     |
| Cook Time | 3 h        |
| Yields    | 6 servings |


## Ingredients {#ingredients}

| Quantity | Item                                                                     |
|----------|--------------------------------------------------------------------------|
| 5 cups   | [broccoli]({{< relref "../items/broccoli.md" >}}) florets, thinly sliced |
| 1        | medium [yellow onion]({{< relref "../items/onion.md" >}}), diced         |
| 3 cloves | [garlic]({{< relref "../items/garlic.md" >}}), minced                    |
| 1 cup    | [carrots]({{< relref "../items/baby_carrot.md" >}}), grated              |
| 2 oz     | reduced fat [cream cheese]({{< relref "../items/cream_cheese.md" >}})    |
| 1 tsp    | [table salt]({{< relref "../items/table_salt.md" >}})                    |
| 1 tsp    | [pepper]({{< relref "../items/black_pepper.md" >}})                      |
| 1/4 tsp  | [nutmeg]({{< relref "../items/nutmeg.md" >}})                            |
| 4 cups   | [vegetable broth]({{< relref "../items/vegetable_broth.md" >}})          |
| 1 Tbsp   | [water]({{< relref "../items/water.md" >}})                              |
| 1/4 cup  | warm [water]({{< relref "../items/water.md" >}})                         |
| 1 Tbsp   | [corn starch]({{< relref "../items/corn_starch.md" >}})                  |
| 1/4 cup  | plain [Greek yogurt]({{< relref "../items/greek_yogurt.md" >}})          |
| 1 cup    | [cheddar]({{< relref "../items/cheddar.md" >}}), grated                  |


## Directions {#directions}

1.  Add vegetable broth, broccoli, onion, garlic, carrots, and cream cheese to slow cooker
    1.  Stir to combine
    2.  Cook on low for 4-5 h or high for 2-3 h
2.  When finished, mix 1 Tbsp of water with cornstarch in a saucepan
    1.  Add Greek yogurt and warm water
    2.  Simmer on low, add cheddar cheese until creamy and melted
    3.  Whisk into slow cooker


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [Skinny Ms](https://skinnyms.com/slow-cooker-broccoli-and-cheddar-soup/)
