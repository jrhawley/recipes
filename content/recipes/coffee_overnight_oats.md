+++
title = "Coffee overnight oats"
author = ["James Hawley"]
date = 2024-07-28T06:29:00-04:00
lastmod = 2024-10-19T01:06:58-04:00
tags = ["cereal", "breakfasts", "recipes"]
draft = false
+++

| Info      | Amount    |
|-----------|-----------|
| Prep Time | 5 min     |
| Cool Time | 4 h       |
| Yields    | 1 serving |


## Equipment {#equipment}


## Ingredients {#ingredients}

| Quantity | Item                                                                |
|----------|---------------------------------------------------------------------|
| 30 mL    | [coffee]({{< relref "../items/coffee.md" >}})                       |
| 1/2 cup  | [milk]({{< relref "../items/milk.md" >}})                           |
| 1 Tbsp   | [peanut butter]({{< relref "../items/peanut_butter.md" >}}) [^fn:1] |
| 1/2 cup  | rolled [oats]({{< relref "../items/oat.md" >}})                     |
| 1 Tbsp   | [chia seeds]({{< relref "../items/chia_seed.md" >}})                |
| 1 Tbsp   | [maple syrup]({{< relref "../items/maple_syrup.md" >}})             |
| 1/2 tsp  | [vanilla extract]({{< relref "../items/vanilla_extract.md" >}})     |
| pinch    | [salt]({{< relref "../items/salt.md" >}})                           |


## Directions {#directions}

1.  Mix all ingredients in a bowl and refrigerate for 4 h before serving.


## References {#references}

1.  Original recipe: [Back Road Coffee Roasters](https://backroadcoffee.ca/blogs/recipes/coffee-overnight-oats)

[^fn:1]: Or other nut butter
