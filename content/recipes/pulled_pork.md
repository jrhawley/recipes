+++
title = "Pulled pork"
author = ["James Hawley"]
date = 2017-12-03T00:00:00-05:00
lastmod = 2023-12-28T01:11:48-05:00
tags = ["pork", "entrees", "lunch", "sandwiches", "recipes", "slow-cooker"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 15 min     |
| Cook Time | 5 h        |
| Yields    | 8 servings |

{{< figure src="../assets/pulled-pork.jpg" caption="<span class=\"figure-number\">Figure 1: </span>Pulled pork" >}}


## Ingredients {#ingredients}

| Quantity | Item                                                                      |
|----------|---------------------------------------------------------------------------|
| 1 tsp    | [vegetable oil]({{< relref "../items/vegetable_oil.md" >}})               |
| 4 lbs    | [pork]({{< relref "../items/pork_chop.md" >}}) shoulder roast             |
| 1 cup    | [barbecue sauce]({{< relref "../items/barbecue_sauce.md" >}})             |
| 1/2 cup  | [apple cider vinegar]({{< relref "../items/apple_cider_vinegar.md" >}})   |
| 1/2 cup  | [chicken broth]({{< relref "../items/chicken_broth.md" >}})               |
| 1/4 cup  | [light brown sugar]({{< relref "../items/brown_sugar.md" >}})             |
| 1 Tbsp   | [mustard]({{< relref "../items/yellow_mustard.md" >}})                    |
| 1 Tbsp   | [Worcestershire sauce]({{< relref "../items/worcestershire_sauce.md" >}}) |
| 1 Tbsp   | [chili powder]({{< relref "../items/chili_powder.md" >}})                 |
| 1        | large [onion]({{< relref "../items/onion.md" >}}), diced                  |
| 2 cloves | [garlic]({{< relref "../items/garlic.md" >}}), minced                     |
| 2/3 tsp  | dried [thyme]({{< relref "../items/thyme.md" >}})                         |
| 8        | [hamburger buns]({{< relref "../items/bun.md" >}})                        |


## Directions {#directions}

1.  Pour vegetable oil into bottom of slow cooker
    1.  Place in pork roast
    2.  Pour in barbecue sauce, apple cider vinegar, and chicken broth
    3.  Stir in brown sugar, mustard, Worcestershire sauce, chili powder, onion, garlic, and thyme
    4.  Cover and cook on high until pork easily shreds with a fork (5-6 h)
2.  Remove roast from slow cooker and shred meat with two forks
    1.  Return shredded pork to slow cooker to stir meat into the juices
3.  Serve on hamburger buns
