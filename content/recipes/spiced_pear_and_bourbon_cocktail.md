+++
title = "Spiced pear and bourbon"
author = ["James Hawley"]
date = 2023-03-11T00:15:00-05:00
lastmod = 2023-12-22T01:14:22-05:00
tags = ["alcohols", "recipes", "beverages"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 2 min      |
| Cook Time | 40 min     |
| Yields    | 1 cocktail |


## Ingredients {#ingredients}

| Quantity | Item                                                       |
|----------|------------------------------------------------------------|
| 2 oz     | [bourbon]({{< relref "../items/bulleit_bourbon.md" >}})    |
| 1 oz     | [Spiced pear syrup]({{< relref "spiced_pear_syrup.md" >}}) |
| 1/2 oz   | [orange juice]({{< relref "../items/orange_juice.md" >}})  |


## Directions {#directions}

1.  Original recipe: [A Flavor Journal](https://aflavorjournal.com/wprm_print/4519)
