+++
title = "Plum citrus galette"
author = ["James Hawley"]
date = 2024-04-01T22:47:00-04:00
lastmod = 2024-05-23T20:30:30-04:00
tags = ["desserts", "baking", "recipes"]
draft = false
+++

| Info      | Amount |
|-----------|--------|
| Prep Time | 15 min |
| Cook Time | 30 min |
| Yields    | 8" pie |


## Equipment {#equipment}

-   1 baking sheet
-   1 medium mixing bowl
-   1 silicone spatula
-   1 small cutting board
-   1 paring knife
-   1 small bowl
-   1 pastry brush


## Ingredients {#ingredients}

| Quantity | Item                                                                |
|----------|---------------------------------------------------------------------|
| 1        | 10" [pie crust]({{< relref "../items/pie_crust.md" >}})             |
| 3        | [plums]({{< relref "../items/plum.md" >}})                          |
| 3/2 Tbsp | [white sugar]({{< relref "../items/white_sugar.md" >}})             |
| 1 tsp    | [lemon zest]({{< relref "../items/lemon_zest.md" >}})               |
| 1 Tbsp   | [lemon juice]({{< relref "../items/lemon_juice.md" >}})             |
| 1 Tbsp   | [all-purpose flour]({{< relref "../items/all-purpose_flour.md" >}}) |
| 1        | [egg]({{< relref "../items/eggs.md" >}})                            |
|          | coarse [sugar]({{< relref "../items/sugar.md" >}})                  |


## Directions {#directions}

1.  Preheat the oven to 190 C (375 F) and let the frozen/colled pie crust thaw while preparing the fruit filling.
2.  Thinly slice each plum and add them to the mixing bowl, along with the sugar, lemon juice, lemon zest, and flour.
3.  Fold the ingredients together, taking care to not bruise the plums.
4.  Lay out the plum slices in the centre of the pastry, then fold a 5 cm (2") crust over, gently overlapping the excess crust.
5.  In a small bowl, beat the egg and use a pastry brush to brush the egg wash over the entire surface of the galette and into every nook.
6.  Lightly sprinkle the coarse sugar over top, then place on a baking sheet and bake for 30 min, or until golden brown.
7.  Let sit for 5 - 10 min before serving.


## References {#references}

1.  Original recipe: [The Chalkboard Magazine](https://thechalkboardmag.com/hamptons-inspired-plum-citrus-galette-recipe/)
