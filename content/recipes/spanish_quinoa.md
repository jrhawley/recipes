+++
title = "Spanish quinoa"
author = ["James Hawley"]
date = 2020-05-21T00:00:00-04:00
lastmod = 2024-05-23T21:32:38-04:00
tags = ["recipes", "vegetarian", "entrees"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 10 min     |
| Cook Time | 20 min     |
| Yields    | 4 servings |

{{< figure src="../assets/spanish-quinoa.jpg" caption="<span class=\"figure-number\">Figure 1: </span>Spanish quinoa" >}}


## Ingredients {#ingredients}

| Quantity | Item                                                                                  |
|----------|---------------------------------------------------------------------------------------|
| 1 Tbsp   | [olive oil]({{< relref "../items/olive_oil.md" >}})                                   |
| 1        | small yellow [onion]({{< relref "../items/onion.md" >}}), finely diced                |
| 3 cloves | [garlic]({{< relref "../items/garlic.md" >}}), minced                                 |
| 2 tsp    | [paprika]({{< relref "../items/paprika.md" >}})                                       |
| 1 tsp    | [cumin]({{< relref "../items/cumin.md" >}})                                           |
| 1 tsp    | [oregano]({{< relref "../items/oregano.md" >}})                                       |
| 1/4 tsp  | [cayenne pepper]({{< relref "../items/cayenne_pepper.md" >}})                         |
| 1/2 tsp  | [table salt]({{< relref "../items/table_salt.md" >}})                                 |
| 1 cup    | [quinoa]({{< relref "../items/quinoa.md" >}}), uncooked                               |
| 15 oz    | [diced tomatoes]({{< relref "../items/diced_tomatoes.md" >}})                         |
| 15 oz    | [chickpeas]({{< relref "../items/chickpea.md" >}}), drained, rinsed                   |
| 2 cups   | [vegetable broth]({{< relref "../items/vegetable_broth.md" >}})                       |
| 15 oz    | [artichoke hearts]({{< relref "../items/artichoke_hearts.md" >}}), quartered, drained |
| 1        | [lemon]({{< relref "../items/lemon.md" >}})                                           |
|          | [parsley]({{< relref "../items/parsley.md" >}})                                       |


## Directions {#directions}

1.  In a large skillet, heat olive oil over medium heat
    1.  Add onion, garlic, cumin, oregano, cayenne, and salt
    2.  Saute until onion is translucent
2.  Add quinoa, diced tomatoes with juices, chickpeas, and vegetable broth
    1.  Bring to a low boil
    2.  Cover with lid and simmer for 20-30 min, stirring intermittently
3.  Stir in artichoke hearts until heated through
4.  Garnish with juice from one lemon and fresh parsley


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [Making Thyme for Health](https://www.makingthymeforhealth.com/one-pot-spanish-quinoa/print/)
