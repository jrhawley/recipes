+++
title = "Roasted beets and carrots with honey-balsamic glaze"
author = ["James Hawley"]
date = 2022-10-02T10:13:00-04:00
lastmod = 2023-12-22T00:57:16-05:00
tags = ["vegan", "sides", "vegetarian", "recipes"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 20 min     |
| Cook Time | 1 h 35 min |
| Yields    | 4 servings |


## Ingredients {#ingredients}

| Quantity | Item                                                              |
|----------|-------------------------------------------------------------------|
| 8        | [carrots]({{< relref "../items/carrot.md" >}})                    |
| 1 Tbsp   | [olive oil]({{< relref "../items/olive_oil.md" >}})               |
| 4        | medium sized [beets]({{< relref "../items/beet.md" >}})           |
| 2 Tbsp   | [honey]({{< relref "../items/honey.md" >}})                       |
| 3 Tbsp   | [balsamic vinegar]({{< relref "../items/balsamic_vinegar.md" >}}) |


## Directions {#directions}

1.  Preheat the oven to 180 C (350 F) and line a baking sheet with aluminium foil.
2.  Peel the carrots and cut into 2.5 cm (1 in) chunks.
3.  Wrap the beets in aluminium foil and place in a baking dish.
    -   Toss carrots with oil and put in a single layer on the baking sheet.
4.  Place beets in the oven and roast for 1 h.
    -   Once the hour is up, add the carrots to the oven and roast for 30 min more.
    -   Remove beets and set aside to cool.
    -   Check that carrots are tender to the fork, and cook for up to 20 min more if needed.
5.  Peel and chop beets into 2.5 cm (1 in) pieces.
6.  Combine balsamic vinegar and honey in a large saucepan over medium heat.
    -   Add beets and carrots and simmer until sauce is thickened and vegetables are glazed (5 - 10 min).


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [AllRecipes](https://allrecipes.com/recipe/278854/roasted-beets-and-carrots-with-honey-balsamic-glaze/)
