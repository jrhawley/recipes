+++
title = "Gnocchi and Brussels sprouts"
author = ["James Hawley"]
date = 2022-11-12T22:25:00-05:00
lastmod = 2024-05-23T21:11:56-04:00
tags = ["pastas", "entrees", "recipes"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 10 min     |
| Cook Time | 15 min     |
| Yields    | 2 servings |


## Ingredients {#ingredients}

| Quantity | Item                                                               |
|----------|--------------------------------------------------------------------|
| 16 oz    | [Brussels sprouts]({{< relref "../items/brussels_sprouts.md" >}})  |
| 1        | [lemon]({{< relref "../items/lemon.md" >}})                        |
| 4 Tbsp   | [olive oil]({{< relref "../items/olive_oil.md" >}})                |
| 1/2 tsp  | [red pepper flakes]({{< relref "../items/red_pepper_flake.md" >}}) |
| 18 oz    | [gnocchi]({{< relref "../items/gnocchi.md" >}})                    |
| 3 Tbsp   | [butter]({{< relref "../items/butter.md" >}})                      |
| 1/2 tsp  | [honey]({{< relref "../items/honey.md" >}})                        |
|          | [parmesan]({{< relref "../items/parmesan.md" >}})                  |
|          | [Kosher salt]({{< relref "../items/kitchen_salt.md" >}})           |
|          | [black pepper]({{< relref "../items/black_pepper.md" >}})          |


## Directions {#directions}

1.  Trim and halve the Brussels sprouts and zest the lemon.
2.  In a large skillet, heat 3 Tbsp of olive oil over medium-high heat.
    1.  Add the Brussels sprouts, season with salt and pepper
    2.  Sprinkle lemon zest over the top, cook until sprouts are browned.
    3.  Transfer to a medium bowl.
3.  Heat remaining olive oil over medium-high heat.
    1.  Break up any gnocchi that are stuck together and add to the pan.
    2.  Cook until slight brown on one side.
    3.  Add butter and honey, season with salt and pepper, and cook for another 1 - 2 min.
4.  Add Brussels sprouts to the pan, then grate parmesan when serving.


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [NYT Cooking](https://cooking.nytimes.com/recipes/1020453-crisp-gnocchi-with-brussels-sprouts-and-brown-butter)
