+++
title = "Beer cheese soup"
author = ["James Hawley"]
date = 2023-03-18T13:30:00-04:00
lastmod = 2024-05-23T20:47:26-04:00
tags = ["entrees", "soups", "recipes"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 10 min     |
| Cook Time | 30 min     |
| Yields    | 4 servings |


## Ingredients {#ingredients}

| Quantity | Item                                                                                                                           |
|----------|--------------------------------------------------------------------------------------------------------------------------------|
| 1/4 cup  | [butter]({{< relref "../items/butter.md" >}})                                                                                  |
| 1        | [white onion]({{< relref "../items/white_onion.md" >}}), diced                                                                 |
| 2        | [carrots]({{< relref "../items/carrot.md" >}}), peeled and diced                                                               |
| 1 clove  | [garlic]({{< relref "../items/garlic.md" >}}), minced                                                                          |
| 1 stalk  | [celery]({{< relref "../items/celery.md" >}}), diced                                                                           |
| 1/4 cup  | [flour]({{< relref "../items/flour.md" >}})                                                                                    |
| 2 tsp    | [brown mustard]({{< relref "../items/brown_mustard.md" >}})                                                                    |
| 1 cup    | light [ale]({{< relref "../items/ale.md" >}})                                                                                  |
| 6 cup    | [vegetable broth]({{< relref "../items/vegetable_broth.md" >}}) or [chicken broth]({{< relref "../items/chicken_broth.md" >}}) |
|          | [fine sea salt]({{< relref "../items/sea_salt.md" >}})                                                                         |
|          | [black pepper]({{< relref "../items/black_pepper.md" >}})                                                                      |
| 3 cup    | [cheddar]({{< relref "../items/cheddar.md" >}}), shredded                                                                      |
| 1/2 cup  | [heavy cream]({{< relref "../items/heavy_cream.md" >}})                                                                        |


## Directions {#directions}

1.  Melt butter in a large pot over medium-high heat
    1.  Add carrots, onion, and celery
    2.  Cook until tender, about 4-5 min
2.  Add flour and coat vegetables, cook an additional 4-5 min, stirring frequently
    1.  Add ground mustard, garlic, salt, pepper, sriracha, beer and stir to make a paste
    2.  Slowly begin adding chicken broth while constantly stirring to blend well
3.  Bring to a simmer for 25 min, stirring occasionally.
4.  Using an immersion blender or food processor, puree vegetables in the soup
    1.  Whisk in heavy whipping cream and cheddar cheese over medium-low heat until cheese is melted
    2.  If soup is not thickened, mix cornstarch and cold water in a small dish and whisk into boiling soup


## References {#references}

1.  Original recipe: [Simply Happenings](https://www.simplyhappenings.com/wprm_print/6721)
