+++
title = "Tequila sour"
author = ["James Hawley"]
date = 2022-11-12T22:56:00-05:00
lastmod = 2023-12-24T19:57:24-05:00
tags = ["alcohols", "recipes", "beverages"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 2 min      |
| Cook Time | 0 min      |
| Yields    | 1 cocktail |


## Ingredients {#ingredients}

| Quantity | Item                                                  |
|----------|-------------------------------------------------------|
| 5/3 oz   | [tequila]({{< relref "../items/tequila.md" >}})       |
| 5/6 oz   | [lime juice]({{< relref "../items/lime_juice.md" >}}) |
| 2/3 oz   | [simple syrup]({{< relref "simple_syrup.md" >}})      |
|          | [egg white]({{< relref "../items/eggs.md" >}})        |
|          | ice cubes                                             |


## Directions {#directions}

1.  Add all ingredients to a cocktail shaker.
2.  Shake without ice, then add ice and shake again.
3.  Strain into a rocks glass over ice.


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [The Periodic Table of Cocktails]({{< relref "../references/Periodic_Table_of_Cocktails.md" >}})
