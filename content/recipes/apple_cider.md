+++
title = "Apple cider"
author = ["James Hawley"]
date = 2017-10-15T00:00:00-04:00
lastmod = 2023-11-20T09:40:34-05:00
tags = ["recipes", "beverages", "slow-cooker"]
draft = false
+++

| Info      | Amount |
|-----------|--------|
| Prep Time | 10 min |
| Cook Time | 4 h    |
| Serves    | 8      |

{{< figure src="/ox-hugo/slow-cooker-apple-cider.jpg" caption="<span class=\"figure-number\">Figure 1: </span>Apple cider" >}}


## Ingredients {#ingredients}

| Quantity | Item                                                           |
|----------|----------------------------------------------------------------|
| 9        | [apples]({{< relref "../items/apple.md" >}}), assorted types   |
| 1        | [orange]({{< relref "../items/orange.md" >}})                  |
| 2        | [pears]({{< relref "../items/pear.md" >}})                     |
| 3        | [cinnamon sticks]({{< relref "../items/cinnamon_stick.md" >}}) |
| 1 seed   | [nutmeg]({{< relref "../items/nutmeg.md" >}})                  |
| 2 tsp    | [ground cloves]({{< relref "../items/clove.md" >}})            |
| 1/2 tsp  | [allspice]({{< relref "../items/allspice.md" >}})              |
| 3 L      | [water]({{< relref "../items/water.md" >}})                    |
| 1/4 cup  | [brown sugar]({{< relref "../items/brown_sugar.md" >}})        |


## Directions {#directions}

1.  Wash fruits, roughly cut into quarters
    1.  Don't need to worry about stems/seeds, since they'll be filtered out later
    2.  Place in slow cooker, and add spices
    3.  Cover mixture with water (leave small amount of space at the top of the slow cooker)
2.  Cook on high for 4 h, or low for 6-8 h
    1.  With 1 h left, use potato masher and mash fruits until they're soft
    2.  Let mixture cook for remaining hour
3.  Strain out apple cider juice into clean pitcher
    1.  Press through fine-mesh or strain with cheese cloth to get the most out of it
