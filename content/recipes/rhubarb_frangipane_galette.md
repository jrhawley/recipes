+++
title = "Rhubarb, bay leaf, and frangipane galette"
author = ["James Hawley"]
date = 2023-03-12T08:54:00-04:00
lastmod = 2023-12-22T00:56:48-05:00
tags = ["desserts", "recipes"]
draft = false
+++

| Info      | Amount    |
|-----------|-----------|
| Prep Time | 40 min    |
| Cook Time | 35 min    |
| Yields    | 1 galette |


## Ingredients {#ingredients}


### Pastry {#pastry}

| Quantity | Item                                                                |
|----------|---------------------------------------------------------------------|
| 3/2 cup  | [all-purpose flour]({{< relref "../items/all-purpose_flour.md" >}}) |
| 1 Tbsp   | [white sugar]({{< relref "../items/white_sugar.md" >}})             |
| 1/2 tsp  | [Kosher salt]({{< relref "../items/kitchen_salt.md" >}})            |
| 1/2 cup  | cold, unsalted [butter]({{< relref "../items/butter.md" >}}), cubed |
| 1/4 cup  | ice [water]({{< relref "../items/water.md" >}})                     |


### Filling {#filling}

| Quantity     | Item                                                              |
|--------------|-------------------------------------------------------------------|
| 3 - 4 stalks | [rhubarb]({{< relref "../items/rhubarb.md" >}})                   |
| 4            | fresh [bay leaves]({{< relref "../items/bay_leaf.md" >}}), minced |
| 1/4 cup      | [white sugar]({{< relref "../items/white_sugar.md" >}})           |


### Frangipane {#frangipane}

| Quantity | Item                                                                     |
|----------|--------------------------------------------------------------------------|
| 1/2 cup  | [almond flour]({{< relref "../items/almond_flour.md" >}})                |
| 2 Tbsp   | [white sugar]({{< relref "../items/white_sugar.md" >}})                  |
| 1/4 tsp  | [Kosher salt]({{< relref "../items/kitchen_salt.md" >}})                 |
| 2 Tbsp   | unsalted [butter]({{< relref "../items/butter.md" >}}), room temperature |
| 1        | [egg]({{< relref "../items/eggs.md" >}})                                 |
| 2 tsp    | [vanilla extract]({{< relref "../items/vanilla_extract.md" >}})          |


### To finish {#to-finish}

| Quantity | Item                                                        |
|----------|-------------------------------------------------------------|
| 1        | [egg]({{< relref "../items/eggs.md" >}}), beaten            |
|          | vanilla [ice cream]({{< relref "../items/ice_cream.md" >}}) |


## Directions {#directions}


### Pastry {#pastry}

1.  In a large bowl, mix the flour, sugar, and salt together
    1.  Add the butter cubes and use your fingertips to lightly work the butter into the flour until it’s the size of peas&nbsp;[^fn:1]
    2.  Add the ice water and continue to use your hands to mix everything together a little more until the dough is crumbly
2.  Lay a large sheet of plastic wrap on a work surface
    1.  Place the crumbly pastry dough onto the plastic wrap
    2.  Press the dough into a ball and wrap
    3.  Set the dough ball aside in the refrigerator until ready to use
3.  Place a 14" (36 cm) square piece of parchment paper onto a clean work surface
    1.  Dust the parchment paper with flour and roll out the dough into a 12" (30 cm) round
    2.  Sprinkle with more flour as needed to prevent it from sticking
    3.  Transfer the dough along with the parchment paper to a baking sheet and place in the refrigerator to set for 30 min


### Filling {#filling}

1.  Preheat the oven to 200 C (400 F)
2.  Cut the rhubarb in half lengthwise, then into 3/2" (4 cm) pieces
3.  In a small bowl, toss the bay leaves with the sugar
    1.  Use your fingers to rub the bay leaves with the sugar&nbsp;[^fn:2]
    2.  Pour your sugar mixture into a large bowl and add the prepared rhubarb
    3.  Mix until the rhubarb is well coated, then set aside


### Frangipane {#frangipane}

1.  In a small food processor, combine the almond flour, sugar, salt, butter, egg, and vanilla, puréeing until smooth


### Combining everything {#combining-everything}

1.  Spoon the frangipane into the centre of the rolled-out dough, leaving a 2.5 cm (1") border
2.  Artfully place the rhubarb on top of the frangipane to cover it
3.  Use the parchment paper as an aid to fold the exposed edge of dough toward the centre, enclosing the filling in a rustic manner
4.  Brush the edge of the dough with your egg wash
5.  Bake for 35 min or until the crust is golden and the filling bubbles like a cauldron
6.  Let the galette rest on a cooling rack for 5 - 10 min before serving
7.  Serve with ice cream or on its own


## References {#references}

1.  Original recipe: [Peak Season]({{< relref "../references/Peak_Season.md" >}})

[^fn:1]: Do your best not to let the butter melt in your hands
[^fn:2]: This will release some of their aromatics into the sugar