+++
title = "Hummus"
author = ["James Hawley"]
date = 2022-07-25T00:00:00-04:00
lastmod = 2023-12-28T01:35:16-05:00
tags = ["appetizers", "vegetarian", "recipes"]
draft = false
+++

| Info      | Amount |
|-----------|--------|
| Prep Time | 15 min |
| Cook Time | 5 min  |
| Yields    | 1 cup  |


## Ingredients {#ingredients}

| Quantity  | Item                                                             |
|-----------|------------------------------------------------------------------|
| 1 clove   | [garlic]({{< relref "../items/garlic.md" >}})                    |
| 3 Tbsp    | [tahini]({{< relref "../items/tahini.md" >}})                    |
| 15 oz can | [chickpeas]({{< relref "../items/chickpea.md" >}})               |
| 1         | [lemon]({{< relref "../items/lemon.md" >}}), juiced              |
| 2 Tbsp    | [olive oil]({{< relref "../items/olive_oil.md" >}})              |
| 1 tsp     | [salt]({{< relref "../items/kitchen_salt.md" >}})                |
| 2 Tbsp    | [parsley]({{< relref "../items/parsley.md" >}}), roughly chopped |
|           | [cumin]({{< relref "../items/cumin.md" >}})                      |
|           | [sumac]({{< relref "../items/sumac.md" >}})                      |


## Directions {#directions}

1.  Put all ingredients, except for parsley, in a food processor and blend until smooth&nbsp;[^fn:1]
2.  Use a spoon or spatula to fold in the chopped parsley
3.  Pour into a serving dish, and spread with a spoon or spatula
    1.  Make small troughs in the hummus and drizzle with olive oil
    2.  Sprinkle with salt, cumin, and sumac to taste


## References &amp; Notes {#references-and-notes}

1.  Inspiring recipes: [Food Republic](https://www.foodrepublic.com/recipes/best-basic-hummus-recipe/), [Classic Chickpea Hummus](https://www.bonappetit.com/recipe/classic-chickpea-hummus), [Refika's Kitchen](https://www.youtube.com/watch?v=88ljbueBLBw)
2.  Cover, store in the fridge for up to a week

[^fn:1]: If blending for a while, add ice cubes to keep cool.
    This will get blended into the hummus and melt, which will make it slightly less viscous.