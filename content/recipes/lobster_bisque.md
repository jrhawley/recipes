+++
title = "Lobster bisque"
author = ["James Hawley"]
date = 2024-01-21T00:00:00-05:00
lastmod = 2024-01-31T22:47:29-05:00
tags = ["recipes", "entrees", "soups", "seafood"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 10 min     |
| Cook Time | 1 h        |
| Yields    | 4 servings |


## Equipment {#equipment}

-   medium pot
-   soup pot
-   chef's knife
-   garlic press
-   kitchen shears
-   large wooden spoon
-   immersion blender


## Ingredients {#ingredients}

| Quantity   | Item                                                                                                                                   |
|------------|----------------------------------------------------------------------------------------------------------------------------------------|
| 4 (4oz)    | [lobster tail]({{< relref "../items/lobster_tail.md" >}}) [^fn:1]                                                                      |
| 1 L        | [clam juice]({{< relref "../items/clam_juice.md" >}})                                                                                  |
| 1 cup      | dry [white wine]({{< relref "../items/white_wine.md" >}})                                                                              |
| 1 L        | [chicken stock]({{< relref "../items/chicken_stock.md" >}})                                                                            |
| 3/2 Tbsp   | [chicken boullion]({{< relref "../items/chicken_boullion.md" >}}) or [lobster boullion]({{< relref "../items/lobster_boullion.md" >}}) |
| 6-7 cloves | [garlic]({{< relref "../items/garlic.md" >}}), pressed                                                                                 |
| 1          | large [white onion]({{< relref "../items/white_onion.md" >}}), rough chopped                                                           |
| 6 stalks   | [celery]({{< relref "../items/celery.md" >}}), rough chopped                                                                           |
| 3          | medium [carrots]({{< relref "../items/carrot.md" >}}), rough chopped                                                                   |
|            | [olive oil]({{< relref "../items/olive_oil.md" >}})                                                                                    |
| 2 sticks   | [butter]({{< relref "../items/butter.md" >}}), unsalted                                                                                |
|            | [salt]({{< relref "../items/salt.md" >}}), to taste                                                                                    |
|            | [red pepper flakes]({{< relref "../items/red_pepper_flake.md" >}})                                                                     |
| 2 tsp      | [paprika]({{< relref "../items/paprika.md" >}})                                                                                        |
| 2/3 cup    | [all-purpose flour]({{< relref "../items/all-purpose_flour.md" >}})                                                                    |
| 1 can      | [tomato paste]({{< relref "../items/tomato_paste.md" >}})                                                                              |
| 1/2 cup    | [heavy cream]({{< relref "../items/heavy_cream.md" >}})                                                                                |
| 1/2 tsp    | [sugar]({{< relref "../items/sugar.md" >}})                                                                                            |
|            | [sour cream]({{< relref "../items/sour_cream.md" >}}) thinned with heavy cream (for garnish)                                           |
|            | [black pepper]({{< relref "../items/black_pepper.md" >}})                                                                              |
|            | [chives]({{< relref "../items/chive.md" >}}), minced (for garnish)                                                                     |


## Directions {#directions}

1.  Start by preparing the lobster tails. Boil clam juice, white wine, chicken stock, and bouillon in a saucepan.
    1.  Bring the liquid to a hard boil for 5 minutes, then turn off the burner and add in the lobster tails. The liquid temp at this point should be around 175-180F/80C. Set a timer for 6 minutes. If using shrimp, boil for only 4 minutes.
    2.  After boiling, check the internal temperature of the lobster tails (130-135°F/55C) using an instant-read thermometer. Transfer the tails to the fridge to cool for 5 minutes.
    3.  Use food scissors to cut the tails open on both sides.Peel off the outer shell and refrigerate the meat while preparing the bisque .
    4.  Chop the lobster shells into chunky pieces. Also, rough chop onion, celery, carrots, and press garlic.
2.  In a large soup pot over medium heat, add a long squeeze of olive oil, butter, and the chopped lobster shells. Fry the shells for 5-6 minutes to infuse the fat with lobster flavor.
    1.  Strain the shells out of the lobster-infused butter, discard the shells, and return the pot and strained butter to the medium heat. Add chopped veggies and a large pinch of salt. Sweat the veggies for 5-6 minutes, avoiding browning.
    2.  Add a pinch of chili flakes, paprika, and 75 grams of all-purpose flour. Stir and cook to create a roux.
3.  Add tomato paste and combine it with the roux, scraping the bottom of the pot.
    1.  Add in the lobster cooking liquid and bring it to a boil.
    2.  Once boiling, cover the pot with a lid and simmer for 15 minutes over medium heat until the veggies have softened.
    3.  After simmering, stir in cream and simmer for an additional 5 minutes.
4.  Use an immersion blender to puree the soup for at least 3 minutes until smooth and velvety. Alternatively use a standard blender. Taste for seasoning and add salt to taste.  Add a small amount of sugar (2 grams)  to enhance lobster flavor.
5.  Chop 1 lobster tail into bite sized pieces and chop the rest a bit more finely. Stir in finely chopped lobster meat and let it warm through.
6.  Ladle bisque into a bowl. Garnish with reserved chunky lobster meat, drizzle with creme fraiche, fresh black pepper, and a sprinkle minced chives.


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [Brian Lagerstrom](https://www.youtube.com/watch?v=XoiaW7XO2sI)

[^fn:1]: Can substitute for 12 [shrimp]({{< relref "../items/shrimp.md" >}})