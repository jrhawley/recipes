+++
title = "Green curry with tofu and cashews"
author = ["James Hawley"]
date = 2022-07-25T00:00:00-04:00
lastmod = 2024-05-23T21:14:35-04:00
tags = ["recipes", "entrees", "vegan", "vegetarian"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 10 min     |
| Cook Time | 15 min     |
| Yields    | 4 servings |


## Ingredients {#ingredients}

| Quantity | Item                                                                   |
|----------|------------------------------------------------------------------------|
| 1 Tbsp   | [coconut oil]({{< relref "../items/coconut_oil.md" >}})                |
| 2        | [shallots]({{< relref "../items/shallot.md" >}})                       |
| 3 cloves | [garlic]({{< relref "../items/garlic.md" >}}), minced                  |
| 2 Tbsp   | [green curry paste]({{< relref "../items/green_curry.md" >}})          |
| 3/2 cups | [vegetable broth]({{< relref "../items/vegetable_broth.md" >}})        |
| 400 mL   | [coconut milk]({{< relref "../items/coconut_milk.md" >}})              |
| 1 Tbsp   | [soy sauce]({{< relref "../items/soy_sauce.md" >}})                    |
| 1 tsp    | [brown sugar]({{< relref "../items/brown_sugar.md" >}})                |
| 3 cups   | [broccoli florets]({{< relref "../items/broccoli.md" >}})              |
| 1        | [zucchini]({{< relref "../items/zucchini.md" >}})                      |
| 6 oz     | [green beans]({{< relref "../items/green_bean.md" >}})                 |
| 1 lb     | [extra-firm tofu]({{< relref "../items/tofu.md" >}})                   |
| 1 cup    | [parsley]({{< relref "../items/parsley.md" >}}) leaves, lightly packed |
| 2 tsp    | [lime juice]({{< relref "../items/lime_juice.md" >}})                  |
| 1        | [lime]({{< relref "../items/lime.md" >}})                              |
| 1 cup    | [rice]({{< relref "../items/basmati_rice.md" >}})                      |
| 1/2 cup  | [cashews]({{< relref "../items/cashew.md" >}})                         |
|          | [kosher salt]({{< relref "../items/kitchen_salt.md" >}})               |
|          | [basil leaves]({{< relref "../items/basil.md" >}})                     |


## Directions {#directions}

1.  Prepare the vegetables.
    1.  Thinly slice the shallots.
    2.  Mince the garlic.
    3.  Halve the zucchini lengthwise and slice into finger-width chunks with a bias.
    4.  Halve the green beans
    5.  Drain the tofu and cut into 1" cubes.
2.  In a large pot, heat oil over medium heat.
    1.  Add shallots and cook until just starting to brown, ~4 min.
    2.  Add garlic and cook for 1 min.
    3.  Add curry paste, cook for 30 s.
3.  Add vegetable broth, coconut milk, soy sauce, brown sugar and salt.
    1.  Raise heat to high.
    2.  When it comes to a boil, add broccoli, zucchini, beans and tofu.
    3.  When it returns to a boil, partially cover and reduce heat to maintain a brisk simmer.
    4.  Cook, gently stirring twice, until vegetables are al dente, ~4 ­min.
    5.  Remove from heat.
4.  Place parsley in a food processor.
    1.  Pulse until finely chopped.
    2.  Add 1 cup (250 mL) of hot cooking liquid.
    3.  Run processor until very finely chopped and liquid is light green.
    4.  Return mixture to curry and gently stir in along with lime juice.
    5.  Taste for salt.
5.  Using a rasp, lightly grate a little lime zest overtop.
    1.  Sprinkle with cashews and basil.
    2.  Serve with steamed rice.


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [LCBO](https://www.lcbo.com/webapp/wcs/stores/servlet/en/lcbo/recipe/vegan-green-curry-with-tofu-cashews/F202105023)
