+++
title = "Goddess bowl"
author = ["James Hawley"]
date = 2023-03-19T10:18:00-04:00
lastmod = 2023-12-21T21:56:37-05:00
tags = ["vegetarian", "salads", "recipes"]
draft = false
+++

| Info      | Amount         |
|-----------|----------------|
| Prep Time | 30 min         |
| Cook Time | 1 h            |
| Yields    | 4 - 6 servings |


## Ingredients {#ingredients}


### Dressing {#dressing}

| Quantity | Item                                                                |
|----------|---------------------------------------------------------------------|
| 1/4 cup  | [tahini]({{< relref "../items/tahini.md" >}})                       |
| 1 clove  | [garlic]({{< relref "../items/garlic.md" >}})                       |
| 1/2 cup  | [lemon juice]({{< relref "../items/lemon_juice.md" >}}) (2 lemons)  |
| 1/4 cup  | [nutritional yeast]({{< relref "../items/nutritional_yeast.md" >}}) |
| 3 Tbsp   | [olive oil]({{< relref "../items/olive_oil.md" >}})                 |
|          | [fine sea salt]({{< relref "../items/sea_salt.md" >}})              |
|          | [black pepper]({{< relref "../items/black_pepper.md" >}})           |


### Salad {#salad}

| Quantity | Item                                                                       |
|----------|----------------------------------------------------------------------------|
| 1 cup    | [lentils]({{< relref "../items/lentil.md" >}})                             |
| 1 cup    | [quinoa]({{< relref "../items/quinoa.md" >}}), uncooked                    |
| 3/2 tsp  | [olive oil]({{< relref "../items/olive_oil.md" >}})                        |
| 1        | small [red onion]({{< relref "../items/red_onion.md" >}}), chopped         |
| 3 cloves | [garlic]({{< relref "../items/garlic.md" >}}), minced                      |
| 1        | [bell pepper]({{< relref "../items/bell_pepper.md" >}}), chopped           |
| 1        | [beefsteak tomato]({{< relref "../items/beefsteak_tomato.md" >}}), chopped |
| 3 cup    | [spinach]({{< relref "../items/spinach.md" >}}), roughly chopped           |
| 1/2 cup  | [parsley]({{< relref "../items/parsley.md" >}}), minced                    |


## Directions {#directions}

1.  In a food processor, combine all the dressing ingredients and blend until smooth
2.  Cook the lentils and quinoa according to instructions, if necessary
3.  In a large skillet, heat the olive oil over medium heat
    1.  Add the onion and garlic, sauteeing for a few min
    2.  Add the pepper and tomato, cooking for 7 - 8 min more
    3.  Stir in the spinach and cook until wilted
4.  Stir in the dressing and cooked lentils and spelt
    1.  Reduce heat to low and simmer for a few min
5.  Top with minced parsley, then season with salt and pepper to taste before serving


## References {#references}

1.  Original recipe: [The Oh She Glows Cookbook]({{< relref "../references/Oh_She_Glows_Cookbook.md" >}})
