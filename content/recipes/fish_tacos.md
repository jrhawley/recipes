+++
title = "Fish tacos"
author = ["James Hawley"]
date = 2022-11-26T11:53:00-05:00
lastmod = 2023-12-28T01:10:02-05:00
tags = ["fish", "entrees", "wraps", "recipes"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 5 min      |
| Cook Time | 20 min     |
| Yields    | 4 servings |


## Ingredients {#ingredients}

| Quantity | Item                                                              |
|----------|-------------------------------------------------------------------|
| 2 filets | breaded [haddock]({{< relref "../items/haddock.md" >}})           |
| 1/4      | [red onion]({{< relref "../items/red_onion.md" >}}), finely diced |
| 2        | [avocadoes]({{< relref "../items/avocado.md" >}}), sliced         |
| 4        | [tortillas]({{< relref "../items/tortilla.md" >}})                |
|          | [cherry tomatoes]({{< relref "../items/cherry_tomato.md" >}})     |
|          | [Kosher salt]({{< relref "../items/kitchen_salt.md" >}})          |
|          | [lime juice]({{< relref "../items/lime_juice.md" >}})             |


## Directions {#directions}

1.  Cook the fish, then dice into chunks
2.  While the fish is cooking, dice the onion and slice the avocadoes and tomatoes
3.  Put everything in the tortillas and drizzle with lime juice and sprinkle with salt, to taste
