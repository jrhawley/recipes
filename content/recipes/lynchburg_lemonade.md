+++
title = "Lynchburg lemonade"
author = ["James Hawley"]
date = 2022-03-13T00:00:00-05:00
lastmod = 2023-12-21T22:11:54-05:00
tags = ["recipes", "alcohols", "beverages"]
draft = false
+++

## Ingredients {#ingredients}

| Quantity | Ingredient                                              |
|----------|---------------------------------------------------------|
| 4 / 3 oz | [whiskey]({{< relref "../items/whiskey.md" >}}) [^fn:1] |
| 2 / 3 oz | [Triple sec]({{< relref "../items/triple_sec.md" >}})   |
| 5 / 6 oz | [lemon juice]({{< relref "../items/lemon_juice.md" >}}) |
|          | lemon-lime soda                                         |

[^fn:1]: Names after Lynchburg, Tennessee, the home of Jack Daniel's.
    Use a Jack Daniel's whiskey for an authentic Lynchburg lemonade.