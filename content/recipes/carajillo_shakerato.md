+++
title = "Carajillo shakerato"
author = ["James Hawley"]
date = 2022-09-20T18:04:00-04:00
lastmod = 2023-12-22T16:37:17-05:00
tags = ["alcohols", "recipes", "beverages", "coffee"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 5 min      |
| Cook Time | 0 min      |
| Yields    | 1 cocktail |


## Ingredients {#ingredients}

| Quantity | Item                                              |
|----------|---------------------------------------------------|
| 1 oz     | [Licor 43]({{< relref "../items/licor_43.md" >}}) |
|          | [espresso]({{< relref "../items/espresso.md" >}}) |
|          | ice                                               |


## Directions {#directions}

1.  Place the ice cubes and Licor 43 in a cocktail shaker.
2.  Pull the single (or double) shot of espresso and pour into the cocktail shaker.
3.  Wearing gloves, shake the shaker until a desired foaminess is reached.
4.  Strain out any ice and serve drink in a coupe or low ball glass.


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [The Espresso Martini Killer - Internet Shaquille](https://www.youtube.com/watch?v=9Ilnnne01So)
