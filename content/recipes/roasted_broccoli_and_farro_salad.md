+++
title = "Roasted broccoli and farro salad"
author = ["James Hawley"]
date = 2023-05-30T13:18:00-04:00
lastmod = 2024-05-23T20:44:43-04:00
tags = ["entrees", "sides", "vegetarian", "recipes"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 10 min     |
| Cook Time | 30 min     |
| Yields    | 4 servings |


## Ingredients {#ingredients}


### Salad {#salad}

| Quantity | Item                                                              |
|----------|-------------------------------------------------------------------|
| 1 cup    | [farro]({{< relref "../items/farro.md" >}}), uncooked             |
| 6 cups   | [broccoli]({{< relref "../items/broccoli.md" >}})                 |
| 1 Tbsp   | [olive oil]({{< relref "../items/olive_oil.md" >}})               |
| 1/4 cup  | [dried cranberries]({{< relref "../items/dried_cranberry.md" >}}) |
| 1/3 cup  | [feta]({{< relref "../items/feta.md" >}}), crumbled               |
|          | [fine sea salt]({{< relref "../items/sea_salt.md" >}})            |
|          | [black pepper]({{< relref "../items/black_pepper.md" >}})         |


### Vinaigrette {#vinaigrette}

| Quantity | Item                                                              |
|----------|-------------------------------------------------------------------|
| 1/4 cup  | [olive oil]({{< relref "../items/olive_oil.md" >}})               |
| 5/2 Tbsp | [red wine vinegar]({{< relref "../items/red_wine_vinegar.md" >}}) |
| 1/4 tsp  | [honey]({{< relref "../items/honey.md" >}})                       |
| 1/2 tsp  | [Dijon mustard]({{< relref "../items/dijon_mustard.md" >}})       |
| 1/2 tsp  | [oregano]({{< relref "../items/oregano.md" >}})                   |
| 1/2 tsp  | [garlic powder]({{< relref "../items/garlic_powder.md" >}})       |
|          | [fine sea salt]({{< relref "../items/sea_salt.md" >}})            |
|          | [black pepper]({{< relref "../items/black_pepper.md" >}})         |


## Directions {#directions}

1.  Preheat oven to 400F and cook farro according to package instructions
2.  Toss broccoli florets in olive oil and salt
    1.  Spread florets onto a baking sheet, ensuring some space between florets
    2.  Pop them in the oven for about 15 min, or until slightly browned, crisp, and tender
3.  Meanwhile, prepare the salad dressing by shaking all dressing ingredients together in a jar with a lid, or whisk them in a small bowl
4.  Remove broccoli from the oven then toss everything together in a large bowl and serve


## References {#references}

1.  Original recipe: [Walder Wellness](https://walderwellness.com/wprm_print/7184)
