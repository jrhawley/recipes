+++
title = "African peanut stew"
author = ["James Hawley"]
date = 2020-01-14T00:00:00-05:00
lastmod = 2024-05-23T21:38:02-04:00
tags = ["soups", "recipes", "vegan", "vegetarian", "entrees"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 20 min     |
| Cook Time | 30 min     |
| Yields    | 6 servings |


## Ingredients {#ingredients}

| Quantity   | Item                                                                       |
|------------|----------------------------------------------------------------------------|
| 1 tsp      | [olive oil]({{< relref "../items/olive_oil.md" >}})                        |
| 1          | medium [sweet onion]({{< relref "../items/sweet_onion.md" >}}), diced      |
| 2 cloves   | [garlic]({{< relref "../items/garlic.md" >}}), minced                      |
| 1          | red [bell pepper]({{< relref "../items/bell_pepper.md" >}}), diced         |
| 1          | medium [sweet potato]({{< relref "../items/yam.md" >}}), diced             |
| 28 oz      | [diced tomatoes]({{< relref "../items/diced_tomatoes.md" >}}), with juices |
| 1/3 cup    | [peanut butter]({{< relref "../items/peanut_butter.md" >}})                |
| 1 L        | [vegetable broth]({{< relref "../items/vegetable_broth.md" >}})            |
| 3/2 tsp    | [chili powder]({{< relref "../items/chili_powder.md" >}})                  |
| 15 oz      | [chickpeas]({{< relref "../items/chickpea.md" >}}), drained and rinsed     |
| 2 handfuls | [spinach]({{< relref "../items/spinach.md" >}})                            |
|            | [Kosher salt]({{< relref "../items/kitchen_salt.md" >}})                   |
|            | [black pepper]({{< relref "../items/black_pepper.md" >}})                  |
|            | [parsley]({{< relref "../items/parsley.md" >}})                            |
|            | [peanuts]({{< relref "../items/peanut.md" >}})                             |


## Directions {#directions}

1.  In a large saucepan, heat olive oil over medium heat
    -   Add onion and garlic, saute until translucent
2.  Add bell pepper, sweet potato, tomatoes, and juices
    -   Raise to medium-high heat, simmer for 5 minutes
    -   Season with salt and pepper
3.  In a medium bowl, whisk together peanut butter, 1 cup of vegetable broth until no clumps remain
    -   Stir into vegetables with remaining vegetable broth with chili powder
4.  Cover with a lid, reduce to medium-low heat, simmer for 10-20 min until sweet potato is tender
5.  Stir in chickpeas and spinach, cook until spinach is wilted
6.  Salt and pepper to taste, garnish with parsley and roasted peanuts before serving


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [The Oh She Glows Cookbook]({{< relref "../references/Oh_She_Glows_Cookbook.md" >}})
