+++
title = "One pot wraps"
author = ["James Hawley"]
date = 2020-02-22T00:00:00-05:00
lastmod = 2024-05-23T21:36:03-04:00
tags = ["lunch", "wraps", "recipes", "vegetarian", "entrees"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 10 min     |
| Cook Time | 20 min     |
| Yields    | 4 servings |


## Ingredients {#ingredients}

| Quantity | Item                                                                     |
|----------|--------------------------------------------------------------------------|
| 1 cup    | [quinoa]({{< relref "../items/quinoa.md" >}}), uncooked                  |
| 7/4 cup  | [vegetable broth]({{< relref "../items/vegetable_broth.md" >}})          |
| 15 oz    | [black beans]({{< relref "../items/black_bean.md" >}}), drained, rinsed  |
| 1        | small red [bell pepper]({{< relref "../items/bell_pepper.md" >}}), diced |
| 12 oz    | diced [pineapple]({{< relref "../items/pineapple.md" >}})                |
| 1 tsp    | [paprika]({{< relref "../items/paprika.md" >}})                          |
| 1 Tbsp   | [lime juice]({{< relref "../items/lime_juice.md" >}})                    |
| 4        | [tortillas]({{< relref "../items/tortilla.md" >}})                       |


## Directions {#directions}

1.  In a large pot, bring quinoa and broth to a simmer over medium heat
    -   Add all other ingredients but the wraps, and mix
2.  Bring to a boil over medium-high heat, reduce to medium, cover, and cook for 15-20 min
3.  Stir and serve in lettuce or wraps
4.  Garnish with cilantro, parsley, avocado, salsa, and/or green onion


## References &amp; Notes {#references-and-notes}

1.  Original recipe: Wendy McNeil
