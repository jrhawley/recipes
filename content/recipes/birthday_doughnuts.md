+++
title = "Birthday doughnuts"
author = ["James Hawley"]
date = 2021-06-16T00:00:00-04:00
lastmod = 2023-12-23T02:02:14-05:00
tags = ["recipes", "doughnuts", "desserts"]
draft = false
+++

| Info      | Amount      |
|-----------|-------------|
| Prep Time | 20 min      |
| Cook Time | 10 min      |
| Yields    | 8 doughnuts |


## Ingredients {#ingredients}


### Dough {#dough}

| Quantity | Item                                                            |
|----------|-----------------------------------------------------------------|
| 1 cup    | [flour]({{< relref "../items/flour.md" >}})                     |
| 1 tsp    | [baking powder]({{< relref "../items/baking_powder.md" >}})     |
| 1/4 tsp  | [baking soda]({{< relref "../items/baking_soda.md" >}})         |
| 1/4 tsp  | [table salt]({{< relref "../items/table_salt.md" >}})           |
| 1/4 tsp  | [nutmeg]({{< relref "../items/nutmeg.md" >}})                   |
| 1/4 cup  | [white sugar]({{< relref "../items/white_sugar.md" >}})         |
| 1 Tbsp   | [brown sugar]({{< relref "../items/brown_sugar.md" >}})         |
| 1/4 cup  | [milk]({{< relref "../items/milk.md" >}})                       |
| 1/4 cup  | [Greek yogurt]({{< relref "../items/greek_yogurt.md" >}})       |
| 1        | [egg]({{< relref "../items/eggs.md" >}})                        |
| 2 Tbsp   | [unsalted butter]({{< relref "../items/butter.md" >}}), melted  |
| 3/2 tsp  | [vanilla extract]({{< relref "../items/vanilla_extract.md" >}}) |
| 1/2 cup  | rainbow [sprinkles]({{< relref "../items/sprinkle.md" >}})      |


### Glaze {#glaze}

| Quantity | Item                                                            |
|----------|-----------------------------------------------------------------|
| 1/4 cup  | [milk]({{< relref "../items/milk.md" >}})                       |
| 2 cup    | [icing sugar]({{< relref "../items/icing_sugar.md" >}})         |
| 1 tsp    | [vanilla extract]({{< relref "../items/vanilla_extract.md" >}}) |
|          | rainbow [sprinkles]({{< relref "../items/sprinkle.md" >}})      |


## Directions {#directions}

1.  Preheat the oven to 350 F
    1.  Grease a doughnut pan or coat with a non-stick spray
2.  In a medium bowl, whisk the flour, baking powder, baking soda, salt, nutmeg, and sugars
3.  In a large bowl, whisk the milk, yogurt, and egg
    1.  Add and whisk the melted butter and vanilla
4.  Pour the dry ingredients into the wet bowl and stir together
    1.  After mostly mixed, add the sprinkles and fold them in gently&nbsp;[^fn:1]
5.  Transfer the batter into each doughnut cup until 3/4 full
6.  Bake for 8 - 10 min, or until the edges become golden
    1.  Remove the doughnuts and let them cool for 2 min on a wire rack before glazing&nbsp;[^fn:2]
7.  While the doughnuts are baking, in a medium saucepan combine the milk, powdered sugar, and vanilla for the glaze over low heat
    1.  Whisk until smooth and uniform
8.  After the glaze has thickened slightly and the doughnuts have cooled, dip the top of each doughnut into the glaze
    1.  Turn the doughnuts to coat evenly
    2.  Return to the wire rack and top lightly with sprinkles
9.  Let the glaze harden, then serve


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [If You Give a Blonde a Kitchen](https://www.ifyougiveablondeakitchen.com/wprm_print/recipe/9392)

[^fn:1]: The sprinkle colours will bleed and turn grey if you mix them too vigorously
[^fn:2]: Put something under the wire rack to catch crumbs and glaze that is likely to drip