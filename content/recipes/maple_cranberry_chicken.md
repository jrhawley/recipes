+++
title = "Maple cranberry chicken"
author = ["James Hawley"]
date = 2020-12-07T00:00:00-05:00
lastmod = 2024-05-23T21:23:50-04:00
tags = ["chicken", "recipes", "entrees"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 10 min     |
| Cook Time | 25 min     |
| Yields    | 6 servings |


## Ingredients {#ingredients}

| Quantity | Item                                                           |
|----------|----------------------------------------------------------------|
| 2 Tbsp   | [olive oil]({{< relref "../items/olive_oil.md" >}})            |
| 6        | [chicken breasts]({{< relref "../items/chicken_breast.md" >}}) |
|          | [kosher salt]({{< relref "../items/kitchen_salt.md" >}})       |
|          | [pepper]({{< relref "../items/black_pepper.md" >}})            |
| 1/4 cup  | [white wine]({{< relref "../items/white_wine.md" >}})          |
| 1/2 cup  | [maple syrup]({{< relref "../items/maple_syrup.md" >}})        |
| 1/4 cup  | [dijon mustard]({{< relref "../items/dijon_mustard.md" >}})    |
| 1 cup    | [cranberries]({{< relref "../items/cranberry.md" >}})          |
| 4 sprigs | fresh [rosemary]({{< relref "../items/rosemary.md" >}})        |


## Directions {#directions}

1.  Preheat your oven to 190 C (375 F).
2.  Heat a large oven-safe skillet over medium-high heat and add the olive oil.
    -   Season the chicken breasts on both sides with salt and pepper and add them to the hot pan to sear them on both sides until golden brown&nbsp;[^fn:1].
    -   Once the chicken is browned, remove them, place them on a plate and set aside.
3.  Add the white wine to the pan with the chicken juices and whisk as the wine begins to evaporate
    -   Add the maple syrup, dijon mustard, and cranberries, and whisk to combine until a thin sauce forms
4.  Add the chicken back into the pan and spoon the sauce over the chicken
    -   Sprinkle the cranberries into the pan over the chicken and add the sprigs of rosemary around the chicken
5.  Bake for about 20-25 minutes until cooked through


## References {#references}

1.  Original recipe: [The Busy Baker](https://thebusybaker.ca/maple-cranberry-roast-chicken/#wprm-recipe-container-12798)

[^fn:1]: Don't worry about cooking them through at this stage, just focus on crisping the skin.
