+++
title = "Thai veggie bowl"
author = ["James Hawley"]
date = 2020-03-13T00:00:00-04:00
lastmod = 2024-05-23T21:35:20-04:00
tags = ["bowls", "recipes", "vegetarian", "vegan", "entrees"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 25 min     |
| Cook Time | 10 min     |
| Yields    | 4 servings |

{{< figure src="../assets/thai-veggie-bowl.jpg" caption="<span class=\"figure-number\">Figure 1: </span>Thai veggie bowl" >}}


## Ingredients {#ingredients}


### Thai peanut sauce {#thai-peanut-sauce}

| Quantity | Item                                                        |
|----------|-------------------------------------------------------------|
| 1 clove  | [garlic]({{< relref "../items/garlic.md" >}}), minced       |
| 2 Tbsp   | [olive oil]({{< relref "../items/olive_oil.md" >}})         |
| 3 Tbsp   | [peanut butter]({{< relref "../items/peanut_butter.md" >}}) |
| 2 tsp    | [ginger]({{< relref "../items/ginger.md" >}}), grated       |
| 3 Tbsp   | [lime juice]({{< relref "../items/lime_juice.md" >}})       |
| 3 Tbsp   | [soy sauce]({{< relref "../items/soy_sauce.md" >}})         |
| 1 tsp    | [white sugar]({{< relref "../items/white_sugar.md" >}})     |


### Salad {#salad}

| Quantity | Item                                                               |
|----------|--------------------------------------------------------------------|
| 1 cup    | [rice]({{< relref "../items/basmati_rice.md" >}}), cooked          |
| 16 oz    | [edamame beans]({{< relref "../items/edamame.md" >}})              |
| 1        | red [bell pepper]({{< relref "../items/bell_pepper.md" >}}), diced |
| 1/2      | [cucumber]({{< relref "../items/cucumber.md" >}}), diced           |
| 1        | [carrot]({{< relref "../items/baby_carrot.md" >}}), juilienned     |
| 4        | [green onions]({{< relref "../items/green_onion.md" >}})           |
| 1/4 cup  | [parsley]({{< relref "../items/parsley.md" >}}), chopped           |
|          | [olive oil]({{< relref "../items/olive_oil.md" >}})                |
|          | [sesame seeds]({{< relref "../items/sesame_seed.md" >}})           |


## Directions {#directions}

1.  In a food processor, combine all sauce ingredients and blend until smooth
2.  Cook the rice/soba noodles according to instructions
    1.  If noodles, drain, rinse under cold water, toss with olive oil
    2.  Transfer to a large bowl
3.  Add remaining salad ingredients
4.  Top with sauce and sesame seeds when serving


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [Oh She Glows](https://ohsheglows.com/book/)
