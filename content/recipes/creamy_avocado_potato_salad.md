+++
title = "Creamy avocado potato salad"
author = ["James Hawley"]
date = 2023-03-19T10:07:00-04:00
lastmod = 2024-05-23T20:47:12-04:00
tags = ["vegetarian", "vegan", "entrees", "salads", "recipes"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 25 min     |
| Cook Time | 30 min     |
| Yields    | 3 servings |


## Ingredients {#ingredients}

| Quantity    | Item                                                                            |
|-------------|---------------------------------------------------------------------------------|
| 900 g       | [baby potatoes]({{< relref "../items/baby_potatoes.md" >}}), cubed              |
| 1 Tbsp      | [olive oil]({{< relref "../items/olive_oil.md" >}})                             |
| 1 bunch     | [asparagus]({{< relref "../items/asparagus.md" >}}), chopped into 2.5 cm pieces |
| 1/2 cup + 1 | [green onion]({{< relref "../items/green_onion.md" >}}), chopped                |
| 1/2 cup     | [avocado]({{< relref "../items/avocado.md" >}})                                 |
| 2 Tbsp      | [dill]({{< relref "../items/dill.md" >}})                                       |
| 4 tsp       | [lemon juice]({{< relref "../items/lemon_juice.md" >}})                         |
|             | [fine sea salt]({{< relref "../items/sea_salt.md" >}})                          |
|             | [black pepper]({{< relref "../items/black_pepper.md" >}})                       |


## Directions {#directions}

1.  Preheat the oven to 220 C (425 F) and line two baking sheets with parchment paper
2.  Spread the cubed potatoes in an even layer on one baking sheet and drizzle with 3/2 tsp of oil
    1.  Spread the asparagus on the second baking sheet, drizzling with the remaining oil
    2.  Season each with salt and pepper, mixing thoroughly
3.  Roast the potatoes for 15 min, flip, then another 15 - 20 min
    1.  After flipping the potatoes, add the asparagus to the oven and roast for 9 - 12 min
    2.  Transfer the potatoes and asparagus to a large mixing bowl and stir in the 1/2 cup of green onion
4.  In a food processor, combined the avocado, dill, lemon juice, remaining green onion, salt, and pepper with 1/4 cup of water and blend until smooth
5.  Pour the dressing over the salad and serve immediately, seasoning with salt and pepper to taste


## References {#references}

1.  Original recipe: [The Oh She Glows Cookbook]({{< relref "../references/Oh_She_Glows_Cookbook.md" >}})
2.  The salad will keep in an airtight container in the fridge for a couple days
