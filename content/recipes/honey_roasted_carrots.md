+++
title = "Honey roasted carrots"
author = ["James Hawley"]
date = 2019-12-07T00:00:00-05:00
lastmod = 2023-12-21T22:04:45-05:00
tags = ["recipes", "vegetarian", "sides"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 10 min     |
| Cook Time | 20 min     |
| Yields    | 6 servings |


## Ingredients {#ingredients}

| Quantity | Item                                                                                                |
|----------|-----------------------------------------------------------------------------------------------------|
| 2 lbs    | [rainbow carrots]({{< relref "../items/baby_carrot.md" >}}), peeled and sliced into 1/4 inch rounds |
| 2 Tbsp   | [butter]({{< relref "../items/butter.md" >}}), melted                                               |
| 2 tsp    | [honey]({{< relref "../items/honey.md" >}})                                                         |
| 1 tsp    | [table salt]({{< relref "../items/table_salt.md" >}})                                               |
| 1/2 tsp  | fresh [thyme]({{< relref "../items/thyme.md" >}}), chopped                                          |
| 1/8      | [cayenne pepper]({{< relref "../items/cayenne_pepper.md" >}})                                       |
| 1/8      | [cumin]({{< relref "../items/cumin.md" >}})                                                         |
| 2 oz     | [goat cheese]({{< relref "../items/goat_cheese.md" >}}), crumbled                                   |
| 1/3 cup  | [pomegranate seeds]({{< relref "../items/pomegranate_aril.md" >}}), about 1/2 pomegranate           |
| 2 Tbsp   | [sunflower seeds]({{< relref "../items/sunflower_seed.md" >}})                                      |


## Instructions {#instructions}

1.  Preheat oven to 425 F and line a baking sheet with foil.
2.  In a medium bowl toss cut carrots with melted butter, honey, and seasonings.
3.  Spread carrots out on the prepared baking sheet and roast for 20 minutes, flipping after 15 minutes.
4.  Transfer warm carrots to a serving dish and top with goat cheese, pomegranate seeds, and sunflower seeds.
5.  Serve warm.


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [The Vintage Mixer](https://www.thevintagemixer.com/honey-roasted-carrots-with-goat-cheese-and-pomegranate-seeds/)
