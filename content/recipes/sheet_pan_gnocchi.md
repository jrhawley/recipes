+++
title = "Sheet pan gnocchi"
author = ["James Hawley"]
date = 2022-09-04T00:00:00-04:00
lastmod = 2024-05-23T21:13:35-04:00
tags = ["recipes", "entrees", "vegetarian", "vegan"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 10 min     |
| Cook Time | 30 min     |
| Yields    | 4 servings |


## Ingredients {#ingredients}

| Quantity | Item                                                              |
|----------|-------------------------------------------------------------------|
| 16 oz    | [gnocchi]({{< relref "../items/gnocchi.md" >}})                   |
| 2 pints  | [cherry tomatoes]({{< relref "../items/cherry_tomato.md" >}})     |
| 12 oz    | [mozzarella ball]({{< relref "../items/mozzarella.md" >}})        |
| 2 cloves | [garlic]({{< relref "../items/garlic.md" >}})                     |
|          | [basil]({{< relref "../items/basil.md" >}})                       |
|          | [parmesan]({{< relref "../items/parmesan.md" >}})                 |
|          | [balsamic vinegar]({{< relref "../items/balsamic_vinegar.md" >}}) |
|          | [olive oil]({{< relref "../items/olive_oil.md" >}})               |
|          | [Kosher salt]({{< relref "../items/kitchen_salt.md" >}})          |
|          | [black pepper]({{< relref "../items/black_pepper.md" >}})         |
|          | [oregano]({{< relref "../items/oregano.md" >}})                   |


## Directions {#directions}

1.  Preheat the oven to 450 F and line a baking sheet with parchment paper.
2.  Dump the package of gnocchi on the pan
    1.  Add the cherry tomatoes to the dry pan.
    2.  Smash the garlic and add to the pan
3.  Coat the tomatoes and gnocchi with olive oil, then add salt, pepper, and oregano to taste.
4.  Arrange as a single layer then roast for 20 min, or until some of the tomatoes have blistered and the gnocchi is golden.
5.  Just before the gnocchi is done, slice up half the mozzarella ball and slice the basil.
6.  Remove the baking sheet when done, let sit for 2 min, then sprinkle with mozzarella and basil.
7.  Serve and top with olive oil and balsamic vinegar, to taste.


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [The Food Charlatan](https://thefoodcharlatan.com/wprm_print/recipe/30333)
