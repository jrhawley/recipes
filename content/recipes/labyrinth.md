+++
title = "Labyrinth"
author = ["James Hawley"]
date = 2022-12-27T15:24:00-05:00
lastmod = 2023-12-21T22:09:13-05:00
tags = ["alcohols", "recipes", "beverages"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 5 min      |
| Cook Time | 0 min      |
| Yields    | 1 cocktail |


## Ingredients {#ingredients}

| Quantity | Item                                                            |
|----------|-----------------------------------------------------------------|
| 3/2 oz   | [aged rum]({{< relref "../items/aged_rum.md" >}})               |
| 1/2 oz   | [Campari]({{< relref "../items/campari.md" >}})                 |
| 3/2 oz   | [pineapple juice]({{< relref "../items/pineapple_juice.md" >}}) |
| 1/2 oz   | [lime juice]({{< relref "../items/lime_juice.md" >}})           |
| 1/2 oz   | [maple syrup]({{< relref "../items/maple_syrup.md" >}})         |
|          | rum-soaked pineapple (optional)                                 |


## Directions {#directions}

1.  Combine all ingredients with ice in a cocktail shaker.
2.  Strain over ice into the glass.
3.  Serve with a slice of rum-soaked pineapple for garnish.


## References &amp; Notes {#references-and-notes}

1.  Collective Arts
