+++
title = "Beet & apple soup"
author = ["James Hawley"]
date = 2022-03-25T00:00:00-04:00
lastmod = 2024-05-23T21:15:14-04:00
tags = ["soups", "recipes", "entrees", "vegetarian"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 10 min     |
| Cook Time | 30 min     |
| Yields    | 4 servings |


## Ingredients {#ingredients}

| Quantity | Item                                                                    |
|----------|-------------------------------------------------------------------------|
| 2 lbs    | [beets]({{< relref "../items/beet.md" >}}) (canned or fresh)            |
| 2 Tbsp   | [butter]({{< relref "../items/butter.md" >}})                           |
| 1/2 cup  | [onion]({{< relref "../items/onion.md" >}}), diced                      |
| 4        | [apples]({{< relref "../items/apple.md" >}}), cored and quartered       |
| 1/4 cup  | [apple cider vinegar]({{< relref "../items/apple_cider_vinegar.md" >}}) |
| 1 Tbsp   | [white sugar]({{< relref "../items/white_sugar.md" >}})                 |
| 4 cup    | [vegetable broth]({{< relref "../items/vegetable_broth.md" >}})         |
| 1 Tbsp   | [honey]({{< relref "../items/honey.md" >}})                             |
| 1/2 cup  | [Greek yogurt]({{< relref "../items/greek_yogurt.md" >}})               |
|          | [salt]({{< relref "../items/table_salt.md" >}})                         |
|          | [pepper]({{< relref "../items/black_pepper.md" >}})                     |


## Directions {#directions}

1.  If using fresh beets:
    1.  Preheat oven to 375 F.
    2.  Wrap the beets in a double layer of aluminium foil and crimp to seal.
    3.  Roast for 1 h, or until tender.
    4.  Remove from the oven and let rest until beets are cool to handle.
    5.  Remove the skin and chop into ~ 1 " chunks
2.  In a large soup pot over medium heat, melt the butter and add diced onions.
    1.  Saute for 6 - 7 min, until soft and translucent.
    2.  Add apples, vinegar, and sugar, and cook for another 1 - 2 min.
    3.  Add the beets and stock.
    4.  Bring to a boil over medium - high heat, then reduce to medium and simmer for 15 - 20 min, or until apples are soft.
    5.  Remove from heat and let cool.
3.  Blend until creamy and smooth.
    1.  Return blended soup to the pot and heat.
    2.  Season with salt and pepper to taste.
4.  Divide the soup into separate bowls.
    1.  In a separate small bowl, mix the honey and yogurt.
    2.  Place a dollop into each bowl.


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [A Rising Tide]({{< relref "../references/A_Rising_Tide.md" >}})
2.  Pairs well Newman Estate Winery's Seyval Blanc
3.  Can garnish with parsley or dill fronds.
4.  The soup, alone without the yogurt mixture, will last for ~ 1 week in a refrigerated airtight container.
