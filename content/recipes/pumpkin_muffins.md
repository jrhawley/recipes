+++
title = "Pumpkin muffins"
author = ["James Hawley"]
date = 2013-11-09T00:00:00-05:00
lastmod = 2024-06-12T19:15:38-04:00
tags = ["breakfasts", "recipes", "muffins", "pumpkin"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 15 min     |
| Cook Time | 25 min     |
| Yields    | 12 muffins |


## Ingredients {#ingredients}

| Quantity  | Item                                                                                                                   |
|-----------|------------------------------------------------------------------------------------------------------------------------|
| 7/4 cup   | [all-purpose flour]({{< relref "../items/all-purpose_flour.md" >}})                                                    |
| 1 tsp     | [baking soda]({{< relref "../items/baking_soda.md" >}})                                                                |
| 3/2 tsp   | [ground cinnamon]({{< relref "../items/cinnamon.md" >}})                                                               |
| 3/2 tsp   | [pumpkin pie spice]({{< relref "../items/pumpkin_pie_spice.md" >}})                                                    |
| 1/4 tsp   | [ground ginger]({{< relref "../items/ginger.md" >}})                                                                   |
| 1/2 tsp   | [table salt]({{< relref "../items/table_salt.md" >}})                                                                  |
| 1/2 cup   | [vegetable oil]({{< relref "../items/vegetable_oil.md" >}}) or [coconut oil]({{< relref "../items/coconut_oil.md" >}}) |
| 1/2 cup   | [white sugar]({{< relref "../items/white_sugar.md" >}})                                                                |
| 1/3 cup   | [brown sugar]({{< relref "../items/brown_sugar.md" >}})                                                                |
| 14 oz can | [pumpkin puree]({{< relref "../items/pumpkin_puree.md" >}})                                                            |
| 2         | [eggs]({{< relref "../items/eggs.md" >}})                                                                              |
| 1/4 cup   | [milk]({{< relref "../items/milk.md" >}})                                                                              |


## Directions {#directions}

1.  Preheat the oven to 220 C (425 F) and butter the muffin pan.
2.  In a large bowl, whisk the oil, sugar, pumpkin puree, eggs, and milk together until combined.
3.  In a medium bowl, whisk the flour, baking soda, cinnamon, pumpkin pie spice, ginger, and salt together.
4.  Fold the dry ingredients into the wet ingredients until everything is combined and no pockets remain.
5.  Spoon the batter into each cup in the muffin tray to the top, sprinkling with coarse sugar, if desired.
6.  Bake for 5 min at 425, then reduce the temperature to 180 C (350 F) and bake for an additional 16 - 18 min.


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [Sally's Baking Addiction](https://sallysbakingaddiction.com/pumpkin-muffins-recipe/print/76102/)
2.  Muffins will last in the fridge for ~ 1 week, or in the freezer for ~ 3 months.
3.  The higher initial temperature can help the muffins rise quickly, ensure a tall muffin top.
