+++
title = "Simple syrup"
author = ["James Hawley"]
date = 2020-06-03T00:00:00-04:00
lastmod = 2023-12-21T22:19:13-05:00
tags = ["syrups", "sweeteners", "recipes", "beverages"]
draft = false
+++

## Ingredients {#ingredients}

| Quantity | Item                                                    |
|----------|---------------------------------------------------------|
| 1 part   | [water]({{< relref "../items/water.md" >}})             |
| 1 part   | [white sugar]({{< relref "../items/white_sugar.md" >}}) |


## Directions {#directions}

1.  Heat water on the stove to just below boiling
2.  Slowly add sugar, allowing it to dissolve fully before adding more
3.  Once all the sugar is added, let cool and store in the fridge
