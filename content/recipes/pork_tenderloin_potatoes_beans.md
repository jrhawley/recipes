+++
title = "Brown sugar pork tenderloin, potatoes, and carrots"
author = ["James Hawley"]
date = 2021-03-01T00:00:00-05:00
lastmod = 2024-10-19T01:08:39-04:00
tags = ["pork", "recipes", "entrees", "slow-cooker"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 10 min     |
| Cook Time | 4 h        |
| Yields    | 4 servings |


## Ingredients {#ingredients}

| Quantity | Item                                                                      |
|----------|---------------------------------------------------------------------------|
| 400 g    | [pork tenderloin]({{< relref "../items/pork_tenderloin.md" >}})           |
| 2 tsp    | [table salt]({{< relref "../items/table_salt.md" >}})                     |
| 1 tsp    | [thyme]({{< relref "../items/thyme.md" >}})                               |
| 1/2 tsp  | [salt]({{< relref "../items/table_salt.md" >}})                           |
| 1/2 tsp  | [pepper]({{< relref "../items/black_pepper.md" >}})                       |
| 1 cup    | [vegetable broth]({{< relref "../items/vegetable_broth.md" >}})           |
| 1/4 cup  | [brown sugar]({{< relref "../items/brown_sugar.md" >}})                   |
| 1 Tbsp   | [olive oil]({{< relref "../items/olive_oil.md" >}})                       |
| 2 Tbsp   | [Worcestershire sauce]({{< relref "../items/worcestershire_sauce.md" >}}) |
| 1 Tbsp   | [soy sauce]({{< relref "../items/soy_sauce.md" >}})                       |
| 680 g    | [baby potatoes]({{< relref "../items/russet_potato.md" >}})               |
| 340 g    | [baby carrots]({{< relref "../items/baby_carrot.md" >}})                  |
| 2 cloves | [garlic]({{< relref "../items/garlic.md" >}}), crushed                    |


## Directions {#directions}

1.  Mix thyme, salt, and pepper in a small bowl
    1.  Rub pork all over with the olive oil and then coat it well with the spice mixture on all sides
2.  Heat a pan to medium-high heat
    1.  Sear all sides of the park tenderloin until brown, but don't cook through
3.  While the pork is searing, mix the broth, brown sugar, olive oil, Worcestershire sauce, and soy sauce together in the slow cooker
    1.  When the pork is finished searing, place it in the slow cooker
    2.  Add the potatoes, carrots, and garlic
4.  Cook for 4 h on low or until the pork registers 160 F
    1.  Let the pork rest for 3 min
5.  While the pork is resting, place the potatoes in a large bowl and add butter and salt to taste
    1.  Stir to combine
    2.  Slice the pork and place it on a serving platter


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [Bless This Mess Please](https://www.blessthismessplease.com/slow-cooker-pork-tenderloin-and-potatoes/)
2.  Can easily substitute carrots with another vegetable
