+++
title = "Spicy halibut"
author = ["James Hawley"]
date = 2013-09-15T00:00:00-04:00
lastmod = 2024-05-23T21:34:20-04:00
tags = ["fish", "recipes", "entrees"]
draft = false
+++

| Info      | Amount   |
|-----------|----------|
| Prep Time | 10 min   |
| Cook Time | 10 min   |
| Yields    | 4 filets |


## Ingredients {#ingredients}

| Quantity | Item                                                          |
|----------|---------------------------------------------------------------|
| 1 slice  | apple wood smoked [bacon]({{< relref "../items/bacon.md" >}}) |
| 1/2 tsp  | [table salt]({{< relref "../items/table_salt.md" >}})         |
| 1/2 tsp  | [paprika]({{< relref "../items/paprika.md" >}})               |
| 1/4 tsp  | [pepper]({{< relref "../items/black_pepper.md" >}})           |
| 2 tsp    | [garlic]({{< relref "../items/garlic.md" >}}), minced         |
| 4 filets | [halibut]({{< relref "../items/halibut.md" >}})               |
| 4 oz     | baby [spinach]({{< relref "../items/spinach.md" >}})          |


## Directions {#directions}

1.  Cook bacon until crisp. Crumble, set aside.
2.  Combine salt, paprika, and pepper in a small bowl
    1.  Sprinkle evenly over filets, using your hands to press them into the filets
    2.  Add filets to the pan with bacon drippings
    3.  Cooke for 3 min each side, or until flaky
3.  Add garlic to pan, cooking for 1 min
4.  Remove fish, add spinach and bacon into the pan, and cook until spinach is wilted
5.  Plate all ingredients and serve
