+++
title = "Rum Old-Fashioned"
author = ["James Hawley"]
date = 2021-02-17T00:00:00-05:00
lastmod = 2023-11-20T09:32:11-05:00
tags = ["recipes", "alcohols", "beverages"]
draft = false
+++

## Ingredients {#ingredients}

| Quantity | Item                                                                                                     |
|----------|----------------------------------------------------------------------------------------------------------|
| 1/3 oz   | [simple syrup](./simple-syrup.md)                                                                        |
| 3 dashes | [Angostura bitters]({{< relref "../items/angostura_bitters.md" >}})                                      |
| 5/3 oz   | [black rum]({{< relref "../items/black_rum.md" >}}) or [gold rum]({{< relref "../items/gold_rum.md" >}}) |
| 1 disk   | [orange peel]({{< relref "../items/orange.md" >}})                                                       |


## Directions {#directions}

1.  Place the orange peel at the bottom of a rocks glass
2.  Add the simple syrup, bitters, and half the rum
3.  Add 2-3 ice cubes and stir 20 - 30 times to dilute
4.  Add the remaining rum, then 2 more ice cubes and stir again
5.  Serve in the glass as is


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [The Periodic Table of Cocktails]({{< relref "../references/Periodic_Table_of_Cocktails.md" >}})
