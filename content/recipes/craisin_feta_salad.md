+++
title = "Craisin and feta salad"
author = ["James Hawley"]
date = 2013-10-19T00:00:00-04:00
lastmod = 2024-05-23T21:45:13-04:00
tags = ["salads", "recipes", "vegetarian", "entrees"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 10 min     |
| Cook Time | 0 min      |
| Yields    | 4 servings |


## Ingredients {#ingredients}


### Salad {#salad}

| Quantity | Item                                                                 |
|----------|----------------------------------------------------------------------|
| 1 box    | [baby romaine lettuce]({{< relref "../items/romaine_lettuce.md" >}}) |
| 1 cup    | [craisins]({{< relref "../items/dried_cranberry.md" >}})             |
| 1/3 cup  | toasted [pine nuts]({{< relref "../items/pine_nut.md" >}})           |
| 1/2 cup  | goat [feta]({{< relref "../items/feta.md" >}})                       |


### Dressing {#dressing}

| Quantity | Item                                                              |
|----------|-------------------------------------------------------------------|
| 1/3 cup  | [olive oil]({{< relref "../items/olive_oil.md" >}})               |
| 1 Tbsp   | [red-wine-vinegar]({{< relref "../items/red_wine_vinegar.md" >}}) |
| 2 Tbsp   | [maple syrup]({{< relref "../items/maple_syrup.md" >}})           |
| 1 tsp    | [dijon mustard]({{< relref "../items/dijon_mustard.md" >}})       |
| 1/2 tsp  | [oregano]({{< relref "../items/oregano.md" >}})                   |
|          | [kosher salt]({{< relref "../items/kitchen_salt.md" >}})          |
|          | [pepper]({{< relref "../items/black_pepper.md" >}})               |


## Directions {#directions}

1.  Place pine nuts in frying pan on low and stir until golden brown
2.  Mix all salad ingredients in a bowl
3.  Mix all dressing ingredients in a small container with a lid
4.  Pour dressing over salad before serving, keeping leftover dressing in the fridge
