+++
title = "Date cookie brownies"
author = ["James Hawley"]
date = 2023-08-12T12:20:00-04:00
lastmod = 2024-01-31T22:45:26-05:00
tags = ["cookies", "brownies", "desserts", "recipes"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 10 min     |
| Cook Time | 12 min     |
| Yields    | 15 cookies |


## Ingredients {#ingredients}

| Quantity | Item                                                              |
|----------|-------------------------------------------------------------------|
| 3/4 cup  | [Medjool dates]({{< relref "../items/medjool_date.md" >}})        |
| 1/2 cup  | [almond butter]({{< relref "../items/almond_butter.md" >}})       |
| 1        | [egg]({{< relref "../items/eggs.md" >}}) [^fn:1]                  |
| 5 Tbsp   | [cocoa powder]({{< relref "../items/cocoa_powder.md" >}})         |
| 1/2 tsp  | [baking soda]({{< relref "../items/baking_soda.md" >}})           |
| 1 tsp    | [vanilla extract]({{< relref "../items/vanilla_extract.md" >}})   |
| 1 tsp    | [balsamic vinegar]({{< relref "../items/balsamic_vinegar.md" >}}) |
| 1 pinch  | [table salt]({{< relref "../items/table_salt.md" >}})             |


## Directions {#directions}

1.  Preheat the oven to 180 C (350 F) and grease a mini muffin tin with butter, spray oil, or your favorite oil&nbsp;[^fn:2].
2.  In a food processor, combine the dates and egg&nbsp;[^fn:1] and process until the dates have broken down into a paste&nbsp;[^fn:3].
3.  Add in the almond butter, cocao powder, baking soda, vanilla, and vinegar and process again until a sticky, uniform dough is created&nbsp;[^fn:4]
    1.  Use a tablespoon and scoop the dough into the greased mini muffin tin.
    2.  Wet your fingers or a fork and gently flatten each mound of dough
4.  Bake until the tops look dry, ~ 12 - 15 min.
5.  Remove from the oven and let the brownies cool completely, ~ 10 min, before removing from the pan and serving.


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [Detoxinista](https://detoxinista.com/wprm_print/23716)
2.  Brownies will stay in the fridge in an airtight container for ~ 2 weeks.

[^fn:1]: If you want this to be vegan, substitute 1 egg with 1 Tbsp ground [flax seeds]({{< relref "../items/flax_seeds.md" >}}) and 1/4 cup [water]({{< relref "../items/water.md" >}}).
[^fn:2]: I laid them out on a cookie sheet, and that was fine.
[^fn:3]: They may turn into a ball in the processor.
[^fn:4]: If needed, add 1 Tbsp water to help it blend smoothly.