+++
title = "Beef macaroni"
author = ["James Hawley"]
date = 2020-12-19T00:00:00-05:00
lastmod = 2024-05-23T21:23:02-04:00
tags = ["beef", "pastas", "recipes", "entrees"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 5 min      |
| Cook Time | 20 min     |
| Yields    | 5 servings |


## Ingredients {#ingredients}

| Quantity | Item                                                                     |
|----------|--------------------------------------------------------------------------|
| 3/2 cups | [macaroni]({{< relref "../items/macaroni.md" >}})                        |
| 2 Tbsp   | [vegetable oil]({{< relref "../items/vegetable_oil.md" >}})              |
| 1        | [onion]({{< relref "../items/onion.md" >}}), sliced                      |
| 1        | small [green pepper]({{< relref "../items/bell_pepper.md" >}}), slivered |
| 1 clove  | [garlic]({{< relref "../items/garlic.md" >}}), minced                    |
| 1 lb     | [ground beef]({{< relref "../items/ground_beef.md" >}})                  |
| 1 tsp    | [oregano]({{< relref "../items/oregano.md" >}})                          |
| 1 tsp    | [table salt]({{< relref "../items/table_salt.md" >}})                    |
| 1/4 tsp  | [pepper]({{< relref "../items/black_pepper.md" >}})                      |
| 800 mL   | [canned tomatoes]({{< relref "../items/beefsteak_tomato.md" >}})         |
| 150 mL   | [tomato paste]({{< relref "../items/tomato_paste.md" >}})                |


## Directions {#directions}

1.  Cook macaroni according to instructions, drain, and set aside
2.  Meanwhile, saute onion, green pepper, and garlic in oil until tender.
    -   Add beef and seasonings, and brown
    -   Drain off excess fat once cooked
3.  Add tomatoes and tomato paste to the pan
    -   Bring to a boil, reduce heat, and simmer uncovered for 15 min
4.  Stir in cooked macaroni, serve, garnish with parmesan or other cheese


## References &amp; Notes {#references-and-notes}

1.  Original recipe: Nana
