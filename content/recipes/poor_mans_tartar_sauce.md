+++
title = "Poor man's tartar sauce"
author = ["James Hawley"]
date = 2022-10-17T12:33:00-04:00
lastmod = 2023-12-21T22:28:56-05:00
tags = ["sauces", "recipes"]
draft = false
+++

| Info      | Amount  |
|-----------|---------|
| Prep Time | 2 min   |
| Cook Time | 0 min   |
| Yields    | 1/4 cup |


## Ingredients {#ingredients}

| Quantity | Item                                            |
|----------|-------------------------------------------------|
| 1/4 cup  | [mayo]({{< relref "../items/mayonnaise.md" >}}) |
|          | [relish]({{< relref "../items/relish.md" >}})   |


## Directions {#directions}

1.  Combine the mayo with a desired amount of relish.


## Notes {#notes}

1.  I had this often as a kid, weren't the most well off, and I didn't know any better about food.
