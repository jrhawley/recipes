+++
title = "Butter chicken"
author = ["James Hawley"]
date = 2013-10-17T00:00:00-04:00
lastmod = 2024-05-23T21:45:38-04:00
tags = ["chicken", "recipes", "slow-cooker", "entrees"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 15 min     |
| Cook Time | 4 h        |
| Yields    | 4 servings |


## Ingredients {#ingredients}

| Quantity | Item                                                                  |
|----------|-----------------------------------------------------------------------|
| 2 tsp    | ground [ginger]({{< relref "../items/ginger.md" >}})                  |
| 2 tsp    | ground [curry powder]({{< relref "../items/curry_powder.md" >}})      |
| 1 tsp    | ground [cumin]({{< relref "../items/cumin.md" >}})                    |
| 1 tsp    | ground [cinnamon]({{< relref "../items/cinnamon.md" >}})              |
| 1        | [white onion]({{< relref "../items/onion.md" >}}), finley diced       |
| 2        | [chicken breasts]({{< relref "../items/chicken_breast.md" >}}), cubed |
| 2 Tbsp   | [butter]({{< relref "../items/butter.md" >}}), melted                 |
| 2 cups   | [tomato sauce]({{< relref "../items/tomato_sauce.md" >}})             |
| 1 Tbsp   | [lime juice]({{< relref "../items/lime_juice.md" >}})                 |
| 1/2 cup  | plain [Greek yogurt]({{< relref "../items/greek_yogurt.md" >}})       |
| 1/4 cup  | [parsley]({{< relref "../items/parsley.md" >}}), chopped to garnish   |


## Directions {#directions}

1.  Combine all spices in small bowl
    1.  Add chicken, sprinkle with spices and top with butter and pasta sauce
    2.  Place onion in centre of slow cooker, surrounded by chicken
2.  Stir, cover, and cook on high for 4 h, or low for 8 h
3.  Stir in lime and yogurt before serving
4.  Garnish with parsley
