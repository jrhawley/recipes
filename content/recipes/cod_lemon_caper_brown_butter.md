+++
title = "Cod with lemon caper brown butter"
author = ["James Hawley"]
date = 2022-08-21T10:25:00-04:00
lastmod = 2024-05-23T21:13:48-04:00
tags = ["entrees", "fish", "recipes"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 5 min      |
| Cook Time | 15 min     |
| Yields    | 2 servings |


## Ingredients {#ingredients}


### Pan-fried cod {#pan-fried-cod}

| Quantity | Item                                                      |
|----------|-----------------------------------------------------------|
| 2 filets | [fresh cod]({{< relref "../items/cod.md" >}})             |
| 1 tsp    | [Kosher salt]({{< relref "../items/kitchen_salt.md" >}})  |
| 1/2 tsp  | [black pepper]({{< relref "../items/black_pepper.md" >}}) |
| 1 cup    | [flour]({{< relref "../items/flour.md" >}})               |
| 2 Tbsp   | [butter]({{< relref "../items/butter.md" >}})             |


### Sauce {#sauce}

| Quantity | Item                                            |
|----------|-------------------------------------------------|
| 1        | [lemon]({{< relref "../items/lemon.md" >}})     |
| 3 Tbsp   | [butter]({{< relref "../items/butter.md" >}})   |
| 1/4 cup  | [capers]({{< relref "../items/caper.md" >}})    |
| 1 Tbsp   | caper brine                                     |
| 6 Tbsp   | [parsley]({{< relref "../items/parsley.md" >}}) |


## Directions {#directions}

1.  Season the code with salt and pepper.
    -   Evenly dredge with flour, shaking off any excess.
2.  Set a large, heavy-bottomed skillet over medium-high heat.
    -   Once warm, heat the butter until bubbles appear and subside.
    -   Gently lay fish in the pan, cooking for 5 min per side.[^fn:1]
    -   Set aside fish aside to rest in a warm place.
3.  Slice lemons into quarters and remove any seeds.
4.  In the same pan, place butter and lemon quarters.
    -   Cook for 3 min, until a rich, golden-brown colour.
    -   Squeeze the juice from the lemons into the butter, then toss to stew.
5.  Add the capers, brine, and parsley to the pan.
    -   Cook for 30 s, then spoon the sauce over the crispy fish.
6.  Serve immediately.


## References {#references}

1.  Original recipe: [A Rising Tide]({{< relref "../references/A_Rising_Tide.md" >}})

[^fn:1]: The fillets will be golden and crispy on the outside, just barely cooked through and opaque in the centre.
