+++
title = "Carajillo"
author = ["James Hawley"]
date = 2022-09-20T18:04:00-04:00
lastmod = 2023-11-20T09:29:43-05:00
tags = ["alcohols", "recipes", "beverages", "coffee"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 5 min      |
| Cook Time | 0 min      |
| Yields    | 1 cocktail |


## Ingredients {#ingredients}

| Quantity | Item                                              |
|----------|---------------------------------------------------|
| 1 oz     | [Licor 43]({{< relref "../items/licor_43.md" >}}) |
|          | [espresso]({{< relref "../items/espresso.md" >}}) |


## Directions {#directions}

1.  Pull the single or double shot of espresso, mix with the Licor 43, and serve.


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [The Espresso Martini Killer - Internet Shaquille](https://www.youtube.com/watch?v=9Ilnnne01So)
2.  Can also be served cold, over ice in a low ball glass.
