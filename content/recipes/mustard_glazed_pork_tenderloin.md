+++
title = "Mustard-glazed pork tenderloin"
author = ["James Hawley"]
date = 2021-03-28T00:00:00-04:00
lastmod = 2024-05-23T21:19:58-04:00
tags = ["pork", "recipes", "entrees"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 15 min     |
| Cook Time | 20 min     |
| Yields    | 4 servings |


## Ingredients {#ingredients}

| Quantity | Item                                                                    |
|----------|-------------------------------------------------------------------------|
| 400 g    | [pork tenderloin]({{< relref "../items/pork_tenderloin.md" >}})         |
| 3 Tbsp   | [brown sugar]({{< relref "../items/brown_sugar.md" >}})                 |
| 2 Tbsp   | [grainy mustard]({{< relref "../items/yellow_mustard.md" >}})           |
| 2 tsp    | [rosemary]({{< relref "../items/rosemary.md" >}})                       |
| 2 Tbsp   | [apple cider vinegar]({{< relref "../items/apple_cider_vinegar.md" >}}) |


## Directions {#directions}

1.  Combine all ingredients in a waterproof bag, let marinate for 15 min, or overnight
2.  Heat a large cast iron skillet or grill pan over medium-high heat
    1.  Brush pan with olive oil and add tenderloins
    2.  Reduce heat to medium-low
    3.  Cook, turning tenderloins every 3 to 4 minutes, until deeply caramelized on all sides, about 20 to 25 minutes
3.  Remove and lightly tent with foil; let rest about 10 minutes before cutting into 1/4- to 1/2-inch slices


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [NYT Cooking](https://cooking.nytimes.com/recipes/10080-mustard-glazed-pork-tenderloin)
