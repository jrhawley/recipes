+++
title = "Basil pesto"
author = ["James Hawley"]
date = 2023-01-28T16:22:00-05:00
lastmod = 2023-12-28T01:15:06-05:00
tags = ["sauces", "recipes"]
draft = false
+++

| Info      | Amount |
|-----------|--------|
| Prep Time | 5 min  |
| Cook Time | 0 min  |
| Yields    | 1 cup  |


## Ingredients {#ingredients}

| Quantity | Item                                                                |
|----------|---------------------------------------------------------------------|
| 1 clove  | [garlic]({{< relref "../items/garlic.md" >}})                       |
| 2 cup    | [basil]({{< relref "../items/basil.md" >}})                         |
| 1/4 cup  | [olive oil]({{< relref "../items/olive_oil.md" >}})                 |
| 2 Tbsp   | [water]({{< relref "../items/water.md" >}})                         |
| 2 Tbsp   | [lemon juice]({{< relref "../items/lemon_juice.md" >}})             |
| 3/2 tsp  | [nutritional yeast]({{< relref "../items/nutritional_yeast.md" >}}) |
| 1/4 tsp  | [Kosher salt]({{< relref "../items/kitchen_salt.md" >}}), to taste  |


## Directions {#directions}

1.  In a food processor or blender, mince the garlic.
2.  Add the remaining ingredients and blend until smooth.


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [Oh She Glows for Dinner]({{< relref "../references/Oh_She_Glows_for_Dinner.md" >}})
2.  Store in an airtight container in the fridge for 1 - 2 weeks, or in the freezer for up to 6 weeks.
