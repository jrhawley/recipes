+++
title = "Negroni"
author = ["James Hawley"]
date = 2021-02-17T00:00:00-05:00
lastmod = 2023-12-21T22:16:59-05:00
tags = ["recipes", "alcohols", "beverages"]
draft = false
+++

## Ingredients {#ingredients}

| Quantity | Item                                                        |
|----------|-------------------------------------------------------------|
| 1 oz     | [Campari]({{< relref "../items/campari.md" >}})             |
| 1 oz     | [gin]({{< relref "../items/gin.md" >}})                     |
| 1 oz     | [sweet vermouth]({{< relref "../items/dry_vermouth.md" >}}) |


## Directions {#directions}

1.  Add all ingredients to an ice-filled rocks glass and stir
2.  Garnish with an orange wedge or twist


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [The Periodic Table of Cocktails]({{< relref "../references/Periodic_Table_of_Cocktails.md" >}})
