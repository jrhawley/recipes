+++
title = "Farro and rapini gratin"
author = ["James Hawley"]
date = 2022-12-31T13:12:00-05:00
lastmod = 2024-05-23T20:58:32-04:00
tags = ["bowls", "vegetarian", "entrees", "recipes"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 10 min     |
| Cook Time | 45 min     |
| Yields    | 4 servings |


## Ingredients {#ingredients}

| Quantity | Item                                                                    |
|----------|-------------------------------------------------------------------------|
| 3 Tbsp   | [olive oil]({{< relref "../items/olive_oil.md" >}}), divided            |
| 1        | [white onion]({{< relref "../items/white_onion.md" >}}), finely chopped |
| 1/4 tsp  | [table salt]({{< relref "../items/table_salt.md" >}})                   |
| 3/2 cup  | [farro]({{< relref "../items/farro.md" >}})                             |
| 5/2 cup  | [water]({{< relref "../items/water.md" >}})                             |
| 2 cup    | [vegetable broth]({{< relref "../items/vegetable_broth.md" >}})         |
| 1/2 cup  | [Panko bread crumbs]({{< relref "../items/breadcrumbs.md" >}})          |
| 1/4 cup  | [Parmesan]({{< relref "../items/parmesan.md" >}}), grated               |
| 454 g    | [rapini]({{< relref "../items/rapini.md" >}})                           |
| 2 cloves | [garlic]({{< relref "../items/garlic.md" >}}), minced                   |
| 1/8 tsp  | [red pepper flakes]({{< relref "../items/red_pepper_flake.md" >}})      |
| 15 oz    | [chickpeas]({{< relref "../items/chickpea.md" >}})                      |
| 3/4 cup  | [sun-dried tomatoes]({{< relref "../items/sun-dried_tomato.md" >}})     |


## Directions {#directions}

1.  Heat 1 Tbsp of oil in a large saucepan over medium heat until shimmering.
    1.  Add onion and salt, sauteeing until softened and lighly browned (5 - 7 min).
    2.  Stir in farro, stirring occassionally, until lightly toasted (2 min).
    3.  Stire in water and broth and bring to a simmer.
    4.  Cooking, stirring often, until farro is tender and remaining liquid has thickening to a creamy sauce (25 - 35 min).
2.  While farro is cooking:
    1.  Toss bread crumbs with 1 Tbsp of oil in a bowl and microwave until golden brown (1 - 2 min).
    2.  Stir in parmesan and set aside.
3.  Bring water to a boil in a large pot.
    1.  Add rapini and 1 Tbsp salt and cook until just tender (2 min).
    2.  Drain and set aside.
    3.  Combine remaining 1 Tbp of oil, garlic, and pepper flakes in the now-empty pot and cook over medium heat until gragrant and sizzling (1 - 2 min).
    4.  Stir in reserved rapini and cook until hot and well-coated (2 min).
4.  Remove from heat and add beans, tomatoes, and farro mixture once done cooking.
    1.  Season with salt and pepper to taste.
5.  Adjust oven rack 30 cm from broiler and heat.
    1.  Transfer farro mixture to a 13"x9" broiler-safe dish and sprinkle with bread crumbs.
    2.  Broil the mixture until lightly browned and hot (1 - 2 min).
    3.  Serve.


## References &amp; Notes {#references-and-notes}

1.  The Cooking School Cookbook: Advances Fundamentals
