+++
title = "Cinnamon simple syrup"
author = ["James Hawley"]
date = 2022-10-04T12:35:00-04:00
lastmod = 2023-12-24T19:58:48-05:00
tags = ["sweeteners", "syrups", "recipes", "beverages"]
draft = false
+++

| Info      | Amount   |
|-----------|----------|
| Prep Time | 1 min    |
| Cook Time | 15 min   |
| Yields    | 3/2 cups |


## Ingredients {#ingredients}

| Quantity | Item                                                     |
|----------|----------------------------------------------------------|
| 1 cup    | water                                                    |
| 1 cup    | [sugar]({{< relref "../items/white_sugar.md" >}})        |
| 4        | [cinnamon sticks]({{< relref "../items/cinnamon.md" >}}) |


## Directions {#directions}

1.  In a small pot, add the ingredients and bring to a boil.
2.  Once a boil is reached, let simmer on low heat for 5 min.
3.  Turn off the heat and let the syrup cool.
4.  Remove the sticks and any strain into a storage container.
