+++
title = "Sweet potato quinoa bowl"
author = ["James Hawley"]
date = 2020-03-01T00:00:00-05:00
lastmod = 2024-05-23T21:35:43-04:00
tags = ["bowls", "recipes", "vegetarian", "vegan", "entrees"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 10 min     |
| Cook Time | 35 min     |
| Yields    | 4 servings |


## Ingredients {#ingredients}

| Quantity  | Item                                                                    |
|-----------|-------------------------------------------------------------------------|
| 1 cup     | [quinoa]({{< relref "../items/quinoa.md" >}}), uncooked                 |
| 2 cups    | [vegetable broth]({{< relref "../items/vegetable_broth.md" >}})         |
| 1/2 lbs   | [broccoli]({{< relref "../items/broccoli.md" >}}) florets               |
| 1         | [sweet potato]({{< relref "../items/yam.md" >}}), chunked               |
| 15 oz     | [chickpeas]({{< relref "../items/chickpea.md" >}}), drained, rinsed     |
| 1 handful | [kale]({{< relref "../items/kale.md" >}}), chopped                      |
| 1/4 cup   | [parsley]({{< relref "../items/parsley.md" >}}), chopped                |
| 3 Tbsp    | [feta]({{< relref "../items/feta.md" >}})                               |
| 1         | [lemon]({{< relref "../items/lemon.md" >}})                             |
| 2 tsp     | [apple cider vinegar]({{< relref "../items/apple_cider_vinegar.md" >}}) |
| 2 tsp     | [maple syrup]({{< relref "../items/maple_syrup.md" >}})                 |
|           | [olive oil]({{< relref "../items/olive_oil.md" >}})                     |
|           | [kosher salt]({{< relref "../items/kitchen_salt.md" >}})                |
|           | [pepper]({{< relref "../items/black_pepper.md" >}})                     |


## Directions {#directions}

1.  Preheat oven to 425 F
2.  Place quinoa and broth in a medium saucepan
    1.  Brin to boil, reduce to simmer, cover, and cook for 15 min
    2.  Remove from heat, stir with a fork, place in a large bowl
3.  Toss broccoli and sweet potato with olive oil
    1.  Place on a baking sheet and roast in the oven for 20 min
4.  Add chickpeas and kale to potatoes and broccoli
    1.  Roast entire sheet for another 15 min
5.  Combine cooked vegetables with quinoa
    1.  Sprinkle parsley and crumble feta
6.  In a small bowl, whisk together lemon, apple cider vinegar, syrup, 3 Tbsp olive oil, salt, and pepper
7.  Plate quinoa and vegetables, top with dressing


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [Eating Bird Food](https://www.eatingbirdfood.com/roasted-broccoli-kale-quinoa-salad/)
