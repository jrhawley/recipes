+++
title = "Frozen berry smoothie"
author = ["James Hawley"]
date = 2015-07-12T00:00:00-04:00
lastmod = 2023-12-21T21:50:37-05:00
tags = ["smoothies", "recipes", "beverages"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 5 min      |
| Yields    | 2 servings |


## Ingredients {#ingredients}

| Quantity | Item                                                                    |
|----------|-------------------------------------------------------------------------|
| 1        | frozen [banana]({{< relref "../items/banana.md" >}}), peeled and sliced |
| 2 cups   | frozen [strawberries]({{< relref "../items/strawberry.md" >}})          |
| 1 cup    | [milk]({{< relref "../items/milk.md" >}})                               |
| 1/2 cup  | [yogurt]({{< relref "../items/yogurt.md" >}})                           |
| 1/2 cup  | [orange juice]({{< relref "../items/orange_juice.md" >}})               |
| 2 Tbsp   | [honey]({{< relref "../items/honey.md" >}}), to taste                   |


## Directions {#directions}

1.  Blend all ingredients, serve
