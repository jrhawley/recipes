+++
title = "Banana bread with espresso glaze"
author = ["James Hawley"]
date = 2024-06-07T18:10:00-04:00
lastmod = 2024-06-12T19:12:25-04:00
tags = ["snacks", "breakfasts", "breads", "recipes"]
draft = false
+++

| Info      | Amount |
|-----------|--------|
| Prep Time | 10 min |
| Cook Time | 75 min |
| Yields    | 1 loaf |


## Equipment {#equipment}

-   1 large mixing bowl
-   2 small mixing bowls
-   1 large whisk
-   1 loaf pan
-   measuring cups and spoons


## Ingredients {#ingredients}


### Bread {#bread}

| Quantity | Item                                                                       |
|----------|----------------------------------------------------------------------------|
| 5/3 cups | [whole wheat flour]({{< relref "../items/whole_wheat_flour.md" >}})        |
| 1 tsp    | [baking soda]({{< relref "../items/baking_soda.md" >}})                    |
| 1/2 tsp  | [salt]({{< relref "../items/salt.md" >}})                                  |
| 1/2 tsp  | [ground cinnamon]({{< relref "../items/cinnamon.md" >}})                   |
| 2        | [eggs]({{< relref "../items/eggs.md" >}})                                  |
| 1 cup    | [brown sugar]({{< relref "../items/brown_sugar.md" >}})                    |
| 1/3 cup  | [milk]({{< relref "../items/milk.md" >}})                                  |
| 1/2 cup  | [coconut oil]({{< relref "../items/coconut_oil.md" >}}), melted and cooled |
| 4        | [bananas]({{< relref "../items/banana.md" >}}), mashed                     |
| 1 tsp    | [vanilla extract]({{< relref "../items/vanilla_extract.md" >}})            |
|          | coarse [sugar]({{< relref "../items/sugar.md" >}})                         |


### Glaze {#glaze}

| Quantity | Item                                                            |
|----------|-----------------------------------------------------------------|
| 3/2 cup  | [icing sugar]({{< relref "../items/icing_sugar.md" >}})         |
| 2 oz     | [espresso]({{< relref "../items/espresso.md" >}})               |
| 1 tsp    | [milk]({{< relref "../items/milk.md" >}})                       |
| 1/2 tsp  | [vanilla extract]({{< relref "../items/vanilla_extract.md" >}}) |


## Directions {#directions}

1.  Preheat oven to 325 F with a baking sheet inside and coat a loaf pan with butter.
2.  In a small bowl, combine the dry ingredients, whisk together, and set aside.
3.  In a large bowl, whisk together the eggs and brown sugar until smooth.
    1.  Add in the milk and coconut oil and whisk to combine.
    2.  Slowly stir in the dry ingredients while mixing.
    3.  Pour batter into the loaf pan and top with coarse sugar.
4.  Whisk together the espresso glaze ingredients in a small bowl, adding icing sugar or milk to achieve the desired viscosity.
5.  Place the loaf pan on the baking sheet and bake for 75 min, until set.
    1.  Remove from the oven and let set for 20 min before removing from the pan to cool completely on a wire rack over the baking sheet.
    2.  Pour the glaze over the loaf and let set for 30 min before eating.
    3.  Clean up the drippings in the baking sheet while the glaze sets.


## References {#references}

1.  Original recipe: [How Sweet Eats](https://www.howsweeteats.com/wprm_print/58256)
