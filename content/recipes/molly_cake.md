+++
title = "Molly cake"
author = ["James Hawley"]
date = 2024-05-23T19:57:00-04:00
lastmod = 2024-05-23T20:14:08-04:00
tags = ["cakes", "desserts", "recipes"]
draft = false
+++

| Info      | Amount          |
|-----------|-----------------|
| Prep Time | 10 min          |
| Cook Time | 20 min          |
| Yields    | 1 cake, 2 tiers |


## Equipment {#equipment}

-   1 stand mixer with bowl
-   1 sifter or fine-mesh collander
-   1 medium mixing bowl
-   1 electric mixer, frother, or whisk
-   1 silicone or cake spatula
-   toothpicks


## Ingredients {#ingredients}

| Quantity        | Item                                                                     |
|-----------------|--------------------------------------------------------------------------|
| 4               | [eggs]({{< relref "../items/eggs.md" >}}), separated                     |
| 300 g (3/2 cup) | [granulated sugar]({{< relref "../items/white_sugar.md" >}})             |
| 1 tsp           | [vanilla extract]({{< relref "../items/vanilla_extract.md" >}})          |
| 180 g (3/2 cup) | [all-purpose flour]({{< relref "../items/all-purpose_flour.md" >}})      |
| 2 tsp           | [baking powder]({{< relref "../items/baking_powder.md" >}})              |
| 1/4 tsp         | [fine sea salt]({{< relref "../items/sea_salt.md" >}})                   |
| 2 cup           | [whipping cream]({{< relref "../items/whipping_cream.md" >}}), separated |
|                 | [raspberries]({{< relref "../items/raspberries.md" >}}) or other berries |
|                 | [icing sugar]({{< relref "../items/icing_sugar.md" >}})                  |


## Directions {#directions}

1.  Preheat the oven to 175 C (350 F) and lightly grease and line the bottom of two 8" cake pans.
2.  In the stand mixer bowl, whisk together on high speed the egg whites and sugar, until the meringue holds stiff peaks.
    1.  Add the egg yolks and vanilla extract and beat at medium speed, just until combined.
    2.  Sift the flour and baking powder into the bowl, and mix on low until combined.
3.  In a separate bowl, use an electric mixer, frother, or whisk, and whip 1 cup of cream together until it holds stiff peaks.
    1.  Fold the whipped cream into the stand mixer bowl until combined and smooth.
    2.  Distribute the batter into each cake pan, scraping the sides of the mixing bowl.
4.  Place both pans in the oven and bake for 25 min each, testing for doneness with a toothpick&nbsp;[^fn:1].
    1.  When finished baking, remove cake pans and allow to cool completely on the stove top.
5.  While the cakes cool, take the remaining 1 cup of cream and whip until it holds stiff peaks.
    1.  Using the spatula, spread the whipped cream over the bottom tier until level.
    2.  Place the second tier on top, then repeat.
    3.  If there is leftover whipped cream, spread around the sides of the entire cake, if desired.
6.  Place the berries around the outer rim of the cake and sift a light dusting of icing sugar over top, to garnish.


## References {#references}

1.  Original recipe: [Baran Bakery](https://baranbakery.com/wprm_print/17443)

[^fn:1]: The tops should be golden brown and spring back when gently pressed, in the centre.
