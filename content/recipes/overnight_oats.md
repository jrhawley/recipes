+++
title = "Overnight oats"
author = ["James Hawley"]
date = 2024-08-12T19:42:00-04:00
lastmod = 2024-10-19T01:08:17-04:00
tags = ["breakfasts", "recipes"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 10 min     |
| Rest Time | 4 h        |
| Yields    | 4 servings |


## Equipment {#equipment}

-   1 large container with a lid


## Ingredients {#ingredients}

| Quantity | Item                                                 |
|----------|------------------------------------------------------|
| 1 cup    | [oats]({{< relref "../items/oat.md" >}})             |
| 1 cup    | [milk]({{< relref "../items/milk.md" >}})            |
| 2 Tbsp   | [chia seeds]({{< relref "../items/chia_seed.md" >}}) |


## Directions {#directions}

1.  Add all ingredients in a container and stir.
2.  Refrigerate for 2 h or longer, then serve with berries or other toppings as desired.
