+++
title = "Mediterranean chickpea casserole"
author = ["James Hawley"]
date = 2023-03-10T18:54:00-05:00
lastmod = 2024-05-23T20:57:00-04:00
tags = ["entrees", "salads", "vegan", "vegetarian", "recipes"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 5 min      |
| Cook Time | 40 min     |
| Yields    | 4 servings |


## Ingredients {#ingredients}

| Quantity      | Item                                                               |
|---------------|--------------------------------------------------------------------|
| 3 tablespoons | [olive oil]({{< relref "../items/olive_oil.md" >}})                |
| 2             | medium [red onions]({{< relref "../items/red_onion.md" >}}), diced |
| 1 clove       | [garlic]({{< relref "../items/garlic.md" >}}), minced              |
| 1/2 teaspoon  | [fine sea salt]({{< relref "../items/sea_salt.md" >}})             |
| 680 mL jar    | [passata]({{< relref "../items/passata.md" >}})                    |
| 1 cup         | [vegetable broth]({{< relref "../items/vegetable_broth.md" >}})    |
| 1 teaspoon    | [oregano]({{< relref "../items/oregano.md" >}})                    |
| 1/4 teaspoon  | [ground cloves]({{< relref "../items/clove.md" >}})                |
| 1/4 teaspoon  | [allspice]({{< relref "../items/allspice.md" >}})                  |
| 1/2 teaspoon  | [paprika]({{< relref "../items/paprika.md" >}})                    |
| 2 15oz cans   | [chickpeas]({{< relref "../items/chickpea.md" >}})                 |
| 3 cups        | [spinach]({{< relref "../items/spinach.md" >}})                    |
| 1/4 cup       | [lemon juice]({{< relref "../items/lemon_juice.md" >}})            |
| 2/3 cup       | [feta]({{< relref "../items/feta.md" >}}) (omit if vegan)          |
|               | [black pepper]({{< relref "../items/black_pepper.md" >}})          |
|               | [parsley]({{< relref "../items/parsley.md" >}})                    |


## Directions {#directions}

1.  Heat olive oil in a Dutch oven over medium-high heat
    1.  Add onion, sauté until translucent, about 5 min
    2.  Reduce heat to medium
    3.  Add garlic and salt, cook, stirring frequently, until fragrant, 1 to 2 min
2.  Stir in passata, vegetable broth, bay leaves, oregano, cloves, allspice, and cayenne
    1.  Cover the pot with a lid
    2.  Bring to a boil and cook for 20 min
3.  Stir in the chickpeas
    1.  Cook uncovered until the liquid has reduced by one-third, 10 - 15 min, stirring occasionally&nbsp;[^fn:1]
4.  Position a oven rack so that the top of the pot will be 4-inches away from the broiler and turn oven to broil.
5.  Remove bay leaves, stir in spinach, and cook until wilted, about 2 min
    1.  Stir in sugar and lemon juice
    2.  Season with salt and pepper, to taste
6.  Crumble feta overtop, then broil until cheese is golden brown, about 5 min
7.  Serve with bread or soft pita for dipping, or spoon over a grain like rice.


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [Evergreen Kitchen](https://evergreenkitchen.ca/print-recipe/2622/)
2.  Can be reheated in the oven (uncovered) at 140°C (275°F), in the microwave, or on the stove (covered) over medium-low heat until warmed through.

[^fn:1]: Reduce heat if the mixture starts to splatter, but keep mixture actively simmering.
