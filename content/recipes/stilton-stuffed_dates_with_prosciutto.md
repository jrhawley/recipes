+++
title = "Stilton-stuffed dates with prosciutto"
author = ["James Hawley"]
date = 2022-09-27T09:19:00-04:00
lastmod = 2023-12-22T01:16:57-05:00
tags = ["appetizers", "recipes"]
draft = false
+++

| Info      | Amount                |
|-----------|-----------------------|
| Prep Time | 10 min                |
| Cook Time | 20 min                |
| Yields    | 12 dates / 6 servings |


## Ingredients {#ingredients}

| Quantity | Item                                                             |
|----------|------------------------------------------------------------------|
| 12       | [dates]({{< relref "../items/medjool_date.md" >}}), pitted       |
| 55 g     | [stilton]({{< relref "../items/stilton_cheese.md" >}}), crumbled |
| 6 slices | [prosciutto]({{< relref "../items/prosciutto.md" >}})            |


## Directions {#directions}

1.  Preheat the oven to 180 C (350 F) and line a baking sheet with aluminium foil.
2.  Cut a lengthwise slit in each date.
    -   Stuff each one with cheese to fill the cavity but not spill out.
    -   Pinch the dates closed.
3.  Wrap each date in a piece of prosciutto and arrange them seam-side down on the baking sheet.
4.  Bake in the oven for 10 min.
    -   Flip and bake for 10 min, being careful not to break the date seam.
5.  Serve warm or at room temperature.


## References &amp; Notes {#references-and-notes}

1.  Original recipe: Wine Bites - Barbara Scott-Goodman
