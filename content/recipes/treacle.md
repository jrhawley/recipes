+++
title = "Treacle"
author = ["James Hawley"]
date = 2022-03-13T00:00:00-05:00
lastmod = 2023-12-24T20:04:13-05:00
tags = ["recipes", "alcohols", "beverages"]
draft = false
+++

## Ingredients {#ingredients}

| Quantity | Ingredient                                                          |
|----------|---------------------------------------------------------------------|
| 1/3 oz   | [simple syrup](../_recipes/simple-syrup.md)                         |
| 2 dashes | [Angostura bitters]({{< relref "../items/angostura_bitters.md" >}}) |
| 5/3 oz   | Jamaican [rum]({{< relref "../items/rum.md" >}})                    |
| 1/2 oz   | [apple juice]({{< relref "../items/apple_juice.md" >}})             |
|          | ice cubes                                                           |
|          | strip of [lemon zest]({{< relref "../items/lemon.md" >}})           |


## Directions {#directions}

1.  Add simple syrup, bitters, half the rum, and 2 ice cubes to a rocks glass; stir.
2.  Add the remaining rum and another 2 ice cubes and stir.
3.  Fill the glass with ice and float the apple juice on top.
4.  Garnish with a strip of lemon zest.


## References {#references}

1.  Original recipe: [The Periodic Table of Cocktails]({{< relref "../references/Periodic_Table_of_Cocktails.md" >}})
