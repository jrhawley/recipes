+++
title = "Bay Breeze"
author = ["James Hawley"]
date = 2023-02-09T18:42:00-05:00
lastmod = 2023-12-28T01:28:36-05:00
tags = ["alcohols", "recipes", "beverages"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 0 min      |
| Cook Time | 0 min      |
| Yields    | 1 cocktail |


## Ingredients {#ingredients}

| Quantity | Item                                                                                                        |
|----------|-------------------------------------------------------------------------------------------------------------|
| 3/2 oz   | [pineapple juice]({{< relref "../items/pineapple_juice.md" >}})                                             |
| 1 oz     | [vodka]({{< relref "../items/vodka.md" >}})                                                                 |
| 3 oz     | [cranberry juice]({{< relref "../items/cranberry_juice.md" >}})                                             |
| 1 tsp    | [simple syrup]({{< relref "simple_syrup.md" >}}) or [maple syrup]({{< relref "../items/maple_syrup.md" >}}) |
| 1        | [lime wedge]({{< relref "../items/lime.md" >}}), garnish                                                    |


## Directions {#directions}

1.  Add pineapple juice and vodka to a glass and stir.
2.  Fill the glass with ice.
3.  In a separate small glass, stir the cranberry juice and syrup.
4.  To ensure the layered look, tip the ice-filled glass slightly and slowly pour the cranberry juice along the side of the glass.
5.  Garnish with a lime wedge and serve.


## References {#references}

1.  Original recipe: [A Couple Cooks](https://www.acouplecooks.com/bay-breeze-cocktail/print/52307/)
