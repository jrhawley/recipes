+++
title = "Winter spiced oatcakes"
author = ["James Hawley"]
date = 2023-03-12T10:36:00-04:00
lastmod = 2023-12-24T20:07:57-05:00
tags = ["snacks", "desserts", "recipes"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 5 min      |
| Cook Time | 30 min     |
| Yields    | 9 oatcakes |


## Ingredients {#ingredients}

| Quantity | Item                                                            |
|----------|-----------------------------------------------------------------|
| 1/2 cup  | [butter]({{< relref "../items/butter.md" >}})                   |
| 1 cup    | [oats]({{< relref "../items/oat.md" >}})                        |
| 3/4 cup  | [flour]({{< relref "../items/flour.md" >}})                     |
| 1/2 cup  | [brown sugar]({{< relref "../items/brown_sugar.md" >}})         |
| 2 tsp    | [ground ginger]({{< relref "../items/ginger_powder.md" >}})     |
| 1 tsp    | [nutmeg]({{< relref "../items/nutmeg.md" >}})                   |
| 1 tsp    | [cardamom]({{< relref "../items/cardamom.md" >}})               |
| 1/2 tsp  | [allspice]({{< relref "../items/allspice.md" >}})               |
| 6 oz     | [white chocolate]({{< relref "../items/white_chocolate.md" >}}) |


## Directions {#directions}

1.  Preheat the oven to 190 °C (375 °F)
2.  In a small pot, melt the butter
    1.  In a 9" square baking pan, using a wooden spoon or spatula, mix together the oats, flour, sugar, ginger, nutmeg, cardamom, and allspice until fully combined
    2.  Pour in the melted butter and mix until the mixture is crumbly and no dry flour is visible
    3.  If needed, use your fingers to get every last bit evenly moist
    4.  Then, using the back of your hand, pack the mixture firmly into the dish in an even layer
3.  Bake until golden brown and starting to look crisp around the edges, 25 - 30 min
4.  As soon as you remove it from the oven, use a metal spatula or butter knife to cut around the outside of the pan and then cut nine squares
    1.  Let cool in the pan for 10 - 15 min
    2.  Transfer the squares to a wire rack and allow to cool completely&nbsp;[^fn:1]
5.  When the squares are fully cool, melt the chopped white chocolate
    1.  In a small saucepan over low heat, begin to melt the chocolate chips, stirring constantly and being careful not to burn (or put them in a small bowl and microwave for 10 s intervals, stirring between intervals)
    2.  Remove from the heat once half of the chocolate has melted
    3.  Keep stirring until smooth and creamy
    4.  Set aside to temper and cool
    5.  You should have approximately 1/2 cup of melted chocolate
    6.  Dip half of each oatcake square into the melted chocolate, hold it over the bowl for a few seconds to allow any excess to drip, off and return to the wire rack to allow the chocolate to set and harden, 30 - 40 min


## References {#references}

1.  Original recipe: [A Rising Tide]({{< relref "../references/A_Rising_Tide.md" >}})
2.  The chocolate-dipped oatcakes will keep in an airtight container at room temperature for up to 1 week.
    The oatcakes without the chocolate will keep in an airtight container in the freezer for 2 - 3 months.

[^fn:1]: The squares may stick together a bit when you’re removing them from the pan, but breaking them apart is quite easy