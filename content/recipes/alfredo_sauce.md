+++
title = "Alfredo sauce"
author = ["James Hawley"]
date = 2013-09-14T00:00:00-04:00
publishDate = 2013-09-14
lastmod = 2023-08-16T23:14:36-04:00
tags = ["recipes", "sauces"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 5 min      |
| Cook Time | 10 min     |
| Yields    | 4 servings |


## Ingredients {#ingredients}

| Quantity | Item                                                                |
|----------|---------------------------------------------------------------------|
| 1 Tbsp   | [butter]({{< relref "../items/butter.md" >}})                       |
| 1/4 cup  | [all-purpose flour]({{< relref "../items/all-purpose_flour.md" >}}) |
| 1 tsp    | [table salt]({{< relref "../items/table_salt.md" >}})               |
| 3/4 cup  | [Parmesan]({{< relref "../items/Parmesan.md" >}})                   |
| 3 cups   | [dairy milk]({{< relref "../items/dairy_milk.md" >}})               |
| 1/2 tsp  | [black pepper]({{< relref "../items/black_pepper.md" >}})           |
| 1 pinch  | [nutmeg]({{< relref "../items/nutmeg.md" >}})                       |


## Directions {#directions}

1.  Melt butter in pan over medium heat
2.  Mix milk and flour in similar proportions until all used&nbsp;[^fn:1]
3.  Reduce heat, add remaining ingredients, stirring constantly
4.  Once all ingredients are added, heat until boiling, then remove from heat and serve&nbsp;[^fn:2]

[^fn:1]: Try to keep a play-doh-like consistency to let the flour absorb the milk
[^fn:2]: In the end, the flour should be evenly distributed through the milk