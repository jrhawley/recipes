+++
title = "Peach burrata and basil salad"
author = ["James Hawley"]
date = 2023-03-12T09:20:00-04:00
lastmod = 2023-12-22T00:53:30-05:00
tags = ["salads", "recipes"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 10 min     |
| Cook Time | 50 min     |
| Yields    | 6 servings |


## Ingredients {#ingredients}

| Quantity  | Item                                                                              |
|-----------|-----------------------------------------------------------------------------------|
| 10        | [peaches]({{< relref "../items/peach.md" >}}), stoned and sliced                  |
| 20 leaves | [basil]({{< relref "../items/basil.md" >}})                                       |
| 1 ball    | [burrata]({{< relref "../items/burrata.md" >}})                                   |
| 3 Tbsp    | [pickled onions]({{< relref "../items/pickled_onion.md" >}}), including the brine |
|           | [sea salt]({{< relref "../items/sea_salt.md" >}})                                 |
|           | [black pepper]({{< relref "../items/black_pepper.md" >}})                         |
| 3 Tbsp    | [olive oil]({{< relref "../items/olive_oil.md" >}})                               |


## Directions {#directions}

1.  Decorate a serving plate with peach slices and basil leaves
2.  Top with burrata and pickled onions, with a spoonful of the pickling brine
3.  Season with salt and pepper, and drizzle with olive oil


## References {#references}

1.  Original recipe: [Peak Season]({{< relref "../references/Peak_Season.md" >}})
