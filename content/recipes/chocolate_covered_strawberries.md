+++
title = "Chocolate covered strawberries"
author = ["James Hawley"]
date = 2018-06-27T00:00:00-04:00
lastmod = 2023-12-19T22:46:47-05:00
tags = ["recipes", "desserts"]
draft = false
+++

| Info      | Amount |
|-----------|--------|
| Prep Time |        |
| Cook Time |        |
| Yields    |        |

{{< figure src="../assets/chocolate-covered-strawberries.jpg" caption="<span class=\"figure-number\">Figure 1: </span>Chocolate covered strawberries" >}}


## Ingredients {#ingredients}

| Quantity | Item                                                            |
|----------|-----------------------------------------------------------------|
| 16 oz    | [chocolate chips]({{< relref "../items/chocolate_chip.md" >}})  |
| 2 Tbsp   | [shortening]({{< relref "../items/vegetable_shortening.md" >}}) |
| 1 lb     | [strawberries]({{< relref "../items/strawberry.md" >}}), fresh  |


## Directions {#directions}

1.  In a double boiler, melt the chocolate and shortening, stirring occasionally until smooth.
2.  Holding them by the toothpicks, dip the strawberries into the chocolate mixture.
3.  Insert toothpicks into the tops of the strawberries.
4.  Turn the strawberries upside down and insert the toothpick into styrofoam for the chocolate to cool.


## References &amp; Notes {#references-and-notes}

1.  Original recipes: [All Recipes](https://www.allrecipes.com/recipe/21712/chocolate-covered-strawberries/)
