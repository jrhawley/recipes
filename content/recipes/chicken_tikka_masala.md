+++
title = "Chicken tikka masala"
author = ["James Hawley"]
date = 2022-11-12T21:25:00-05:00
lastmod = 2024-05-23T21:12:20-04:00
tags = ["chicken", "entrees", "recipes"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 10 min     |
| Cook Time | 15 min     |
| Yields    | 4 servings |


## Ingredients {#ingredients}

| Quantity | Item                                                            |
|----------|-----------------------------------------------------------------|
| 1 cup    | [basmati rice]({{< relref "../items/basmati_rice.md" >}})       |
| 2        | [chicken breasts]({{< relref "../items/chicken_breast.md" >}})  |
| 1        | [white onion]({{< relref "../items/onion.md" >}}), diced        |
| 2        | [carrots]({{< relref "../items/carrot.md" >}}), diced           |
| 15 oz    | [chickpeas]({{< relref "../items/chickpea.md" >}})              |
| 400 mL   | [Tikka masala sauce]({{< relref "../items/tikka_masala.md" >}}) |
|          | [olive oil]({{< relref "../items/olive_oil.md" >}})             |
|          | [Kosher salt]({{< relref "../items/kitchen_salt.md" >}})        |


## Directions {#directions}

1.  Cook the rice according to instructions.
2.  While rice is cooking, heat olive oil and salt over medium-high heat in a large saucepan.
    -   If using frozen chicken, cook until sealed, then place on a cutting board
3.  Sautee the diced onion.
    -   When soft, add diced carrots.
4.  Add diced chicken and cook until sealed.
5.  Reduce heat, pour in tikka masala sauce, and add chickpeas.
    -   Cover and simmer for 10 min, stirring occasionally.
6.  When rice has finished cooking, remove saucepan from heat and allow both to cool for a few minutes.
7.  Plate rice and spoon chicken, vegetables, and tikka masala over rice.
