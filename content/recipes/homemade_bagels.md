+++
title = "Homemade bagels"
author = ["James Hawley"]
date = 2023-12-02T12:01:00-05:00
lastmod = 2024-06-12T19:14:36-04:00
tags = ["breakfasts", "lunch", "snacks", "bagels", "recipes"]
draft = false
+++

| Info      | Amount   |
|-----------|----------|
| Prep Time | 24 h     |
| Cook Time | 25 min   |
| Yields    | 6 bagels |


## Equipment {#equipment}

-   small mixing bowl
-   large mixing bowl
-   flexible spatula
-   cutting board
-   2 large baking sheets with rims
-   wire rack
-   bench scraper
-   slotted spoon
-   tea towel
-   Dutch oven
-   serrated knife


## Ingredients {#ingredients}


### Dough {#dough}

| Quantity | Item                                                                                                             |
|----------|------------------------------------------------------------------------------------------------------------------|
| 9/8 tsp  | [traditional yeast]({{< relref "../items/traditional_yeast.md" >}})                                              |
| 9/8 cup  | [water]({{< relref "../items/water.md" >}})                                                                      |
| 1 Tbsp   | [malt syrup]({{< relref "../items/malt_syrup.md" >}}) or [molasses]({{< relref "../items/fancy_molasses.md" >}}) |
| 1 Tbsp   | [kosher salt]({{< relref "../items/kitchen_salt.md" >}})                                                         |
| 13/4 cup | [bread flour]({{< relref "../items/bread_flour.md" >}})                                                          |


### Assembly {#assembly}

| Quantity | Item                                                             |
|----------|------------------------------------------------------------------|
| 1/2 tsp  | [baking soda]({{< relref "../items/baking_soda.md" >}})          |
| 1/7 cup  | [malt syrup]({{< relref "../items/malt_syrup.md" >}})            |
| 1 oz     | [sesame seeds]({{< relref "../items/sesame_seed.md" >}}) [^fn:1] |
|          | [water]({{< relref "../items/water.md" >}})                      |


## Directions {#directions}


### Dough {#dough}

1.  Pour 1/2 cup lukewarm water into a small bowl.
    1.  Whisk in malt syrup / molasses and yeast until both dissolve.
    2.  Let sit for 5 min.
2.  In a large bowl, combine bread flour and salt, then make a well in the centre.
    1.  Pour in the yeast misture and remaining water.
    2.  Mix until the dough is "shaggy".
3.  Knead the mixture in the bowl with a wet hand, continuously folding onto itself and pressing fown firmly.
    1.  On a clean work surface, continue kneading until there are no dry spots.
    2.  Continue until you have a stiff but very smooth dough that is slightly tacky, ~15 min.
4.  Gather dough into a ball, dust it lightly with flour, and place in a large bowl, seam side down.
    1.  Cover with a damp tea towel and let rise at room temperature until it has doubled in size, ~2 h.
5.  Lightly degas the dough by hitting and flattening the dough.
    1.  Place on a clean surface and cut into 6 equal pieces with the dough cutter (125 g each).
6.  Form each piece into a tight ball.
    1.  Working one at a time, pinch edges together firmly to make a teardrop share.
    2.  Place dough seam side down, cup your hand over the top in a loose grip.
    3.  Drag the dough across the surface until it has a tight dome.
    4.  Repeat with all pieces.
    5.  Cover them with the damp towel and rest for 5 min.
7.  Line each baking sheet with parchment paper and lightly brush with oil.
8.  Working one at a time, roll out a ball on your work surface into a ~25 cm rope.
    1.  Thin out the ends, then wrap the entire rope around one hand, with the ends overlapping across your palm.
    2.  Press the two ends together in the palm of your hand.
    3.  Roll the dough under your hand back and forth to seal the ends.
    4.  Slip the ring of dough off your hand and stretch it around until uniformly thick.
    5.  Place each ring on the baking sheet, then repeat with the rest of the dough.
9.  Cover each baking sheet with plastic wrap, then a damp towel.
    1.  Transfer baking sheets to the fridge and chill for 4 - 24 h.


### Baking {#baking}

1.  When the dough is ready&nbsp;[^fn:2], fill the Dutch oven with water and bring to a boil.
    1.  Whisk in the baking soda and malt syrup / molasses&nbsp;[^fn:3]
    2.  Reduce heat to maintain a gentle boil and skim off any foam.
2.  Uncover one baking sheet and tranfer 3 bagels into the water.
    1.  Boil for 1 min, flipping halfway through.
    2.  Remove each bagel with a slotted spoon and transfer to a wire rack over a tray or towel to catch the water.
    3.  Repeat with the rest of the bagels.
3.  If adding a topping, place in a wide shallow bowl and gently press each ring into the topping.
4.  Evenly space each bagel on the empty baking sheet, flat side down.
    1.  Bake in the over until the bagels have browned for 12 min.
    2.  Rotate the baking sheet and bake for another 8 - 13 min (20 - 25 min total).
    3.  While the first sheet is in the over, repeat the boiling process with the second sheet.
5.  Remove cooked bagels from the oven and place on a wire rack to cook.
6.  Slice with a serrated knife and eat immediately.


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [NYT Cooking / Claire Saffitz](https://cooking.nytimes.com/guides/81-how-to-make-bagels?smtyp=cur)

[^fn:1]: Or whatever topping mix you want. Optional.
[^fn:2]: To check if the dough is ready, take one ring from the fridge and place in a small bowl of room temperature water.
    If the ring floats, they're ready.
    If not, dry the dough, return to the baking sheet, and let proof for another 30 min before checking again.
[^fn:3]: The water should resemble strong black tea.
