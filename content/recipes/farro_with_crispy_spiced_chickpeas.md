+++
title = "Farro with crispy spiced chickpeas"
author = ["James Hawley"]
date = 2023-05-24T18:27:00-04:00
lastmod = 2024-05-23T20:45:03-04:00
tags = ["vegetarian", "entrees", "recipes"]
draft = false
+++

| Info      | Amount         |
|-----------|----------------|
| Prep Time | 5 min          |
| Cook Time | 45 min         |
| Yields    | 4 - 6 servings |


## Ingredients {#ingredients}

| Quantity     | Item                                                                   |
|--------------|------------------------------------------------------------------------|
| 2 15 oz cans | [chickpeas]({{< relref "../items/chickpea.md" >}}), drained and rinsed |
| 135 mL       | [olive oil]({{< relref "../items/olive_oil.md" >}})                    |
| 5/2 tsp      | [Kosher salt]({{< relref "../items/kitchen_salt.md" >}})               |
|              | [black pepper]({{< relref "../items/black_pepper.md" >}})              |
| 4 Tbsp       | [butter]({{< relref "../items/butter.md" >}})                          |
| 1 tsp        | [fennel seeds]({{< relref "../items/fennel.md" >}})                    |
| 1/2 tsp      | [red pepper flakes]({{< relref "../items/red_pepper_flake.md" >}})     |
| 1            | [lemon]({{< relref "../items/lemon.md" >}}), juiced and zested         |
| 12 stalks    | [green onion]({{< relref "../items/green_onion.md" >}})                |
| 3 sprigs     | [thyme]({{< relref "../items/thyme.md" >}})                            |
| 227 g        | [cherry tomatoes]({{< relref "../items/cherry_tomato.md" >}}), halved  |
| 1 cup        | [parsley]({{< relref "../items/parsley.md" >}}), roughly chopped       |
| 3/2 cup      | [farro]({{< relref "../items/farro.md" >}})                            |
| 1 clove      | [garlic]({{< relref "../items/garlic.md" >}})                          |


## Directions {#directions}

1.  Dry the drained chickpeas and heat in a large skillet with 3 Tbsp oil over medium-high heat.
    1.  Add 1/2 tsp of salt and some black pepper and cook until crisped, stirring occassionally (~ 10 - 12 min).
    2.  Add butter, fennel seeds, and red pepper flakes, and cook until golden.
    3.  Remove from heat, transfer to a bowl, and add 1 - 2 tsp lemon juice.
2.  Add the green onion, some oil, and salt, and cook until tender.
3.  Add 3 cups water, thyme, and 2 tsp Kosher salt and bring to a simmer.
    1.  Stir in the farro, then lower to medium-low heat and cover the pan.
    2.  Cook until the liquid has evaporated and the farro is tender (~ 25 - 35 min).&nbsp;[^fn:1]
4.  Discard the thyme sprigs, stir in the tomatoes, garlic, lemon zest, and remainin lemon juice.
    1.  Season with lemon juice, salt, and pepper as needed.
5.  Combine everything together just before serving.

[^fn:1]: If the water dries out before the farro is finished cooking, add a bit more water.
