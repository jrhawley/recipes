+++
title = "Bean medley salad"
author = ["James Hawley"]
date = 2015-01-17T00:00:00-05:00
lastmod = 2024-05-23T21:44:54-04:00
tags = ["salads", "recipes", "gluten-free", "vegetarian", "lunch", "entrees"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 30 min     |
| Cook Time | 20 min     |
| Yields    | 6 servings |


## Ingredients {#ingredients}

| Quantity | Item                                                          |
|----------|---------------------------------------------------------------|
| 1 cup    | [quinoa]({{< relref "../items/quinoa.md" >}})                 |
| 10 oz    | [kale]({{< relref "../items/kale.md" >}})                     |
| 1/2      | [red onion]({{< relref "../items/red_onion.md" >}})           |
| 6 stalks | [green onion]({{< relref "../items/green_onion.md" >}})       |
| 15 oz    | [black beans]({{< relref "../items/black_bean.md" >}})        |
| 15 oz    | [chickpeas]({{< relref "../items/chickpea.md" >}})            |
| 100 g    | [feta]({{< relref "../items/feta.md" >}})                     |
|          | [kosher salt]({{< relref "../items/kitchen_salt.md" >}})      |
|          | [pepper]({{< relref "../items/black_pepper.md" >}})           |
|          | [cumin]({{< relref "../items/cumin.md" >}})                   |
|          | [lemon]({{< relref "../items/lemon.md" >}})                   |
|          | [cayenne pepper]({{< relref "../items/cayenne_pepper.md" >}}) |
|          | [chili pepper]({{< relref "../items/chili_pepper.md" >}})     |


## Directions {#directions}

1.  Make quinoa according to instructions, set aside
2.  Rinse greens (parsley or kale or spinach)
    -   Remove stems, chop
3.  Heat pan, add olive oil, add 1/4 greens and some water
    -   Add more water until all the greens are added and hydrated
    -   Be careful not to burn though
4.  Add chick peas to pan, heat for a few minutes
    -   Add black beans and quinoa, heat for a few minutes
    -   Add chopped onions
    -   Add spices and stir, lowering heat
5.  Add feta, lemon, and extra olive oil if desired, and serve&nbsp;[^fn:1]

[^fn:1]: Don't cook the quinoa, feta, and other vegetables for too long.
    The salad will clump together if overcooked.
