+++
title = "Tequila sunrise"
author = ["James Hawley"]
date = 2022-03-13T00:00:00-05:00
lastmod = 2023-12-24T19:57:46-05:00
tags = ["recipes", "alcohols", "beverages"]
draft = false
+++

## Ingredients {#ingredients}

| Quantity | Ingredients                                               |
|----------|-----------------------------------------------------------|
| 5/3 oz   | [tequila]({{< relref "../items/tequila.md" >}})           |
| 5/2 oz   | [orange juice]({{< relref "../items/orange_juice.md" >}}) |
| 1 tsp    | [grenadine]({{< relref "../items/grenadine.md" >}})       |


## Directions {#directions}

1.  Add the tequila and orange juice to a cocktail shaker.
2.  Fill with ice and shake.
3.  Stain into a highball glass filled with ice.
4.  Pour the grenadine into the top of the glass.


## References {#references}

1.  Original recipe: [The Periodic Table of Cocktails]({{< relref "../references/Periodic_Table_of_Cocktails.md" >}})
