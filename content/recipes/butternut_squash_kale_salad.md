+++
title = "Butternut squash kale salad"
author = ["James Hawley"]
date = 2023-07-30T13:28:00-04:00
lastmod = 2023-10-26T00:23:52-04:00
tags = ["salads", "vegetarian", "recipes"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 15 min     |
| Cook Time | 25 min     |
| Yields    | 4 servings |


## Ingredients {#ingredients}


### Salad {#salad}

| Quantity | Item                                                                     |
|----------|--------------------------------------------------------------------------|
| 3 cups   | [butternut squash]({{< relref "../items/butternut_squash.md" >}}), cubed |
| 2 Tbsp   | [olive oil]({{< relref "../items/olive_oil.md" >}})                      |
| 1/2 tsp  | [Kosher salt]({{< relref "../items/kitchen_salt.md" >}})                 |
| 1/2 tsp  | [garlic powder]({{< relref "../items/garlic_powder.md" >}})              |
| 1/2 tsp  | [cumin]({{< relref "../items/cumin.md" >}})                              |
| 1/2 tsp  | [paprika]({{< relref "../items/paprika.md" >}})                          |
| 1 head   | [kale]({{< relref "../items/kale.md" >}}), stemmed                       |
| 2 oz     | [goat cheese]({{< relref "../items/goat_cheese.md" >}}) crumble          |
| 1/4 cup  | [pumpkin seeds]({{< relref "../items/pumpkin_seed.md" >}})               |


### Vinaigrette {#vinaigrette}

| Quantity | Item                                                                    |
|----------|-------------------------------------------------------------------------|
| 1/4 cup  | [apple cider vinegar]({{< relref "../items/apple_cider_vinegar.md" >}}) |
| 1/2 cup  | [olive oil]({{< relref "../items/olive_oil.md" >}})                     |
| 2 Tbsp   | [apricot preserves]({{< relref "../items/apricot_preserve.md" >}})      |
| 2 cloves | [garlic]({{< relref "../items/garlic.md" >}}), minced                   |
| 1/4 tsp  | [table salt]({{< relref "../items/table_salt.md" >}})                   |
| 1/4 tsp  | [black pepper]({{< relref "../items/black_pepper.md" >}})               |
| 1 pinch  | [red pepper flakes]({{< relref "red_pepper_flake.md" >}})               |


## Directions {#directions}

1.  Preheat the oven to 200 C (425 F) and line a baking sheet with parchment paper.
2.  Place the cubed squash on the sheet and toss with 1 Tbsp of olive oil.
    -   Season with salt, pepper, garlic powder, cumin, and paprika.
    -   Toss well, then roast for 20 - 25 min.
3.  While the squash is roasting, chop the kale leaves.
    -   Place the leaves in a large bowl and drizzle 1 - 2 tsp of olive oil.
    -   Use your hands to massage the kale for a few minutes and let set for 5 min.&nbsp;[^fn:1]
4.  Add a pinch of salt and pepper to the kale and toss.
    -   Add the crumbled goat cheese and roasted pepitas/pumpkin seeds.
5.  In a bowl or jar, whisk the vinegar, garlic, salt, pepper, and pepper flakes. Stream in the olive oil while whisking until emulsified.
6.  Once the squash has finished roasting, combine everything in a large salad bowl and toss.
    -   Drizzle with salad dressing when ready to serve.


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [How Sweet Eats](https://www.howsweeteats.com/2020/02/butternut-squash-kale-salad/)
2.  Vinaigrette will last sealed in the fridge for 3 - 4 days.

[^fn:1]: Seriously, massage the kale.
    It really helps soften the texture, especially since you're not roasting or frying it.