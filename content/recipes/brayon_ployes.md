+++
title = "Brayon ployes"
author = ["James Hawley"]
date = 2023-04-04T22:27:00-04:00
lastmod = 2024-06-12T19:12:45-04:00
tags = ["breakfasts", "vegetarian", "recipes"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 10 min     |
| Cook Time | 10 min     |
| Yields    | 6 servings |


## Ingredients {#ingredients}

| Quantity | Item                                                                                |
|----------|-------------------------------------------------------------------------------------|
| 1 Tbsp   | [baking powder]({{< relref "../items/baking_powder.md" >}})                         |
| 1 tsp    | [baking soda]({{< relref "../items/baking_soda.md" >}})                             |
| 2 cup    | [water]({{< relref "../items/water.md" >}}) (1 cup boiling, 1 cup room temperature) |
| 3/2 cup  | [buckwheat flour]({{< relref "../items/buckwheat_flour.md" >}})                     |
| 1 cup    | [all-purpose flour]({{< relref "../items/flour.md" >}})                             |
| 1 tsp    | [fine sea salt]({{< relref "../items/sea_salt.md" >}})                              |
|          | [butter]({{< relref "../items/butter.md" >}})                                       |


## Directions {#directions}

1.  In a small bowl, combine the baking powder, baking soda, and boiling water, and let sit for 5 min
2.  In a large mixing bowl, mix together both flours and salt
    1.  Pour in the water mixture and slowly add the room temparature water to form a batter
    2.  Let sit for 3 min
3.  Preheat the oven to 165 C (325 F), then turn it off once the temperature has been reached
4.  Place a heavy-bottomed skillet or griddle over medium-high heat and add butter to lightly grease it
    1.  When the butter is bubbling, ladle 1/3 cup of batter into a circle
    2.  Let cook until the top is bubbling, firm to the touch (1 - 2 min)
    3.  Remove the ploye from the pan, then place on a dish or baking sheet in the oven to keep warm
    4.  Repeat until all the batter is used
5.  Serve with molasses, maple syrup, and/or fruit


## References {#references}

1.  Original recipe: [A Rising Tide]({{< relref "../references/A_Rising_Tide.md" >}})
2.  Best enjoyed day-of, but can laste in an airtight container in the fridge for up to 3 days
