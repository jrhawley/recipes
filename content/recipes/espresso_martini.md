+++
title = "Espresso martini"
author = ["James Hawley"]
date = 2022-12-31T17:15:00-05:00
lastmod = 2023-12-05T22:26:22-05:00
tags = ["coffee", "alcohols", "recipes", "beverages"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 2 min      |
| Cook Time | 0 min      |
| Yields    | 1 cocktail |


## Ingredients {#ingredients}

| Quantity | Item                                          |
|----------|-----------------------------------------------|
| 1 oz     | [Kahlua]({{< relref "../items/kahlua.md" >}}) |
| 1 oz     | [vodka]({{< relref "../items/vodka.md" >}})   |
| 1 oz     | espresso                                      |


## Directions {#directions}

1.  Pour all ingredients into a cocktail shaker with ice.
2.  Shake thoroughly and strain into a coupe glass.
3.  Garnish with a 1 - 3 whole coffee beans floating on the foam.


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [Kahlua](https://www.kahlua.com/en-us/drinks/espresso-martini/)
