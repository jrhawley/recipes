+++
title = "Black Manhattan"
author = ["James Hawley"]
date = 2021-09-25T00:00:00-04:00
lastmod = 2023-12-19T22:06:35-05:00
tags = ["recipes", "alcohols", "beverages"]
draft = false
+++

## Ingredients {#ingredients}

| Quantity | Item                                                                     |
|----------|--------------------------------------------------------------------------|
| 2 oz     | [rye whiskey]({{< relref "../items/whiskey.md" >}})                      |
| 1 oz     | [amaro]({{< relref "../items/lucano_amaro.md" >}})                       |
| 5 dashes | [Angostura bitters]({{< relref "../items/angostura_bitters.md" >}})      |
| 1/8 tsp  | [cinnamon]({{< relref "../items/cinnamon.md" >}})                        |
|          | [orange peel]({{< relref "../items/orange.md" >}}) (optional)            |
| 1        | [cinnamon stick]({{< relref "../items/cinnamon_stick.md" >}}) (optional) |


## Directions {#directions}

1.  Pour whiskey, amaro, bitters, and ground cinnamon into a mixing glass partially filled with ice.
    -   Stir until chilled.
2.  Pull orange peel over the glass, spritzing its oils.
3.  Rub peel around the outside rim, then garnish the drink with the peel and the cinnamon stick.


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [LCBO](https://www.lcbo.com/webapp/wcs/stores/servlet/en/lcbo/recipe/cinnamon-scented-black-manhattan/F202105042)
