+++
title = "Lox and pickled radish with Montreal-style bagels"
author = ["James Hawley"]
date = 2023-03-12T10:05:00-04:00
lastmod = 2023-12-28T01:31:41-05:00
tags = ["bagels", "lunch", "recipes"]
draft = false
+++

| Info      | Amount         |
|-----------|----------------|
| Prep Time | 5 min          |
| Cook Time | 0 min          |
| Yields    | 2 - 4 servings |


## Ingredients {#ingredients}

| Quantity | Item                                                                                                                     |
|----------|--------------------------------------------------------------------------------------------------------------------------|
| 1/2 cup  | [creme fraiche]({{< relref "../items/creme_fraiche.md" >}}) or [cream cheese]({{< relref "../items/cream_cheese.md" >}}) |
| 1 tsp    | [ground sumac]({{< relref "../items/sumac.md" >}})                                                                       |
| 1 tsp    | [white wine vinegar]({{< relref "../items/white_wine_vinegar.md" >}})                                                    |
| 2        | Montreal-style [bagels]({{< relref "../items/bagel.md" >}})                                                              |
| 1/4 cup  | quick pickled [radishes]({{< relref "../items/radish.md" >}})                                                            |
| 225 g    | [smoked salmon]({{< relref "../items/smoked_salmon.md" >}})                                                              |
| 2 Tbsp   | [pickled onions]({{< relref "../items/pickled_onion.md" >}})                                                             |
| 3/2 Tbsp | [capers]({{< relref "../items/caper.md" >}})                                                                             |
|          | [olive oil]({{< relref "../items/olive_oil.md" >}})                                                                      |
|          | fresh [dill]({{< relref "../items/dill.md" >}})                                                                          |
|          | [black pepper]({{< relref "../items/black_pepper.md" >}})                                                                |


## Directions {#directions}

1.  In a small mixing bowl, whisk the crème fraîche, sumac, vinegar, and salt together until well combined
    1.  Taste and season with more salt, if needed
    2.  Set aside for spreading and reserve any extra in a sealed container for up to 3 days
2.  Spread the sumac crème fraîche evenly across each bagel slice
    1.  Place four to six slices of pickled radish on each bagel and layer on the smoked salmon
    2.  Top with pickled onions and capers
    3.  Drizzle the bagels with olive oil
    4.  Sprinkle with dill and season with pepper to serve


## References {#references}

1.  Original recipe: [Peak Season]({{< relref "../references/Peak_Season.md" >}})
