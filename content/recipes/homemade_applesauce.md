+++
title = "Homemade applesauce"
author = ["James Hawley"]
date = 2024-02-10T15:50:00-05:00
lastmod = 2024-02-18T22:26:33-05:00
tags = ["sauces", "desserts", "recipes"]
draft = false
+++

| Info      | Amount |
|-----------|--------|
| Prep Time | 5 min  |
| Cook Time | 25 min |
| Yields    | 5 cups |


## Equipment {#equipment}

-   large soup pot with tight-fitting lid
-   immersion blender
-   soup spoon
-   large cutting board
-   prep knife
-   measuring cups
-   measuring spoons


## Ingredients {#ingredients}

| Quantity | Item                                                     |
|----------|----------------------------------------------------------|
| 2 kg     | assorted [apples]({{< relref "../items/apple.md" >}})    |
| 1/4 cup  | [sugar]({{< relref "../items/sugar.md" >}})              |
| 1 Tbsp   | [lemon juice]({{< relref "../items/lemon_juice.md" >}})  |
| 1/4 tsp  | [ground cinnamon]({{< relref "../items/cinnamon.md" >}}) |
|          | [Kosher salt]({{< relref "../items/kitchen_salt.md" >}}) |


## Directions {#directions}

1.  Peel, core, and chop the apples.
2.  Combine apples, sugar, and 1/2 tsp of salt to a boil in the pot over medium-high heat.
    -   Reduce heat to medium-low, cover with the lid, and simmer for 18 - 20 min, until apples are soft.
3.  Stir in the lemon juice and cinnamon, then let cool for 5 min.
    -   With the immersion blender, puree to desired viscosity.
4.  Let cool, then transfer to a bowl and refrigerate.


## References {#references}

1.  Original recipe: [The Food Network](https://www.foodnetwork.com/recipes/food-network-kitchen/the-best-homemade-applesauce-8613215)
2.  Store leftovers in an airtight container in the fridge for up to 5 days.
