+++
title = "Taco bowl with lime yogurt dressing"
author = ["James Hawley"]
date = 2024-01-31T22:09:00-05:00
lastmod = 2024-02-18T22:26:21-05:00
tags = ["poultry", "entrees", "recipes"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 10 min     |
| Cook Time | 10 min     |
| Yields    | 4 servings |


## Equipment {#equipment}

-   1 large sauce pan
-   1 wooden spoon
-   1 large cutting board
-   1 chef's knife
-   1 large salad bowl
-   salad tongs


## Ingredients {#ingredients}


### Salad {#salad}

| Quantity | Item                                                                                                                   |
|----------|------------------------------------------------------------------------------------------------------------------------|
| 500 g    | [ground turkey]({{< relref "../items/ground_turkey.md" >}}) or [ground beef]({{< relref "../items/ground_beef.md" >}}) |
|          | [olive oil]({{< relref "../items/olive_oil.md" >}})                                                                    |
|          | [paprika]({{< relref "../items/paprika.md" >}})                                                                        |
|          | [cumin]({{< relref "../items/cumin.md" >}})                                                                            |
|          | [salt]({{< relref "../items/salt.md" >}})                                                                              |
|          | [black pepper]({{< relref "../items/black_pepper.md" >}})                                                              |
| 1        | [avocado]({{< relref "../items/avocado.md" >}}), diced                                                                 |
| 1        | [mango]({{< relref "../items/mango.md" >}}), diced                                                                     |
| 6 cups   | [spring greens]({{< relref "../items/spring_greens.md" >}})                                                            |


### Dressing {#dressing}

| Quantity | Item                                                                    |
|----------|-------------------------------------------------------------------------|
| 1/2 cup  | [Greek yogurt]({{< relref "../items/greek_yogurt.md" >}})               |
| 1 Tbsp   | [lime juice]({{< relref "../items/lime_juice.md" >}})                   |
| 1 Tbsp   | [apple cider vinegar]({{< relref "../items/apple_cider_vinegar.md" >}}) |
| 1 tsp    | [honey]({{< relref "../items/honey.md" >}})                             |
|          | [salt]({{< relref "../items/salt.md" >}})                               |
|          | [black pepper]({{< relref "../items/black_pepper.md" >}})               |


## Directions {#directions}

1.  Heat the olive oil in the sauce pan on high heat, then add the ground meat and cook and add seasonings to taste.
2.  While the meat is cooking, wash the spring greens and add to the salad bowl.
    1.  Dice the avocado and mango and add to the bowl.
    2.  When the meat finishes cooking, let rest off heat to cool slightly, before adding to the bowl&nbsp;[^fn:1].
    3.  Toss with salad tongs and season to taste.
3.  Mix all dressing ingredients in a small mason jar, then serve the salad and drizzle the dressing, to taste.


## References &amp; Notes {#references-and-notes}

1.  Original recipeL [Half-Baked Harvest](https://www.halfbakedharvest.com/wprm_print/73163)
2.  Can be easily made into a proper taco or burrito.

[^fn:1]: Ground turkey is much less fatty than ground beef, leaving the salad a bit dry.
    To counteract this, add 1/2 cup of water to the pan while the turkey is cooking, add more olive oil to the meat, or top with extra dressing.