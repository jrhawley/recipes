+++
title = "Chili"
author = ["James Hawley"]
date = 2018-09-12T00:00:00-04:00
lastmod = 2024-05-23T21:41:17-04:00
tags = ["soups", "recipes", "entrees"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 20 min     |
| Cook Time | 2 h        |
| Yields    | 5 servings |


## Ingredients {#ingredients}

| Quantity | Item                                                                          |
|----------|-------------------------------------------------------------------------------|
| 2 lbs    | [ground beef]({{< relref "../items/ground_beef.md" >}})                       |
| 1 Tbsp   | [olive oil]({{< relref "../items/olive_oil.md" >}})                           |
| 3        | [onions]({{< relref "../items/onion.md" >}}), diced                           |
| 3 stalks | [celery]({{< relref "../items/celery.md" >}}), diced                          |
| 1        | green [bell pepper]({{< relref "../items/bell_pepper.md" >}})                 |
| 2 cloves | [garlic]({{< relref "../items/garlic.md" >}}), minced                         |
| 19 oz    | [kidney beans]({{< relref "../items/red_kidney_bean.md" >}}), rinsed, drained |
| 20 oz    | [tomato soup]({{< relref "../items/tomato_soup.md" >}}), undiluted            |
| 28 oz    | [diced tomatoes]({{< relref "../items/diced_tomatoes.md" >}})                 |
| 1 Tbsp   | [chili powder]({{< relref "../items/chili_powder.md" >}}), divided            |
| 1 tsp    | [oregano]({{< relref "../items/oregano.md" >}})                               |
| 1 tsp    | [table salt]({{< relref "../items/table_salt.md" >}})                         |
| 1/2 tsp  | [pepper]({{< relref "../items/black_pepper.md" >}})                           |
| 1 tsp    | [white sugar]({{< relref "../items/white_sugar.md" >}})                       |


## Directions {#directions}

1.  Brown the ground beef in the olive oil in a large frying pan over medium heat until no longer pink
    1.  Place in a large pot or slow cooker
    2.  Do not drain
2.  Saute onion, celery and green pepper in the drippings, in the same frying pan used for the ground beef, for about 5 min until onion is translucent.
    1.  Add a little of the chili powder.
    2.  Add sauteed veggies to the ground beef.
    3.  Add kidney beans, tomato soup and tomatoes with the liquid.
    4.  Add the rest of the chili powder, oregano, salt, pepper, sugar, and garlic.
    5.  Stir ingredients together well.
3.  Cover and simmer for 2 h, stirring occasionally.
    1.  Remove cover and simmer for 30 minutes or more, stirring occasionally.


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [Genius Kitchen](https://www.geniuskitchen.com/recipe/tim-hortons-chili-446951)
2.  Could probably add starch or flour to thicken the chili
3.  Serve with ciabatta bun
