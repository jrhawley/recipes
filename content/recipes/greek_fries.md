+++
title = "Greek fries"
author = ["James Hawley"]
date = 2021-02-26T00:00:00-05:00
lastmod = 2023-12-28T01:34:02-05:00
tags = ["sides", "vegetarian", "recipes"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 10 min     |
| Cook Time | 40 min     |
| Yields    | 3 servings |

{{< figure src="../assets/greek-fries.jpg" caption="<span class=\"figure-number\">Figure 1: </span>Greek fries" >}}


## Ingredients {#ingredients}

| Quantity  | Item                                                                |
|-----------|---------------------------------------------------------------------|
| 6 Tbsp    | [olive oil]({{< relref "../items/olive_oil.md" >}}), divided        |
| 3         | large [Russet potatoes]({{< relref "../items/russet_potato.md" >}}) |
| 3 cloves  | [garlic]({{< relref "../items/garlic.md" >}}), minced               |
|           | [kosher salt]({{< relref "../items/kitchen_salt.md" >}})            |
|           | [pepper]({{< relref "../items/black_pepper.md" >}})                 |
|           | [thyme]({{< relref "../items/thyme.md" >}})                         |
|           | [oregano]({{< relref "../items/oregano.md" >}})                     |
|           | [cayenne pepper]({{< relref "../items/cayenne_pepper.md" >}})       |
|           | [feta]({{< relref "../items/feta.md" >}}), crumbled                 |
| 1 handful | [parsley]({{< relref "../items/parsley.md" >}})                     |
|           | [Tzatziki]({{< relref "../items/tzatziki.md" >}})                   |


## Directions {#directions}

1.  Preheat oven to 400 F
2.  Slice potatoes into 6 - 8 wedges each, then place into a large bowl of hot water for 10 min
    1.  Drain and spread wedges on a large clean towel
    2.  Pat dry
3.  Transfer wedges to a large mixing bowl
    1.  Season generously with salt and pepper
    2.  Drizzle with half the olive oil
    3.  Toss to combine
4.  Spread wedges on a lightly oiled bakign sheet in a single layer
    1.  Bake in oven for 20 min
    2.  Flip wedges, then return for another 15 - 25 min
    3.  Transfer to serving platter
5.  In a small bowl, combine remaining olive oil, garlic, thyme, oregano, and cayenne
    1.  Drizzle oil mixture over the wedges
    2.  Crumble feta and sprinkle fresh parsley over top
    3.  Serve with a side of tzatziki sauce


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [The Mediterranean Dish](https://www.themediterraneandish.com/greek-style-oven-fries/)
2.  Do not skip soaking the potato wedges in hot water, this helps get rid of the starch so that they potatoes don't stick to the pan.
3.  Leftovers can be stored in the fridge in a container for 3 - 4 days.
