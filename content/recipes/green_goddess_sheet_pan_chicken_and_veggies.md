+++
title = "Green goddess sheet pan chicken and veggies"
author = ["James Hawley"]
date = 2023-03-31T18:08:00-04:00
lastmod = 2024-05-23T20:46:39-04:00
tags = ["chicken", "entrees", "recipes"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 20 min     |
| Cook Time | 45 min     |
| Yields    | 4 servings |


## Ingredients {#ingredients}


### Chicken {#chicken}

| Quantity | Item                                                                       |
|----------|----------------------------------------------------------------------------|
| 15 oz    | [chickpeas]({{< relref "../items/chickpea.md" >}})                         |
| 227 g    | [cherry tomatoes]({{< relref "../items/cherry_tomato.md" >}})              |
| 1/4      | [red onion]({{< relref "../items/red_onion.md" >}}), thinly sliced         |
| 3 cup    | [Yukon gold potato]({{< relref "../items/yukon_gold_potato.md" >}}), cubed |
| 3        | [chicken breasts]({{< relref "../items/chicken_breast.md" >}}), sliced     |
| 1/4 cup  | [olive oil]({{< relref "../items/olive_oil.md" >}}), split                 |
| 1/2      | [lemon]({{< relref "../items/lemon.md" >}}), juiced, split                 |
|          | [sea salt]({{< relref "../items/sea_salt.md" >}})                          |
|          | [black pepper]({{< relref "../items/black_pepper.md" >}})                  |
|          | [paprika]({{< relref "../items/paprika.md" >}})                            |


### Green goddess sauce {#green-goddess-sauce}

| Quantity | Item                                                        |
|----------|-------------------------------------------------------------|
| 2/3 cup  | [Greek yogurt]({{< relref "../items/greek_yogurt.md" >}})   |
| 1/2      | [lemon]({{< relref "../items/lemon.md" >}}), juiced         |
| 1 Tbsp   | [Dijon mustard]({{< relref "../items/dijon_mustard.md" >}}) |
| 1/2 Tbsp | [honey]({{< relref "../items/honey.md" >}})                 |
| 1/4 cup  | [parsley]({{< relref "../items/parsley.md" >}})             |
| 1 clove  | [garlic]({{< relref "../items/garlic.md" >}})               |
|          | [sea salt]({{< relref "../items/sea_salt.md" >}})           |
|          | [black pepper]({{< relref "../items/black_pepper.md" >}})   |


## Directions {#directions}

1.  Preheat the oven to 220 C (425 F) and line a large baking sheet with parchment paper.
2.  Add the chickpeas, tomatoes, onion, and potatoes to the tray then toss with half of the lemon juice, olive oil, sea salt, black pepper, and paprika
    1.  Add the sliced chicken to the tray, add the remaining half of the chicken ingredients, and toss all to combine
    2.  Transfer to oven and roast for 25 min
    3.  Gently toss everything then roast for another 20 - 25 min
3.  While the tray is baking in the oven, prepare the green goddess sauce by adding sauce ingredients to a blender and blending until smooth
4.  When chicken and veggies are done roasting, remove from oven, sprinkle with feta and serve with the sauce


## References {#references}

1.  Original recipe: [Gathered Nutrition](https://gatherednutrition.com/green-goddess-sheet-pan-chicken-and-veggies/print/5943/)
2.  The Green Goddess sauce can be kept in the fridge in an airtight container for up to 4 d
