+++
title = "Slow cooker pulled chicken with peanut sauce"
author = ["James Hawley"]
date = 2024-04-20T13:06:00-04:00
lastmod = 2024-04-20T13:20:21-04:00
tags = ["chicken", "entrees", "slow-cooker", "recipes"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 10 min     |
| Cook Time | 3 h        |
| Yields    | 2 servings |


## Equipment {#equipment}

-   1 cutting board
-   1 chef's knife
-   1 slow cooker
-   1 small mixing bowl
-   1 garlic press


## Ingredients {#ingredients}

| Quantity | Item                                                                     |
|----------|--------------------------------------------------------------------------|
| 1/3 cup  | [peanut butter]({{< relref "../items/peanut_butter.md" >}})              |
| 1 clove  | [garlic]({{< relref "../items/garlic.md" >}}), minced                    |
| 1 Tbsp   | [maple syrup]({{< relref "../items/maple_syrup.md" >}})                  |
| 4 Tbsp   | [soy sauce]({{< relref "../items/soy_sauce.md" >}})                      |
| 4 Tbsp   | [lime juice]({{< relref "../items/lime_juice.md" >}})                    |
| 1/3 cup  | [water]({{< relref "../items/water.md" >}})                              |
| 2        | [chicken breasts]({{< relref "../items/chicken_breast.md" >}})           |
| 2 cup    | [broccoli]({{< relref "../items/broccoli.md" >}}) florets                |
| 2        | [bell peppers]({{< relref "../items/bell_pepper.md" >}}), largely sliced |
| 2 stalks | [green onion]({{< relref "../items/green_onion.md" >}})                  |


## Directions {#directions}

1.  In a medium bowl, whisk all the sauce ingredients together.
2.  Add the chicken breasts with 1/2 cup of the peanut sauce to the slow cooker, spreading out to coat the chicken breasts and base of the slow cooker.
3.  Cover and cook on high for 3 - 4 h, or low for 5 h, until the chicken easily shreds with a fork, without burning the sauce.
4.  Add the broccoli and peppers for the last 30 min.
5.  Shred or chop the chicken and mix everything together, then serve.


## References {#references}

1.  Original recipe: [Real Food Whole Life](https://realfoodwholelife.com/wprm_print/15494)
