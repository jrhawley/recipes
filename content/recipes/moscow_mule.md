+++
title = "Moscow mule"
author = ["James Hawley"]
date = 2020-05-18T00:00:00-04:00
lastmod = 2023-12-21T22:15:54-05:00
tags = ["recipes", "alcohols", "beverages"]
draft = false
+++

## Ingredients {#ingredients}

| Quantity | Item                                                               |
|----------|--------------------------------------------------------------------|
| 1.5 oz   | [vodka]({{< relref "../items/vodka.md" >}})                        |
| 4.5 oz   | [ginger beer]({{< relref "../items/fever_tree_ginger_beer.md" >}}) |
| 1 oz     | [lime juice]({{< relref "../items/lime_juice.md" >}})              |
|          | ice cubes                                                          |


## Directions {#directions}

1.  Fill copper mug with ice cubes
2.  Mix remaining ingredients in mug and stir


## References &amp; Notes {#references-and-notes}

1.  Ginger beer can be substituted with ginger ale, although it is not as sharp
