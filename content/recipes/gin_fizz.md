+++
title = "Gin fizz"
author = ["James Hawley"]
date = 2020-12-19T00:00:00-05:00
lastmod = 2023-12-21T21:52:43-05:00
tags = ["recipes", "alcohols", "beverages"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 2 min      |
| Yields    | 1 cocktail |


## Ingredients {#ingredients}

| Quantity | Item                                                  |
|----------|-------------------------------------------------------|
| 1.5 oz   | [gin]({{< relref "../items/gin.md" >}})               |
| 1 oz     | [simple syrup](./simple-syrup.md)                     |
| 1 oz     | [lime juice]({{< relref "../items/lime_juice.md" >}}) |
|          | ice cubes                                             |
| 4 oz     | [club soda]({{< relref "../items/club_soda.md" >}})   |


## Directions {#directions}

1.  Place all ingredients but the club soda into a cocktail shaker.
2.  Cover and shake vigorously
3.  Strain into highball glasses, then add in club soda
