+++
title = "Chia seed pancakes"
author = ["James Hawley"]
date = 2024-01-27T11:48:00-05:00
lastmod = 2024-06-12T19:13:16-04:00
tags = ["breakfasts", "recipes"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 5 min      |
| Cook Time | 15 min     |
| Yields    | 6 pancakes |


## Equipment {#equipment}

-   1 large mixing bowl
-   1 spatula
-   1 large saucepan or griddle
-   1 whisk
-   measuring cups and spoons


## Ingredients {#ingredients}

| Quantity | Item                                                                |
|----------|---------------------------------------------------------------------|
| 1 cup    | [whole wheat flour]({{< relref "../items/whole_wheat_flour.md" >}}) |
| 1 tsp    | [baking soda]({{< relref "../items/baking_soda.md" >}})             |
| 1 tsp    | [salt]({{< relref "../items/salt.md" >}})                           |
| 2        | [eggs]({{< relref "../items/eggs.md" >}})                           |
| 3 Tbsp   | [Greek yogurt]({{< relref "../items/greek_yogurt.md" >}})           |
| 3/4 cup  | [almond milk]({{< relref "../items/almond_milk.md" >}})             |
| 2 Tbsp   | [chia seeds]({{< relref "../items/chia_seed.md" >}})                |


## Directions {#directions}

1.  Whisk eggs, yogurt, and milk together; then add flour, baking soda and chia seeds and whisk together
2.  Heat a nonstick griddle or skillet over medium-low and scoop batter out onto it.
3.  Let cook for 1 - 2 min, then flip.
4.  Repeat with remaining batter.


## References {#references}

1.  Original recipe: [The Almond Eater](https://thealmondeater.com/wprm_print/16886)
