+++
title = "Lentil soup"
author = ["James Hawley"]
date = 2021-02-27T00:00:00-05:00
lastmod = 2024-05-23T21:21:11-04:00
tags = ["soups", "recipes", "entrees", "slow-cooker"]
draft = false
+++

| Info      | Amount |
|-----------|--------|
| Prep Time |        |
| Cook Time |        |
| Yields    |        |


## Ingredients {#ingredients}

| Quantity | Item                                                                       |
|----------|----------------------------------------------------------------------------|
| 4 cup    | [vegetable broth]({{< relref "../items/vegetable_broth.md" >}})            |
| 14 oz    | [diced tomatoes]({{< relref "../items/diced_tomatoes.md" >}}), with juices |
| 1        | [onion]({{< relref "../items/onion.md" >}})                                |
| 1        | [carrot]({{< relref "../items/baby_carrot.md" >}}), diced                  |
| 1 stalk  | [celery]({{< relref "../items/celery.md" >}})                              |
| 1 cup    | [lentils]({{< relref "../items/lentil.md" >}})                             |
| 1 Tbsp   | [olive oil]({{< relref "../items/olive_oil.md" >}})                        |
| 2 cloves | [garlic]({{< relref "../items/garlic.md" >}}), minced                      |
| 1 tsp    | [table salt]({{< relref "../items/table_salt.md" >}})                      |
| 1 tsp    | [tomato paste]({{< relref "../items/tomato_paste.md" >}})                  |
| 1        | [bay leaf]({{< relref "../items/bay_leaf.md" >}})                          |
| 1/2 tsp  | [cumin]({{< relref "../items/cumin.md" >}})                                |
| 1/2 tsp  | [cilantro]({{< relref "../items/parsley.md" >}})                           |
| 1/4 tsp  | [paprika]({{< relref "../items/paprika.md" >}})                            |
| 2 tsp    | [red-wine-vinegar]({{< relref "../items/red_wine_vinegar.md" >}})          |
|          | [Greek yogurt]({{< relref "../items/greek_yogurt.md" >}})                  |
|          | [parsley]({{< relref "../items/parsley.md" >}})                            |


## Directions {#directions}

1.  Place all the ingredients except the vinegar into slow cooker and stir to combine
2.  Cover and cook on the LOW setting until the lentils are tender, about 8 h
3.  Remove the bay leaf and stir in the red wine vinegar
4.  Ladle into bowls and garnish with a dollop of yogurt, a drizzle of olive oil, and chopped fresh parsley


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [The Kitchn](https://www.thekitchn.com/recipe-slow-cooker-easy-lentil-soup-253472)
2.  Leftovers last in the fridge for up to 1 week, or 3 months in the freezer
