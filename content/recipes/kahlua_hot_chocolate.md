+++
title = "Kahlua Hot Chocolate"
author = ["James Hawley"]
date = 2021-04-17T00:00:00-04:00
lastmod = 2023-12-22T00:41:02-05:00
tags = ["recipes", "alcohols", "beverages"]
draft = false
+++

## Ingredients {#ingredients}

| Quantity | Item                                                      |
|----------|-----------------------------------------------------------|
| 2 cups   | [milk]({{< relref "../items/milk.md" >}})                 |
| 1 Tbsp   | [white sugar]({{< relref "../items/white_sugar.md" >}})   |
| 3/2 Tbsp | [cocoa powder]({{< relref "../items/cocoa_powder.md" >}}) |
| 1/4 tsp  | [cinnamon]({{< relref "../items/cinnamon.md" >}})         |
| pinch    | [nutmeg]({{< relref "../items/nutmeg.md" >}})             |
| 1 oz     | [Kahlua]({{< relref "../items/kahlua.md" >}})             |


## Directions {#directions}

1.  Combine all ingredients except the Kahlua in a small pot
2.  Heat on medium heat and stir
3.  Top with marshmallows, whipped cream, or a caramel syrup


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [Damn Delicious](https://damndelicious.net/2013/12/03/kahlua-hot-chocolate/)
2.  Don't let the milk boil or else it will spoil
