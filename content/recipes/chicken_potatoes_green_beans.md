+++
title = "Chicken, potatoes, green beans"
author = ["James Hawley"]
date = 2019-03-23T00:00:00-04:00
lastmod = 2024-05-23T21:40:14-04:00
tags = ["chicken", "recipes", "slow-cooker", "entrees"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 15 min     |
| Cook Time | 4 h        |
| Yields    | 4 servings |


## Ingredients {#ingredients}

| Quantity | Item                                                                |
|----------|---------------------------------------------------------------------|
| 2 lbs    | [chicken breasts]({{< relref "../items/chicken_breast.md" >}})      |
| 1/2 lbs  | [green beans]({{< relref "../items/green_bean.md" >}})              |
| 3/2 lbs  | red [potatoes]({{< relref "../items/russet_potato.md" >}}), chunked |
| 1/3 cup  | [lemon juice]({{< relref "../items/lemon_juice.md" >}})             |
| 1/4 cup  | [olive oil]({{< relref "../items/olive_oil.md" >}})                 |
| 1 tsp    | [oregano]({{< relref "../items/oregano.md" >}})                     |
| 1/4 tsp  | [onion powder]({{< relref "../items/onion_powder.md" >}})           |
| 2 cloves | [garlic]({{< relref "../items/garlic.md" >}}), minced               |
|          | [kosher salt]({{< relref "../items/kitchen_salt.md" >}})            |
|          | [pepper]({{< relref "../items/black_pepper.md" >}})                 |


## Directions {#directions}

1.  Place chicken, green beans, and potatoes into the slow cooker
2.  In a separate bowl, whisk together remaining ingredients
    1.  Pour evenly over slow cooker contents
3.  Cover, cook on high for 4 h
