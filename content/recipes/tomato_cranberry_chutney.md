+++
title = "Tomato cranberry chutney"
author = ["James Hawley"]
date = 2023-03-12T09:59:00-04:00
lastmod = 2023-12-24T20:02:49-05:00
tags = ["sauces", "recipes"]
draft = false
+++

| Info      | Amount |
|-----------|--------|
| Prep Time | 5 min  |
| Cook Time | 35 min |
| Yields    | 180 mL |


## Ingredients {#ingredients}

| Quantity | Item                                                                              |
|----------|-----------------------------------------------------------------------------------|
| 2 Tbsp   | [olive oil]({{< relref "../items/olive_oil.md" >}})                               |
| 1        | [yellow onion]({{< relref "../items/sweet_onion.md" >}}), roughly chopped         |
| 2 cloves | [garlic]({{< relref "../items/garlic.md" >}}), minced                             |
| 1/2 tsp  | [Kosher salt]({{< relref "../items/kitchen_salt.md" >}})                          |
| 1        | large [field tomato]({{< relref "../items/field_tomato.md" >}}), seeded and diced |
| 1/2 cup  | [fresh cranberries]({{< relref "../items/cranberry.md" >}})                       |
| 2 tbsp   | [honey]({{< relref "../items/honey.md" >}})                                       |


## Directions {#directions}

1.  In a large skillet over medium, heat the oil
    1.  Once the skillet is hot, sauté the onions to a blond caramelization, about 10 min
    2.  Reduce the heat to medium-low and add the garlic and salt
    3.  Stir and cook until fragrant, 2 - 3 min
2.  Add the diced tomatoes and cranberries
    1.  Simmer on low for 20 min, until the tomatoes and cranberries take on a deep red, jammy consistency
    2.  Remove from the heat to cool slightly
3.  Add the tomato-cranberry mixture and honey to a food processor
    1.  Pulse until everything is well incorporated but still slightly chunky
    2.  Taste and season with more salt, if needed


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [Peak Season]({{< relref "../references/Peak_Season.md" >}})
2.  If you are not using your chutney right away, it can be stored in a sealed container in the refrigerator for up to 2 weeks
