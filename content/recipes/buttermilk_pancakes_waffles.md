+++
title = "Buttermilk pancakes and waffles"
author = ["James Hawley"]
date = 2011-09-01T00:00:00-04:00
lastmod = 2024-06-12T19:12:59-04:00
tags = ["recipes", "breakfasts"]
draft = false
+++

| Info      | Amount                        |
|-----------|-------------------------------|
| Prep Time | 15 min                        |
| Cook Time | 15 min                        |
| Yields    | 8 10 cm pancakes or 2 waffles |


## Equipment {#equipment}

-   2 medium mixing bowls
-   1 small microwaveable bowl
-   1 whisk
-   1 waffle iron


## Ingredients {#ingredients}

| Quantity | Item                                                        |
|----------|-------------------------------------------------------------|
| 1 cup    | [flour]({{< relref "../items/flour.md" >}})                 |
| 1 tsp    | [baking powder]({{< relref "../items/baking_powder.md" >}}) |
| 1/2 tsp  | [baking soda]({{< relref "../items/baking_soda.md" >}})     |
| 1/4 tsp  | [table salt]({{< relref "../items/table_salt.md" >}})       |
| 1 cup    | [milk]({{< relref "../items/milk.md" >}})                   |
| 1        | [egg]({{< relref "../items/eggs.md" >}})                    |
| 3/2 Tbsp | [butter]({{< relref "../items/butter.md" >}}), melted       |


## Directions {#directions}

1.  Heat lightly oiled pan or griddle to medium-high
2.  In separate bowls, combine dry ingredients and wet ingredients, respectively
    1.  Add the dry bowl into the wet bowl, stirring slowly
    2.  Whisk together until batter is fully mixed and slightly lumpy
3.  Cook for ~ 3 min, until golden brown
4.  Add fruit, cinnamon, icing sugar, etc as toppings
