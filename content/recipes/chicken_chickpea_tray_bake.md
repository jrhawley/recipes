+++
title = "Chicken and chickpea tray bake"
author = ["James Hawley"]
date = 2017-02-10T00:00:00-05:00
lastmod = 2024-05-23T21:43:35-04:00
tags = ["chicken", "recipes", "entrees"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 10 min     |
| Cook Time | 25 min     |
| Yields    | 4 servings |

{{< figure src="../assets/chicken-chickpea-traybake.jpg" caption="<span class=\"figure-number\">Figure 1: </span>Chicken chickpea tray bake" >}}


## Ingredients {#ingredients}


### Dressing {#dressing}

| Quantity | Item                                                               |
|----------|--------------------------------------------------------------------|
| 2 Tbsp   | [olive oil]({{< relref "../items/olive_oil.md" >}})                |
| 4 cloves | [garlic]({{< relref "../items/garlic.md" >}}), minced              |
| 1 tsp    | [paprika]({{< relref "../items/paprika.md" >}})                    |
| 1 tsp    | [cumin]({{< relref "../items/cumin.md" >}})                        |
| 1 pinch  | [red pepper flakes]({{< relref "../items/red_pepper_flake.md" >}}) |
|          | [kosher salt]({{< relref "../items/kitchen_salt.md" >}})           |
|          | [pepper]({{< relref "../items/black_pepper.md" >}})                |


### Chicken {#chicken}

| Quantity | Item                                                                |
|----------|---------------------------------------------------------------------|
| 6        | [chicken thighs]({{< relref "../items/chicken_broth.md" >}})        |
| 2 cups   | [cherry tomatoes]({{< relref "../items/cherry_tomato.md" >}})       |
| 15 oz    | [chickpeas]({{< relref "../items/chickpea.md" >}}), drained, rinsed |
| 1 cup    | [parsley]({{< relref "../items/parsley.md" >}}), chopped            |
| 1/4 cup  | plain [Greek yogurt]({{< relref "../items/greek_yogurt.md" >}})     |


## Directions {#directions}

1.  Preheat oven to 450 F
2.  Whisk all dressing ingredients in a small bowl
    1.  Take 1 Tbsp of the mixture and stir into the yogurt
    2.  Toss remaining dressing with chicken, chickpeas, and tomatoes
3.  Arrange into a single layer on a baking sheet
4.  Bake for 20-25 min
5.  Serve with a dollop of the spiced yogurt
