+++
title = "Chicken lettuce wraps"
author = ["James Hawley"]
date = 2020-01-15T00:00:00-05:00
lastmod = 2024-05-23T21:37:10-04:00
tags = ["chicken", "recipes", "entrees"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 10 min     |
| Cook Time | 20 min     |
| Yields    | 4 servings |


## Ingredients {#ingredients}


### Sauce {#sauce}

| Quantity | Item                                                        |
|----------|-------------------------------------------------------------|
| 2 Tbsp   | [honey]({{< relref "../items/honey.md" >}})                 |
| 1/4 tsp  | [table salt]({{< relref "../items/table_salt.md" >}})       |
| 2 Tbsp   | [soy sauce]({{< relref "../items/soy_sauce.md" >}})         |
| 1 Tbsp   | [ketchup]({{< relref "../items/ketchup.md" >}})             |
| 1 tsp    | [vinegar]({{< relref "../items/white_vinegar.md" >}})       |
| 1 Tbsp   | [Sriracha sauce]({{< relref "../items/sriracha.md" >}})     |
| 2 cloves | [garlic]({{< relref "../items/garlic.md" >}}), minced       |
| 1 Tbsp   | [Hoisin sauce]({{< relref "../items/hoisin_sauce.md" >}})   |
| 1 Tbsp   | [oyster sauce]({{< relref "../items/oyster_sauce.md" >}})   |
| 1/4 cup  | [chicken broth]({{< relref "../items/chicken_broth.md" >}}) |


### Chicken {#chicken}

| Quantity | Item                                                               |
|----------|--------------------------------------------------------------------|
| 1 cup    | [chicken]({{< relref "../items/chicken_breast.md" >}}), minced     |
| 1        | small [onion]({{< relref "../items/onion.md" >}}), diced           |
| 1 Tbsp   | [ginger]({{< relref "../items/ginger.md" >}}), grated              |
| 4 cloves | [garlic]({{< relref "../items/garlic.md" >}}), minced              |
| 2 Tbsp   | [vegetable oil]({{< relref "../items/vegetable_oil.md" >}})        |
| 1 pinch  | [red pepper flakes]({{< relref "../items/red_pepper_flake.md" >}}) |
| 1 stalk  | [green onion]({{< relref "../items/green_onion.md" >}})            |
|          | [sesame seeds]({{< relref "../items/sesame_seed.md" >}}), garnish  |


## Directions {#directions}

1.  In a bowl add the ingredients for sauce and mix well.
2.  Heat oil in a large pan or wok.
    1.  Saute ginger, garlic, and onion until soft
3.  Add minced chicken and stir to separate the chicken and cook evenly
    1.  Add sauce mix and mix well
    2.  Cook until chicken is cooked well and sauce is mostly dried
4.  Add the sesame seeds, green onion, and red pepper flakes
5.  Arrange the lettuce leaves and place two to three tablespoon of chicken on each leaf and serve
