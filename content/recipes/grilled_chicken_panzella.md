+++
title = "Grilled chicken and panzella"
author = ["James Hawley"]
date = 2021-08-15T00:00:00-04:00
lastmod = 2024-05-23T21:19:42-04:00
tags = ["chicken", "recipes", "entrees"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 10 min     |
| Cook Time | 15 min     |
| Yields    | 4 servings |


## Ingredients {#ingredients}

| Quantity | Item                                                              |
|----------|-------------------------------------------------------------------|
| 4        | [chicken breasts]({{< relref "../items/chicken_breast.md" >}})    |
| 1        | [baguette]({{< relref "../items/baguette.md" >}})                 |
| 2        | [cucumber]({{< relref "../items/cucumber.md" >}})                 |
| 3/2 cup  | [cherry tomatoes]({{< relref "../items/cherry_tomato.md" >}})     |
| 1 Tbsp   | [kalamata olives]({{< relref "../items/kalamata_olive.md" >}})    |
| 1 clove  | [garlic]({{< relref "../items/garlic.md" >}})                     |
| 1/4 cup  | grated [Parmesan]({{< relref "../items/parmesan.md" >}})          |
| 2 Tbsp   | [mayonnaise]({{< relref "../items/mayonnaise.md" >}})             |
| 2 Tbsp   | [red-wine-vinegar]({{< relref "../items/red_wine_vinegar.md" >}}) |
|          | [olive oil]({{< relref "../items/olive_oil.md" >}})               |
|          | [kosher salt]({{< relref "../items/kitchen_salt.md" >}})          |
|          | [pepper]({{< relref "../items/black_pepper.md" >}})               |


## Directions {#directions}

1.  Heat the barbecue
    1.  Drizzle olive oil on the chicken breasts, season with salt, pepper, and Italian seasoning
    2.  Cook for 5-6 min each side
2.  Halve the baguette and drizzle olive oil on both sides
    1.  Toast on the barbecue for the last few minutes that the chicken is cooking
3.  Halve tomatoes and olives, dice cucumbers, and mince garlic into a fine zest
    1.  In a medium mixing bowl, combine with Parmesan, mayo, vinegar, Italian seasoning, salt, and pepper


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [Blue Apron](https://www.blueapron.com/recipes/grilled-chicken-thighs-panzanella-with-parmesan-garlic-dressing)
