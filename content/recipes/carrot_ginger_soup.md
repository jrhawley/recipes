+++
title = "Carrot ginger soup"
author = ["James Hawley"]
date = 2018-03-26T00:00:00-04:00
lastmod = 2024-05-23T21:42:23-04:00
tags = ["soups", "recipes", "vegan", "vegetarian", "entrees"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 15 min     |
| Cook Time | 30 min     |
| Yields    | 4 servings |


## Ingredients {#ingredients}

| Quantity | Item                                                                          |
|----------|-------------------------------------------------------------------------------|
| 3 Tbsp   | unsalted [butter]({{< relref "../items/butter.md" >}})                        |
| 3 lbs    | [carrots]({{< relref "../items/baby_carrot.md" >}}), peeled and thinly sliced |
| 2 cups   | [onion]({{< relref "../items/onion.md" >}}), diced                            |
|          | [kosher salt]({{< relref "../items/kitchen_salt.md" >}})                      |
| 1 tsp    | [ginger]({{< relref "../items/ginger.md" >}}), minced                         |
| 2 cups   | [chicken broth]({{< relref "../items/chicken_broth.md" >}})                   |
| 2 cups   | [water]({{< relref "../items/water.md" >}})                                   |
| 3        | large [orange zest]({{< relref "../items/orange.md" >}}) strips               |
|          | [parsley]({{< relref "../items/parsley.md" >}})                               |


## Directions {#directions}

1.  Melt butter in a soup pot over medium eat
    1.  Saute onions and carrots, stirring occasionally
    2.  Do not let them brown
    3.  Sprinkle tsp of salt as they cook
2.  Add stock, water, ginger, and orange zest
    1.  Bring to a simmer, cover, and cook until carrots are soft (~ 20 min)
3.  Remove orange peel
    1.  They're there for flavouring, but won't be good in the soup
4.  Puree the soup with a stick blender until completely smooth
    1.  Add salt to taste
5.  Garnish with chopped vegetables as desired


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [Simply Recipes](https://www.simplyrecipes.com/recipes/carrot_ginger_soup/)
