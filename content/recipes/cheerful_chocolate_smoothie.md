+++
title = "Cheerful chocolate smoothie"
author = ["James Hawley"]
date = 2023-03-18T13:36:00-04:00
lastmod = 2023-12-19T22:37:08-05:00
tags = ["smoothies", "recipes", "beverages"]
draft = false
+++

| Info      | Amount      |
|-----------|-------------|
| Prep Time | 5 min       |
| Cook Time | 0 min       |
| Yields    | 2 smoothies |


## Ingredients {#ingredients}

| Quantity | Item                                                               |
|----------|--------------------------------------------------------------------|
| 2 cup    | [almond milk]({{< relref "../items/almond_milk.md" >}})            |
| 1/4 cup  | [avocado]({{< relref "../items/avocado.md" >}})                    |
| 1 tsp    | [vanilla extract]({{< relref "../items/vanilla_extract.md" >}})    |
|          | [fine sea salt]({{< relref "../items/sea_salt.md" >}})             |
| 4 - 6    | [Medjool dates]({{< relref "../items/medjool_date.md" >}}), pitted |
| 4 - 6    | ice cubes                                                          |


## Directions {#directions}

1.  Blend all ingredients together in a blender until smooth


## References {#references}

1.  Original recipe: [The Oh She Glows Cookbook]({{< relref "../references/Oh_She_Glows_Cookbook.md" >}})
