+++
title = "Harvard"
author = ["James Hawley"]
date = 2022-11-13T00:19:00-05:00
lastmod = 2023-12-21T22:02:52-05:00
tags = ["alcohols", "recipes", "beverages"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 2 min      |
| Cook Time | 0 min      |
| Yields    | 1 cocktail |


## Ingredients {#ingredients}

| Quantity | Item                                                                |
|----------|---------------------------------------------------------------------|
| 2 oz     | [cognac]({{< relref "../items/cognac.md" >}})                       |
| 2/3 oz   | [sweet vermouth]({{< relref "../items/sweet_vermouth.md" >}})       |
| 2 dashes | [Angostura bitters]({{< relref "../items/angostura_bitters.md" >}}) |
| 3 oz     | [club soda]({{< relref "../items/club_soda.md" >}})                 |


## Directions {#directions}

1.  Fill a mixing glass with ice, pour cognac, vermouth, and bitters until chilled.
2.  Strain into a chilled cocktail glass and top with 3 oz of soda water.
3.  Garnish with an orange peel.


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [The Periodic Table of Cocktails]({{< relref "../references/Periodic_Table_of_Cocktails.md" >}})
