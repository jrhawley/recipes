+++
title = "Irish soda bread"
author = ["James Hawley"]
date = 2023-03-12T08:35:00-04:00
lastmod = 2024-06-12T19:14:47-04:00
tags = ["breakfasts", "breads", "recipes"]
draft = false
+++

| Info      | Amount |
|-----------|--------|
| Prep Time | 5 min  |
| Cook Time | 50 min |
| Yields    | 1 loaf |


## Ingredients {#ingredients}

| Quantity | Item                                                                |
|----------|---------------------------------------------------------------------|
| 2 cup    | [all-purpose flour]({{< relref "../items/all-purpose_flour.md" >}}) |
| 2 cup    | [whole wheat flour]({{< relref "../items/whole_wheat_flour.md" >}}) |
| 1 tsp    | [baking soda]({{< relref "../items/baking_soda.md" >}})             |
| 4 Tbsp   | [butter]({{< relref "../items/butter.md" >}}), unsalted             |
| 2        | [eggs]({{< relref "../items/eggs.md" >}}), room temperature         |
| 3/2 tsp  | [Kosher salt]({{< relref "../items/kitchen_salt.md" >}})            |
| 5/3 cup  | [buttermilk]({{< relref "../items/buttermilk.md" >}})               |


## Directions {#directions}

1.  Preheat the oven to 200ºC (400ºF) and generously grease a 5×9" loaf pan with butter
2.  In a large mixing bowl, mix the all-purpose flour, whole wheat flour, and baking soda using a spatula
    1.  Use your hands or a pastry cutter to cut the butter into the flour
    2.  Mix until the flour feels like coarse crumbs
3.  In a separate large mixing bowl, whisk the eggs and salt together
    1.  Gradually pour in the buttermilk, whisking until blended
    2.  Pour the wet ingredients into the dry ingredients and stir gently to moisten the flour
4.  Gently fold the dough together until it is too stiff to stir and a thick, spongy dough has formed.
    1.  Scrape the batter into the pan and push down to cover all four corners of the pan.
5.  Bake until the bread has a firm golden-brown crust that releases easily from the pan, 45 to 55 min
    1.  Transfer the pan to a wire rack and let it rest for 5 min
    2.  Run a knife around the edges of the bread before turning the bread out onto a wire rack to cool at room temperature
    3.  Serve warm with butter and jam or use to sop up your favourite soups and stews


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [Peak Season]({{< relref "../references/Peak_Season.md" >}})
2.  Leftover bread can be stored at room temperature for up to 2 days
