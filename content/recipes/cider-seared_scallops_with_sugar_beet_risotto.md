+++
title = "Cider-seared scallops with sugar beet risotto"
author = ["James Hawley"]
date = 2023-08-17T01:24:00-04:00
lastmod = 2023-12-28T01:16:26-05:00
tags = ["entrees", "fish", "recipes"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 10 min     |
| Cook Time | 70 min     |
| Yields    | 4 servings |


## Ingredients {#ingredients}

| Quantity | Item                                                                                                                                                                   |
|----------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 4        | small [beets]({{< relref "../items/beet.md" >}})                                                                                                                       |
| 8 cup    | [chicken stock]({{< relref "../items/chicken_stock.md" >}})                                                                                                            |
| 2        | large [shallots]({{< relref "../items/shallot.md" >}})                                                                                                                 |
| 2 cloves | [garlic]({{< relref "../items/garlic.md" >}})                                                                                                                          |
| 1        | small [fennel]({{< relref "../items/fennel.md" >}}) bulb                                                                                                               |
| 2 Tbsp   | [olive oil]({{< relref "../items/olive_oil.md" >}})                                                                                                                    |
| 4 Tbsp   | [butter]({{< relref "../items/butter.md" >}})                                                                                                                          |
| 1 tsp    | [sea salt]({{< relref "../items/sea_salt.md" >}})                                                                                                                      |
| 1 tsp    | [black pepper]({{< relref "../items/black_pepper.md" >}})                                                                                                              |
| 2 cup    | [Arborio rice]({{< relref "../items/arborio_rice.md" >}})                                                                                                              |
| 3/2 cup  | dry [cider]({{< relref "../items/cider.md" >}}) (e.g. [No Boats on Sunday Cranberry Rose Cider]({{< relref "../items/no_boats_on_sunday_cranberry_rose_cider.md" >}})) |
| 16 - 20  | [scallops]({{< relref "../items/scallop.md" >}}) (for a native dish, use fresh [Digby scallops]({{< relref "../items/digby_scallop.md" >}}))                           |
| 1 cup    | [Parmesan]({{< relref "../items/parmesan.md" >}}), grated                                                                                                              |


## Directions {#directions}

1.  If using fresh beets and not pre-cooked beets, preheat the oven to 220 C (400 F).
    1.  Trim the ends of the beets and slice them in half.
    2.  Wrap the beets tightly in aluminium foil and roast until extremely and tender (30 - 45 min).
    3.  Remove the beets from the foil, allowing them to cool to room temperature.
    4.  Peel off the skin and finely grate the beets with a box grater&nbsp;[^fn:1].
2.  In a large pot over medium-low heat, warm the stock&nbsp;[^fn:2].
3.  Finely mince the shallots and garlic.
    -   Dice the fennel, reserving the tops for garnish.
4.  In a large skillet or wok over medium heat, place the oil and 2 Tbsp of butter.
    -   Add the shallots and fennel and saute for 2 min, until transluscent.
    -   Add the rice and allow it to absorb the butter and oil, stirring continuously.
5.  Add garlic and grated beets, stirring to combine.
    -   Slowly add 1 cup of the cider, stirring continuously.
    -   Once the rice has fully absorbed the cider, add 1 ladleful of stock, stirring continuously until the rice has absorbed this liquid, too.
    -   Repeat until there are 2 - 3 ladlefuls of stock left, the rice is tender but still textured, and the beets are almost all dissolved&nbsp;[^fn:3].
    -   Remove from heat and set aside.
6.  In a large pan, pour in the remaining 1/2 cup of cider, bring to a boil and reduce for 2 min.
    -   Turn down the heat to medium-low, and whisk in 1 Tbsp butter.
    -   Continue reducing until a caramelized glaze has formed, being careful not to burn it.
    -   Pour in a small bowl, set aside, and wipe the pan clean.
7.  Thaw scallops, if needed, and pat dry.
    -   Melt the remaining butter and 2 Tbsp of olive oil in the pan.
    -   Add the scallops one-by-one, keeping track of the order in which you added them
    -   Sear for 1 - 2 min per side, spooning the reserved glaze over the top until crisp&nbsp;[^fn:4].
    -   Transfer to a side plate and repeat with all remaining scallops.
8.  Place the risotto back on the stovetop over medium heat.
    -   Pour in one ladleful of warm stock and stir until combined.
9.  After the risotto has loosened, add a half ladle of stock and the Parmesan.
    -   Stir well to combine and cook until the cheese melts.
    -   Remove from heat and transfer to a serving platter.
    -   Top with scallops and reserved fennel tops.
    -   Drizzle with any remaining cider glaze and sprinkle with salt, pepper, and Parmesan as desired.


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [A Rising Tide]({{< relref "../references/A_Rising_Tide.md" >}})
2.  Risotto will keep in the fridge for up to 3 days and can be reheated with some stock.
3.  Scallops are best enjoyed on the day they are made.

[^fn:1]: If you can, use gloves for this step, or else your hands will be stained for days.
    You can also avoid this process entirely (except for the grating) by buying pre-cooked beets, either packaged or in a can.
    This cuts down on lots of time-consuming prep work.
[^fn:2]: This is crucial, as it drastically reduces the amount of time required for the rice to absorb the stock.
[^fn:3]: If the risotto is too goopy, let the liquid boil off and keep stirring.
    If too crunchy, add more stock and keep cooking until the rice becomes tender.
[^fn:4]: Each scallop should be well-browned and crisp on the outside and no longer translucent on the inside when cut.