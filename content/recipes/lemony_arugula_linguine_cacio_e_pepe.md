+++
title = "Lemony arugula linguine cacio e pepe"
author = ["James Hawley"]
date = 2024-01-13T18:28:00-05:00
lastmod = 2024-01-31T22:46:50-05:00
tags = ["vegetarian", "pastas", "entrees", "recipes"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 0 min      |
| Cook Time | 10 min     |
| Yields    | 4 servings |


## Equipment {#equipment}

-   1 large soup pot
-   1 pasta spoon


## Ingredients {#ingredients}

| Quantity | Item                                                               |
|----------|--------------------------------------------------------------------|
|          | [Kosher salt]({{< relref "../items/kitchen_salt.md" >}})           |
| 12 oz    | [linguine]({{< relref "../items/linguine.md" >}})                  |
| 1 tsp    | [red pepper flakes]({{< relref "../items/red_pepper_flake.md" >}}) |
| 2 tsp    | [black pepper]({{< relref "../items/black_pepper.md" >}})          |
| 1/4 cup  | [lemon juice]({{< relref "../items/lemon_juice.md" >}})            |
| 3/2 cup  | [Parmesan]({{< relref "../items/parmesan.md" >}})                  |
| 3 cup    | [arugula]({{< relref "../items/arugula.md" >}})                    |


## Directions {#directions}

1.  Fill pot with water, add salt, and boil
    1.  Cook pasta to al dente, according to directions
    2.  Reserve 1 cup of pasta water before draining
2.  Add lemon juice to drained pasta, parmesan, and toss
    1.  Add pasta water a bit at a time, to help the cheese coat evenly
    2.  Add arugula and toss until it wilts
3.  Season to taste and serve
