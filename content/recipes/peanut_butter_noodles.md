+++
title = "Peanut butter noodles"
author = ["James Hawley"]
date = 2020-05-21T00:00:00-04:00
lastmod = 2024-05-23T21:31:37-04:00
tags = ["pastas", "recipes", "vegetarian", "entrees"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 5 min      |
| Cook Time | 10 min     |
| Yields    | 6 servings |


## Ingredients {#ingredients}

| Quantity | Item                                                               |
|----------|--------------------------------------------------------------------|
| 16 oz    | [brown rice noodles]({{< relref "../items/rice_noodle.md" >}})     |
| 2 cloves | [garlic]({{< relref "../items/garlic.md" >}}), minced              |
| 1 Tbsp   | [olive oil]({{< relref "../items/olive_oil.md" >}})                |
| 2/3 cup  | [peanut butter]({{< relref "../items/peanut_butter.md" >}})        |
| 3 Tbsp   | [soy sauce]({{< relref "../items/soy_sauce.md" >}})                |
| 1/4 cup  | [vegetable broth]({{< relref "../items/vegetable_broth.md" >}})    |
| 1 Tbsp   | [rice vinegar]({{< relref "../items/white_vinegar.md" >}})         |
| 1 Tbsp   | [maple syrup]({{< relref "../items/maple_syrup.md" >}})            |
| 1/4 cup  | [peanuts]({{< relref "../items/peanut.md" >}}), crushed            |
| 1/2 tsp  | [red pepper flakes]({{< relref "../items/red_pepper_flake.md" >}}) |
| 1 stalk  | [green onion]({{< relref "../items/green_onion.md" >}})            |


## Directions {#directions}

1.  Cook noodles according to instructions
2.  Saute garlic on low heat until golden brown
3.  Add remaining sauce ingredients in a bowl, whisk until smooth
4.  Add sauce to noodles and mix thoroughly
5.  Garnish with crushed peanuts, green onion, and red pepper flakes


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [Eat with Clarity](https://eatwithclarity.com/spicy-peanut-butter-noodles/print/9682/)
