+++
title = "Creamy carbonara scape gnocchi"
author = ["James Hawley"]
date = 2023-09-06T16:32:00-04:00
lastmod = 2023-12-28T01:18:08-05:00
tags = ["entrees", "pastas", "vegetarian", "recipes"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 45 min     |
| Cook Time | 1 h        |
| Yields    | 4 servings |


## Ingredients {#ingredients}


### Gnocchi {#gnocchi}

| Quantity | Item                                                                |
|----------|---------------------------------------------------------------------|
| 2        | [russet potatoes]({{< relref "../items/russet_potato.md" >}})       |
| 12 - 15  | [garlic scapes]({{< relref "../items/garlic_scape.md" >}})          |
| 1 cup    | [ricotta]({{< relref "../items/ricotta.md" >}})                     |
| 2        | [eggs]({{< relref "../items/eggs.md" >}})                           |
| 1 tsp    | [sea salt]({{< relref "../items/sea_salt.md" >}})                   |
| 5/2 cup  | [all-purpose flour]({{< relref "../items/all-purpose_flour.md" >}}) |


### Carbonara sauce {#carbonara-sauce}

| Quantity | Item                                                          |
|----------|---------------------------------------------------------------|
| 4        | [egg yolks]({{< relref "../items/eggs.md#egg-yolk" >}})       |
| 2 Tbsp   | [olive oil]({{< relref "../items/olive_oil.md" >}})           |
| 1/2 cup  | [whipping cream]({{< relref "../items/whipping_cream.md" >}}) |
| 1 cup    | [Parmesan]({{< relref "../items/parmesan.md" >}}), grated     |
|          | [sea salt]({{< relref "../items/sea_salt.md" >}})             |
|          | [black pepper]({{< relref "../items/black_pepper.md" >}})     |


## Directions {#directions}


### Gnocchi {#gnocchi}

1.  Preheat the oven to 190 C (375 F).
2.  Wash and scrub potatoes and pierce them several times with a fork.
    1.  Place directly on the bottom rack of the oven.
    2.  Bake for 45 - 50 min, turning once or twice.
    3.  Remove from oven and let cool.
3.  While potatoes are baking, bring a large pot of salted water to a boil.
    1.  Blanch the garlic scapes until slightly soft, yet still firm, in texture, 2 - 3 min.
    2.  Remove scapes from water, shaking of any excess and place on a cutting board.
    3.  Reserve water for cooking the gnocchi later.
4.  Roughly chop the scapes and transfer to a food processor.
    1.  Puree the scapes.
    2.  Add ricotta and blend until smooth and uniformly light green.
    3.  Set aside.
5.  Slice potatoes in half, scoop out flesh, and rice the potatoes&nbsp;[^fn:1].
6.  Place potatoes in a large bowl, make a well in the centre, and all eggs and salt.
    1.  Using a fork, whisk the eggs together, then gently fold them into the potatoes to make a paste.
    2.  Add the scape-ricotta puree and combine them.
7.  Add flour 1/2 cup at a time to make a soft dough.
    1.  Form the dough into a ball, dust generously with flour, and wrap in plastic wrap and chill in the fridge for 15 - 20 min.
8.  On a well-floured surface, divide the cough into four pieces and roll each piece into a log ~45 cm (18 ") long and 2.5 cm (1") in diameter.
    1.  Cut the logs into 1 cm (1/2 ") rounds and dust with flour.
    2.  Gently press a fork into the gnocchi lengthwise, folding the two outer sides in toward the centre, and roll the fork back over the top.


### Carbonara sauce {#carbonara-sauce}

1.  In a mixing bowl, whisk together egg yolks and oil until creamy and frothy.
    1.  Add the cream and continue to whisk until fully combined.
    2.  Add parmesan, salt, and pepper to taste and whisk to combine.
2.  Pour into a large saucepan and place over medium-low heat.
    1.  Stirring constantly, pour in reserved gnocchi cooking liquid and allow the egg mixture to warm slowly for 1 - 2 min.
    2.  Add gnocchi and continue to stir until the egg has started to thicken and gnocchi are well-coated.


## References {#references}

1.  Original recipe: [A Rising Tide]({{< relref "../references/A_Rising_Tide.md" >}})
2.  Once cooked, gnocchi and carbonara are best enjoyed on the day off.
    Uncooked gnocchi can be frozen on a baking sheet to prevent them from sticking, then transferred to an airtight container and kept frozen for 6 months.
    Cook from frozen and boil until the gnocchi start to float on the surface of the water.

[^fn:1]: If you don't have a ricer, mash the potatoes slightly and press them through a fine-mesh strainer.