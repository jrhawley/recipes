+++
title = "Strawberry daiquiri"
author = ["James Hawley"]
date = 2020-05-18T00:00:00-04:00
lastmod = 2023-12-22T01:17:24-05:00
tags = ["recipes", "alcohols", "beverages"]
draft = false
+++

## Ingredients {#ingredients}

| Quantity | Item                                                    |
|----------|---------------------------------------------------------|
| 1 oz     | [simple syrup]({{< relref "simple_syrup.md" >}})        |
| 1 cup    | [strawberries]({{< relref "../items/strawberry.md" >}}) |
| 3/2 oz   | [white rum]({{< relref "../items/white_rum.md" >}})     |
| 3/4 oz   | [lime juice]({{< relref "../items/lime_juice.md" >}})   |
| 1/2 cup  | ice cubes                                               |


## Directions {#directions}

1.  Add all ingredients to blender, blend until smooth


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [Whitney Bond](https://whitneybond.com/wprm_print/31641)
2.  Can easily be made virgin by omitting rum
