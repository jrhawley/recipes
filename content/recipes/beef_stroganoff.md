+++
title = "Beef stroganoff"
author = ["James Hawley"]
date = 2018-08-26T00:00:00-04:00
lastmod = 2024-05-23T21:41:32-04:00
tags = ["beef", "recipes", "slow-cooker", "entrees"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 20 min     |
| Cook Time | 4 h        |
| Yields    | 8 servings |


## Ingredients {#ingredients}

| Quantity | Item                                                      |
|----------|-----------------------------------------------------------|
| 2 lbs    | [ground beef]({{< relref "../items/ground_beef.md" >}})   |
| 2        | [onions]({{< relref "../items/onion.md" >}}), diced       |
| 2 cloves | [garlic]({{< relref "../items/garlic.md" >}}), minced     |
| 1 cup    | [beef consomme]({{< relref "../items/consomme.md" >}})    |
| 3 Tbsp   | [tomato paste]({{< relref "../items/tomato_paste.md" >}}) |
| 2 Tbsp   | [flour]({{< relref "../items/flour.md" >}})               |
|          | [kosher salt]({{< relref "../items/kitchen_salt.md" >}})  |
|          | [pepper]({{< relref "../items/black_pepper.md" >}})       |


## Directions {#directions}

1.  Brown ground beef in a large skillet
    1.  Add onions, garlic, and mushrooms
    2.  Saute until onions are golden brown
2.  Place into slow cooker with remaining ingredients except sour cream and flour
    1.  Stir thoroughly
    2.  Cover and cook on high for 3h, or low for 7-9h
3.  Whisk sour cream and flour together
    1.  Stir sour cream into mix 1h before serving
4.  Serve over hot butter noodles or rice&nbsp;[^fn:1]


## References {#references}

1.  Original recipe: Nana

[^fn:1]: From Nana:

    > Use good noodles.
