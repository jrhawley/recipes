+++
title = "Chicken lo mein"
author = ["James Hawley"]
date = 2020-01-15T00:00:00-05:00
lastmod = 2024-05-23T21:37:40-04:00
tags = ["chicken", "recipes", "slow-cooker", "entrees"]
draft = false
+++

| Info      | Amount |
|-----------|--------|
| Prep Time | 30 min |
| Cook Time | 3 h    |
| Yields    | 6      |


## Ingredients {#ingredients}

| Quantity | Item                                                                              |
|----------|-----------------------------------------------------------------------------------|
| 2 lbs    | boneless, skinless [chicken breasts]({{< relref "../items/chicken_breast.md" >}}) |
| 5 oz     | [snap peas]({{< relref "../items/snap_pea.md" >}})                                |
| 1        | large [red bell pepper]({{< relref "../items/bell_pepper.md" >}}), sliced         |
| 5 oz     | [water chestnuts]({{< relref "../items/water_chestnut.md" >}}), sliced            |
| 4 stalks | [green onion]({{< relref "../items/green_onion.md" >}}), chopped                  |
| 1 cup    | [vegetable broth]({{< relref "../items/vegetable_broth.md" >}})                   |
| 1/4 cup  | [worcestershire sauce]({{< relref "../items/worcestershire_sauce.md" >}})         |
| 1/4 cup  | low sodium [soy sauce]({{< relref "../items/soy_sauce.md" >}})                    |
| 2 Tbsp   | [honey]({{< relref "../items/honey.md" >}})                                       |
| 2 tsp    | [garlic]({{< relref "../items/garlic.md" >}}), minced                             |
| 1 tsp    | ground [ginger]({{< relref "../items/ginger.md" >}})                              |
| 14 oz    | [lo mein noodles]({{< relref "../items/lo_mein.md" >}}), uncooked                 |


## Directions {#directions}

1.  Slice chicken and place chicken breasts in slow cooker
    1.  Add snap peas, bell pepper, water chestnuts, green onion, broth
2.  In a small bowl, whisk together Worcestershire sauce, soy sauce, honey, garlic, and ginger
    1.  Pour into slow cooker to coat chicken
3.  Cook on low for 6 h or high for 3 h
4.  After slow cooker has finished, cook noodles according to instructions
    1.  Stir noodles into slow cooker, serve
    2.  Garnish with extra green onion


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [Eating on a Dime](https://www.eatingonadime.com/wprm_print/23335)
