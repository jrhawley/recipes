+++
title = "Martini"
author = ["James Hawley"]
date = 2021-02-17T00:00:00-05:00
lastmod = 2023-12-21T22:13:04-05:00
tags = ["recipes", "alcohols", "beverages"]
draft = false
+++

## Ingredients {#ingredients}

| Quantity | Item                                                  |
|----------|-------------------------------------------------------|
| 1/3 oz   | [vermouth]({{< relref "../items/dry_vermouth.md" >}}) |
| 2 oz     | [gin]({{< relref "../items/gin.md" >}})               |


## Directions {#directions}

1.  Fill a mixing glass with ice
2.  Add vermouth, stir to coat ice
3.  Add gin and stir until chilled and diluted
4.  Strain into a chilled cocktail glass
5.  Garnish with a strip of lemon peel


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [The Periodic Table of Cocktails]({{< relref "../references/Periodic_Table_of_Cocktails.md" >}})
