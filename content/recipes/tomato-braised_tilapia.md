+++
title = "Tomato-braised tilapia"
author = ["James Hawley"]
date = 2023-03-19T19:53:00-04:00
lastmod = 2024-05-23T20:46:57-04:00
tags = ["fish", "entrees", "recipes"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 10 min     |
| Cook Time | 20 min     |
| Yields    | 4 servings |


## Ingredients {#ingredients}

| Quantity | Item                                                                            |
|----------|---------------------------------------------------------------------------------|
| 4 filets | [tilapia]({{< relref "../items/tilapia.md" >}})                                 |
| 1/4 tsp  | [salt]({{< relref "../items/table_salt.md" >}})                                 |
| 1 Tbsp   | [lemon juice]({{< relref "../items/lemon_juice.md" >}})                         |
| 2 Tbsp   | [olive oil]({{< relref "../items/olive_oil.md" >}})                             |
| 1        | [red onion]({{< relref "../items/red_onion.md" >}}), chopped                    |
| 10 oz    | [diced tomatoes]({{< relref "../items/diced_tomatoes.md" >}})                   |
| 3/4 cup  | [roasted red peppers]({{< relref "../items/roasted_red_pepper.md" >}}), chopped |
| 1/2 cup  | [vegetable broth]({{< relref "../items/vegetable_broth.md" >}})                 |
| 1/4 cup  | [tomato paste]({{< relref "../items/tomato_paste.md" >}})                       |
| 1 tsp    | [oregano]({{< relref "../items/oregano.md" >}})                                 |
| 1 cup    | [Jasmine rice]({{< relref "../items/jasmine_rice.md" >}}), uncooked             |


## Directions {#directions}

1.  Sprinkle fillets with seasoned salt and drizzle with lemon juice
2.  Cook rice according to instructions
3.  In a large skillet, heat oil over medium-high heat
    1.  Add onion; cook and stir until tender
    2.  Add tomatoes, peppers, broth, tomato paste, garlic powder and oregano
    3.  Cook and stir 2 - 3 min
4.  Place fillets over tomato mixture; cook, covered, 6-8 min or until fish flakes easily with a fork


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [Taste of Home](https://www.tasteofhome.com/recipes/savory-tomato-braised-tilapia/print/)
2.  This was an okay meal, it wasn't great.
