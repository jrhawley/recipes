+++
title = "French onion soup"
author = ["James Hawley"]
date = 2020-09-22T00:00:00-04:00
lastmod = 2024-05-23T21:29:18-04:00
tags = ["slow-cooker", "soups", "recipes", "vegetarian", "entrees"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 30 min     |
| Cook Time | 5 h        |
| Yields    | 6 servings |


## Ingredients {#ingredients}

| Quantity | Item                                                                         |
|----------|------------------------------------------------------------------------------|
| 3        | yellow [onion]({{< relref "../items/onion.md" >}}), peeled and sliced thinly |
| 3 Tbsp   | [butter]({{< relref "../items/butter.md" >}})                                |
| 2 Tbsp   | [brown sugar]({{< relref "../items/brown_sugar.md" >}})                      |
| 2 Tbsp   | [worcestershire sauce]({{< relref "../items/worcestershire_sauce.md" >}})    |
| 2 Tbsp   | [garlic]({{< relref "../items/garlic.md" >}}), minced                        |
| 8 cups   | low sodium [beef broth]({{< relref "../items/beef_broth.md" >}})             |
| 1        | [bay leaf]({{< relref "../items/bay_leaf.md" >}})                            |
| 1 sprig  | fresh [thyme]({{< relref "../items/thyme.md" >}})                            |
| 6 slices | [French bread]({{< relref "../items/french_bread.md" >}})                    |
| 1 cup    | [Swiss cheese]({{< relref "../items/swiss_cheese.md" >}}), shredded          |
| 1 cup    | [parmesan]({{< relref "../items/parmesan.md" >}}), shredded                  |


## Directions {#directions}

1.  Peel and slice the onions into thin slices
    1.  In a medium skillet on medium-high heat, add the butter, sliced onions, and brown sugar to saute until translucent and caramelized
2.  Transfer cooked onions into a slow cooker and add broth, garlic, bay leaf, and thyme
    1.  Cover and cook on low for 4-5 h
3.  Serve soup in oven-proof bowls and top with a slice of French bread
    1.  Add Swiss cheese and parmesan to the top of the bread
    2.  Place bowls on a cookie sheet and place under the broiler for 1 min at most, until cheese is melted and bubbles


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [Crockpot Ladies](https://crockpotladies.com/wp-content/plugins/wp-ultimate-recipe-premium/core/templates/print.php)
