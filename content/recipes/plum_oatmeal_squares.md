+++
title = "Plum oatmeal squares"
author = ["James Hawley"]
date = 2024-06-12T16:52:00-04:00
lastmod = 2024-06-12T19:04:54-04:00
tags = ["breakfasts", "snacks", "recipes"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 10 min     |
| Cook Time | 45 min     |
| Yields    | 16 squares |


## Equipment {#equipment}

-   1 large mixing bowl
-   1 medium mixing bowl
-   1 small bowl
-   1 whisk
-   1 paring knife
-   1 small cutting board
-   1 8x8" square pan
-   measuring cups and spoons


## Ingredients {#ingredients}

| Quantity | Item                                                                                                      |
|----------|-----------------------------------------------------------------------------------------------------------|
| 1/2 cup  | [almonds]({{< relref "../items/almond.md" >}}) or [walnuts]({{< relref "../items/walnut.md" >}}), chopped |
| 2 cup    | rolled [oats]({{< relref "../items/oat.md" >}})                                                           |
| 2 Tbsp   | [ground cinnamon]({{< relref "../items/cinnamon.md" >}})                                                  |
| 1 tsp    | [baking powder]({{< relref "../items/baking_powder.md" >}})                                               |
| 1/2 tsp  | [sea salt]({{< relref "../items/sea_salt.md" >}})                                                         |
| 2 cup    | [milk]({{< relref "../items/milk.md" >}})                                                                 |
| 1        | [egg]({{< relref "../items/eggs.md" >}})                                                                  |
| 1/3 cup  | [maple syrup]({{< relref "../items/maple_syrup.md" >}})                                                   |
| 2 Tbsp   | [butter]({{< relref "../items/butter.md" >}}), melted                                                     |
| 2 tsp    | [vanilla extract]({{< relref "../items/vanilla_extract.md" >}})                                           |
| 3        | [plums]({{< relref "../items/plum.md" >}}), sliced                                                        |


## Directions {#directions}

1.  Preheat the oven to 190 C (375 F) and grease the inside of the square pan.
2.  Chop the almonds/walnuts and slice the plums.
3.  In a large bowl, stir in half the almonds/walnuts and all the dry ingredients.
4.  In a medium bowl, whisk all the wet ingredients together.
    1.  Pour wet ingredients into the dry bowl, slowly whisking together.
5.  Line the bottom of the pan with plum slices, then pour the batter over top and spread evenly.
    1.  Gently place the remainin plum slices and almonds/walnuts on the surface.
    2.  Place pan in the oven and let bake for ~ 45 min.


## References {#references}

1.  Original recipe: [Girl Versus Dough](https://www.girlversusdough.com/cinnamon-plum-baked-oatmeal-with-toasted-almonds/print/9210/)
