+++
title = "Roasted red pepper and gouda soup"
author = ["James Hawley"]
date = 2023-11-05T10:44:00-05:00
lastmod = 2024-02-18T18:43:06-05:00
tags = ["entrees", "soups", "vegetarian", "recipes"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 10 min     |
| Cook Time | 30 min     |
| Yields    | 4 servings |


## Equipment {#equipment}

-   large cutting board
-   chef's knife
-   garlic press
-   large soup pot
-   wooden spoon
-   ladle
-   grater
-   measuring cups
-   tasting spoon


## Ingredients {#ingredients}

| Quantity | Item                                                                          |
|----------|-------------------------------------------------------------------------------|
| 2 Tbsp   | [olive oil]({{< relref "../items/olive_oil.md" >}})                           |
| 1        | medium [onion]({{< relref "../items/onion.md" >}}), finely chopped            |
| 3 cloves | [garlic]({{< relref "../items/garlic.md" >}}), minced                         |
| 5/2 tsp  | [kosher salt]({{< relref "../items/kitchen_salt.md" >}})                      |
| 1 L      | [vegetable broth]({{< relref "../items/vegetable_broth.md" >}})               |
| 12 oz    | [roasted red peppers]({{< relref "../items/roasted_red_pepper.md" >}}), diced |
| 1/2 tsp  | [paprika]({{< relref "../items/paprika.md" >}})                               |
| 1/2 cup  | [heavy cream]({{< relref "../items/heavy_cream.md" >}}) [^fn:1]               |
| 2 Tbsp   | [lemon juice]({{< relref "../items/lemon_juice.md" >}})                       |
| 1/2 tsp  | [black pepper]({{< relref "../items/black_pepper.md" >}})                     |
| 1 cup    | [gouda]({{< relref "../items/gouda.md" >}}), shredded [^fn:1]                 |
| 1/4 cup  | [basil]({{< relref "../items/basil.md" >}}) leaves                            |


## Directions {#directions}

1.  In a large pot over medium heat, heat the olive oil and cook onion, garlic, and 1/2 tsp of salt until softened.
2.  Stir in broth, red peppers, paprika, and 1 tsp salt and bring to a boil.
    1.  Reduce heat to medium-low, and simmer to 10 min, stirring occassionally.
3.  Puree with an immersion blender, bring back up to a boil, and stir in cream.
    1.  Season with lemon juice, salt, and pepper to taste..
4.  Serve into soup bowls and top with gouda, basil, salt, and pepper to taste.


## References {#references}

1.  Original recipe: [Delish](https://www.delish.com/cooking/recipe-ideas/a40359348/roasted-red-pepper-soup-with-gouda-recipe/)

[^fn:1]: If making this dish vegan, substitute with an appropriate plant-based cream and cheese.