+++
title = "Balsamic chicken"
author = ["James Hawley"]
date = 2020-01-05T00:00:00-05:00
lastmod = 2024-05-23T21:38:20-04:00
tags = ["chicken", "recipes", "slow-cooker", "entrees"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 10 min     |
| Cook Time | 4 h        |
| Yields    | 4 servings |


## Ingredients {#ingredients}

| Quantity | Item                                                              |
|----------|-------------------------------------------------------------------|
| 4        | [chicken breasts]({{< relref "../items/chicken_breast.md" >}})    |
| 1        | [onion]({{< relref "../items/onion.md" >}}), sliced               |
| 4 cloves | [garlic]({{< relref "../items/garlic.md" >}}), minced             |
| 14 oz    | [crushed tomatoes]({{< relref "../items/passata.md" >}})          |
| 2 Tbsp   | [olive oil]({{< relref "../items/olive_oil.md" >}})               |
| 1 tsp    | [oregano]({{< relref "../items/oregano.md" >}})                   |
| 1 tsp    | [basil]({{< relref "../items/basil.md" >}})                       |
| 1 tsp    | [rosemary]({{< relref "../items/rosemary.md" >}})                 |
| 1/2 tsp  | [thyme]({{< relref "../items/thyme.md" >}})                       |
| 1/2 cup  | [balsamic vinegar]({{< relref "../items/balsamic_vinegar.md" >}}) |
|          | [kosher salt]({{< relref "../items/kitchen_salt.md" >}})          |
|          | [pepper]({{< relref "../items/black_pepper.md" >}})               |


## Directions {#directions}

1.  Drizzle olive oil into slow cooker
2.  Place chicken breasts, season with salt and pepper, then top with remaining ingredients
3.  Cook on high for 4 h, until chicken breasts are fully cooked


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [All Recipes](https://www.allrecipes.com/recipe/234664/slow-cooker-balsamic-chicken/)
