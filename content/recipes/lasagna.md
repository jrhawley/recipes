+++
title = "Lasagna"
author = ["James Hawley"]
date = 2019-03-23T00:00:00-04:00
lastmod = 2024-05-23T21:40:40-04:00
tags = ["pastas", "recipes", "vegetarian", "entrees"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 30 min     |
| Cook Time | 4 h        |
| Yields    | 8 servings |

{{< figure src="/ox-hugo/lasagna.jpg" caption="<span class=\"figure-number\">Figure 1: </span>Lasagna" >}}


## Ingredients {#ingredients}

| Quantity   | Item                                                                            |
|------------|---------------------------------------------------------------------------------|
| 500 g      | [ground beef]({{< relref "../items/ground_beef.md" >}}) (exclude if vegetarian) |
| 3 handfuls | [spinach]({{< relref "../items/spinach.md" >}}) (exclude if carnivorous)        |
| 2 tsp      | [table salt]({{< relref "../items/table_salt.md" >}})                           |
| 1 tsp      | [pepper]({{< relref "../items/black_pepper.md" >}})                             |
| 1          | large [onion]({{< relref "../items/onion.md" >}}), chopped                      |
| 1 tsp      | [Italian seasoning]({{< relref "../items/italian_seasoning.md" >}})             |
| 2 Tbsp     | dried [parsley]({{< relref "../items/parsley.md" >}})                           |
| 2 tsp      | [garlic powder]({{< relref "../items/garlic_powder.md" >}})                     |
| 6 oz       | [tomato paste]({{< relref "../items/tomato_paste.md" >}})                       |
| 28 oz      | [tomato sauce]({{< relref "../items/tomato_sauce.md" >}})                       |
| 5/4 cups   | [water]({{< relref "../items/water.md" >}})                                     |
| 8 oz       | [lasagna noodles]({{< relref "../items/lasagna.md" >}}), uncooked               |
| 4 cups     | [mozzarella]({{< relref "../items/mozzarella.md" >}}), shredded                 |
| 3/2 cups   | [ricotta]({{< relref "../items/ricotta.md" >}})                                 |
| 1/2 cup    | [parmesan]({{< relref "../items/parmesan.md" >}})                               |


## Directions {#directions}

1.  In a large skillet over medium heat brown the ground beef and onion
2.  Add salt, pepper, Italian seasoning, parsley, garlic powder, tomato paste, tomato sauce, sugar, and water
    1.  Stir and simmer 15-20 min
3.  Spread a fourth of the meat sauce into the bottom of your slow cooker
    1.  Arrange a third of the noodles over the sauce (break the noodles to fit, if necessary)
4.  Combine the mozzarella, cottage cheese and parmesan cheese in a medium bowl
    1.  Spoon a third of the mixture over noodles
5.  Repeat layers twice
6.  Top with remaining meat sauce and sprinkle with extra mozzarella cheese, if desired
7.  Cover and cook on low for about 4-5 hours or until noodles are tender


## References &amp; Notes {#references-and-notes}

1.  [Original recipe](https://tastesbetterfromscratch.com/slow-cooker-lasagna/)
