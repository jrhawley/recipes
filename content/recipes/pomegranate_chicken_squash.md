+++
title = "Pomegranate chicken and roasted acorn squash"
author = ["James Hawley"]
date = 2021-12-08T00:00:00-05:00
lastmod = 2024-05-23T21:15:42-04:00
tags = ["chicken", "recipes", "entrees"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 10 min     |
| Cook Time | 25 min     |
| Yields    | 2 servings |


## Ingredients {#ingredients}

| Quantity | Item                                                               |
|----------|--------------------------------------------------------------------|
| 1/2 cup  | [basmati rice]({{< relref "../items/basmati_rice.md" >}})          |
| 1/4 cup  | sweet Thai chili sauce                                             |
| 1/4 cup  | [soy sauce]({{< relref "../items/soy_sauce.md" >}})                |
| 1 tsp    | [ginger powder]({{< relref "../items/ginger_powder.md" >}})        |
| 1 clove  | [garlic]({{< relref "../items/garlic.md" >}}), minced              |
| 2        | [chicken breasts]({{< relref "../items/chicken_breast.md" >}})     |
| 1        | [acorn squash]({{< relref "../items/acorn_squash.md" >}})          |
| 1 Tbsp   | [olive oil]({{< relref "../items/olive_oil.md" >}})                |
|          | [Kosher salt]({{< relref "../items/kitchen_salt.md" >}})           |
|          | [black pepper]({{< relref "../items/black_pepper.md" >}})          |
| 1/2 cup  | [pomegranate arils]({{< relref "../items/pomegranate_aril.md" >}}) |


## Directions {#directions}

1.  Preheat oven to 220 C (425 F).
2.  In a medium bowl, combine the sweet Thai chili sauce, pomegranate juice, soy sauce, ginger, and garlic.
3.  If using fresh chicken breasts, toss with half the sauce, then place on a large baking sheet.
4.  If starting from frozen, heat olive oil in a pan and cook the chicken on medium heat until the skin is white and the breasts are soft.
5.  Cook the rice according to instructions.
6.  Slice the acorn squash into half rounds, 1/2" thick.
7.  Place on a baking sheet, drizzle with olive oil, salt, and pepper.
8.  Place the entire baking sheet (including the chicken breasts, if fresh) for 20 min, flipping half way through.
9.  Serve chicken and squash over rice in a bowl, garnishing with arils and extra sauce.


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [Half-baked Harbest](https://www.halfbakedharvest.com/wprm_print/41880)
