+++
title = "Caffe shakerato"
author = ["James Hawley"]
date = 2022-09-20T18:04:00-04:00
lastmod = 2023-12-19T22:00:04-05:00
tags = ["alcohols", "recipes", "beverages", "coffee"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 5 min      |
| Cook Time | 0 min      |
| Yields    | 1 cocktail |


## Ingredients {#ingredients}

| Quantity | Item                                                                                                                                                         |
|----------|--------------------------------------------------------------------------------------------------------------------------------------------------------------|
|          | [espresso]({{< relref "../items/espresso.md" >}})                                                                                                            |
| 1 tsp    | [simple syrup]({{< relref "simple_syrup.md" >}})                                                                                                             |
| 1 dash   | [aged rum]({{< relref "../items/aged_rum.md" >}}), [white rum]({{< relref "../items/white_rum.md" >}}), or [amaretto]({{< relref "../items/amaretto.md" >}}) |
|          | ice                                                                                                                                                          |


## Directions {#directions}

1.  Place the ice cubes and liqueur of choice in a cocktail shaker.
2.  Pull the single (or double) shot of espresso and pour into the cocktail shaker.
3.  Wearing gloves, shake the shaker until a desired foaminess is reached.
4.  Strain out any ice and serve drink in a coupe or low ball glass.


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [The Espresso Martini Killer - Internet Shaquille](https://www.youtube.com/watch?v=9Ilnnne01So)
2.  Original recipe: [The Caffe Shakerato - Three Recipes - James Hoffmann](https://www.youtube.com/watch?v=XYUuot8lJAY)
