+++
title = "Braised lamb shanks with pommes puree"
author = ["James Hawley"]
date = 2024-02-17T23:13:00-05:00
lastmod = 2024-02-18T22:25:57-05:00
tags = ["lamb", "entrees", "recipes"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 10 min     |
| Cook Time | 2 h 30 min |
| Yields    | 4 servings |


## Equipment {#equipment}

-   large cutting board
-   chef's knife
-   garlic press
-   Dutch oven
-   wooden spoon
-   large plate
-   large pot
-   colander
-   potato masher or food mill
-   tongs
-   ladle
-   small pot


## Ingredients {#ingredients}


### Braised lamb shanks {#braised-lamb-shanks}

| Quantity | Item                                                                      |
|----------|---------------------------------------------------------------------------|
| 30 mL    | [olive oil]({{< relref "../items/olive_oil.md" >}})                       |
| 4        | [lamb shanks]({{< relref "../items/lamb_shank.md" >}})                    |
| 3        | [carrots]({{< relref "../items/carrot.md" >}}), peeled and diced          |
| 4 stalks | [celery]({{< relref "../items/celery.md" >}}), diced                      |
| 4        | [shallots]({{< relref "../items/shallot.md" >}}), chopped                 |
| 4 cloves | [garlic]({{< relref "../items/garlic.md" >}}), minced                     |
| 250 mL   | dry [red wine]({{< relref "../items/red_wine.md" >}})                     |
| 800 mL   | [whole peeled tomatoes]({{< relref "../items/whole_peeled_tomato.md" >}}) |
| 1 L      | [chicken stock]({{< relref "../items/chicken_stock.md" >}})               |
| 2 sprigs | [rosemary]({{< relref "../items/rosemary.md" >}})                         |
| 2 sprigs | [thyme]({{< relref "../items/thyme.md" >}})                               |
| 2        | [bay leaves]({{< relref "../items/bay_leaf.md" >}})                       |
| 30 mL    | unsalted [butter]({{< relref "../items/butter.md" >}})                    |
| 30 mL    | [parsley]({{< relref "../items/parsley.md" >}})                           |
|          | [Kosher salt]({{< relref "../items/kitchen_salt.md" >}})                  |
|          | [black pepper]({{< relref "../items/black_pepper.md" >}})                 |


### Pommes puree {#pommes-puree}

| Quantity | Item                                                                          |
|----------|-------------------------------------------------------------------------------|
| 900 g    | [Yukon gold potatoes]({{< relref "../items/yukon_gold_potato.md" >}}), peeled |
| 450 g    | unsalted [butter]({{< relref "../items/butter.md" >}}), chunked               |
| 75 mL    | [milk]({{< relref "../items/milk.md" >}})                                     |
|          | [Kosher salt]({{< relref "../items/kitchen_salt.md" >}})                      |


## Directions {#directions}


### Lamb shanks {#lamb-shanks}

1.  Preheat the oven to 160 C (325 F).
2.  In a Dutch oven, heat olive oil over medium heat.
    1.  Add shanks, 2 at a time, and brown btoh sides, ~ 10 min per batch.
    2.  Transfer shanks to a large plate.
3.  Add carrots, celery, shallots, onion, and garlic to the pot and cook until tender, ~ 10 min.
    1.  Deglze with wine and bring to a boil, then reduce for 3 - 4 min.
    2.  Using a wooden spoon, scrape any brown bits.
4.  Add tomatoes and their juices, stock, rosemary, thyme, and bay leaves.
    1.  Return lamb shanks to the Dutch oven and submerge them in the braising liquid.
    2.  Bring to a boil, cover, then place in the oven.
    3.  Braise for 1.5 - 2 h, until the meat is nearly falling off the bone.


### Pommes puree {#pommes-puree}

1.  In a large pot, cover the potatoes with cold, salted water.
    1.  Bring to a boil, then reduce heat to low and simmer until tender, ~ 40 min.
    2.  Drain, then puree potatoes until smooth&nbsp;[^fn:1].
    3.  Add butter, a few cubes at a time, allowing each to melt before adding the next, stirring constantly.
2.  Add milk slowly, whisking constantly, preventing milk from cooling the potatoes.
    1.  Whisk vigorously, and season to taste.
    2.  Cover and keep warm.


### Serving {#serving}

1.  Using tongs, remove the shanks from the Dutch oven and place on a clean plate.
2.  Spoon off as much fat as possible from the surface of the braising liquid.
    1.  Strain the liquid into a small pot, and reduce over medium heat until thickened&nbsp;[^fn:2].
    2.  Remove from heat and whisk in butter and parsley.
    3.  Season with salt and pepper.
3.  Divide pommes puree among the plates, place lamb shanks on top, then spoon thickened sauce over top.


## References {#references}

1.  Original recipe: [Farm to Chef: Cooking Through the Seasons - Lynn Crawford (2017)]({{< relref "../references/farm_to_chef_-_lynn_crawford_(2017).md" >}})

[^fn:1]: Pureeing can be done in a few ways, depending on how smooth you want the puree to be.
    Pushing potatoes through a food mill on the finest setting will work best for a smooth puree.
    Mashing them with a potato masher or hand mixer can leave them slightly lumpy.
[^fn:2]: An alternative to get restaurant-style sauce is to add gelatin packs, or flour.