+++
title = "El Presidente"
author = ["James Hawley"]
date = 2021-02-17T00:00:00-05:00
lastmod = 2023-12-21T21:44:07-05:00
tags = ["recipes", "alcohols", "beverages"]
draft = false
+++

## Ingredients {#ingredients}

| Quantity | Item                                                  |
|----------|-------------------------------------------------------|
| 2 oz     | [rum]({{< relref "../items/rum.md" >}})               |
| 1 oz     | [vermouth]({{< relref "../items/dry_vermouth.md" >}}) |
| 1/3 oz   | [Triple Sec]({{< relref "../items/triple_sec.md" >}}) |
| 1 dash   | [grenadine]({{< relref "../items/grenadine.md" >}})   |


## Directions {#directions}

1.  Fill a mixing glass with ice and add liquids
2.  Stir until chilled
3.  Strain into a chilled serving glass and garnish with orange peel


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [The Periodic Table of Cocktails]({{< relref "../references/Periodic_Table_of_Cocktails.md" >}})
