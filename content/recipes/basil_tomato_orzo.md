+++
title = "Basil and tomato orzo"
author = ["James Hawley"]
date = 2017-05-28T00:00:00-04:00
lastmod = 2024-05-23T21:43:02-04:00
tags = ["pastas", "recipes", "vegetarian", "lunch", "entrees"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 15 min     |
| Cook Time | 10 min     |
| Yields    | 4 servings |


## Ingredients {#ingredients}

| Quantity | Item                                                                  |
|----------|-----------------------------------------------------------------------|
| 2 cups   | [orzo]({{< relref "../items/orzo.md" >}}), uncooked                   |
| 25       | fresh [basil]({{< relref "../items/basil.md" >}}) leaves              |
| 1 Tbsp   | [oregano]({{< relref "../items/oregano.md" >}}) leaves                |
| 1        | [shallot]({{< relref "../items/shallot.md" >}})                       |
| 1 clove  | [garlic]({{< relref "../items/garlic.md" >}}), minced                 |
| 1/3 cup  | [olive oil]({{< relref "../items/olive_oil.md" >}})                   |
| 1 tsp    | [honey]({{< relref "../items/honey.md" >}})                           |
| 1 Tbsp   | [water]({{< relref "../items/water.md" >}})                           |
| 1 cup    | [cherry tomatoes]({{< relref "../items/cherry_tomato.md" >}}), halved |
| 1 cup    | [parmesan]({{< relref "../items/parmesan.md" >}}), grated             |
| 5        | [mint]({{< relref "../items/mint.md" >}}) leaves, chopped             |
| 1        | [lemon]({{< relref "../items/lemon.md" >}}), zested and juiced        |
|          | [kosher salt]({{< relref "../items/kitchen_salt.md" >}})              |


## Directions {#directions}

1.  Cook orzo pasta according to package directions. Set aside.
2.  Place basil leaves, shallot, garlic, oil, lemon zest and juice, honey and salt in a blender with water and give it a whiz until blended.
3.  Mix dressing into pasta
    -   Stir in cherry tomatoes and Parmesan
    -   Garnish with fresh chopped mint
