+++
title = "Yogurt parfait"
author = ["James Hawley"]
date = 2023-04-02T21:08:00-04:00
lastmod = 2024-06-12T19:16:33-04:00
tags = ["breakfasts", "snacks", "dairy", "recipes"]
draft = false
+++

| Info      | Amount    |
|-----------|-----------|
| Prep Time | 2 min     |
| Cook Time | 0 min     |
| Yields    | 1 serving |


## Ingredients {#ingredients}

| Quantity | Item                                                                 |
|----------|----------------------------------------------------------------------|
| 2/3 cup  | [Greek yogurt]({{< relref "../items/greek_yogurt.md" >}})            |
|          | [blueberries]({{< relref "../items/blueberry.md" >}})                |
|          | [Cinnamon Maple Granola]({{< relref "cinnamon_maple_granola.md" >}}) |


## Directions {#directions}

1.  Mix all ingredients in a bowl and serve immediately
