+++
title = "Linguine, zucchine, e gamberetti"
author = ["James Hawley"]
date = 2020-11-01T00:00:00-04:00
lastmod = 2024-05-23T21:24:50-04:00
tags = ["pastas", "recipes", "entrees"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 5 min      |
| Cook Time | 20 min     |
| Yields    | 4 servings |


## Ingredients {#ingredients}

| Quantity | Item                                                                          |
|----------|-------------------------------------------------------------------------------|
| 1 clove  | [garlic]({{< relref "../items/garlic.md" >}}), finely diced                   |
| 2        | [shallots]({{< relref "../items/shallot.md" >}}), finely diced                |
| 1        | small [zucchini]({{< relref "../items/zucchini.md" >}}), sliced in half moons |
| 300 g    | [prawns]({{< relref "../items/prawn.md" >}}), thawed                          |
| 500 g    | [linguine]({{< relref "../items/linguine.md" >}})                             |
| 2 Tbsp   | [olive oil]({{< relref "../items/olive_oil.md" >}})                           |
|          | [thyme]({{< relref "../items/thyme.md" >}})                                   |
|          | [kosher salt]({{< relref "../items/kitchen_salt.md" >}})                      |
|          | [pepper]({{< relref "../items/black_pepper.md" >}})                           |


## Directions {#directions}

1.  In a hot non-stick pan, add olive oil, garlic, and shallots
    1.  Season with salt, pepper, and thyme
2.  Once garlic is golden, add the zucchini
    1.  Season again with salt, pepper, and thyme
    2.  Reduce to medium-high heat and toss together
    3.  Slowly add a cup of water to stop the zucchini from burning
    4.  If not soft by the time the water has boiled, add another cup until almost cooked
3.  When zucchini is 3/4 of the way cooked, add the prawns
    1.  Season again with salt and pepper
    2.  Reduce to medium heat and let simmer
    3.  Cook until the prawns are firm and their colour changes
4.  While the prawns are cooking, cook pasta to _al dente_
5.  Drain pasta and toss together with prawns and zucchini


## References &amp; Notes {#references-and-notes}

1.  Original recipe: Giacomo Grillo.

> We can say that this is a classic.
> In Italy you may find a risotto cooked with the same ingredients, but a pasta dish is a perfect way to celebrate two ingredients made for one another.

1.  Try to use "elongated shallots" (aka "banana shallots") because they're sweeter and less harsh on the palate.

2.  Make sure that frozen prawns are completely defrosted / fresh prawns are clean (I [Giacomo] normally use raw peeled frozen prawns)
