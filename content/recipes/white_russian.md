+++
title = "White Russian"
author = ["James Hawley"]
date = 2022-11-12T22:56:00-05:00
lastmod = 2023-12-24T20:05:10-05:00
tags = ["alcohols", "recipes", "beverages"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 2 min      |
| Cook Time | 0 min      |
| Yields    | 1 cocktail |


## Ingredients {#ingredients}

| Quantity | Item                                                          |
|----------|---------------------------------------------------------------|
| 4/3 oz   | [vodka]({{< relref "../items/vodka.md" >}})                   |
| 2/3 oz   | [Kahlua]({{< relref "../items/kahlua.md" >}})                 |
| 2/3 oz   | [whipping cream]({{< relref "../items/whipping_cream.md" >}}) |


## Directions {#directions}

1.  Pour vodka and kahlua into a rocks glass filled with ice and mix.
2.  Slowly pour the cream on top and garnish with nutmeg, if desired.


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [The Periodic Table of Cocktails]({{< relref "../references/Periodic_Table_of_Cocktails.md" >}})
