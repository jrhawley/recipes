+++
title = "Sweet potato fries"
author = ["James Hawley"]
date = 2013-09-14T00:00:00-04:00
lastmod = 2023-11-13T18:47:59-05:00
tags = ["recipes", "vegetarian", "sides"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 10 min     |
| Cook Time | 30 min     |
| Yields    | 2 servings |


## Ingredients {#ingredients}

| Quantity | Item                                                     |
|----------|----------------------------------------------------------|
| 1        | [sweet potato]({{< relref "../items/yam.md" >}})         |
| 2 Tbsp   | [olive oil]({{< relref "../items/olive_oil.md" >}})      |
|          | [corn starch]({{< relref "../items/corn_starch.md" >}})  |
|          | [kosher salt]({{< relref "../items/kitchen_salt.md" >}}) |
|          | [pepper]({{< relref "../items/black_pepper.md" >}})      |


## Directions {#directions}

1.  Preheat the oven to 450 F
2.  Peel the sweet potato(es) and cut them into fries&nbsp;[^fn:1]
3.  Toss fries into a mixing bowl, coat with cornstarch and olive oil
    1.  Season with salt, pepper, and any other seasonings
4.  Pour fries onto non-stick baking sheet into a single layer
5.  Bake for 25-30 min, flipping half way through


## References &amp; Notes {#references-and-notes}

[^fn:1]: Cut the potatoes into similar shapes to ensure even cooking