+++
title = "Dark Chocolate Waffles"
author = ["James Hawley"]
date = 2017-01-22T00:00:00-05:00
lastmod = 2024-06-12T19:13:57-04:00
tags = ["recipes", "breakfasts"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 15 min     |
| Cook Time | 15 min     |
| Yields    | 4 servings |
| Cost      |            |


## Ingredients {#ingredients}

| Quantity | Item                                                                   |
|----------|------------------------------------------------------------------------|
| 2        | [eggs]({{< relref "../items/cherry_tomato.md" >}}), large              |
| 2 cup    | [flour]({{< relref "../items/flour.md" >}})                            |
| 2 tsp    | [baking powder]({{< relref "../items/baking_powder.md" >}})            |
| 1/3 cup  | [brown sugar]({{< relref "../items/brown_sugar.md" >}})                |
| 1/3 cup  | [cocoa powder]({{< relref "../items/cocoa_powder.md" >}}), unsweetened |
| 1/2 tsp  | [table salt]({{< relref "../items/table_salt.md" >}})                  |
| 1/4 cup  | [chocolate chips]({{< relref "../items/chocolate_chip.md" >}})         |
| 1/3 cup  | [olive oil]({{< relref "../items/olive_oil.md" >}})                    |
| 7/4 cup  | [milk]({{< relref "../items/milk.md" >}})                              |


## Directions {#directions}

1.  Separate egg yolks from whites
    1.  Whisk egg whites until peaks form
    2.  Set each aside
2.  In a large bowl, mix all wet ingredients
3.  In another large bowl, whisk together all dry ingredients, except chocolate chips
    1.  Add egg yolks and whisk
4.  Slowly add wet mix to dry mix, and whisk together until slightly lumpy
5.  Fold in egg whites
6.  Add oil to waffle iron, and cook
