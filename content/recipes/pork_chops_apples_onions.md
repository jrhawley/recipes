+++
title = "Pork chops, apples, and onions"
author = ["James Hawley"]
date = 2022-09-19T16:31:00-04:00
lastmod = 2024-05-23T21:13:19-04:00
tags = ["pork", "entrees", "recipes"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 5 min      |
| Cook Time | 20 min     |
| Yields    | 3 servings |


## Ingredients {#ingredients}

| Quantity | Item                                                            |
|----------|-----------------------------------------------------------------|
| 2 Tbsp   | [olive oil]({{< relref "../items/olive_oil.md" >}}), divided    |
| 3        | [pork chops]({{< relref "../items/pork_chop.md" >}})            |
| 1/2 cup  | [vegetable broth]({{< relref "../items/vegetable_broth.md" >}}) |
| 1/4 cup  | [whipping cream]({{< relref "../items/whipping_cream.md" >}})   |
| 1 tsp    | [Dijon mustard]({{< relref "../items/dijon_mustard.md" >}})     |
| 3/2 tsp  | [rosemary]({{< relref "../items/rosemary.md" >}})               |
| 1/2 tsp  | [thyme]({{< relref "../items/thyme.md" >}})                     |
|          | [Kosher salt]({{< relref "../items/kitchen_salt.md" >}})        |
|          | [black pepper]({{< relref "../items/black_pepper.md" >}})       |
| 2        | [apples]({{< relref "../items/apple.md" >}})                    |
| 1        | [onion]({{< relref "../items/onion.md" >}})                     |


## Directions {#directions}

1.  Season both sides of pork chops with kosher salt and black pepper.
    1.  Add 1 Tbsp olive oil to large heavy bottomed pan and heat over medium-high heat.
    2.  Add pork chops to pan and sear 3-5 min per side, or until pork chops are mostly done&nbsp;[^fn:1].
    3.  Remove pork chops to a plate.
2.  In a small mixing bowl, whisk together chicken stock and mustard, set aside.
3.  Add remaining 1 Tbsp oil to the pan, then add apples and onions.
    1.  Cook 4 min, stirring occasionally.
    2.  Season with salt, pepper, sage, rosemary, and thyme. Stir to combine.
4.  Pour in stock mixture, using a wooden spoon to gently scrape the bottom of the pan.
5.  Slide pork chops back into the pan, nestling them down in between the apples.
    1.  Cook 2-3 minutes, until pork chops are finished cooking and liquid has reduced by half.


## References {#references}

1.  Inspiration: [The Chunky Chef](https://www.thechunkychef.com/one-pan-pork-chops-apples-onions/)

[^fn:1]: The pork will continue cooking in the sauce later.
