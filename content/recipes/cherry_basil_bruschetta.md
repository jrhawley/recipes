+++
title = "Cherry basil bruschetta"
author = ["James Hawley"]
date = 2023-03-19T10:00:00-04:00
lastmod = 2023-11-05T16:13:33-05:00
tags = ["vegetarian", "appetizers", "recipes"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 20 min     |
| Cook Time | 5 min      |
| Yields    | 2 servings |


## Ingredients {#ingredients}

| Quantity | Item                                                                               |
|----------|------------------------------------------------------------------------------------|
| 1 cup    | [cherries]({{< relref "../items/cherry.md" >}}), pitted and finely chopped         |
| 3 cup    | [strawberries]({{< relref "../items/strawberry.md" >}}), hulled and finely chopped |
| 1/4 cup  | [basil]({{< relref "../items/basil.md" >}}), minced                                |
| 1/4 cup  | [mint]({{< relref "../items/mint.md" >}}), minced                                  |
| 3 Tbsp   | [red onion]({{< relref "../items/red_onion.md" >}}), finely chopped                |
| 4 tsp    | [balsamic vinegar]({{< relref "../items/balsamic_vinegar.md" >}})                  |
| 1        | [baguette]({{< relref "../items/baguette.md" >}}), sliced on a bias                |
| 2 Tbsp   | [olive oil]({{< relref "../items/olive_oil.md" >}})                                |


## Directions {#directions}

1.  Preheat the oven to 230 C (450 F)
2.  In a large bowl, combine the cherries, strawberries, basil, mint, onion, and vinegar, then set aside for 10 - 15 min
3.  Slice the baguette, and brush one side with olive oil
    1.  Place the oiled side face down on a large rimmed baking sheet
    2.  Bake for 5 - 7 min, watching closely to avoid burning
4.  Spoon the bruschetta mixture over the toasted bread
5.  (Optional) drizzle each piece with a balsamic reduction&nbsp;[^fn:1] immediately before serving


## References {#references}

1.  Original recipe: [The Oh She Glows Cookbook]({{< relref "../references/Oh_She_Glows_Cookbook.md" >}})

[^fn:1]: Low boiled balsamic vinegar that has been reduced by half