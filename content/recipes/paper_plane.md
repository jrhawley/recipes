+++
title = "Paper plane"
author = ["James Hawley"]
date = 2021-08-07T00:00:00-04:00
lastmod = 2023-12-21T22:20:27-05:00
tags = ["recipes", "alcohols", "beverages"]
draft = false
+++

## Ingredients {#ingredients}

| Quantity | Item                                                    |
|----------|---------------------------------------------------------|
| 3/4 oz   | [bourbon]({{< relref "../items/bulleit_bourbon.md" >}}) |
| 3/4 oz   | [Aperol]({{< relref "../items/aperol.md" >}})           |
| 3/4 oz   | [amaro]({{< relref "../items/lucano_amaro.md" >}})      |
| 3/4 oz   | [lemon juice]({{< relref "../items/lemon_juice.md" >}}) |


## Directions {#directions}

1.  Add ice all ingredients to a cocktail shaker; shake until cool
2.  Strain into a coupe glass


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [Liquor.com](https://www.liquor.com/recipes/the-paper-plane/)
