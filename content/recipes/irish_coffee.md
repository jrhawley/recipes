+++
title = "Irish coffee"
author = ["James Hawley"]
date = 2022-11-12T23:42:00-05:00
lastmod = 2023-12-21T22:06:35-05:00
tags = ["alcohols", "recipes", "beverages"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 10 min     |
| Cook Time | 0 min      |
| Yields    | 1 cocktail |


## Ingredients {#ingredients}

| Quantity | Item                                                          |
|----------|---------------------------------------------------------------|
| 2 oz     | [whisky]({{< relref "../items/whiskey.md" >}})                |
| 1 oz     | [simple syrup]({{< relref "simple_syrup.md" >}})              |
|          | [coffee]({{< relref "../items/coffee.md" >}})                 |
|          | [whipping cream]({{< relref "../items/whipping_cream.md" >}}) |


## Directions {#directions}

1.  Brew the fresh coffee using your preferred method.
2.  Just before the coffee is finished brewing, pour some hot water into a mug to heat it.
    -   Rinse the water around the mug to heat evenly, then pour it out.
3.  Add the whiskey and simple syrup, then fill the mug to 3/4 full with coffee.
4.  Pour the whipping cream in a cocktail shaker witout ice and lightly shake it.
5.  Float the cream on the drink by placing a spoon just under the surface and slowly pouring the lightly whipped cream into it.
    -   Gently remove the spoon without stirring.


## References &amp; Notes {#references-and-notes}

1.  Original recipe: The Periodic Table of Cocktails
