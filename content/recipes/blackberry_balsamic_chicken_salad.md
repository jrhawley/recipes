+++
title = "Blackberry balsamic chicken salad"
author = ["James Hawley"]
date = 2019-08-12T00:00:00-04:00
lastmod = 2024-05-23T21:39:57-04:00
tags = ["chicken", "salads", "recipes", "lunch", "entrees"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 30 min     |
| Cook Time | 10 min     |
| Yields    | 4 servings |

{{< figure src="../assets/blackberry-balsamic-salad.jpg" caption="<span class=\"figure-number\">Figure 1: </span>Blackberry balsamic chicken salad" >}}


## Ingredients {#ingredients}


### Vinaigrette {#vinaigrette}

| Quantity | Item                                                              |
|----------|-------------------------------------------------------------------|
| 1/2 cup  | [blackberries]({{< relref "../items/blackberry.md" >}})           |
| 2 Tbsp   | [balsamic vinegar]({{< relref "../items/balsamic_vinegar.md" >}}) |
| 2 Tbsp   | [olive oil]({{< relref "../items/olive_oil.md" >}})               |
| 2 Tbsp   | [honey]({{< relref "../items/honey.md" >}})                       |
| 2 tsp    | [dijon mustard]({{< relref "../items/dijon_mustard.md" >}})       |
| 1 tsp    | [soy sauce]({{< relref "../items/soy_sauce.md" >}})               |
| 1 clove  | [garlic]({{< relref "../items/garlic.md" >}}), minced             |
|          | [kosher salt]({{< relref "../items/kitchen_salt.md" >}})          |
|          | [pepper]({{< relref "../items/black_pepper.md" >}})               |


### Cheese {#cheese}

| Quantity | Item                                                                            |
|----------|---------------------------------------------------------------------------------|
| 8 oz     | [goat cheese]({{< relref "../items/goat_cheese.md" >}}), sliced into 1/4" discs |
| 1/4 cup  | [flour]({{< relref "../items/flour.md" >}})                                     |
| 1        | [egg]({{< relref "../items/eggs.md" >}}), lightly beaten                        |
| 1 cup    | [breadcrumbs]({{< relref "../items/breadcrumbs.md" >}})                         |


### Salad {#salad}

| Quantity | Item                                                           |
|----------|----------------------------------------------------------------|
| 1/2 lbs  | [chicken breasts]({{< relref "../items/chicken_breast.md" >}}) |
| 6 cups   | [spinach]({{< relref "../items/spinach.md" >}})                |
| 1 cup    | [blackberries]({{< relref "../items/blackberry.md" >}})        |
| 1 cup    | [avocado]({{< relref "../items/avocado.md" >}})                |
| 1/4 cup  | [red onion]({{< relref "../items/red_onion.md" >}}), sliced    |
| 1/4 cup  | [walnuts]({{< relref "../items/walnut.md" >}})                 |


## Directions {#directions}

1.  Puree vinaigrette ingredients in a blender, store separately
2.  Dredge goat cheese slices in the flower
    1.  Coat with eggs, then breadcrumbs
    2.  Fry in oil over medium heat until golden
    3.  Set aside on paper towels to drain
3.  Marinate chicken in half of the vinaigrette for 30 min
    1.  Grill over medium-high heat until slightly charred
4.  Assemble all ingredients and serve


## References &amp; Notes {#references-and-notes}

1.  [Original recipe](https://www.closetcooking.com/blackberry-balsamic-grilled-chicken/)
