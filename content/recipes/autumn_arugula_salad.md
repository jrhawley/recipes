+++
title = "Autumn arugula salad"
author = ["James Hawley"]
date = 2020-12-22T00:00:00-05:00
lastmod = 2024-05-23T21:22:43-04:00
tags = ["salads", "recipes", "vegetarian", "entrees"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 10 min     |
| Cook Time | 30 min     |
| Yields    | 4 servings |


## Ingredients {#ingredients}


### Salad {#salad}

| Quantity | Item                                                                |
|----------|---------------------------------------------------------------------|
| 2 Tbsp   | [olive oil]({{< relref "../items/olive_oil.md" >}})                 |
| 1        | [acorn squash]({{< relref "../items/acorn_squash.md" >}})           |
| 1/4 tsp  | [table salt]({{< relref "../items/table_salt.md" >}})               |
| 1/4 tsp  | [black pepper]({{< relref "../items/black_pepper.md" >}})           |
| 2 tsp    | [brown sugar]({{< relref "../items/brown_sugar.md" >}})             |
| 1/2 cup  | [pecans]({{< relref "../items/pecan.md" >}}), chopped               |
| 1/4 tsp  | [pumpkin pie spice]({{< relref "../items/pumpkin_pie_spice.md" >}}) |
| 6 cup    | [arugula]({{< relref "../items/arugula.md" >}})                     |
| 1        | [avocado]({{< relref "../items/avocado.md" >}})                     |
| 1/2 cup  | [pomegranate seeds]({{< relref "../items/pomegranate_aril.md" >}})  |
| 1        | [cucumber]({{< relref "../items/cucumber.md" >}}), sliced           |


### Vinaigrette {#vinaigrette}

| Quantity | Item                                                                    |
|----------|-------------------------------------------------------------------------|
| 1/3 cup  | [pomegranate juice]({{< relref "../items/pomegranate_juice.md" >}})     |
| 1/4 cup  | [apple cider vinegar]({{< relref "../items/apple_cider_vinegar.md" >}}) |
| 1/2 tsp  | [ginger]({{< relref "../items/ginger.md" >}}), freshly grated           |
| 1 clove  | [garlic]({{< relref "../items/garlic.md" >}}), grated                   |
| 1/4 tsp  | [table salt]({{< relref "../items/table_salt.md" >}})                   |
| 1/4 tsp  | [black pepper]({{< relref "../items/black_pepper.md" >}})               |
| 1/4 cup  | [olive oil]({{< relref "../items/olive_oil.md" >}})                     |


## Directions {#directions}

1.  Preheat oven to 220 C (425 F)
    -   Slice acorn squash into 1 cm chunks, removing seeds
    -   Place on a baking sheet, cover with olive oil, salt, pepper, and brown sugar
    -   Place in over, roast for 10 min, flip, and bake for another 10 min
2.  In a small saucepan over low heat, toss pecans
    -   Toast until golden and fragment, adding a bit of brown sugar and pumpkin pie spice
3.  Slice avocado and cucumber, add remaining ingredients into a large bowl
    -   When the squash is done, cut into smaller pieces and add to salad
4.  Combine all vinaigrette ingredients in a small bowl, drizzle over salad as desired


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [How Sweet Eats](https://howsweeteats.com/wprm_print/59460)
