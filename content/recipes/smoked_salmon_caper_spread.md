+++
title = "Smoked salmon and caper spread"
author = ["James Hawley"]
date = 2022-09-27T08:57:00-04:00
lastmod = 2023-12-22T01:10:39-05:00
tags = ["appetizers", "recipes"]
draft = false
+++

| Info      | Amount         |
|-----------|----------------|
| Prep Time | 5 min          |
| Cook Time | 0 min          |
| Yields    | 6 - 8 servings |


## Ingredients {#ingredients}

| Quantity | Item                                                                          |
|----------|-------------------------------------------------------------------------------|
| 225 g    | [cream cheese]({{< relref "../items/cream_cheese.md" >}}) at room temperature |
| 1 Tbsp   | [milk]({{< relref "../items/milk.md" >}})                                     |
| 1/4 cup  | [parsley]({{< relref "../items/parsley.md" >}}), chopped                      |
| 2 Tbsp   | [lemon juice]({{< relref "../items/lemon_juice.md" >}})                       |
| 1 tsp    | [capers]({{< relref "../items/caper.md" >}}), rinsed and drained              |
| 115 g    | [smoked salmon]({{< relref "../items/smoked_salmon.md" >}})                   |
| 1/2      | [red onion]({{< relref "../items/red_onion.md" >}})                           |
|          | [black pepper]({{< relref "../items/black_pepper.md" >}})                     |
|          | crackers or baguette slices                                                   |


## Directions {#directions}

1.  Combine cream cheese, milk, parsley, lemon juice, and half of the salmon in a food processor until smooth.
    -   Add black pepper and remaining salmon, pulsing to incorporate them without blending smooth.
2.  Spread on toasted bread, topping with capers and thin slices of onion.


## References &amp; Notes {#references-and-notes}

1.  Original recipe: Wine Bites - Barbara Scott-Goodman
2.  Mixture will keep when covered in the fridge for up to 2 days.
3.  Can simplify this recipe by just bringing out all ingredients and letting each person assemble them.
