+++
title = "Classic daiquiri"
author = ["James Hawley"]
date = 2020-05-18T00:00:00-04:00
lastmod = 2023-12-19T22:56:12-05:00
tags = ["recipes", "alcohols", "beverages"]
draft = false
+++

## Ingredients {#ingredients}

| Quantity | Item                                                  |
|----------|-------------------------------------------------------|
| 1/2 oz   | [Simple syrup]({{< relref "simple_syrup.md" >}})      |
| 5/3 oz   | [white rum]({{< relref "../items/white_rum.md" >}})   |
| 2/3 oz   | [lime juice]({{< relref "../items/lime_juice.md" >}}) |
|          | ice cubes                                             |
|          | [lime]({{< relref "../items/lime.md" >}})             |


## Directions {#directions}

1.  Add all ingredients to a cocktail shaker
2.  Strain into couple glasses
3.  (Optional) Garnish with a lime wedge or wheel


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [The Periodic Table of Cocktails]({{< relref "../references/Periodic_Table_of_Cocktails.md" >}})
