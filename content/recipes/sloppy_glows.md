+++
title = "Sloppy glows"
author = ["James Hawley"]
date = 2021-02-08T00:00:00-05:00
lastmod = 2024-05-23T21:21:36-04:00
tags = ["lunch", "sandwiches", "recipes", "vegan", "vegetarian", "entrees"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 15 min     |
| Cook Time | 15 min     |
| Yields    | 4 servings |

{{< figure src="../assets/sloppy-glows.png" caption="<span class=\"figure-number\">Figure 1: </span>Sloppy glows" >}}


## Ingredients {#ingredients}

| Quantity | Item                                                                      |
|----------|---------------------------------------------------------------------------|
| 4        | [hamburger buns]({{< relref "../items/bun.md" >}})                        |
| 1        | [red onion]({{< relref "../items/onion.md" >}})                           |
| 2 Tbsp   | [olive oil]({{< relref "../items/olive_oil.md" >}})                       |
| 4 cloves | [garlic]({{< relref "../items/garlic.md" >}})                             |
| 2 tsp    | [cumin]({{< relref "../items/cumin.md" >}})                               |
| 1 cup    | [tomato sauce]({{< relref "../items/tomato_sauce.md" >}})                 |
| 2/3 cup  | [roasted red peppers]({{< relref "../items/roasted_red_pepper.md" >}})    |
| 1/3 cup  | [sun-dried tomatoes]({{< relref "../items/sun-dried_tomato.md" >}})       |
| 400 mL   | [lentils]({{< relref "../items/lentil.md" >}})                            |
| 2 Tbsp   | [Worcestershire sauce]({{< relref "../items/worcestershire_sauce.md" >}}) |
| 2 Tbsp   | [brown sugar]({{< relref "../items/brown_sugar.md" >}})                   |
| 1 tsp    | [apple cider vinegar]({{< relref "../items/apple_cider_vinegar.md" >}})   |
|          | [kosher salt]({{< relref "../items/kitchen_salt.md" >}})                  |
|          | [pepper]({{< relref "../items/black_pepper.md" >}})                       |


## Directions {#directions}

1.  Slice red onion in half
    1.  Slice one half into thin circles for garnishing, later
    2.  Finely chop the other half
2.  In a large skillet, heat oil over medium heat
    1.  Add finely chopped onion and garlic
    2.  Saute for 5 - 6 min
3.  Stir in chili powder and cumin
    1.  Cook for a minute
4.  Add tomato sauce, roasted red peppers, sun-dried tomatoes, lentils, Worcestershire sauce, brown sugar, salt, and black pepper
    1.  Stir to combine
    2.  Raise to medium-high heat, bring to a rapid simmer
    3.  Reduce the heat to medium, simmer uncovered, stirring occasionally for 5 - 8 min
5.  Stir vinegar into mixture
    1.  Add most salt, pepper, Worcestershire sauce, and/or sugar to taste
    2.  Reduce to low heat, cover, simmer until ready to serve


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [Oh She Glows for Dinner]({{< relref "../references/Oh_She_Glows_for_Dinner.md" >}})
