+++
title = "Crumpets"
author = ["James Hawley"]
date = 2023-02-04T13:33:00-05:00
lastmod = 2024-06-12T19:13:49-04:00
tags = ["breakfasts", "recipes"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 40 min     |
| Cook Time | 10 min     |
| Yields    | 8 crumpets |


## Ingredients {#ingredients}

| Quantity | Item                                                              |
|----------|-------------------------------------------------------------------|
| 3/2 cup  | [dairy milk]({{< relref "../items/dairy_milk.md" >}})             |
| 1 Tbsp   | [sugar]({{< relref "../items/white_sugar.md" >}})                 |
| 2 tsp    | [quick-rise yeast]({{< relref "../items/quick-rise_yeast.md" >}}) |
| 1 cup    | [flour]({{< relref "../items/flour.md" >}})                       |
| 1 tsp    | [baking powder]({{< relref "../items/baking_powder.md" >}})       |
| 1/2 tsp  | fine [table salt]({{< relref "../items/table_salt.md" >}})        |
| 4 Tbsp   | [butter]({{< relref "../items/butter.md" >}}), divided            |
|          | [jam]({{< relref "../items/jam.md" >}}), to serve                 |


## Directions {#directions}

1.  In a small pot over medium heat, warm the milk until it reaches 45 C (110 F).
    1.  Remove from heat, sprinkle sugar over top, and stir to combine.
    2.  Sprinkle the yeast over top and let sit 10 min to bloom and froth.
2.  In a mixing bowl, whisk together flower, baking powder, and salt.
    1.  Make a well in the centre of the mix and pour the milk-yeast mixture.
    2.  Starting in the centre and working outwards, whisk to incorporate all the flower&nbsp;[^fn:1].
3.  Cover with a tea towl and let rest for 30 min in a warm, draft-free place until bubbles start forming on top and the batter has risen.
4.  In a large, heavy-bottomed skillet over medium heat, melt 2 Tbsp of the butter.
    1.  Use 1 tsp of the remaining butter to grease the inside of each egg ring.
    2.  Place the egg rings in the skillet and scoop batter into each ring.
    3.  Cook for 5 min, then remove the rings with tongs and flip each crumpet.
    4.  Cook for another 30 - 45 s, then remove.
5.  Repeat with the remaining batter, and serve with jam.


## References {#references}

1.  Original recipe: [A Rising Tide]({{< relref "../references/A_Rising_Tide.md" >}})
2.  The crumpets will keep in an airtight container in the fridge for up to 1 week, or in the freezer for 3 months.

[^fn:1]: This will still be quite watery at this point, which is okay.
