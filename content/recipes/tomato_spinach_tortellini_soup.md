+++
title = "Tomato spinach tortellini soup"
author = ["James Hawley"]
date = 2020-10-20T00:00:00-04:00
lastmod = 2024-05-23T21:28:29-04:00
tags = ["soups", "recipes", "vegetarian", "entrees"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 10 min     |
| Cook Time | 20 min     |
| Yields    | 4 servings |

{{< figure src="../assets/tomato-spinach-tortellini-soup.jpg" caption="<span class=\"figure-number\">Figure 1: </span>Tomato spinach tortellini soup" >}}


## Ingredients {#ingredients}

| Quantity | Item                                                                |
|----------|---------------------------------------------------------------------|
| 2 Tbsp   | [butter]({{< relref "../items/butter.md" >}})                       |
| 1        | [onion]({{< relref "../items/onion.md" >}}), diced                  |
| 2 cloves | [garlic]({{< relref "../items/garlic.md" >}}), chopped              |
| 1/4 cup  | [all-purpose flour]({{< relref "../items/all-purpose_flour.md" >}}) |
| 3 cups   | [vegetable broth]({{< relref "../items/vegetable_broth.md" >}})     |
| 28 oz    | [diced tomatoes]({{< relref "../items/diced_tomatoes.md" >}})       |
| 2 Tbsp   | [tomato paste]({{< relref "../items/tomato_paste.md" >}})           |
| 8 oz     | cheese [tortellini]({{< relref "../items/tortellini.md" >}})        |
| 1/2 cup  | [parmesan]({{< relref "../items/parmesan.md" >}}), grated           |
| 10 oz    | [spinach]({{< relref "../items/spinach.md" >}}), coarsely chopped   |
| 1/2 cup  | [Greek yogurt]({{< relref "../items/greek_yogurt.md" >}})           |
| 1/4 cup  | [basil]({{< relref "../items/basil.md" >}}), chopped (optional)     |
|          | [kosher salt]({{< relref "../items/kitchen_salt.md" >}})            |
|          | [pepper]({{< relref "../items/black_pepper.md" >}})                 |
|          | [red pepper flakes]({{< relref "../items/red_pepper_flake.md" >}})  |


## Directions {#directions}

1.  Melt the butter in a pan over medium heat
    1.  add the onion and cook until tender, about 5-7 min
    2.  Add the garlic and red pepper flakes and cook until fragrant, about 1 min
    3.  Add the flour and cook for another minute
2.  Add the broth, tomatoes, tomato paste and tortellini, bring to a boil
    1.  Reduce the heat and simmer until the tortellini is tender, about 10 min
3.  Add the parmesan, let it melt
    1.  Add the spinach, let it wilt
    2.  Add the cream, season with salt and pepper to taste
    3.  Remove from heat before adding the basil


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [Closet Cooking](https://www.closetcooking.com/creamy-parmesan-tomato-and-spinach/)
