+++
title = "Toronto (cocktail)"
author = ["James Hawley"]
date = 2022-11-19T19:44:00-05:00
lastmod = 2023-12-24T20:03:51-05:00
tags = ["alcohols", "recipes", "beverages"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 2 min      |
| Cook Time | 0 min      |
| Yields    | 1 cocktail |


## Ingredients {#ingredients}

| Quantity | Item                                                                |
|----------|---------------------------------------------------------------------|
| 2 oz     | [rye whiskey]({{< relref "../items/rye_whiskey.md" >}})             |
| 1/4 oz   | [Fernet-Branca]({{< relref "../items/fernet_branca.md" >}})         |
| 1/4 oz   | [simple syrup]({{< relref "simple_syrup.md" >}})                    |
| 2 dashes | [Angostura bitters]({{< relref "../items/angostura_bitters.md" >}}) |


## Directions {#directions}

1.  Pour all ingredients into a mixing glass with ice. Stir until chilled.
2.  Strain into a coupe glass and garnish with an orange twist.


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [Liquor.com](https://www.liquor.com/recipes/toronto/)
