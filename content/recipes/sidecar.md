+++
title = "Sidecar"
author = ["James Hawley"]
date = 2022-11-13T00:25:00-05:00
lastmod = 2023-12-22T01:07:19-05:00
tags = ["alcohols", "recipes", "beverages"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 2 min      |
| Cook Time | 0 min      |
| Yields    | 1 cocktail |


## Ingredients {#ingredients}

| Quantity | Item                                                    |
|----------|---------------------------------------------------------|
| 2 oz     | cognac                                                  |
| 1 oz     | [Triple Sec]({{< relref "../items/triple_sec.md" >}})   |
| 1 oz     | [lemon juice]({{< relref "../items/lemon_juice.md" >}}) |


## Directions {#directions}

1.  Add all ingredients to a cocktail shaker, shake hard, and strain into a chilled coupe glass.
2.  Garnish with a strip of lemon zest.


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [The Periodic Table of Cocktails]({{< relref "../references/Periodic_Table_of_Cocktails.md" >}})
