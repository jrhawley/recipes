+++
title = "Sun-dried tomato chicken and farro"
author = ["James Hawley"]
date = 2022-12-28T21:27:00-05:00
lastmod = 2024-05-23T20:58:57-04:00
tags = ["entrees", "chicken", "recipes"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 10 min     |
| Cook Time | 30 min     |
| Yields    | 4 servings |


## Ingredients {#ingredients}

| Quantity | Item                                                                   |
|----------|------------------------------------------------------------------------|
| 2 Tbsp   | [olive oil]({{< relref "../items/olive_oil.md" >}})                    |
| 4        | [chicken breasts]({{< relref "../items/chicken_breast.md" >}})         |
| 2 Tbsp   | [balsamic vinegar]({{< relref "../items/balsamic_vinegar.md" >}})      |
| 1 Tbsp   | [oregano]({{< relref "../items/oregano.md" >}})                        |
| 1 Tbsp   | [paprika]({{< relref "../items/paprika.md" >}})                        |
| 2 cloves | [garlic]({{< relref "../items/garlic.md" >}}), minced                  |
| 1 cup    | [farro]({{< relref "../items/farro.md" >}}), uncooked                  |
| 5/2 cup  | [vegetable broth]({{< relref "../items/vegetable_broth.md" >}})        |
| 2 cup    | [spinach]({{< relref "../items/spinach.md" >}})                        |
| 1/2 cup  | [sun-dried tomatoes]({{< relref "../items/sun-dried_tomato.md" >}})    |
| 1/3 cup  | [kalamata olives]({{< relref "../items/kalamata_olive.md" >}}), pitted |
| 1        | [lemon]({{< relref "../items/lemon.md" >}}), juiced                    |
|          | [Kosher salt]({{< relref "../items/kitchen_salt.md" >}})               |
|          | [black pepper]({{< relref "../items/black_pepper.md" >}})              |
|          | [feta]({{< relref "../items/feta.md" >}}) (optional)                   |


## Directions {#directions}

1.  Preheat oven to 200 C (400 F).
2.  In a medium bowl, combine 2 Tbsp olive oil, chicken, balsamic vinegar, oregano, paprika, garlic, salt, and pepper. Toss to combine.
3.  Heat 2 Tbsp of olive oil in a large oven-safe skillet or dutch oven over medium-high heat.
    1.  Add the chicken and sear on both sides until golden (3 - 5 min / side).
    2.  Remove chicken from skillet.
4.  Add the farro to the skillet, cooking for 2 - 3 min.
    1.  Add the broth, spinach, sun-dried tomatoes, olives, and lemon juice.
    2.  Bring to a boil over high heat and stir.
    3.  Slide in the chicken and vegetables, then cover and place in the oven for 20 min.
5.  Garnish with feta, dill, oregano, or pine nuts and serve.


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [Half-Baked Harvest](https://www.halfbakedharvest.com/wprm_print/73130)
