+++
title = "Mulberry-stained meringue cookies"
author = ["James Hawley"]
date = 2023-03-12T09:12:00-04:00
lastmod = 2023-12-21T22:16:17-05:00
tags = ["desserts", "recipes"]
draft = false
+++

| Info      | Amount        |
|-----------|---------------|
| Prep Time | 15 min        |
| Cook Time | 1 h           |
| Yields    | 6 - 8 cookies |


## Ingredients {#ingredients}

| Quantity | Item                                                              |
|----------|-------------------------------------------------------------------|
| 4        | [egg]({{< relref "../items/eggs.md" >}}) whites, room temperature |
| 1/8 tsp  | [cream of tartar]({{< relref "../items/cream_of_tartar.md" >}})   |
| 1/8 tsp  | [Kosher salt]({{< relref "../items/kitchen_salt.md" >}})          |
| 1 cup    | [granular sugar]({{< relref "../items/white_sugar.md" >}})        |
| 1 tsp    | [vanilla extract]({{< relref "../items/vanilla_extract.md" >}})   |
| 2 cup    | [mulberries]({{< relref "../items/mulberry.md" >}}) [^fn:1]       |


## Directions {#directions}

1.  Preheat the oven to 110 C (225 F) and line a baking sheet with parchment paper
2.  Place the egg whites, cream of tartar, and salt in a medium mixing bowl or a stand mixer fitted with the whisk attachment
    1.  Turn on the mixer, or use a handheld electric mixer, and mix on low speed until the egg whites foam
    2.  Increase the speed to high and gradually add the sugar
    3.  Add more sugar as soon as the last spoonful has dissolved&nbsp;[^fn:2]
    4.  Continue to beat until stiff peaks form, then gently stir in the vanilla
3.  Use a large spoon to scoop out about 1/4 cup of the fluffy meringue onto the prepared baking sheet&nbsp;[^fn:3]
    1.  Repeat with the rest of the meringue
4.  Bake for 1 h
    1.  Turn off the oven after the allotted time and place a wooden spoon in the door to let the oven cool with the baking sheet full of meringues still in the oven&nbsp;[^fn:4]
    2.  If you choose to store them in an airtight container, place them in the refrigerator and avoid high-moisture areas, as this can soften your meringues
5.  The meringues are est when cracked with a spoon and showered in mulberries that bleed into the crevices

[^fn:1]: Substitute mulberries for [raspberries]({{< relref "../items/raspberries.md" >}}) or [blackberries]({{< relref "../items/blackberry.md" >}}) if needed
[^fn:2]: To test this, rub a small bit of the mixture between your fingers. If it feels gritty, the sugar has not fully dissolved.
[^fn:3]: Do not sweat over distancing the meringues; they will not spread like cookies when they bake
[^fn:4]: This will allow the meringues to cool completely and crisp up nicely