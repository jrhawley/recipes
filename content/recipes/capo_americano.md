+++
title = "Capo Americano"
author = ["James Hawley"]
date = 2021-09-25T00:00:00-04:00
lastmod = 2023-12-19T22:18:52-05:00
tags = ["recipes", "alcohols", "beverages"]
draft = false
+++

## Ingredients {#ingredients}

| Quantity | Item                                                            |
|----------|-----------------------------------------------------------------|
| 3/2 oz   | [amaro]({{< relref "../items/lucano_amaro.md" >}})              |
| 3/2 oz   | [sweet red vermouth]({{< relref "../items/dry_vermouth.md" >}}) |
| 3/2 oz   | [tonic water]({{< relref "../items/tonic_water.md" >}})         |
| 1        | [lemon wedge]({{< relref "../items/lemon.md" >}})               |


## Directions {#directions}

1.  Fill a rocks glass with ice.
2.  Add amaro and vermouth and stir to combine.
3.  Top with tonic water and squeeze the lemon wedge to add a splash of lemon juice.
4.  Garnish with the lemon wedge.


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [LCBO](https://www.lcbo.com/webapp/wcs/stores/servlet/en/lcbo/recipe/capo-americano/F202105041)
