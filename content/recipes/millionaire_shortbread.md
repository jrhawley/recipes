+++
title = "Millionaire Shortbread"
author = ["James Hawley"]
date = 2018-03-30T00:00:00-04:00
lastmod = 2023-12-22T00:50:28-05:00
tags = ["recipes", "desserts"]
draft = false
+++

| Info      | Amount |
|-----------|--------|
| Prep Time | 10 min |
| Cook Time | 20 min |
| Yields    |        |


## Ingredients {#ingredients}


### Shortbread {#shortbread}

| Quantity | Item                                                                |
|----------|---------------------------------------------------------------------|
| 4 oz     | [butter]({{< relref "../items/butter.md" >}})                       |
| 2 oz     | [white sugar]({{< relref "../items/white_sugar.md" >}})             |
| 6 oz     | [all-purpose flour]({{< relref "../items/all-purpose_flour.md" >}}) |


### Caramel {#caramel}

| Quantity | Item                                                                          |
|----------|-------------------------------------------------------------------------------|
| 8 oz     | [butter]({{< relref "../items/butter.md" >}})                                 |
| 4 Tbsp   | [maple syrup]({{< relref "../items/maple_syrup.md" >}})                       |
| 4 oz     | [white sugar]({{< relref "../items/white_sugar.md" >}})                       |
| 400 oz   | [condensed milk]({{< relref "../items/condensed_milk.md" >}})                 |
|          | drops of [vanilla extract]({{< relref "../items/vanilla_extract.md" >}})      |
| 100 g    | [milk chocolate]({{< relref "../items/chocolate_milk.md" >}}) (bars or chips) |


## Directions {#directions}

1.  Cream together margarine and sugar, then mix in flour
2.  Line a square baking tray with parchment paper or grease
3.  Cook at 180 C/350 F until golden (about 20 min? possibly?)
4.  While shortbread is baking, melt together caramel ingredients and boil for 5 min
5.  Pour over shortbread and let set
6.  Melt chocolate and pour on top of caramel layer
7.  Refrigerate for 1 h before serving


## References &amp; Notes {#references-and-notes}

1.  Original recipe: Julie Milner
