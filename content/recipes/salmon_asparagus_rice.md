+++
title = "Salmon, asparagus, and rice"
author = ["James Hawley"]
date = 2015-03-05T00:00:00-05:00
lastmod = 2024-05-23T21:44:31-04:00
tags = ["fish", "recipes", "entrees"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 10 min     |
| Cook Time | 20 min     |
| Yields    | 4 servings |


## Ingredients {#ingredients}

| Quantity | Item                                                       |
|----------|------------------------------------------------------------|
| 4 filets | [salmon]({{< relref "../items/salmon.md" >}})              |
| 1 lb     | [asparagus]({{< relref "../items/asparagus.md" >}})        |
| 3 Tbsp   | [olive oil]({{< relref "../items/olive_oil.md" >}})        |
| 2 cloves | [garlic]({{< relref "../items/garlic.md" >}}), minced      |
| 1        | [lemon]({{< relref "../items/lemon.md" >}}), thinly sliced |
|          | [kosher salt]({{< relref "../items/kitchen_salt.md" >}})   |
|          | [pepper]({{< relref "../items/black_pepper.md" >}})        |
|          | [dill]({{< relref "../items/dill.md" >}})                  |
|          | [thyme]({{< relref "../items/thyme.md" >}})                |
|          | [rosemary]({{< relref "../items/rosemary.md" >}})          |
|          | [parsley]({{< relref "../items/parsley.md" >}})            |


## Directions {#directions}

1.  Preheat oven to 400 F and tear four sheets of aluminium foil
2.  Divide asparagus into 4 equal portions and layer each in the centre of each strip of foil
3.  In a small bowl, stir together olive oil, garlic, salt, and pepper
    1.  Drizzle each filet with the oil mixture
4.  Rinse salmon and allow excess water to run off
    1.  Season each filet with salt and pepper, and any or all of dill, thyme, rosemary, and parsley
5.  Layer filets over each stack of asparagus
    1.  Drizzle remaining olive oil mixture over filets
    2.  Top each with 2 sprigs of dill (or 1-2 tsp of other herbs) and 2 lemon slices
    3.  Wrap sides of the foil inwards, over the salmon, to enclose
6.  Place foil pouches on a baking sheet
    1.  Bake for approximately 20 min, depending on thickness of filets
