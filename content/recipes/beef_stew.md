+++
title = "Beef stew"
author = ["James Hawley"]
date = 2018-02-03T00:00:00-05:00
lastmod = 2024-05-23T20:46:04-04:00
tags = ["soups", "recipes", "entrees"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 20 min     |
| Cook Time | 7 h        |
| Yields    | 4 servings |


## Ingredients {#ingredients}

| Quantity | Item                                                                                |
|----------|-------------------------------------------------------------------------------------|
| 2 lbs    | [beef chuck]({{< relref "../items/beef_chuck.md" >}}), cut into 1" pieces           |
| 1/4 cup  | [flour]({{< relref "../items/flour.md" >}})                                         |
| 3 Tbsp   | [canola oil]({{< relref "../items/canola_oil.md" >}})                               |
| 1        | [carrots]({{< relref "../items/baby_carrot.md" >}})                                 |
| 1        | [onion]({{< relref "../items/onion.md" >}})                                         |
| 2 cloves | [garlic]({{< relref "../items/garlic.md" >}}), minced                               |
| 12 oz    | stout beer, divided                                                                 |
| 3/2 cups | [chicken stock]({{< relref "../items/chicken_broth.md" >}})                         |
| 1        | [Yukon gold potato]({{< relref "../items/russet_potato.md" >}}), cut into 1" chunks |
| 3 sprigs | [thyme]({{< relref "../items/thyme.md" >}})                                         |
| 2 Tbsp   | [molasses]({{< relref "../items/fancy_molasses.md" >}})                             |
| 1 Tbsp   | [tomato paste]({{< relref "../items/tomato_paste.md" >}})                           |
| 1 tsp    | [table salt]({{< relref "../items/table_salt.md" >}})                               |
| 1/2 cup  | [frozen peas]({{< relref "../items/pea.md" >}})                                     |
| 1/2 cup  | fresh [parsley]({{< relref "../items/parsley.md" >}}), chopped                      |
|          | [kosher salt]({{< relref "../items/kitchen_salt.md" >}})                            |
|          | [pepper]({{< relref "../items/black_pepper.md" >}})                                 |


## Directions {#directions}

1.  Season the beef with salt and pepper then dredge in flour
2.  In a large skillet set over high, heat 1 Tbsp of the canola oil
    1.  Sear beef in a single layer in skillet until deep brown on all sides, about 10 to 12 minutes
    2.  Remove beef from pan and place in the pot of a 4-quart slow cooker
3.  Add the remaining oil to the skillet
    1.  Add carrot, onion and garlic
    2.  Cook until vegetables are fragrant and beginning to brown, about 3 minutes
    3.  Pour half the beer over vegetables
    4.  Using a wooden spoon, scrape the bottom of the pan, lifting any brown bits
4.  Add contents of the pan into slow cooker
    1.  Pour in remaining beer and stock and add in potatoes, thyme, molasses, tomato paste and salt
    2.  Stir to combine
    3.  Cover slow cooker and cook on low for 7 hours
5.  During the last 10 min of cooking, stir in green peas
6.  Discard thyme sprigs. Serve, garnished with fresh parsley.


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [Food Network](http://www.foodnetwork.ca/recipe/slow-cooker-canadian-stout-and-alberta-beef-stew/20790/)
