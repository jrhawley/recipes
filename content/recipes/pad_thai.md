+++
title = "Pad Thai"
author = ["James Hawley"]
date = 2017-11-27T00:00:00-05:00
lastmod = 2024-04-17T17:27:44-04:00
tags = ["chicken", "recipes", "entrees"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 15 min     |
| Cook Time | 15 min     |
| Yields    | 4 servings |


## Ingredients {#ingredients}


### Sauce {#sauce}

| Quantity | Item                                                        |
|----------|-------------------------------------------------------------|
| 1/3 cup  | [soy sauce]({{< relref "../items/soy_sauce.md" >}})         |
| 2 Tbsp   | [maple syrup]({{< relref "../items/maple_syrup.md" >}})     |
| 2 Tsp    | [fish sauce]({{< relref "../items/fish_sauce.md" >}})       |
| 2 Tbsp   | [oyster sauce]({{< relref "../items/oyster_sauce.md" >}})   |
| 2 Tbsp   | [vegetable oil]({{< relref "../items/vegetable_oil.md" >}}) |


### Noodles {#noodles}

| Quantity | Item                                                             |
|----------|------------------------------------------------------------------|
| 6 oz     | [rice noodles]({{< relref "../items/rice_noodle.md" >}})         |
| 2 Tbsp   | [vegetable oil]({{< relref "../items/vegetable_oil.md" >}})      |
| 1 lb     | [chicken breasts]({{< relref "../items/chicken_breast.md" >}})   |
| 1        | [bell pepper]({{< relref "../items/bell_pepper.md" >}})          |
| 1        | [shallot]({{< relref "../items/shallot.md" >}}), sliced          |
| 2 cloves | [garlic]({{< relref "../items/garlic.md" >}})                    |
| 2        | [eggs]({{< relref "../items/eggs.md" >}}), beaten                |
| 2 cup    | [bean sprouts]({{< relref "../items/bean_sprout.md" >}})         |
| 1/4 cup  | [carrots]({{< relref "../items/carrot.md" >}}), shredded         |
| 4        | [green onion]({{< relref "../items/green_onion.md" >}}), chopped |
| 1/3 cup  | [peanuts]({{< relref "../items/peanut.md" >}}), chopped          |
|          | [lime juice]({{< relref "../items/lime_juice.md" >}})            |


## Directions {#directions}

1.  Cook rice noodles before adding to pan
    1.  Boil water in a pot, remove from heat
    2.  Set the noodles in it for ~10 mins until they're soft but not mushy
2.  Cook chicken in oil
    1.  When sealed (aka whole outside is white) add eggs into the pan and immediately mix around.
    2.  Add the shallot, garlic, and bell pepper with some pepper and saute.
3.  Mix all sauce ingredients in a small bowl, then pour down the sides of the wok.
    1.  Add noodles to the pan and toss to combine.
    2.  After 1 - 2 min, push the noodles to one side and add the eggs to the other.
    3.  Allow the edges of the eggs to begin to set, then scramble the egg and toss with the noodles.
4.  Add sprouts and carrots and mix a bit.
    1.  Take pan off heat 1-2 mins after you've added the remaining vegetables.
5.  Garnish with chopped peanuts and lime juice


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [Half Baked Harvest](https://www.halfbakedharvest.com/wprm_print/143345)
2.  Taste the noodles to ensure they're like al dente pasta before you put them in the pan.
    They won't cook that much in the pan.
3.  Don't worry if it doesn't seem that saucy.
    You don't want it saucy like pasta; the taste is too strong.
    You want it all covered in sauce but a bit dry
4.  [Tips for improving pad thai](https://www.youtube.com/watch?v=fdw031OP3rw)
    -   high quality fish sauce (Squid or Tiparos brand)
    -   medium sized noodles (3mm wide), cooked slowly with warm water, not boiling, finish cooking noodles with rest of the ingredients
        -   slightly softer than al dente pasta
    -   pressed tofu (firmer than extra firm), sliced to small pieces
    -   low heat to prevent sauce from boiling too quickly
    -   many of these are in some Asian grocery stores
5.  Pad Thai series
    -   [Pad Thai Recipe (ผัดไทย) - Part One: The Pan - SheSimmers](https://shesimmers.com/2011/05/pad-thai-recipe-part-one-pan-and.html)
    -   [Pad Thai Recipe (ผัดไทย) - Part Two: The Noodles - SheSimmers](https://shesimmers.com/2011/06/pad-thai-recipe-part-two-noodles.html)
    -   [Pad Thai Recipe (ผัดไทย) - Part Three: The Notable Ingredients](https://shesimmers.com/2011/06/pad-thai-recipe-part-three-notable.html)
    -   [Pad Thai Recipe (ผัดไทย) - Part Four: Pad Thai Sauce - SheSimmers](https://shesimmers.com/2011/11/pad-thai-recipe-part-four-pad-thai.html)
    -   [Pad Thai Recipe (ผัดไทย) - Part Five: Making Pad Thai - SheSimmers](https://shesimmers.com/2011/11/pad-thai-recipe-part-five-making-pad.html)
