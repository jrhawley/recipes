+++
title = "Tartar sauce"
author = ["James Hawley"]
date = 2022-10-17T12:28:00-04:00
lastmod = 2023-12-24T19:57:01-05:00
tags = ["sauces", "recipes"]
draft = false
+++

| Info      | Amount |
|-----------|--------|
| Prep Time | 10 min |
| Cook Time | 0 min  |
| Yields    | 2 cups |


## Ingredients {#ingredients}

| Quantity | Item                                                              |
|----------|-------------------------------------------------------------------|
| 1        | [shallot]({{< relref "../items/shallot.md" >}})                   |
| 1 clove  | [garlic]({{< relref "../items/garlic.md" >}})                     |
| 1 cup    | [mayo]({{< relref "../items/mayonnaise.md" >}})                   |
| 2 Tbsp   | [capers]({{< relref "../items/caper.md" >}})                      |
| 1 Tbsp   | [chives]({{< relref "../items/chive.md" >}})                      |
| 1 Tbsp   | [parsley]({{< relref "../items/parsley.md" >}})                   |
| 1 Tbsp   | [lemon juice]({{< relref "../items/lemon_juice.md" >}})           |
| 1 tsp    | [Dijon mustard]({{< relref "../items/dijon_mustard.md" >}})       |
| 1 tsp    | [whole grain mustard]({{< relref "../items/brown_mustard.md" >}}) |
|          | [Kosher salt]({{< relref "../items/kitchen_salt.md" >}})          |
|          | [black pepper]({{< relref "../items/black_pepper.md" >}})         |


## Directions {#directions}

1.  Finely chop the shallot, capers, chives, and parsley and grate the garlic.
2.  Whisk together all ingredients in a medium bowl and season with salt and pepper.


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [Bon Appetit](https://www.bonappetit.com/recipe/classic-tartar-sauce)
2.  Can last in the fridge, covered, for 1 week.
