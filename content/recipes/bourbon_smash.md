+++
title = "Bourbon Smash"
author = ["James Hawley"]
date = 2022-03-13T00:00:00-05:00
lastmod = 2023-12-19T22:07:12-05:00
tags = ["recipes", "alcohols", "beverages"]
draft = false
+++

## Ingredients {#ingredients}

| Quantity | Ingredient                                              |
|----------|---------------------------------------------------------|
| 6        | [raspberries]({{< relref "../items/raspberries.md" >}}) |
| 6        | [mint leaves]({{< relref "../items/mint.md" >}})        |
| 5 / 3 oz | [bourbon]({{< relref "../items/bulleit_bourbon.md" >}}) |
| 5 / 6 oz | [lime juice]({{< relref "../items/lime_juice.md" >}})   |
| 2 /3 oz  | [simple syrup](simple-syrup.md)                         |


## Directions {#directions}

1.  Gently muddle the raspberries and mint in the bottom of a cocktail shaker&nbsp;[^fn:1].
2.  Add the bourbon, lime juice, and simple syrup with ice to the shaker.
3.  Shake and strain into an ice-filled highball glass.
4.  Garnish with a lime wedge or mint sprig (or both).


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [The Periodic Table of Cocktails]({{< relref "../references/Periodic_Table_of_Cocktails.md" >}})

[^fn:1]: Be careful not to overly-muddle the mint, as it will bring out the bitter flavours in the leaves.