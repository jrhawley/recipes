+++
title = "Tortellini soup"
author = ["James Hawley"]
date = 2021-01-18T00:00:00-05:00
lastmod = 2024-04-01T23:43:19-04:00
tags = ["soups", "recipes", "entrees"]
draft = false
+++

| Info      | Amount |
|-----------|--------|
| Prep Time | 10 min |
| Cook Time | 15 min |
| Yields    | serves |


## Ingredients {#ingredients}

| Quantity | Item                                                            |
|----------|-----------------------------------------------------------------|
| 1        | [onion]({{< relref "../items/onion.md" >}}), finely diced       |
| 4 cloves | [garlic]({{< relref "../items/garlic.md" >}}), minced           |
| 1 L      | [vegetable broth]({{< relref "../items/vegetable_broth.md" >}}) |
| 400 mL   | [crushed tomatoes]({{< relref "../items/passata.md" >}})        |
| 2 Tbsp   | [tomato paste]({{< relref "../items/tomato_paste.md" >}})       |
| 300 mL   | [tortellini]({{< relref "../items/tortellini.md" >}})           |
| 250 mL   | [heavy cream]({{< relref "../items/heavy_cream.md" >}})         |
|          | [spinach]({{< relref "../items/spinach.md" >}})                 |
|          | [kosher salt]({{< relref "../items/kitchen_salt.md" >}})        |
|          | [pepper]({{< relref "../items/black_pepper.md" >}})             |
|          | [parmesan]({{< relref "../items/parmesan.md" >}})               |
|          | [sourdough]({{< relref "../items/sourdough.md" >}})             |


## Directions {#directions}

1.  Brown sausage until cooked half way through
2.  Add onions and garlic, cook until translucent
3.  Add chicken stock, crushed tomatoes, and tomato paste
4.  Season with salt and simmer for 5 min
5.  Add spinach, heavy cream, and tortellini
6.  Cook for 5 min, until tortellini is soft
7.  Garnish with shavings of parmesan cheese and sourdough slices
