+++
title = "The Erica"
author = ["James Hawley"]
date = 2022-10-04T12:31:00-04:00
lastmod = 2023-12-24T20:23:55-05:00
tags = ["alcohols", "recipes", "beverages"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 5 min      |
| Cook Time | 0 min      |
| Yields    | 1 cocktail |

A cocktail made for Erica Bristow on her 30th birthday.


## Ingredients {#ingredients}

| Quantity | Item                                                                                        |
|----------|---------------------------------------------------------------------------------------------|
| 3/4 oz   | [Crown Royal Apple Whiskey]({{< relref "../items/crown_royal_apple_whiskey.md" >}}) [^fn:1] |
| 1/2 oz   | [cinnamon simple syrup]({{< relref "cinnamon_simple-syrup.md" >}}) [^fn:2]                  |
| 2 dashes | cranberry [bitters]({{< relref "../items/bitters.md" >}}) [^fn:3]                           |
|          | [prosecco]({{< relref "../items/prosecco.md" >}}) [^fn:4]                                   |
| 1 sprig  | [rosemary]({{< relref "../items/rosemary.md" >}}) [^fn:5]                                   |


## Directions {#directions}

1.  Combine whiskey, simple syrup, and bitters in a cocktail shaker with ice.
2.  Shake well and pour into a champagne flute.
3.  Top with prosecco and garnish with rosemary.

[^fn:1]: For her love of apple whiskey and Canadian heritage.
[^fn:2]: For being sweet, and for her spicy takes are roast fire.
[^fn:3]: For growing up on cranberry lane.
[^fn:4]: For her 30th birthday being a celebration and the importance of prosecco in cottage life.
[^fn:5]: For her love of the outdoors and doing things her own way.