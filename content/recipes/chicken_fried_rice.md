+++
title = "Chicken fried rice"
author = ["James Hawley"]
date = 2020-07-12T00:00:00-04:00
lastmod = 2024-05-23T21:31:15-04:00
tags = ["chicken", "recipes", "entrees"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 5 min      |
| Cook Time | 10 min     |
| Yields    | 4 servings |

{{< figure src="../assets/chicken-fried-rice.jpg" caption="<span class=\"figure-number\">Figure 1: </span>Chicken fried rice" >}}


## Ingredients {#ingredients}

| Quantity | Item                                                                   |
|----------|------------------------------------------------------------------------|
| 3 Tbsp   | [butter]({{< relref "../items/butter.md" >}})                          |
| 2        | [eggs]({{< relref "../items/eggs.md" >}}), lightly beaten              |
| 2        | [carrots]({{< relref "../items/baby_carrot.md" >}}), peeled, diced     |
| 1        | [onion]({{< relref "../items/onion.md" >}}), diced                     |
| 2 cloves | [garlic]({{< relref "../items/garlic.md" >}}), minced                  |
|          | [kosher salt]({{< relref "../items/kitchen_salt.md" >}})               |
|          | [pepper]({{< relref "../items/black_pepper.md" >}})                    |
| 4 cup    | [basmati rice]({{< relref "../items/basmati_rice.md" >}}), uncooked    |
| 3 stalks | [green onion]({{< relref "../items/green_onion.md" >}}), thinly sliced |
| 3 Tbsp   | [soy sauce]({{< relref "../items/soy_sauce.md" >}})                    |
| 2 tsp    | [oyster sauce]({{< relref "../items/oyster_sauce.md" >}})              |
| 2 tsp    | [olive oil]({{< relref "../items/olive_oil.md" >}})                    |
| 2        | [chicken breasts]({{< relref "../items/chicken_breast.md" >}})         |


## Directions {#directions}

1.  Cook the rice according to package directions
    1.  Place on plate and set aside to cool completely
    2.  Once cooled, break it up with your hands into individual grains
2.  Heat 1/2 Tbsp butter in a large pan or wok over medium-high heat.
    1.  Add eggs, cook until scrambled.
    2.  Place egg on a separate plate.
3.  Turn heat to medium, add and heat oil, placing chicken breast in the skillet
    1.  Cook 3 minutes on each side
    2.  Transfer the chicken to a cutting board, cut into very thin slices
4.  Add remaining butter, diced onion, carrot, and garlic.
    1.  Season with salt and pepper and cook until soft.
    2.  Add rice, green onion, soy sauce, and oyster sauce.
    3.  Turn heat to high, fry for another 5 min, stirring as needed.
    4.  Add eggs and diced chicken back, stirring all together.


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [Gimme Some Oven](https://www.gimmesomeoven.com/fried-rice-recipe/print-recipe/62154/)
2.  Will last in a refrigerated container for 3 days.
