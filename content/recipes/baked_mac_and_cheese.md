+++
title = "Baked mac and cheese"
author = ["James Hawley"]
date = 2023-02-19T22:41:00-05:00
lastmod = 2024-01-31T22:28:26-05:00
tags = ["dairy", "vegetarian", "pastas", "entrees", "recipes"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 15 min     |
| Cook Time | 35 min     |
| Yields    | 8 servings |


## Equipment {#equipment}

-   1 9" x 13" casserole dish
-   1 large skillet
-   1 large pot
-   1 medium cutting board
-   1 prep knife
-   1 block grater
-   1 mixing spoon
-   1 pasta spoon
-   measuring cups and spoons


## Ingredients {#ingredients}

| Quantity | Item                                                            |
|----------|-----------------------------------------------------------------|
| 454 g    | [macaroni]({{< relref "../items/macaroni.md" >}})               |
| 2 Tbsp   | [butter]({{< relref "../items/butter.md" >}})                   |
| 1/4 cup  | [flour]({{< relref "../items/flour.md" >}})                     |
| 1        | [onion]({{< relref "../items/onion.md" >}}), diced              |
| 2 cup    | [milk]({{< relref "../items/milk.md" >}})                       |
| 1 cup    | [vegetable broth]({{< relref "../items/vegetable_broth.md" >}}) |
| 227 g    | [cheddar]({{< relref "../items/cheddar.md" >}}), shredded       |
| 2 Tbsp   | [Parmesan]({{< relref "../items/parmesan.md" >}}), grated       |
|          | [table salt]({{< relref "../items/table_salt.md" >}})           |
|          | [black pepper]({{< relref "../items/black_pepper.md" >}})       |


## Directions {#directions}

1.  Preheat the oven to 190 C (375 F) and butter a casserole dish.
2.  In a large skillet, melt the butter.
    1.  Add flour and cook over low heat until the flour and butter are blended together into clumps
    2.  Add the onion and cook for another 2 min.
3.  Add milk and broth, slowly, whisking to form the cream.
    1.  Once all the milk and broth are in, raise the temperature and simmer until smooth and thick.
    2.  Season with salt and pepper.
4.  Remove skillet from heat and add grated cheese, mixing until it melts.
5.  While the sauce is thickening, cook pasta according to the package directions.
6.  Add the sauce to the pot with the cooked pasta, stirring to coat all the noodles.
7.  Pour the pasta and sauce into the baking dish, top with parmesan, place in the oven, and cook for 15 - 20 min.
8.  Broil for a few min to brown the top, then remove and serve.


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [Skinnytaste](https://www.skinnytaste.com/wprm_print/50150)
2.  If adding more vegetables to the meal, add them once the cheese sauce is thickened.
    If you add them too early, the cheese will melt and attach itself to the surface of the vegetables, and not diffuse throughout the sauce.
