+++
title = "Honey mustard chicken and potatoes"
author = ["James Hawley"]
date = 2020-08-17T00:00:00-04:00
lastmod = 2024-05-23T21:30:54-04:00
tags = ["recipes", "chicken", "potatoes", "entrees"]
draft = false
+++

| Info      | Amount     |
|-----------|------------|
| Prep Time | 10 min     |
| Cook Time | 1 h        |
| Yields    | 4 servings |


## Ingredients {#ingredients}

| Quantity | Item                                                                       |
|----------|----------------------------------------------------------------------------|
| 4        | [chicken breasts]({{< relref "../items/chicken_breast.md" >}})             |
| 1 Tbsp   | [garlic powder]({{< relref "../items/garlic_powder.md" >}})                |
| 1 Tbsp   | [olive oil]({{< relref "../items/olive_oil.md" >}})                        |
| 2 cloves | [garlic]({{< relref "../items/garlic.md" >}}), minced                      |
| 1/4 cup  | [honey]({{< relref "../items/honey.md" >}})                                |
| 3 Tbsp   | [whole grain mustard]({{< relref "../items/yellow_mustard.md" >}})         |
| 2 Tbsp   | [dijon mustard]({{< relref "../items/dijon_mustard.md" >}})                |
| 2 Tbsp   | [water]({{< relref "../items/water.md" >}})                                |
| 1 lbs    | baby [red potatoes]({{< relref "../items/russet_potato.md" >}}), quartered |
| 8 oz     | [green beans]({{< relref "../items/green_bean.md" >}}), halved             |
| 2 sprigs | [rosemary]({{< relref "../items/rosemary.md" >}})                          |
|          | [kosher salt]({{< relref "../items/kitchen_salt.md" >}})                   |
|          | [pepper]({{< relref "../items/black_pepper.md" >}})                        |


## Directions {#directions}

1.  Preheat oven to 400 F
2.  Generously season chicken breasts with salt, pepper and garlic powder
3.  Heat olive oil in a large, oven-proof non-stick pan over medium-high heat
    1.  Sear chicken thighs for 3 minutes each side, until the skin becomes golden and crisp
    2.  Leave 2 tablespoons of chicken juices in the pan for added flavour, and drain any excess
4.  Fry the garlic in the same pan around the chicken for 1 min until fragrant
    1.  Add the honey, both mustards, and water to the pan, mixing well, and combine all around the chicken
5.  Add in the potatoes; mix them through the sauce
    1.  Season with salt and pepper, to your tastes
    2.  Allow the honey mustard sauce to simmer for 2 min
    3.  Transfer to the hot oven and bake for 40-45 min
6.  Remove from the oven after 30 minutes
    1.  Add in the green beans
    2.  Return to the oven to bake for a further 15 min


## References &amp; Notes {#references-and-notes}

1.  Original recipe: [Cafe Delites](https://cafedelites.com/honey-mustard-chicken-potatoes/)
