+++
title = "Steak"
author = ["James Hawley"]
date = 2016-12-18T00:00:00-05:00
lastmod = 2024-05-23T21:44:05-04:00
tags = ["beef", "recipes", "entrees"]
draft = false
+++

| Info      | Amount   |
|-----------|----------|
| Prep Time | 2 h      |
| Cook Time | 5 min    |
| Yields    | 2 steaks |


## Ingredients {#ingredients}

| Quantity | Item                                                              |
|----------|-------------------------------------------------------------------|
| 1/4 cup  | [olive oil]({{< relref "../items/olive_oil.md" >}})               |
| 2 Tbsp   | [balsamic vinegar]({{< relref "../items/balsamic_vinegar.md" >}}) |
| 2 cloves | [garlic]({{< relref "../items/garlic.md" >}}), peeled, crushed    |
|          | [kosher salt]({{< relref "../items/kitchen_salt.md" >}})          |
|          | [pepper]({{< relref "../items/black_pepper.md" >}})               |
| 2        | [steaks]({{< relref "../items/steak.md" >}})                      |


## Directions {#directions}

1.  Place steak in a plastic, re-sealable bag
2.  In a glass, whisk together remaining ingredients
    1.  Pour over steak and let marinate for 2 h
    2.  Let meat sit at room temperature for 1 h prior to cooking
3.  Heat a saucepan to high heat
    1.  Sprinkle the pan with course salt
4.  Lay steak in the pan, shifting around to absorb some salt
5.  Flip after 1 min, then every 30 s until desired tenderness
