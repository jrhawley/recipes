+++
title = "Fall sangria"
author = ["James Hawley"]
date = 2022-10-02T23:02:00-04:00
lastmod = 2023-12-21T21:44:32-05:00
tags = ["alcohols", "recipes", "beverages"]
draft = false
+++

| Info      | Amount   |
|-----------|----------|
| Prep Time | 10 min   |
| Cook Time | 0 min    |
| Yields    | 8 drinks |


## Ingredients {#ingredients}

| Quantity | Item                                                        |
|----------|-------------------------------------------------------------|
| 4        | [apples]({{< relref "../items/apple.md" >}}), thinly sliced |
| 5 cup    | [apple cider]({{< relref "../items/apple_cider.md" >}})     |
| 1.5 L    | pinot grigio                                                |
| 1 cup    | Fireball                                                    |
| 3        | cinnamon sticks                                             |
| 7.5 oz   | ginger ale                                                  |


## Directions {#directions}

1.  Add all ingredients to a pitcher and mix.
