+++
title = "A Rising Tide: A Cookbook of Recipes & Stories from Canada's Atlantic Coast"
author = ["James Hawley"]
date = 2023-09-07T00:00:00-04:00
lastmod = 2023-12-21T22:07:43-05:00
tags = ["books", "cookbooks", "food"]
draft = false
+++
