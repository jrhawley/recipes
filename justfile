# Serve the site on localhost
serve:
    hugo serve

# Run pagefind to index the site for search
index:
    pagefind --site public/

# Build the site into `public/`
build:
    hugo
    pagefind --site public/
