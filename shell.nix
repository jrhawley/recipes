{ pkgs, ...}: pkgs.mkShell {
  name = "recipes";

  packages = (with pkgs; [
    alejandra
    emacs
    emacsPackages.ox-hugo
    hugo
    just
    nil
    pagefind
  ]);
}
